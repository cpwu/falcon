/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.cockpits.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmCockpitsConstants
{
	public static final String EXTENSIONNAME = "fmcockpits";
	
	protected GeneratedFmCockpitsConstants()
	{
		// private constructor
	}
	
	
}
