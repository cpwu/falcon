/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmCoreConstants
{
	public static final String EXTENSIONNAME = "fmcore";
	public static class TC
	{
		public static final String APPARELSIZEVARIANTPRODUCT = "ApparelSizeVariantProduct".intern();
		public static final String APPARELSTYLEVARIANTPRODUCT = "ApparelStyleVariantProduct".intern();
		public static final String B2BCHECKOUTFLOWENUM = "B2BCheckoutFlowEnum".intern();
		public static final String B2BCHECKOUTPCIOPTIONENUM = "B2BCheckoutPciOptionEnum".intern();
		public static final String BTGORGANIZATIONTOTALSPENTINCURRENCYLASTYEAROPERAND = "BTGOrganizationTotalSpentInCurrencyLastYearOperand".intern();
		public static final String BTGORGANIZATIONTOTALSPENTINCURRENCYRELATIVEDATESOPERAND = "BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand".intern();
		public static final String BUSINESSUNITTYPE = "BusinessUnitType".intern();
		public static final String CATEGORYSALESBENCHMARKCUSTOMER = "CategorySalesBenchmarkCustomer".intern();
		public static final String CATEGORYSALESBENCHMARKPERCENTS = "CategorySalesBenchmarkPercents".intern();
		public static final String COLORENUM = "ColorEnum".intern();
		public static final String CPL1CUSTOMER = "CPL1Customer".intern();
		public static final String CSBCUSTOMERGROUP = "CSBCustomerGroup".intern();
		public static final String CSBPERCENTS3612 = "CSBPercents3612".intern();
		public static final String FMADMINADDNEWUSERPROCESS = "FMAdminAddNewUserProcess".intern();
		public static final String FMB2BADDRESS = "FMB2BAddress".intern();
		public static final String FMB2BBREGISTRATIONAPPROVALPROCESS = "FMB2BBRegistrationApprovalProcess".intern();
		public static final String FMB2SBTAXADMINAPPROVALPROCESS = "FMB2SBTaxAdminApprovalProcess".intern();
		public static final String FMB2SBTAXAPPROVALPROCESS = "FMB2SBTaxApprovalProcess".intern();
		public static final String FMBANNERCOMPONENT = "FMBannerComponent".intern();
		public static final String FMBRANDCAROUSELCOMPONENT = "FMBrandCarouselComponent".intern();
		public static final String FMCMSIMAGECOMPONENT = "FMCMSImageComponent".intern();
		public static final String FMCORPORATE = "FMCorporate".intern();
		public static final String FMCSRACCOUNTLIST = "FMCsrAccountList".intern();
		public static final String FMCUSTOMER = "FMCustomer".intern();
		public static final String FMCUSTOMERACCOUNT = "FMCustomerAccount".intern();
		public static final String FMCUSTOMERADMINPROCESS = "FMCustomerAdminProcess".intern();
		public static final String FMCUSTOMERPARTNERFUNCTION = "FMCustomerPartnerFunction".intern();
		public static final String FMCUSTOMERREGISTRATIONPROCESS = "FMCustomerRegistrationProcess".intern();
		public static final String FMCUSTOMERREVIEWPROCESS = "FMCustomerReviewProcess".intern();
		public static final String FMDCCENTER = "FMDCCenter".intern();
		public static final String FMDCSHIPPING = "FMDCShipping".intern();
		public static final String FMDISTRUBTIONCENTER = "FMDistrubtionCenter".intern();
		public static final String FMEXTENDEDFOOTERCOMPONENT = "FmExtendedFooterComponent".intern();
		public static final String FMFILEDOWNLOAD = "FMFileDownload".intern();
		public static final String FMFITMENT = "FMFitment".intern();
		public static final String FMFOOTERCOMPONENT = "FmFooterComponent".intern();
		public static final String FMHOMECAROUSELCOMPONENT = "FMHomeCarouselComponent".intern();
		public static final String FMIPOORDER = "FMIPOOrder".intern();
		public static final String FMIPOORDERENTRY = "FMIPOOrderEntry".intern();
		public static final String FMLEADGENERATIONCALLBACK = "FMLeadGenerationCallBack".intern();
		public static final String FMLEARNINGCENTERCAROUSELCOMPONENT = "FMLearningcenterCarouselComponent".intern();
		public static final String FMNAVIGATIONBARCOMPONENT = "FmNavigationBarComponent".intern();
		public static final String FMPARAGRAPHCOMPONENT = "FMParagraphComponent".intern();
		public static final String FMPART = "FMPart".intern();
		public static final String FMPARTALSOFITS = "FMPartAlsoFits".intern();
		public static final String FMREASONFORRETURN = "FMReasonForReturn".intern();
		public static final String FMRETURNITEMS = "FMReturnItems".intern();
		public static final String FMRETURNORDER = "FMReturnOrder".intern();
		public static final String FMSHIPPERORDERTRACKING = "FMShipperOrderTracking".intern();
		public static final String FMSUPPORTABOUTUSCOMPONENT = "FMSupportAboutusComponent".intern();
		public static final String FMTAXDOCUMENT = "FMTaxDocument".intern();
		public static final String FMTAXVALIDATIONTYPE = "fmTaxValidationType".intern();
		public static final String FMUNIT = "FMUnit".intern();
		public static final String FMUSERTYPE = "Fmusertype".intern();
		public static final String FMVIDEOCAROUSELCOMPONENT = "FMVideoCarouselComponent".intern();
		public static final String FMYEARMAKEMODELVEHICLETYPE = "FMYearMakeModelVehicleType".intern();
		public static final String FMZONES = "FMZones".intern();
		public static final String LEADGENERATIONSUBJECTS = "LeadGenerationSubjects".intern();
		public static final String LOYALTYORDERCONFIRMATIONPROCESS = "LoyaltyOrderConfirmationProcess".intern();
		public static final String MULTIPLECATALOGSSYNCCRONJOB = "MultipleCatalogsSyncCronJob".intern();
		public static final String ORGANIZATIONORDERSREPORTINGCRONJOB = "OrganizationOrdersReportingCronJob".intern();
		public static final String ORGANIZATIONORDERSTATISTICS = "OrganizationOrderStatistics".intern();
		public static final String PARTINTERCHANGE = "PartInterchange".intern();
		public static final String PARTSTATUS = "PartStatus".intern();
		public static final String POWERTOOLSSIZEVARIANTPRODUCT = "PowertoolsSizeVariantProduct".intern();
		public static final String REFERAFRIENDEMAILPROCESS = "ReferAFriendEmailProcess".intern();
		public static final String SALESCHANNEL = "SalesChannel".intern();
		public static final String SALESORGANIZATION = "SalesOrganization".intern();
		public static final String TSCLOCATION = "TSCLocation".intern();
		public static final String UPLOADORDER = "UploadOrder".intern();
		public static final String UPLOADORDERENTRY = "UploadOrderEntry".intern();
		public static final String UPLOADORDERHISTORY = "UploadOrderHistory".intern();
		public static final String UPLOADORDERPROCESS = "UploadOrderProcess".intern();
		public static final String UPLOADORDERPROCESSEMAILNOTIFICATION = "UploadOrderProcessEmailNotification".intern();
		public static final String UPLOADORDERSTATUS = "UploadOrderStatus".intern();
	}
	public static class Attributes
	{
		public static class AbstractOrder
		{
			public static final String CUSTPONUMBER = "custponumber".intern();
			public static final String FMORDERTYPE = "fmordertype".intern();
			public static final String ORDERCOMMENTS = "ordercomments".intern();
			public static final String POCUSTID = "pocustid".intern();
			public static final String SAPORDERNUMBER = "sapordernumber".intern();
		}
		public static class AbstractOrderEntry
		{
			public static final String DISTRUBTIONCENTER = "distrubtionCenter".intern();
			public static final String SAPERRORMSG = "saperrormsg".intern();
		}
		public static class B2BCustomer
		{
			public static final String CUSTOMERTYPE = "customerType".intern();
		}
		public static class B2BUnit
		{
			public static final String UNITTYPE = "unitType".intern();
		}
		public static class CMSParagraphComponent
		{
			public static final String FMSUPPORTABOUTUSCOMPONENT = "fmSupportAboutusComponent".intern();
		}
		public static class Customer
		{
			public static final String BRAND = "Brand".intern();
		}
		public static class DeliveryMode
		{
			public static final String CARRIER = "carrier".intern();
		}
		public static class PointOfService
		{
			public static final String BRAND = "brand".intern();
			public static final String SHOPTYPE = "shopType".intern();
		}
		public static class Product
		{
			public static final String LOYALTYPOINTS = "loyaltyPoints".intern();
		}
		public static class User
		{
			public static final String FMUSERTYPE = "fmusertype".intern();
		}
	}
	public static class Enumerations
	{
		public static class B2BCheckoutFlowEnum
		{
			public static final String SINGLE = "SINGLE".intern();
			public static final String MULTISTEP = "MULTISTEP".intern();
		}
		public static class B2BCheckoutPciOptionEnum
		{
			public static final String DEFAULT = "Default".intern();
			public static final String HOP = "HOP".intern();
		}
		public static class BusinessUnitType
		{
			public static final String BTOB = "BTOB".intern();
			public static final String BTOSB = "BTOSB".intern();
			public static final String BTOC = "BTOC".intern();
		}
		public static class ColorEnum
		{
			public static final String YELLOW = "YELLOW".intern();
			public static final String ORANGE = "ORANGE".intern();
			public static final String WHITE = "WHITE".intern();
			public static final String BROWN = "BROWN".intern();
			public static final String PINK = "PINK".intern();
			public static final String NAVY = "NAVY".intern();
			public static final String RED = "RED".intern();
			public static final String GREEN = "GREEN".intern();
			public static final String BLACK = "BLACK".intern();
			public static final String SILVER = "SILVER".intern();
			public static final String PURPLE = "PURPLE".intern();
			public static final String GREY = "GREY".intern();
			public static final String BLUE = "BLUE".intern();
		}
		public static class FMReasonForReturn
		{
			public static final String DAMAGED = "Damaged".intern();
			public static final String RETURNCUSTOMERERROR = "ReturnCustomerError".intern();
			public static final String WARRANTYRETURN = "WarrantyReturn".intern();
			public static final String WARRANTYRETURNDESTROYINFIELD = "WarrantyReturnDestroyinField".intern();
			public static final String WARRANTYWLABOURCLAIM = "WarrantywLabourClaim".intern();
			public static final String PRODUCTRECALL = "ProductRecall".intern();
			public static final String SHIPPINGDISCREPANCY = "ShippingDiscrepancy".intern();
			public static final String OBSOLETE = "Obsolete".intern();
			public static final String CUSTOMERSERVICEERROR = "CustomerServiceError".intern();
			public static final String RETURNDUETOLATEDELIVERY = "ReturnduetoLateDelivery".intern();
		}
		public static class FmTaxValidationType
		{
			public static final String REJECTED = "Rejected".intern();
			public static final String APPROVED = "Approved".intern();
			public static final String NOTREVIEWED = "NotReviewed".intern();
		}
		public static class FMUnit
		{
			public static final String PIECES = "Pieces".intern();
			public static final String EAU = "EAU".intern();
		}
		public static class Fmusertype
		{
			public static final String JOBBERPARTSTORE = "JobberPartStore".intern();
			public static final String SHOPOWNERTECHNICIAN = "ShopOwnerTechnician".intern();
			public static final String WAREHOUSEDISTRIBUTORLIGHTVEHICLE = "WarehouseDistributorLightVehicle".intern();
			public static final String CONSUMERDIFM = "ConsumerDIFM".intern();
			public static final String WAREHOUSEDISTRIBUTORCOMMERCIALVEHICLE = "WarehouseDistributorCommercialVehicle".intern();
			public static final String RETAILER = "Retailer".intern();
			public static final String CONSUMERDIY = "ConsumerDIY".intern();
		}
		public static class LeadGenerationSubjects
		{
			public static final String PRODUCTENQUERY = "ProductEnquery".intern();
			public static final String DCTCLOCATIONENQUERY = "DCTCLocationEnquery".intern();
			public static final String ORDERENQUERY = "OrderEnquery".intern();
			public static final String CALLBACK = "CallBack".intern();
			public static final String GENERALENQUERY = "Generalenquery".intern();
			public static final String PROMOTIANDOFFERSENQUERY = "PromotiAndoffersEnquery".intern();
		}
		public static class PartStatus
		{
			public static final String OBSOLETEPART = "obsoletepart".intern();
			public static final String ACTIVEPART = "activepart".intern();
			public static final String SUPERSEDEDPART = "supersededpart".intern();
			public static final String NEWPART = "newpart".intern();
		}
		public static class UploadOrderStatus
		{
			public static final String FILEPARSED = "fileParsed".intern();
			public static final String SYSTEMERROR = "SystemError".intern();
			public static final String CANCELLED = "Cancelled".intern();
			public static final String PARTRESOLUTIONISSUE = "PartResolutionIssue".intern();
			public static final String NEW = "New".intern();
			public static final String INPROGRESS = "InProgress".intern();
			public static final String SUBMITTED = "Submitted".intern();
			public static final String FILEPARSEERROR = "FileParseError".intern();
			public static final String PARTSRESOLVED = "PartsResolved".intern();
			public static final String ORDERERROR = "OrderError".intern();
			public static final String PARTRESOLUTIONERROR = "PartResolutionError".intern();
		}
	}
	public static class Relations
	{
		public static final String BANNERSFORFMHOMECAROUSELCOMPONENT = "BannersForFMHomeCarouselComponent".intern();
		public static final String FITMENTPARTRELATION = "FitmentPartRelation".intern();
		public static final String FMB2BCUSTOMERACCOUNT2FMTAXDOCUMENTS = "FMB2BCustomerAccount2FMTaxDocuments".intern();
		public static final String FMCUSTACCOUNT2PARTNERFUNCTION = "FMCustAccount2PartnerFunction".intern();
		public static final String FMCUSTOMERACCOUNT2SALESCHANNEL = "FMCustomerAccount2SalesChannel".intern();
		public static final String FMCUSTOMERACCOUNT2SALESORGANIZATION = "FMCustomerAccount2SalesOrganization".intern();
		public static final String FMIPOORDER2FMIPOORDERENTRY = "FMIPOOrder2FMIPOOrderEntry".intern();
		public static final String HOMEFMSUPPORTABOUTUSCOMPONENT = "HomeFMSupportAboutusComponent".intern();
		public static final String UPLOADORDER2UPLOADORDERENTRY = "UploadOrder2UploadOrderEntry".intern();
		public static final String UPLOADORDER2UPLOADORDERHISTORY = "UploadOrder2UploadOrderHistory".intern();
		public static final String UPLOADORDERENTRY2UPLOADORDERHISTORY = "UploadOrderEntry2UploadOrderHistory".intern();
	}
	
	protected GeneratedFmCoreConstants()
	{
		// private constructor
	}
	
	
}
