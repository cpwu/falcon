/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.user.Address;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.user.Address FMB2BAddress}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMB2BAddress extends Address
{
	/** Qualifier of the <code>FMB2BAddress.streetName2</code> attribute **/
	public static final String STREETNAME2 = "streetName2";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Address.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(STREETNAME2, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMB2BAddress.streetName2</code> attribute.
	 * @return the streetName2 - Customer street name2
	 */
	public String getStreetName2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STREETNAME2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMB2BAddress.streetName2</code> attribute.
	 * @return the streetName2 - Customer street name2
	 */
	public String getStreetName2()
	{
		return getStreetName2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMB2BAddress.streetName2</code> attribute. 
	 * @param value the streetName2 - Customer street name2
	 */
	public void setStreetName2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STREETNAME2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMB2BAddress.streetName2</code> attribute. 
	 * @param value the streetName2 - Customer street name2
	 */
	public void setStreetName2(final String value)
	{
		setStreetName2( getSession().getSessionContext(), value );
	}
	
}
