/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import com.federalmogul.falcon.core.jalo.FMCsrAccountList;
import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.b2b.jalo.B2BCustomer FMCustomer}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCustomer extends B2BCustomer
{
	/** Qualifier of the <code>FMCustomer.csrAccountEntry</code> attribute **/
	public static final String CSRACCOUNTENTRY = "csrAccountEntry";
	/** Qualifier of the <code>FMCustomer.shopBays</code> attribute **/
	public static final String SHOPBAYS = "shopBays";
	/** Qualifier of the <code>FMCustomer.lmsSigninID</code> attribute **/
	public static final String LMSSIGNINID = "lmsSigninID";
	/** Qualifier of the <code>FMCustomer.brands</code> attribute **/
	public static final String BRANDS = "brands";
	/** Qualifier of the <code>FMCustomer.aboutShop</code> attribute **/
	public static final String ABOUTSHOP = "aboutShop";
	/** Qualifier of the <code>FMCustomer.promSubscription</code> attribute **/
	public static final String PROMSUBSCRIPTION = "promSubscription";
	/** Qualifier of the <code>FMCustomer.crmCustomerID</code> attribute **/
	public static final String CRMCUSTOMERID = "crmCustomerID";
	/** Qualifier of the <code>FMCustomer.newsLetterSubscription</code> attribute **/
	public static final String NEWSLETTERSUBSCRIPTION = "newsLetterSubscription";
	/** Qualifier of the <code>FMCustomer.loyaltySubscription</code> attribute **/
	public static final String LOYALTYSUBSCRIPTION = "loyaltySubscription";
	/** Qualifier of the <code>FMCustomer.techAcademySubscription</code> attribute **/
	public static final String TECHACADEMYSUBSCRIPTION = "techAcademySubscription";
	/** Qualifier of the <code>FMCustomer.b2cLoyaltyMembershipId</code> attribute **/
	public static final String B2CLOYALTYMEMBERSHIPID = "b2cLoyaltyMembershipId";
	/** Qualifier of the <code>FMCustomer.lastName</code> attribute **/
	public static final String LASTNAME = "lastName";
	/** Qualifier of the <code>FMCustomer.fmUnit</code> attribute **/
	public static final String FMUNIT = "fmUnit";
	/** Qualifier of the <code>FMCustomer.isGarageRewardMember</code> attribute **/
	public static final String ISGARAGEREWARDMEMBER = "isGarageRewardMember";
	/** Qualifier of the <code>FMCustomer.newPrductAlerts</code> attribute **/
	public static final String NEWPRDUCTALERTS = "newPrductAlerts";
	/** Qualifier of the <code>FMCustomer.mostIntersted</code> attribute **/
	public static final String MOSTINTERSTED = "mostIntersted";
	/** Qualifier of the <code>FMCustomer.shopBanner</code> attribute **/
	public static final String SHOPBANNER = "shopBanner";
	/** Qualifier of the <code>FMCustomer.shopType</code> attribute **/
	public static final String SHOPTYPE = "shopType";
	/** Qualifier of the <code>FMCustomer.surveyStatus</code> attribute **/
	public static final String SURVEYSTATUS = "surveyStatus";
	/** Qualifier of the <code>FMCustomer.workContactNo</code> attribute **/
	public static final String WORKCONTACTNO = "workContactNo";
	/** Qualifier of the <code>FMCustomer.channelCode</code> attribute **/
	public static final String CHANNELCODE = "channelCode";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(B2BCustomer.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CSRACCOUNTENTRY, AttributeMode.INITIAL);
		tmp.put(SHOPBAYS, AttributeMode.INITIAL);
		tmp.put(LMSSIGNINID, AttributeMode.INITIAL);
		tmp.put(BRANDS, AttributeMode.INITIAL);
		tmp.put(ABOUTSHOP, AttributeMode.INITIAL);
		tmp.put(PROMSUBSCRIPTION, AttributeMode.INITIAL);
		tmp.put(CRMCUSTOMERID, AttributeMode.INITIAL);
		tmp.put(NEWSLETTERSUBSCRIPTION, AttributeMode.INITIAL);
		tmp.put(LOYALTYSUBSCRIPTION, AttributeMode.INITIAL);
		tmp.put(TECHACADEMYSUBSCRIPTION, AttributeMode.INITIAL);
		tmp.put(B2CLOYALTYMEMBERSHIPID, AttributeMode.INITIAL);
		tmp.put(LASTNAME, AttributeMode.INITIAL);
		tmp.put(FMUNIT, AttributeMode.INITIAL);
		tmp.put(ISGARAGEREWARDMEMBER, AttributeMode.INITIAL);
		tmp.put(NEWPRDUCTALERTS, AttributeMode.INITIAL);
		tmp.put(MOSTINTERSTED, AttributeMode.INITIAL);
		tmp.put(SHOPBANNER, AttributeMode.INITIAL);
		tmp.put(SHOPTYPE, AttributeMode.INITIAL);
		tmp.put(SURVEYSTATUS, AttributeMode.INITIAL);
		tmp.put(WORKCONTACTNO, AttributeMode.INITIAL);
		tmp.put(CHANNELCODE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.aboutShop</code> attribute.
	 * @return the aboutShop - About Your Shop
	 */
	public String getAboutShop(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ABOUTSHOP);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.aboutShop</code> attribute.
	 * @return the aboutShop - About Your Shop
	 */
	public String getAboutShop()
	{
		return getAboutShop( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.aboutShop</code> attribute. 
	 * @param value the aboutShop - About Your Shop
	 */
	public void setAboutShop(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ABOUTSHOP,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.aboutShop</code> attribute. 
	 * @param value the aboutShop - About Your Shop
	 */
	public void setAboutShop(final String value)
	{
		setAboutShop( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.b2cLoyaltyMembershipId</code> attribute.
	 * @return the b2cLoyaltyMembershipId - B2CLoyaltyMembershipId
	 */
	public String getB2cLoyaltyMembershipId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, B2CLOYALTYMEMBERSHIPID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.b2cLoyaltyMembershipId</code> attribute.
	 * @return the b2cLoyaltyMembershipId - B2CLoyaltyMembershipId
	 */
	public String getB2cLoyaltyMembershipId()
	{
		return getB2cLoyaltyMembershipId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.b2cLoyaltyMembershipId</code> attribute. 
	 * @param value the b2cLoyaltyMembershipId - B2CLoyaltyMembershipId
	 */
	public void setB2cLoyaltyMembershipId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, B2CLOYALTYMEMBERSHIPID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.b2cLoyaltyMembershipId</code> attribute. 
	 * @param value the b2cLoyaltyMembershipId - B2CLoyaltyMembershipId
	 */
	public void setB2cLoyaltyMembershipId(final String value)
	{
		setB2cLoyaltyMembershipId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.brands</code> attribute.
	 * @return the brands
	 */
	public String getBrands(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRANDS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.brands</code> attribute.
	 * @return the brands
	 */
	public String getBrands()
	{
		return getBrands( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.brands</code> attribute. 
	 * @param value the brands
	 */
	public void setBrands(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRANDS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.brands</code> attribute. 
	 * @param value the brands
	 */
	public void setBrands(final String value)
	{
		setBrands( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.channelCode</code> attribute.
	 * @return the channelCode - Customer business channel code
	 */
	public EnumerationValue getChannelCode(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, CHANNELCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.channelCode</code> attribute.
	 * @return the channelCode - Customer business channel code
	 */
	public EnumerationValue getChannelCode()
	{
		return getChannelCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.channelCode</code> attribute. 
	 * @param value the channelCode - Customer business channel code
	 */
	public void setChannelCode(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, CHANNELCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.channelCode</code> attribute. 
	 * @param value the channelCode - Customer business channel code
	 */
	public void setChannelCode(final EnumerationValue value)
	{
		setChannelCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.crmCustomerID</code> attribute.
	 * @return the crmCustomerID - CRM Customer ID
	 */
	public String getCrmCustomerID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CRMCUSTOMERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.crmCustomerID</code> attribute.
	 * @return the crmCustomerID - CRM Customer ID
	 */
	public String getCrmCustomerID()
	{
		return getCrmCustomerID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.crmCustomerID</code> attribute. 
	 * @param value the crmCustomerID - CRM Customer ID
	 */
	public void setCrmCustomerID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CRMCUSTOMERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.crmCustomerID</code> attribute. 
	 * @param value the crmCustomerID - CRM Customer ID
	 */
	public void setCrmCustomerID(final String value)
	{
		setCrmCustomerID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.csrAccountEntry</code> attribute.
	 * @return the csrAccountEntry - Customer Group
	 */
	public List<FMCsrAccountList> getCsrAccountEntry(final SessionContext ctx)
	{
		List<FMCsrAccountList> coll = (List<FMCsrAccountList>)getProperty( ctx, CSRACCOUNTENTRY);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.csrAccountEntry</code> attribute.
	 * @return the csrAccountEntry - Customer Group
	 */
	public List<FMCsrAccountList> getCsrAccountEntry()
	{
		return getCsrAccountEntry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.csrAccountEntry</code> attribute. 
	 * @param value the csrAccountEntry - Customer Group
	 */
	public void setCsrAccountEntry(final SessionContext ctx, final List<FMCsrAccountList> value)
	{
		setProperty(ctx, CSRACCOUNTENTRY,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.csrAccountEntry</code> attribute. 
	 * @param value the csrAccountEntry - Customer Group
	 */
	public void setCsrAccountEntry(final List<FMCsrAccountList> value)
	{
		setCsrAccountEntry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.fmUnit</code> attribute.
	 * @return the fmUnit - Subscription to FM News letters
	 */
	public FMCustomerAccount getFmUnit(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMUNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.fmUnit</code> attribute.
	 * @return the fmUnit - Subscription to FM News letters
	 */
	public FMCustomerAccount getFmUnit()
	{
		return getFmUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.fmUnit</code> attribute. 
	 * @param value the fmUnit - Subscription to FM News letters
	 */
	public void setFmUnit(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, FMUNIT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.fmUnit</code> attribute. 
	 * @param value the fmUnit - Subscription to FM News letters
	 */
	public void setFmUnit(final FMCustomerAccount value)
	{
		setFmUnit( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.isGarageRewardMember</code> attribute.
	 * @return the isGarageRewardMember - Garage Member Login
	 */
	public Boolean isIsGarageRewardMember(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, ISGARAGEREWARDMEMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.isGarageRewardMember</code> attribute.
	 * @return the isGarageRewardMember - Garage Member Login
	 */
	public Boolean isIsGarageRewardMember()
	{
		return isIsGarageRewardMember( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @return the isGarageRewardMember - Garage Member Login
	 */
	public boolean isIsGarageRewardMemberAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isIsGarageRewardMember( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @return the isGarageRewardMember - Garage Member Login
	 */
	public boolean isIsGarageRewardMemberAsPrimitive()
	{
		return isIsGarageRewardMemberAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @param value the isGarageRewardMember - Garage Member Login
	 */
	public void setIsGarageRewardMember(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, ISGARAGEREWARDMEMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @param value the isGarageRewardMember - Garage Member Login
	 */
	public void setIsGarageRewardMember(final Boolean value)
	{
		setIsGarageRewardMember( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @param value the isGarageRewardMember - Garage Member Login
	 */
	public void setIsGarageRewardMember(final SessionContext ctx, final boolean value)
	{
		setIsGarageRewardMember( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.isGarageRewardMember</code> attribute. 
	 * @param value the isGarageRewardMember - Garage Member Login
	 */
	public void setIsGarageRewardMember(final boolean value)
	{
		setIsGarageRewardMember( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.lastName</code> attribute.
	 * @return the lastName - Customer last name
	 */
	public String getLastName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LASTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.lastName</code> attribute.
	 * @return the lastName - Customer last name
	 */
	public String getLastName()
	{
		return getLastName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.lastName</code> attribute. 
	 * @param value the lastName - Customer last name
	 */
	public void setLastName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LASTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.lastName</code> attribute. 
	 * @param value the lastName - Customer last name
	 */
	public void setLastName(final String value)
	{
		setLastName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.lmsSigninID</code> attribute.
	 * @return the lmsSigninID - LMS Signin ID
	 */
	public String getLmsSigninID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LMSSIGNINID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.lmsSigninID</code> attribute.
	 * @return the lmsSigninID - LMS Signin ID
	 */
	public String getLmsSigninID()
	{
		return getLmsSigninID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.lmsSigninID</code> attribute. 
	 * @param value the lmsSigninID - LMS Signin ID
	 */
	public void setLmsSigninID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LMSSIGNINID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.lmsSigninID</code> attribute. 
	 * @param value the lmsSigninID - LMS Signin ID
	 */
	public void setLmsSigninID(final String value)
	{
		setLmsSigninID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.loyaltySubscription</code> attribute.
	 * @return the loyaltySubscription - Enroll in FM Rewards
	 */
	public Boolean isLoyaltySubscription(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, LOYALTYSUBSCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.loyaltySubscription</code> attribute.
	 * @return the loyaltySubscription - Enroll in FM Rewards
	 */
	public Boolean isLoyaltySubscription()
	{
		return isLoyaltySubscription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @return the loyaltySubscription - Enroll in FM Rewards
	 */
	public boolean isLoyaltySubscriptionAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isLoyaltySubscription( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @return the loyaltySubscription - Enroll in FM Rewards
	 */
	public boolean isLoyaltySubscriptionAsPrimitive()
	{
		return isLoyaltySubscriptionAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @param value the loyaltySubscription - Enroll in FM Rewards
	 */
	public void setLoyaltySubscription(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, LOYALTYSUBSCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @param value the loyaltySubscription - Enroll in FM Rewards
	 */
	public void setLoyaltySubscription(final Boolean value)
	{
		setLoyaltySubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @param value the loyaltySubscription - Enroll in FM Rewards
	 */
	public void setLoyaltySubscription(final SessionContext ctx, final boolean value)
	{
		setLoyaltySubscription( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.loyaltySubscription</code> attribute. 
	 * @param value the loyaltySubscription - Enroll in FM Rewards
	 */
	public void setLoyaltySubscription(final boolean value)
	{
		setLoyaltySubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.mostIntersted</code> attribute.
	 * @return the mostIntersted
	 */
	public String getMostIntersted(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MOSTINTERSTED);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.mostIntersted</code> attribute.
	 * @return the mostIntersted
	 */
	public String getMostIntersted()
	{
		return getMostIntersted( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.mostIntersted</code> attribute. 
	 * @param value the mostIntersted
	 */
	public void setMostIntersted(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MOSTINTERSTED,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.mostIntersted</code> attribute. 
	 * @param value the mostIntersted
	 */
	public void setMostIntersted(final String value)
	{
		setMostIntersted( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newPrductAlerts</code> attribute.
	 * @return the newPrductAlerts - New Product alerts flag
	 */
	public Boolean isNewPrductAlerts(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, NEWPRDUCTALERTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newPrductAlerts</code> attribute.
	 * @return the newPrductAlerts - New Product alerts flag
	 */
	public Boolean isNewPrductAlerts()
	{
		return isNewPrductAlerts( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @return the newPrductAlerts - New Product alerts flag
	 */
	public boolean isNewPrductAlertsAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isNewPrductAlerts( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @return the newPrductAlerts - New Product alerts flag
	 */
	public boolean isNewPrductAlertsAsPrimitive()
	{
		return isNewPrductAlertsAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @param value the newPrductAlerts - New Product alerts flag
	 */
	public void setNewPrductAlerts(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, NEWPRDUCTALERTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @param value the newPrductAlerts - New Product alerts flag
	 */
	public void setNewPrductAlerts(final Boolean value)
	{
		setNewPrductAlerts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @param value the newPrductAlerts - New Product alerts flag
	 */
	public void setNewPrductAlerts(final SessionContext ctx, final boolean value)
	{
		setNewPrductAlerts( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newPrductAlerts</code> attribute. 
	 * @param value the newPrductAlerts - New Product alerts flag
	 */
	public void setNewPrductAlerts(final boolean value)
	{
		setNewPrductAlerts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newsLetterSubscription</code> attribute.
	 * @return the newsLetterSubscription - Subscription to FM News letters
	 */
	public Boolean isNewsLetterSubscription(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, NEWSLETTERSUBSCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newsLetterSubscription</code> attribute.
	 * @return the newsLetterSubscription - Subscription to FM News letters
	 */
	public Boolean isNewsLetterSubscription()
	{
		return isNewsLetterSubscription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @return the newsLetterSubscription - Subscription to FM News letters
	 */
	public boolean isNewsLetterSubscriptionAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isNewsLetterSubscription( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @return the newsLetterSubscription - Subscription to FM News letters
	 */
	public boolean isNewsLetterSubscriptionAsPrimitive()
	{
		return isNewsLetterSubscriptionAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @param value the newsLetterSubscription - Subscription to FM News letters
	 */
	public void setNewsLetterSubscription(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, NEWSLETTERSUBSCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @param value the newsLetterSubscription - Subscription to FM News letters
	 */
	public void setNewsLetterSubscription(final Boolean value)
	{
		setNewsLetterSubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @param value the newsLetterSubscription - Subscription to FM News letters
	 */
	public void setNewsLetterSubscription(final SessionContext ctx, final boolean value)
	{
		setNewsLetterSubscription( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.newsLetterSubscription</code> attribute. 
	 * @param value the newsLetterSubscription - Subscription to FM News letters
	 */
	public void setNewsLetterSubscription(final boolean value)
	{
		setNewsLetterSubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.promSubscription</code> attribute.
	 * @return the promSubscription - New Promotion Flag
	 */
	public Boolean isPromSubscription(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, PROMSUBSCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.promSubscription</code> attribute.
	 * @return the promSubscription - New Promotion Flag
	 */
	public Boolean isPromSubscription()
	{
		return isPromSubscription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @return the promSubscription - New Promotion Flag
	 */
	public boolean isPromSubscriptionAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isPromSubscription( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @return the promSubscription - New Promotion Flag
	 */
	public boolean isPromSubscriptionAsPrimitive()
	{
		return isPromSubscriptionAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @param value the promSubscription - New Promotion Flag
	 */
	public void setPromSubscription(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, PROMSUBSCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @param value the promSubscription - New Promotion Flag
	 */
	public void setPromSubscription(final Boolean value)
	{
		setPromSubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @param value the promSubscription - New Promotion Flag
	 */
	public void setPromSubscription(final SessionContext ctx, final boolean value)
	{
		setPromSubscription( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.promSubscription</code> attribute. 
	 * @param value the promSubscription - New Promotion Flag
	 */
	public void setPromSubscription(final boolean value)
	{
		setPromSubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopBanner</code> attribute.
	 * @return the shopBanner - About Shop Banner
	 */
	public String getShopBanner(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHOPBANNER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopBanner</code> attribute.
	 * @return the shopBanner - About Shop Banner
	 */
	public String getShopBanner()
	{
		return getShopBanner( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopBanner</code> attribute. 
	 * @param value the shopBanner - About Shop Banner
	 */
	public void setShopBanner(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHOPBANNER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopBanner</code> attribute. 
	 * @param value the shopBanner - About Shop Banner
	 */
	public void setShopBanner(final String value)
	{
		setShopBanner( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopBays</code> attribute.
	 * @return the shopBays - About Shop Bays
	 */
	public String getShopBays(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHOPBAYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopBays</code> attribute.
	 * @return the shopBays - About Shop Bays
	 */
	public String getShopBays()
	{
		return getShopBays( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopBays</code> attribute. 
	 * @param value the shopBays - About Shop Bays
	 */
	public void setShopBays(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHOPBAYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopBays</code> attribute. 
	 * @param value the shopBays - About Shop Bays
	 */
	public void setShopBays(final String value)
	{
		setShopBays( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopType</code> attribute.
	 * @return the shopType - About Shop Type
	 */
	public String getShopType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHOPTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.shopType</code> attribute.
	 * @return the shopType - About Shop Type
	 */
	public String getShopType()
	{
		return getShopType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopType</code> attribute. 
	 * @param value the shopType - About Shop Type
	 */
	public void setShopType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHOPTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.shopType</code> attribute. 
	 * @param value the shopType - About Shop Type
	 */
	public void setShopType(final String value)
	{
		setShopType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.surveyStatus</code> attribute.
	 * @return the surveyStatus - Survey Status
	 */
	public String getSurveyStatus(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SURVEYSTATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.surveyStatus</code> attribute.
	 * @return the surveyStatus - Survey Status
	 */
	public String getSurveyStatus()
	{
		return getSurveyStatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.surveyStatus</code> attribute. 
	 * @param value the surveyStatus - Survey Status
	 */
	public void setSurveyStatus(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SURVEYSTATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.surveyStatus</code> attribute. 
	 * @param value the surveyStatus - Survey Status
	 */
	public void setSurveyStatus(final String value)
	{
		setSurveyStatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.techAcademySubscription</code> attribute.
	 * @return the techAcademySubscription - FM Technical Academy subscription
	 */
	public Boolean isTechAcademySubscription(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, TECHACADEMYSUBSCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.techAcademySubscription</code> attribute.
	 * @return the techAcademySubscription - FM Technical Academy subscription
	 */
	public Boolean isTechAcademySubscription()
	{
		return isTechAcademySubscription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @return the techAcademySubscription - FM Technical Academy subscription
	 */
	public boolean isTechAcademySubscriptionAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isTechAcademySubscription( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @return the techAcademySubscription - FM Technical Academy subscription
	 */
	public boolean isTechAcademySubscriptionAsPrimitive()
	{
		return isTechAcademySubscriptionAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @param value the techAcademySubscription - FM Technical Academy subscription
	 */
	public void setTechAcademySubscription(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, TECHACADEMYSUBSCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @param value the techAcademySubscription - FM Technical Academy subscription
	 */
	public void setTechAcademySubscription(final Boolean value)
	{
		setTechAcademySubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @param value the techAcademySubscription - FM Technical Academy subscription
	 */
	public void setTechAcademySubscription(final SessionContext ctx, final boolean value)
	{
		setTechAcademySubscription( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.techAcademySubscription</code> attribute. 
	 * @param value the techAcademySubscription - FM Technical Academy subscription
	 */
	public void setTechAcademySubscription(final boolean value)
	{
		setTechAcademySubscription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.workContactNo</code> attribute.
	 * @return the workContactNo - Work Contact Number
	 */
	public String getWorkContactNo(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WORKCONTACTNO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomer.workContactNo</code> attribute.
	 * @return the workContactNo - Work Contact Number
	 */
	public String getWorkContactNo()
	{
		return getWorkContactNo( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.workContactNo</code> attribute. 
	 * @param value the workContactNo - Work Contact Number
	 */
	public void setWorkContactNo(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WORKCONTACTNO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomer.workContactNo</code> attribute. 
	 * @param value the workContactNo - Work Contact Number
	 */
	public void setWorkContactNo(final String value)
	{
		setWorkContactNo( getSession().getSessionContext(), value );
	}
	
}
