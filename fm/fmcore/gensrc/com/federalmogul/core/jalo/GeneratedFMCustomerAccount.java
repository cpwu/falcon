/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerPartnerFunction;
import com.federalmogul.core.jalo.FMTaxDocument;
import com.federalmogul.core.jalo.SalesChannel;
import com.federalmogul.core.jalo.SalesOrganization;
import de.hybris.platform.b2b.jalo.B2BUnit;
import de.hybris.platform.jalo.ConsistencyCheckException;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.util.OneToManyHandler;
import de.hybris.platform.util.PartOfHandler;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.b2b.jalo.B2BUnit FMCustomerAccount}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCustomerAccount extends B2BUnit
{
	/** Qualifier of the <code>FMCustomerAccount.shippingCode</code> attribute **/
	public static final String SHIPPINGCODE = "shippingCode";
	/** Qualifier of the <code>FMCustomerAccount.shipToLimitFlag</code> attribute **/
	public static final String SHIPTOLIMITFLAG = "shipToLimitFlag";
	/** Qualifier of the <code>FMCustomerAccount.buyingGroup</code> attribute **/
	public static final String BUYINGGROUP = "buyingGroup";
	/** Qualifier of the <code>FMCustomerAccount.distributionChannel</code> attribute **/
	public static final String DISTRIBUTIONCHANNEL = "distributionChannel";
	/** Qualifier of the <code>FMCustomerAccount.customerTerritory</code> attribute **/
	public static final String CUSTOMERTERRITORY = "customerTerritory";
	/** Qualifier of the <code>FMCustomerAccount.association</code> attribute **/
	public static final String ASSOCIATION = "association";
	/** Qualifier of the <code>FMCustomerAccount.customerGroup</code> attribute **/
	public static final String CUSTOMERGROUP = "customerGroup";
	/** Qualifier of the <code>FMCustomerAccount.carrierAccountCode</code> attribute **/
	public static final String CARRIERACCOUNTCODE = "carrierAccountCode";
	/** Qualifier of the <code>FMCustomerAccount.division</code> attribute **/
	public static final String DIVISION = "division";
	/** Qualifier of the <code>FMCustomerAccount.salesOrgs</code> attribute **/
	public static final String SALESORGS = "salesOrgs";
	/** Qualifier of the <code>FMCustomerAccount.accComments</code> attribute **/
	public static final String ACCCOMMENTS = "accComments";
	/** Qualifier of the <code>FMCustomerAccount.accComments2</code> attribute **/
	public static final String ACCCOMMENTS2 = "accComments2";
	/** Qualifier of the <code>FMCustomerAccount.nabsAccountCode</code> attribute **/
	public static final String NABSACCOUNTCODE = "nabsAccountCode";
	/** Qualifier of the <code>FMCustomerAccount.buyingAffiliationCode</code> attribute **/
	public static final String BUYINGAFFILIATIONCODE = "buyingAffiliationCode";
	/** Qualifier of the <code>FMCustomerAccount.taxID</code> attribute **/
	public static final String TAXID = "taxID";
	/** Qualifier of the <code>FMCustomerAccount.channelCode</code> attribute **/
	public static final String CHANNELCODE = "channelCode";
	/** Qualifier of the <code>FMCustomerAccount.fmCustTargetAccountCode</code> attribute **/
	public static final String FMCUSTTARGETACCOUNTCODE = "fmCustTargetAccountCode";
	/** Relation ordering override parameter constants for FMCustAccount2PartnerFunction from ((fmcore))*/
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED = "relation.FMCustAccount2PartnerFunction.source.ordered";
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED = "relation.FMCustAccount2PartnerFunction.target.ordered";
	/** Relation disable markmodifed parameter constants for FMCustAccount2PartnerFunction from ((fmcore))*/
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED = "relation.FMCustAccount2PartnerFunction.markmodified";
	/** Qualifier of the <code>FMCustomerAccount.prospectuid</code> attribute **/
	public static final String PROSPECTUID = "prospectuid";
	/** Qualifier of the <code>FMCustomerAccount.fmtaxDocument</code> attribute **/
	public static final String FMTAXDOCUMENT = "fmtaxDocument";
	/** Qualifier of the <code>FMCustomerAccount.fmTaxDocuments</code> attribute **/
	public static final String FMTAXDOCUMENTS = "fmTaxDocuments";
	/** Qualifier of the <code>FMCustomerAccount.salesorg</code> attribute **/
	public static final String SALESORG = "salesorg";
	/** Qualifier of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute **/
	public static final String VIEWINVOICEFLAG = "viewInvoiceFlag";
	/** Qualifier of the <code>FMCustomerAccount.customerSalesChannel</code> attribute **/
	public static final String CUSTOMERSALESCHANNEL = "customerSalesChannel";
	/** Qualifier of the <code>FMCustomerAccount.creditCardFlag</code> attribute **/
	public static final String CREDITCARDFLAG = "creditCardFlag";
	/** Qualifier of the <code>FMCustomerAccount.freeFreightAmt</code> attribute **/
	public static final String FREEFREIGHTAMT = "freeFreightAmt";
	/** Qualifier of the <code>FMCustomerAccount.customerServiceTerritory</code> attribute **/
	public static final String CUSTOMERSERVICETERRITORY = "customerServiceTerritory";
	/** Qualifier of the <code>FMCustomerAccount.customerRegion</code> attribute **/
	public static final String CUSTOMERREGION = "customerRegion";
	/**
	* {@link OneToManyHandler} for handling 1:n SALESORGS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SalesOrganization> SALESORGSHANDLER = new OneToManyHandler<SalesOrganization>(
	FmCoreConstants.TC.SALESORGANIZATION,
	true,
	"fmCustAccount",
	null,
	false,
	true,
	CollectionType.LIST
	);
	/**
	* {@link OneToManyHandler} for handling 1:n FMTAXDOCUMENTS's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<FMTaxDocument> FMTAXDOCUMENTSHANDLER = new OneToManyHandler<FMTaxDocument>(
	FmCoreConstants.TC.FMTAXDOCUMENT,
	true,
	"fmCustomerAccountUnit",
	null,
	false,
	true,
	CollectionType.LIST
	);
	/**
	* {@link OneToManyHandler} for handling 1:n CUSTOMERSALESCHANNEL's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<SalesChannel> CUSTOMERSALESCHANNELHANDLER = new OneToManyHandler<SalesChannel>(
	FmCoreConstants.TC.SALESCHANNEL,
	true,
	"fmCustAccSalesChannel",
	null,
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(B2BUnit.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(SHIPPINGCODE, AttributeMode.INITIAL);
		tmp.put(SHIPTOLIMITFLAG, AttributeMode.INITIAL);
		tmp.put(BUYINGGROUP, AttributeMode.INITIAL);
		tmp.put(DISTRIBUTIONCHANNEL, AttributeMode.INITIAL);
		tmp.put(CUSTOMERTERRITORY, AttributeMode.INITIAL);
		tmp.put(ASSOCIATION, AttributeMode.INITIAL);
		tmp.put(CUSTOMERGROUP, AttributeMode.INITIAL);
		tmp.put(CARRIERACCOUNTCODE, AttributeMode.INITIAL);
		tmp.put(DIVISION, AttributeMode.INITIAL);
		tmp.put(ACCCOMMENTS, AttributeMode.INITIAL);
		tmp.put(ACCCOMMENTS2, AttributeMode.INITIAL);
		tmp.put(NABSACCOUNTCODE, AttributeMode.INITIAL);
		tmp.put(BUYINGAFFILIATIONCODE, AttributeMode.INITIAL);
		tmp.put(TAXID, AttributeMode.INITIAL);
		tmp.put(CHANNELCODE, AttributeMode.INITIAL);
		tmp.put(PROSPECTUID, AttributeMode.INITIAL);
		tmp.put(FMTAXDOCUMENT, AttributeMode.INITIAL);
		tmp.put(SALESORG, AttributeMode.INITIAL);
		tmp.put(VIEWINVOICEFLAG, AttributeMode.INITIAL);
		tmp.put(CREDITCARDFLAG, AttributeMode.INITIAL);
		tmp.put(FREEFREIGHTAMT, AttributeMode.INITIAL);
		tmp.put(CUSTOMERSERVICETERRITORY, AttributeMode.INITIAL);
		tmp.put(CUSTOMERREGION, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.accComments</code> attribute.
	 * @return the accComments - Comments
	 */
	public String getAccComments(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ACCCOMMENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.accComments</code> attribute.
	 * @return the accComments - Comments
	 */
	public String getAccComments()
	{
		return getAccComments( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.accComments</code> attribute. 
	 * @param value the accComments - Comments
	 */
	public void setAccComments(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ACCCOMMENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.accComments</code> attribute. 
	 * @param value the accComments - Comments
	 */
	public void setAccComments(final String value)
	{
		setAccComments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.accComments2</code> attribute.
	 * @return the accComments2 - Comments
	 */
	public String getAccComments2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ACCCOMMENTS2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.accComments2</code> attribute.
	 * @return the accComments2 - Comments
	 */
	public String getAccComments2()
	{
		return getAccComments2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.accComments2</code> attribute. 
	 * @param value the accComments2 - Comments
	 */
	public void setAccComments2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ACCCOMMENTS2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.accComments2</code> attribute. 
	 * @param value the accComments2 - Comments
	 */
	public void setAccComments2(final String value)
	{
		setAccComments2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.association</code> attribute.
	 * @return the association - Customer association details
	 */
	public String getAssociation(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASSOCIATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.association</code> attribute.
	 * @return the association - Customer association details
	 */
	public String getAssociation()
	{
		return getAssociation( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.association</code> attribute. 
	 * @param value the association - Customer association details
	 */
	public void setAssociation(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASSOCIATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.association</code> attribute. 
	 * @param value the association - Customer association details
	 */
	public void setAssociation(final String value)
	{
		setAssociation( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.buyingAffiliationCode</code> attribute.
	 * @return the buyingAffiliationCode - Buying affiliation code
	 */
	public String getBuyingAffiliationCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BUYINGAFFILIATIONCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.buyingAffiliationCode</code> attribute.
	 * @return the buyingAffiliationCode - Buying affiliation code
	 */
	public String getBuyingAffiliationCode()
	{
		return getBuyingAffiliationCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.buyingAffiliationCode</code> attribute. 
	 * @param value the buyingAffiliationCode - Buying affiliation code
	 */
	public void setBuyingAffiliationCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BUYINGAFFILIATIONCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.buyingAffiliationCode</code> attribute. 
	 * @param value the buyingAffiliationCode - Buying affiliation code
	 */
	public void setBuyingAffiliationCode(final String value)
	{
		setBuyingAffiliationCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.buyingGroup</code> attribute.
	 * @return the buyingGroup - Buying group
	 */
	public String getBuyingGroup(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BUYINGGROUP);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.buyingGroup</code> attribute.
	 * @return the buyingGroup - Buying group
	 */
	public String getBuyingGroup()
	{
		return getBuyingGroup( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.buyingGroup</code> attribute. 
	 * @param value the buyingGroup - Buying group
	 */
	public void setBuyingGroup(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BUYINGGROUP,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.buyingGroup</code> attribute. 
	 * @param value the buyingGroup - Buying group
	 */
	public void setBuyingGroup(final String value)
	{
		setBuyingGroup( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.carrierAccountCode</code> attribute.
	 * @return the carrierAccountCode - Carrier account code
	 */
	public String getCarrierAccountCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIERACCOUNTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.carrierAccountCode</code> attribute.
	 * @return the carrierAccountCode - Carrier account code
	 */
	public String getCarrierAccountCode()
	{
		return getCarrierAccountCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.carrierAccountCode</code> attribute. 
	 * @param value the carrierAccountCode - Carrier account code
	 */
	public void setCarrierAccountCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIERACCOUNTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.carrierAccountCode</code> attribute. 
	 * @param value the carrierAccountCode - Carrier account code
	 */
	public void setCarrierAccountCode(final String value)
	{
		setCarrierAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.channelCode</code> attribute.
	 * @return the channelCode - Customer business channel code
	 */
	public String getChannelCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CHANNELCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.channelCode</code> attribute.
	 * @return the channelCode - Customer business channel code
	 */
	public String getChannelCode()
	{
		return getChannelCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.channelCode</code> attribute. 
	 * @param value the channelCode - Customer business channel code
	 */
	public void setChannelCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CHANNELCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.channelCode</code> attribute. 
	 * @param value the channelCode - Customer business channel code
	 */
	public void setChannelCode(final String value)
	{
		setChannelCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.creditCardFlag</code> attribute.
	 * @return the creditCardFlag - Customer can place an order using the credit card
	 */
	public Boolean isCreditCardFlag(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, CREDITCARDFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.creditCardFlag</code> attribute.
	 * @return the creditCardFlag - Customer can place an order using the credit card
	 */
	public Boolean isCreditCardFlag()
	{
		return isCreditCardFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @return the creditCardFlag - Customer can place an order using the credit card
	 */
	public boolean isCreditCardFlagAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isCreditCardFlag( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @return the creditCardFlag - Customer can place an order using the credit card
	 */
	public boolean isCreditCardFlagAsPrimitive()
	{
		return isCreditCardFlagAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @param value the creditCardFlag - Customer can place an order using the credit card
	 */
	public void setCreditCardFlag(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, CREDITCARDFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @param value the creditCardFlag - Customer can place an order using the credit card
	 */
	public void setCreditCardFlag(final Boolean value)
	{
		setCreditCardFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @param value the creditCardFlag - Customer can place an order using the credit card
	 */
	public void setCreditCardFlag(final SessionContext ctx, final boolean value)
	{
		setCreditCardFlag( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.creditCardFlag</code> attribute. 
	 * @param value the creditCardFlag - Customer can place an order using the credit card
	 */
	public void setCreditCardFlag(final boolean value)
	{
		setCreditCardFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerGroup</code> attribute.
	 * @return the customerGroup - Customer Group
	 */
	public String getCustomerGroup(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERGROUP);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerGroup</code> attribute.
	 * @return the customerGroup - Customer Group
	 */
	public String getCustomerGroup()
	{
		return getCustomerGroup( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerGroup</code> attribute. 
	 * @param value the customerGroup - Customer Group
	 */
	public void setCustomerGroup(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERGROUP,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerGroup</code> attribute. 
	 * @param value the customerGroup - Customer Group
	 */
	public void setCustomerGroup(final String value)
	{
		setCustomerGroup( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerRegion</code> attribute.
	 * @return the customerRegion - Customer region
	 */
	public String getCustomerRegion(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERREGION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerRegion</code> attribute.
	 * @return the customerRegion - Customer region
	 */
	public String getCustomerRegion()
	{
		return getCustomerRegion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerRegion</code> attribute. 
	 * @param value the customerRegion - Customer region
	 */
	public void setCustomerRegion(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERREGION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerRegion</code> attribute. 
	 * @param value the customerRegion - Customer region
	 */
	public void setCustomerRegion(final String value)
	{
		setCustomerRegion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerSalesChannel</code> attribute.
	 * @return the customerSalesChannel
	 */
	public List<SalesChannel> getCustomerSalesChannel(final SessionContext ctx)
	{
		return (List<SalesChannel>)CUSTOMERSALESCHANNELHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerSalesChannel</code> attribute.
	 * @return the customerSalesChannel
	 */
	public List<SalesChannel> getCustomerSalesChannel()
	{
		return getCustomerSalesChannel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerSalesChannel</code> attribute. 
	 * @param value the customerSalesChannel
	 */
	public void setCustomerSalesChannel(final SessionContext ctx, final List<SalesChannel> value)
	{
		CUSTOMERSALESCHANNELHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerSalesChannel</code> attribute. 
	 * @param value the customerSalesChannel
	 */
	public void setCustomerSalesChannel(final List<SalesChannel> value)
	{
		setCustomerSalesChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customerSalesChannel. 
	 * @param value the item to add to customerSalesChannel
	 */
	public void addToCustomerSalesChannel(final SessionContext ctx, final SalesChannel value)
	{
		CUSTOMERSALESCHANNELHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to customerSalesChannel. 
	 * @param value the item to add to customerSalesChannel
	 */
	public void addToCustomerSalesChannel(final SalesChannel value)
	{
		addToCustomerSalesChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customerSalesChannel. 
	 * @param value the item to remove from customerSalesChannel
	 */
	public void removeFromCustomerSalesChannel(final SessionContext ctx, final SalesChannel value)
	{
		CUSTOMERSALESCHANNELHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from customerSalesChannel. 
	 * @param value the item to remove from customerSalesChannel
	 */
	public void removeFromCustomerSalesChannel(final SalesChannel value)
	{
		removeFromCustomerSalesChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerServiceTerritory</code> attribute.
	 * @return the customerServiceTerritory - Customer service territory
	 */
	public String getCustomerServiceTerritory(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERSERVICETERRITORY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerServiceTerritory</code> attribute.
	 * @return the customerServiceTerritory - Customer service territory
	 */
	public String getCustomerServiceTerritory()
	{
		return getCustomerServiceTerritory( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerServiceTerritory</code> attribute. 
	 * @param value the customerServiceTerritory - Customer service territory
	 */
	public void setCustomerServiceTerritory(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERSERVICETERRITORY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerServiceTerritory</code> attribute. 
	 * @param value the customerServiceTerritory - Customer service territory
	 */
	public void setCustomerServiceTerritory(final String value)
	{
		setCustomerServiceTerritory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerTerritory</code> attribute.
	 * @return the customerTerritory - Customer territory
	 */
	public String getCustomerTerritory(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERTERRITORY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.customerTerritory</code> attribute.
	 * @return the customerTerritory - Customer territory
	 */
	public String getCustomerTerritory()
	{
		return getCustomerTerritory( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerTerritory</code> attribute. 
	 * @param value the customerTerritory - Customer territory
	 */
	public void setCustomerTerritory(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERTERRITORY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.customerTerritory</code> attribute. 
	 * @param value the customerTerritory - Customer territory
	 */
	public void setCustomerTerritory(final String value)
	{
		setCustomerTerritory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.distributionChannel</code> attribute.
	 * @return the distributionChannel - Customer Distribution Channel
	 */
	public String getDistributionChannel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DISTRIBUTIONCHANNEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.distributionChannel</code> attribute.
	 * @return the distributionChannel - Customer Distribution Channel
	 */
	public String getDistributionChannel()
	{
		return getDistributionChannel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.distributionChannel</code> attribute. 
	 * @param value the distributionChannel - Customer Distribution Channel
	 */
	public void setDistributionChannel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DISTRIBUTIONCHANNEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.distributionChannel</code> attribute. 
	 * @param value the distributionChannel - Customer Distribution Channel
	 */
	public void setDistributionChannel(final String value)
	{
		setDistributionChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.division</code> attribute.
	 * @return the division - Customer Division
	 */
	public String getDivision(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIVISION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.division</code> attribute.
	 * @return the division - Customer Division
	 */
	public String getDivision()
	{
		return getDivision( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.division</code> attribute. 
	 * @param value the division - Customer Division
	 */
	public void setDivision(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIVISION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.division</code> attribute. 
	 * @param value the division - Customer Division
	 */
	public void setDivision(final String value)
	{
		setDivision( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmCustTargetAccountCode</code> attribute.
	 * @return the fmCustTargetAccountCode
	 */
	public List<FMCustomerPartnerFunction> getFmCustTargetAccountCode(final SessionContext ctx)
	{
		final List<FMCustomerPartnerFunction> items = getLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true)
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmCustTargetAccountCode</code> attribute.
	 * @return the fmCustTargetAccountCode
	 */
	public List<FMCustomerPartnerFunction> getFmCustTargetAccountCode()
	{
		return getFmCustTargetAccountCode( getSession().getSessionContext() );
	}
	
	public long getFmCustTargetAccountCodeCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			true,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null
		);
	}
	
	public long getFmCustTargetAccountCodeCount()
	{
		return getFmCustTargetAccountCodeCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmCustTargetAccountCode</code> attribute. 
	 * @param value the fmCustTargetAccountCode
	 */
	public void setFmCustTargetAccountCode(final SessionContext ctx, final List<FMCustomerPartnerFunction> value)
	{
		new PartOfHandler<List<FMCustomerPartnerFunction>>()
		{
			@Override
			protected List<FMCustomerPartnerFunction> doGetValue(final SessionContext ctx)
			{
				return getFmCustTargetAccountCode( ctx );
			}
			@Override
			protected void doSetValue(final SessionContext ctx, final List<FMCustomerPartnerFunction> _value)
			{
				final List<FMCustomerPartnerFunction> value = _value;
				setLinkedItems( 
					ctx,
					true,
					FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
					null,
					value,
					Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
					Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
					Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
				);
			}
		}.setValue( ctx, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmCustTargetAccountCode</code> attribute. 
	 * @param value the fmCustTargetAccountCode
	 */
	public void setFmCustTargetAccountCode(final List<FMCustomerPartnerFunction> value)
	{
		setFmCustTargetAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmCustTargetAccountCode. 
	 * @param value the item to add to fmCustTargetAccountCode
	 */
	public void addToFmCustTargetAccountCode(final SessionContext ctx, final FMCustomerPartnerFunction value)
	{
		addLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmCustTargetAccountCode. 
	 * @param value the item to add to fmCustTargetAccountCode
	 */
	public void addToFmCustTargetAccountCode(final FMCustomerPartnerFunction value)
	{
		addToFmCustTargetAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmCustTargetAccountCode. 
	 * @param value the item to remove from fmCustTargetAccountCode
	 */
	public void removeFromFmCustTargetAccountCode(final SessionContext ctx, final FMCustomerPartnerFunction value)
	{
		removeLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
		);
		if( !getLinkedItems( ctx, true,FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,null).contains( value ) )
		{
			try
			{
				value.remove( ctx );
			}
			catch( ConsistencyCheckException e )
			{
				throw new JaloSystemException(e);
			}
		}
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmCustTargetAccountCode. 
	 * @param value the item to remove from fmCustTargetAccountCode
	 */
	public void removeFromFmCustTargetAccountCode(final FMCustomerPartnerFunction value)
	{
		removeFromFmCustTargetAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmtaxDocument</code> attribute.
	 * @return the fmtaxDocument - Tax Document upload location.
	 */
	public List<FMTaxDocument> getFmtaxDocument(final SessionContext ctx)
	{
		List<FMTaxDocument> coll = (List<FMTaxDocument>)getProperty( ctx, FMTAXDOCUMENT);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmtaxDocument</code> attribute.
	 * @return the fmtaxDocument - Tax Document upload location.
	 */
	public List<FMTaxDocument> getFmtaxDocument()
	{
		return getFmtaxDocument( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmtaxDocument</code> attribute. 
	 * @param value the fmtaxDocument - Tax Document upload location.
	 */
	public void setFmtaxDocument(final SessionContext ctx, final List<FMTaxDocument> value)
	{
		setProperty(ctx, FMTAXDOCUMENT,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmtaxDocument</code> attribute. 
	 * @param value the fmtaxDocument - Tax Document upload location.
	 */
	public void setFmtaxDocument(final List<FMTaxDocument> value)
	{
		setFmtaxDocument( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmTaxDocuments</code> attribute.
	 * @return the fmTaxDocuments
	 */
	public List<FMTaxDocument> getFmTaxDocuments(final SessionContext ctx)
	{
		return (List<FMTaxDocument>)FMTAXDOCUMENTSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.fmTaxDocuments</code> attribute.
	 * @return the fmTaxDocuments
	 */
	public List<FMTaxDocument> getFmTaxDocuments()
	{
		return getFmTaxDocuments( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmTaxDocuments</code> attribute. 
	 * @param value the fmTaxDocuments
	 */
	public void setFmTaxDocuments(final SessionContext ctx, final List<FMTaxDocument> value)
	{
		FMTAXDOCUMENTSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.fmTaxDocuments</code> attribute. 
	 * @param value the fmTaxDocuments
	 */
	public void setFmTaxDocuments(final List<FMTaxDocument> value)
	{
		setFmTaxDocuments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmTaxDocuments. 
	 * @param value the item to add to fmTaxDocuments
	 */
	public void addToFmTaxDocuments(final SessionContext ctx, final FMTaxDocument value)
	{
		FMTAXDOCUMENTSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmTaxDocuments. 
	 * @param value the item to add to fmTaxDocuments
	 */
	public void addToFmTaxDocuments(final FMTaxDocument value)
	{
		addToFmTaxDocuments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmTaxDocuments. 
	 * @param value the item to remove from fmTaxDocuments
	 */
	public void removeFromFmTaxDocuments(final SessionContext ctx, final FMTaxDocument value)
	{
		FMTAXDOCUMENTSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmTaxDocuments. 
	 * @param value the item to remove from fmTaxDocuments
	 */
	public void removeFromFmTaxDocuments(final FMTaxDocument value)
	{
		removeFromFmTaxDocuments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute.
	 * @return the freeFreightAmt - Amount to get free freight
	 */
	public Integer getFreeFreightAmt(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, FREEFREIGHTAMT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute.
	 * @return the freeFreightAmt - Amount to get free freight
	 */
	public Integer getFreeFreightAmt()
	{
		return getFreeFreightAmt( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @return the freeFreightAmt - Amount to get free freight
	 */
	public int getFreeFreightAmtAsPrimitive(final SessionContext ctx)
	{
		Integer value = getFreeFreightAmt( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @return the freeFreightAmt - Amount to get free freight
	 */
	public int getFreeFreightAmtAsPrimitive()
	{
		return getFreeFreightAmtAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @param value the freeFreightAmt - Amount to get free freight
	 */
	public void setFreeFreightAmt(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, FREEFREIGHTAMT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @param value the freeFreightAmt - Amount to get free freight
	 */
	public void setFreeFreightAmt(final Integer value)
	{
		setFreeFreightAmt( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @param value the freeFreightAmt - Amount to get free freight
	 */
	public void setFreeFreightAmt(final SessionContext ctx, final int value)
	{
		setFreeFreightAmt( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.freeFreightAmt</code> attribute. 
	 * @param value the freeFreightAmt - Amount to get free freight
	 */
	public void setFreeFreightAmt(final int value)
	{
		setFreeFreightAmt( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.nabsAccountCode</code> attribute.
	 * @return the nabsAccountCode - Customer NABS account number
	 */
	public String getNabsAccountCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NABSACCOUNTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.nabsAccountCode</code> attribute.
	 * @return the nabsAccountCode - Customer NABS account number
	 */
	public String getNabsAccountCode()
	{
		return getNabsAccountCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.nabsAccountCode</code> attribute. 
	 * @param value the nabsAccountCode - Customer NABS account number
	 */
	public void setNabsAccountCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NABSACCOUNTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.nabsAccountCode</code> attribute. 
	 * @param value the nabsAccountCode - Customer NABS account number
	 */
	public void setNabsAccountCode(final String value)
	{
		setNabsAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.prospectuid</code> attribute.
	 * @return the prospectuid - B2b prospect customer account number
	 */
	public String getProspectuid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PROSPECTUID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.prospectuid</code> attribute.
	 * @return the prospectuid - B2b prospect customer account number
	 */
	public String getProspectuid()
	{
		return getProspectuid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.prospectuid</code> attribute. 
	 * @param value the prospectuid - B2b prospect customer account number
	 */
	public void setProspectuid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PROSPECTUID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.prospectuid</code> attribute. 
	 * @param value the prospectuid - B2b prospect customer account number
	 */
	public void setProspectuid(final String value)
	{
		setProspectuid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.salesorg</code> attribute.
	 * @return the salesorg - Customer Sales Organization
	 */
	public String getSalesorg(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESORG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.salesorg</code> attribute.
	 * @return the salesorg - Customer Sales Organization
	 */
	public String getSalesorg()
	{
		return getSalesorg( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.salesorg</code> attribute. 
	 * @param value the salesorg - Customer Sales Organization
	 */
	public void setSalesorg(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESORG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.salesorg</code> attribute. 
	 * @param value the salesorg - Customer Sales Organization
	 */
	public void setSalesorg(final String value)
	{
		setSalesorg( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.salesOrgs</code> attribute.
	 * @return the salesOrgs
	 */
	public List<SalesOrganization> getSalesOrgs(final SessionContext ctx)
	{
		return (List<SalesOrganization>)SALESORGSHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.salesOrgs</code> attribute.
	 * @return the salesOrgs
	 */
	public List<SalesOrganization> getSalesOrgs()
	{
		return getSalesOrgs( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.salesOrgs</code> attribute. 
	 * @param value the salesOrgs
	 */
	public void setSalesOrgs(final SessionContext ctx, final List<SalesOrganization> value)
	{
		SALESORGSHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.salesOrgs</code> attribute. 
	 * @param value the salesOrgs
	 */
	public void setSalesOrgs(final List<SalesOrganization> value)
	{
		setSalesOrgs( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to salesOrgs. 
	 * @param value the item to add to salesOrgs
	 */
	public void addToSalesOrgs(final SessionContext ctx, final SalesOrganization value)
	{
		SALESORGSHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to salesOrgs. 
	 * @param value the item to add to salesOrgs
	 */
	public void addToSalesOrgs(final SalesOrganization value)
	{
		addToSalesOrgs( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from salesOrgs. 
	 * @param value the item to remove from salesOrgs
	 */
	public void removeFromSalesOrgs(final SessionContext ctx, final SalesOrganization value)
	{
		SALESORGSHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from salesOrgs. 
	 * @param value the item to remove from salesOrgs
	 */
	public void removeFromSalesOrgs(final SalesOrganization value)
	{
		removeFromSalesOrgs( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.shippingCode</code> attribute.
	 * @return the shippingCode - Shipping code
	 */
	public String getShippingCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPINGCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.shippingCode</code> attribute.
	 * @return the shippingCode - Shipping code
	 */
	public String getShippingCode()
	{
		return getShippingCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.shippingCode</code> attribute. 
	 * @param value the shippingCode - Shipping code
	 */
	public void setShippingCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPINGCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.shippingCode</code> attribute. 
	 * @param value the shippingCode - Shipping code
	 */
	public void setShippingCode(final String value)
	{
		setShippingCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.shipToLimitFlag</code> attribute.
	 * @return the shipToLimitFlag - Customer Sales Organization
	 */
	public String getShipToLimitFlag(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPTOLIMITFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.shipToLimitFlag</code> attribute.
	 * @return the shipToLimitFlag - Customer Sales Organization
	 */
	public String getShipToLimitFlag()
	{
		return getShipToLimitFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.shipToLimitFlag</code> attribute. 
	 * @param value the shipToLimitFlag - Customer Sales Organization
	 */
	public void setShipToLimitFlag(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPTOLIMITFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.shipToLimitFlag</code> attribute. 
	 * @param value the shipToLimitFlag - Customer Sales Organization
	 */
	public void setShipToLimitFlag(final String value)
	{
		setShipToLimitFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.taxID</code> attribute.
	 * @return the taxID - B2b customer tax number
	 */
	public String getTaxID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TAXID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.taxID</code> attribute.
	 * @return the taxID - B2b customer tax number
	 */
	public String getTaxID()
	{
		return getTaxID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.taxID</code> attribute. 
	 * @param value the taxID - B2b customer tax number
	 */
	public void setTaxID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TAXID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.taxID</code> attribute. 
	 * @param value the taxID - B2b customer tax number
	 */
	public void setTaxID(final String value)
	{
		setTaxID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute.
	 * @return the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public Boolean isViewInvoiceFlag(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, VIEWINVOICEFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute.
	 * @return the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public Boolean isViewInvoiceFlag()
	{
		return isViewInvoiceFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @return the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public boolean isViewInvoiceFlagAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isViewInvoiceFlag( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @return the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public boolean isViewInvoiceFlagAsPrimitive()
	{
		return isViewInvoiceFlagAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @param value the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public void setViewInvoiceFlag(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, VIEWINVOICEFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @param value the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public void setViewInvoiceFlag(final Boolean value)
	{
		setViewInvoiceFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @param value the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public void setViewInvoiceFlag(final SessionContext ctx, final boolean value)
	{
		setViewInvoiceFlag( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerAccount.viewInvoiceFlag</code> attribute. 
	 * @param value the viewInvoiceFlag - Customer allowed to view invoice details
	 */
	public void setViewInvoiceFlag(final boolean value)
	{
		setViewInvoiceFlag( getSession().getSessionContext(), value );
	}
	
}
