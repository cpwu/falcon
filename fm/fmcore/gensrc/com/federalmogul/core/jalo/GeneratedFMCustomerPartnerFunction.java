/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem FMCustomerPartnerFunction}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCustomerPartnerFunction extends GenericItem
{
	/** Qualifier of the <code>FMCustomerPartnerFunction.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMCustomerPartnerFunction.PartnerFMAccountType</code> attribute **/
	public static final String PARTNERFMACCOUNTTYPE = "PartnerFMAccountType";
	/** Qualifier of the <code>FMCustomerPartnerFunction.PartnerAccountType</code> attribute **/
	public static final String PARTNERACCOUNTTYPE = "PartnerAccountType";
	/** Qualifier of the <code>FMCustomerPartnerFunction.fmCustSourceAccountCode</code> attribute **/
	public static final String FMCUSTSOURCEACCOUNTCODE = "fmCustSourceAccountCode";
	/** Relation ordering override parameter constants for FMCustAccount2PartnerFunction from ((fmcore))*/
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED = "relation.FMCustAccount2PartnerFunction.source.ordered";
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED = "relation.FMCustAccount2PartnerFunction.target.ordered";
	/** Relation disable markmodifed parameter constants for FMCustAccount2PartnerFunction from ((fmcore))*/
	protected static String FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED = "relation.FMCustAccount2PartnerFunction.markmodified";
	/** Qualifier of the <code>FMCustomerPartnerFunction.FMTargetCustomerAccount</code> attribute **/
	public static final String FMTARGETCUSTOMERACCOUNT = "FMTargetCustomerAccount";
	/** Qualifier of the <code>FMCustomerPartnerFunction.FMSourceCustomerAccount</code> attribute **/
	public static final String FMSOURCECUSTOMERACCOUNT = "FMSourceCustomerAccount";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(PARTNERFMACCOUNTTYPE, AttributeMode.INITIAL);
		tmp.put(PARTNERACCOUNTTYPE, AttributeMode.INITIAL);
		tmp.put(FMTARGETCUSTOMERACCOUNT, AttributeMode.INITIAL);
		tmp.put(FMSOURCECUSTOMERACCOUNT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.code</code> attribute.
	 * @return the code - Partner Function unique id (SoldTo + ShipTo)
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.code</code> attribute.
	 * @return the code - Partner Function unique id (SoldTo + ShipTo)
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.code</code> attribute. 
	 * @param value the code - Partner Function unique id (SoldTo + ShipTo)
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.code</code> attribute. 
	 * @param value the code - Partner Function unique id (SoldTo + ShipTo)
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.fmCustSourceAccountCode</code> attribute.
	 * @return the fmCustSourceAccountCode
	 */
	public List<FMCustomerAccount> getFmCustSourceAccountCode(final SessionContext ctx)
	{
		final List<FMCustomerAccount> items = getLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true)
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.fmCustSourceAccountCode</code> attribute.
	 * @return the fmCustSourceAccountCode
	 */
	public List<FMCustomerAccount> getFmCustSourceAccountCode()
	{
		return getFmCustSourceAccountCode( getSession().getSessionContext() );
	}
	
	public long getFmCustSourceAccountCodeCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			false,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null
		);
	}
	
	public long getFmCustSourceAccountCodeCount()
	{
		return getFmCustSourceAccountCodeCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.fmCustSourceAccountCode</code> attribute. 
	 * @param value the fmCustSourceAccountCode
	 */
	public void setFmCustSourceAccountCode(final SessionContext ctx, final List<FMCustomerAccount> value)
	{
		setLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			value,
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.fmCustSourceAccountCode</code> attribute. 
	 * @param value the fmCustSourceAccountCode
	 */
	public void setFmCustSourceAccountCode(final List<FMCustomerAccount> value)
	{
		setFmCustSourceAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmCustSourceAccountCode. 
	 * @param value the item to add to fmCustSourceAccountCode
	 */
	public void addToFmCustSourceAccountCode(final SessionContext ctx, final FMCustomerAccount value)
	{
		addLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmCustSourceAccountCode. 
	 * @param value the item to add to fmCustSourceAccountCode
	 */
	public void addToFmCustSourceAccountCode(final FMCustomerAccount value)
	{
		addToFmCustSourceAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmCustSourceAccountCode. 
	 * @param value the item to remove from fmCustSourceAccountCode
	 */
	public void removeFromFmCustSourceAccountCode(final SessionContext ctx, final FMCustomerAccount value)
	{
		removeLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FMCUSTACCOUNT2PARTNERFUNCTION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_SRC_ORDERED, true),
			Utilities.getRelationOrderingOverride(FMCUSTACCOUNT2PARTNERFUNCTION_TGT_ORDERED, true),
			Utilities.getMarkModifiedOverride(FMCUSTACCOUNT2PARTNERFUNCTION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmCustSourceAccountCode. 
	 * @param value the item to remove from fmCustSourceAccountCode
	 */
	public void removeFromFmCustSourceAccountCode(final FMCustomerAccount value)
	{
		removeFromFmCustSourceAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.FMSourceCustomerAccount</code> attribute.
	 * @return the FMSourceCustomerAccount - Sold To account code
	 */
	public FMCustomerAccount getFMSourceCustomerAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMSOURCECUSTOMERACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.FMSourceCustomerAccount</code> attribute.
	 * @return the FMSourceCustomerAccount - Sold To account code
	 */
	public FMCustomerAccount getFMSourceCustomerAccount()
	{
		return getFMSourceCustomerAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.FMSourceCustomerAccount</code> attribute. 
	 * @param value the FMSourceCustomerAccount - Sold To account code
	 */
	public void setFMSourceCustomerAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, FMSOURCECUSTOMERACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.FMSourceCustomerAccount</code> attribute. 
	 * @param value the FMSourceCustomerAccount - Sold To account code
	 */
	public void setFMSourceCustomerAccount(final FMCustomerAccount value)
	{
		setFMSourceCustomerAccount( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.FMTargetCustomerAccount</code> attribute.
	 * @return the FMTargetCustomerAccount - Ship To account code
	 */
	public FMCustomerAccount getFMTargetCustomerAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMTARGETCUSTOMERACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.FMTargetCustomerAccount</code> attribute.
	 * @return the FMTargetCustomerAccount - Ship To account code
	 */
	public FMCustomerAccount getFMTargetCustomerAccount()
	{
		return getFMTargetCustomerAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.FMTargetCustomerAccount</code> attribute. 
	 * @param value the FMTargetCustomerAccount - Ship To account code
	 */
	public void setFMTargetCustomerAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, FMTARGETCUSTOMERACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.FMTargetCustomerAccount</code> attribute. 
	 * @param value the FMTargetCustomerAccount - Ship To account code
	 */
	public void setFMTargetCustomerAccount(final FMCustomerAccount value)
	{
		setFMTargetCustomerAccount( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.PartnerAccountType</code> attribute.
	 * @return the PartnerAccountType - Ship To account code
	 */
	public FMCustomerAccount getPartnerAccountType(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, PARTNERACCOUNTTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.PartnerAccountType</code> attribute.
	 * @return the PartnerAccountType - Ship To account code
	 */
	public FMCustomerAccount getPartnerAccountType()
	{
		return getPartnerAccountType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.PartnerAccountType</code> attribute. 
	 * @param value the PartnerAccountType - Ship To account code
	 */
	public void setPartnerAccountType(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, PARTNERACCOUNTTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.PartnerAccountType</code> attribute. 
	 * @param value the PartnerAccountType - Ship To account code
	 */
	public void setPartnerAccountType(final FMCustomerAccount value)
	{
		setPartnerAccountType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.PartnerFMAccountType</code> attribute.
	 * @return the PartnerFMAccountType - Ship To account code
	 */
	public String getPartnerFMAccountType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNERFMACCOUNTTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerPartnerFunction.PartnerFMAccountType</code> attribute.
	 * @return the PartnerFMAccountType - Ship To account code
	 */
	public String getPartnerFMAccountType()
	{
		return getPartnerFMAccountType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.PartnerFMAccountType</code> attribute. 
	 * @param value the PartnerFMAccountType - Ship To account code
	 */
	public void setPartnerFMAccountType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNERFMACCOUNTTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerPartnerFunction.PartnerFMAccountType</code> attribute. 
	 * @param value the PartnerFMAccountType - Ship To account code
	 */
	public void setPartnerFMAccountType(final String value)
	{
		setPartnerFMAccountType( getSession().getSessionContext(), value );
	}
	
}
