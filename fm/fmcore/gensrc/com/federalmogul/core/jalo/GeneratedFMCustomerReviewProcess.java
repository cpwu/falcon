/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.commerceservices.jalo.process.StoreFrontProcess;
import de.hybris.platform.customerreview.jalo.CustomerReview;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.commerceservices.jalo.process.StoreFrontProcess FMCustomerReviewProcess}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCustomerReviewProcess extends StoreFrontProcess
{
	/** Qualifier of the <code>FMCustomerReviewProcess.customerReview</code> attribute **/
	public static final String CUSTOMERREVIEW = "customerReview";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(StoreFrontProcess.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CUSTOMERREVIEW, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerReviewProcess.customerReview</code> attribute.
	 * @return the customerReview
	 */
	public CustomerReview getCustomerReview(final SessionContext ctx)
	{
		return (CustomerReview)getProperty( ctx, CUSTOMERREVIEW);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCustomerReviewProcess.customerReview</code> attribute.
	 * @return the customerReview
	 */
	public CustomerReview getCustomerReview()
	{
		return getCustomerReview( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerReviewProcess.customerReview</code> attribute. 
	 * @param value the customerReview
	 */
	public void setCustomerReview(final SessionContext ctx, final CustomerReview value)
	{
		setProperty(ctx, CUSTOMERREVIEW,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCustomerReviewProcess.customerReview</code> attribute. 
	 * @param value the customerReview
	 */
	public void setCustomerReview(final CustomerReview value)
	{
		setCustomerReview( getSession().getSessionContext(), value );
	}
	
}
