/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.media.Media;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.core.jalo.FMFileDownload FMFileDownload}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMFileDownload extends Media
{
	/** Qualifier of the <code>FMFileDownload.country</code> attribute **/
	public static final String COUNTRY = "country";
	/** Qualifier of the <code>FMFileDownload.fileName</code> attribute **/
	public static final String FILENAME = "fileName";
	/** Qualifier of the <code>FMFileDownload.fileSize</code> attribute **/
	public static final String FILESIZE = "fileSize";
	/** Qualifier of the <code>FMFileDownload.fileFormat</code> attribute **/
	public static final String FILEFORMAT = "fileFormat";
	/** Qualifier of the <code>FMFileDownload.lastRevisedDate</code> attribute **/
	public static final String LASTREVISEDDATE = "lastRevisedDate";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Media.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(COUNTRY, AttributeMode.INITIAL);
		tmp.put(FILENAME, AttributeMode.INITIAL);
		tmp.put(FILESIZE, AttributeMode.INITIAL);
		tmp.put(FILEFORMAT, AttributeMode.INITIAL);
		tmp.put(LASTREVISEDDATE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.country</code> attribute.
	 * @return the country - Country
	 */
	public String getCountry(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COUNTRY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.country</code> attribute.
	 * @return the country - Country
	 */
	public String getCountry()
	{
		return getCountry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.country</code> attribute. 
	 * @param value the country - Country
	 */
	public void setCountry(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COUNTRY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.country</code> attribute. 
	 * @param value the country - Country
	 */
	public void setCountry(final String value)
	{
		setCountry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileFormat</code> attribute.
	 * @return the fileFormat - File Format
	 */
	public String getFileFormat(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FILEFORMAT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileFormat</code> attribute.
	 * @return the fileFormat - File Format
	 */
	public String getFileFormat()
	{
		return getFileFormat( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileFormat</code> attribute. 
	 * @param value the fileFormat - File Format
	 */
	public void setFileFormat(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FILEFORMAT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileFormat</code> attribute. 
	 * @param value the fileFormat - File Format
	 */
	public void setFileFormat(final String value)
	{
		setFileFormat( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileName</code> attribute.
	 * @return the fileName - File Name
	 */
	public String getFileName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FILENAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileName</code> attribute.
	 * @return the fileName - File Name
	 */
	public String getFileName()
	{
		return getFileName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileName</code> attribute. 
	 * @param value the fileName - File Name
	 */
	public void setFileName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FILENAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileName</code> attribute. 
	 * @param value the fileName - File Name
	 */
	public void setFileName(final String value)
	{
		setFileName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileSize</code> attribute.
	 * @return the fileSize - File Size
	 */
	public String getFileSize(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FILESIZE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.fileSize</code> attribute.
	 * @return the fileSize - File Size
	 */
	public String getFileSize()
	{
		return getFileSize( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileSize</code> attribute. 
	 * @param value the fileSize - File Size
	 */
	public void setFileSize(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FILESIZE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.fileSize</code> attribute. 
	 * @param value the fileSize - File Size
	 */
	public void setFileSize(final String value)
	{
		setFileSize( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.lastRevisedDate</code> attribute.
	 * @return the lastRevisedDate - Last Revised
	 */
	public String getLastRevisedDate(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LASTREVISEDDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFileDownload.lastRevisedDate</code> attribute.
	 * @return the lastRevisedDate - Last Revised
	 */
	public String getLastRevisedDate()
	{
		return getLastRevisedDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.lastRevisedDate</code> attribute. 
	 * @param value the lastRevisedDate - Last Revised
	 */
	public void setLastRevisedDate(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LASTREVISEDDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFileDownload.lastRevisedDate</code> attribute. 
	 * @param value the lastRevisedDate - Last Revised
	 */
	public void setLastRevisedDate(final String value)
	{
		setLastRevisedDate( getSession().getSessionContext(), value );
	}
	
}
