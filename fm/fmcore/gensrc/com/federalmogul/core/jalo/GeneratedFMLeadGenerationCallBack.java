/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.b2b.jalo.B2BCustomer FMLeadGenerationCallBack}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMLeadGenerationCallBack extends B2BCustomer
{
	/** Qualifier of the <code>FMLeadGenerationCallBack.callBackDescription</code> attribute **/
	public static final String CALLBACKDESCRIPTION = "callBackDescription";
	/** Qualifier of the <code>FMLeadGenerationCallBack.subjects</code> attribute **/
	public static final String SUBJECTS = "subjects";
	/** Qualifier of the <code>FMLeadGenerationCallBack.jobtitle</code> attribute **/
	public static final String JOBTITLE = "jobtitle";
	/** Qualifier of the <code>FMLeadGenerationCallBack.department</code> attribute **/
	public static final String DEPARTMENT = "department";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(B2BCustomer.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CALLBACKDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(SUBJECTS, AttributeMode.INITIAL);
		tmp.put(JOBTITLE, AttributeMode.INITIAL);
		tmp.put(DEPARTMENT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.callBackDescription</code> attribute.
	 * @return the callBackDescription - Lead generation call back description
	 */
	public String getCallBackDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CALLBACKDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.callBackDescription</code> attribute.
	 * @return the callBackDescription - Lead generation call back description
	 */
	public String getCallBackDescription()
	{
		return getCallBackDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.callBackDescription</code> attribute. 
	 * @param value the callBackDescription - Lead generation call back description
	 */
	public void setCallBackDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CALLBACKDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.callBackDescription</code> attribute. 
	 * @param value the callBackDescription - Lead generation call back description
	 */
	public void setCallBackDescription(final String value)
	{
		setCallBackDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.department</code> attribute.
	 * @return the department - Department
	 */
	public String getDepartment(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DEPARTMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.department</code> attribute.
	 * @return the department - Department
	 */
	public String getDepartment()
	{
		return getDepartment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.department</code> attribute. 
	 * @param value the department - Department
	 */
	public void setDepartment(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DEPARTMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.department</code> attribute. 
	 * @param value the department - Department
	 */
	public void setDepartment(final String value)
	{
		setDepartment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.jobtitle</code> attribute.
	 * @return the jobtitle - Job Title
	 */
	public String getJobtitle(final SessionContext ctx)
	{
		return (String)getProperty( ctx, JOBTITLE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.jobtitle</code> attribute.
	 * @return the jobtitle - Job Title
	 */
	public String getJobtitle()
	{
		return getJobtitle( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.jobtitle</code> attribute. 
	 * @param value the jobtitle - Job Title
	 */
	public void setJobtitle(final SessionContext ctx, final String value)
	{
		setProperty(ctx, JOBTITLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.jobtitle</code> attribute. 
	 * @param value the jobtitle - Job Title
	 */
	public void setJobtitle(final String value)
	{
		setJobtitle( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.subjects</code> attribute.
	 * @return the subjects - Lead generation call back subject list
	 */
	public EnumerationValue getSubjects(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, SUBJECTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMLeadGenerationCallBack.subjects</code> attribute.
	 * @return the subjects - Lead generation call back subject list
	 */
	public EnumerationValue getSubjects()
	{
		return getSubjects( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.subjects</code> attribute. 
	 * @param value the subjects - Lead generation call back subject list
	 */
	public void setSubjects(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, SUBJECTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMLeadGenerationCallBack.subjects</code> attribute. 
	 * @param value the subjects - Lead generation call back subject list
	 */
	public void setSubjects(final EnumerationValue value)
	{
		setSubjects( getSession().getSessionContext(), value );
	}
	
}
