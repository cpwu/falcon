/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomer;
import com.federalmogul.core.jalo.FMCustomerAccount;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.jalo.c2l.Region;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.core.jalo.FMTaxDocument FMTaxDocument}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMTaxDocument extends Media
{
	/** Qualifier of the <code>FMTaxDocument.uploadedBy</code> attribute **/
	public static final String UPLOADEDBY = "uploadedBy";
	/** Qualifier of the <code>FMTaxDocument.approvedBy</code> attribute **/
	public static final String APPROVEDBY = "approvedBy";
	/** Qualifier of the <code>FMTaxDocument.validate</code> attribute **/
	public static final String VALIDATE = "validate";
	/** Qualifier of the <code>FMTaxDocument.fmCustomerAccountUnit</code> attribute **/
	public static final String FMCUSTOMERACCOUNTUNIT = "fmCustomerAccountUnit";
	/** Qualifier of the <code>FMTaxDocument.state</code> attribute **/
	public static final String STATE = "state";
	/** Qualifier of the <code>FMTaxDocument.docname</code> attribute **/
	public static final String DOCNAME = "docname";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n FMCUSTOMERACCOUNTUNIT's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedFMTaxDocument> FMCUSTOMERACCOUNTUNITHANDLER = new BidirectionalOneToManyHandler<GeneratedFMTaxDocument>(
	FmCoreConstants.TC.FMTAXDOCUMENT,
	false,
	"fmCustomerAccountUnit",
	null,
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Media.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(UPLOADEDBY, AttributeMode.INITIAL);
		tmp.put(APPROVEDBY, AttributeMode.INITIAL);
		tmp.put(VALIDATE, AttributeMode.INITIAL);
		tmp.put(FMCUSTOMERACCOUNTUNIT, AttributeMode.INITIAL);
		tmp.put(STATE, AttributeMode.INITIAL);
		tmp.put(DOCNAME, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.approvedBy</code> attribute.
	 * @return the approvedBy
	 */
	public String getApprovedBy(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMTaxDocument.getApprovedBy requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, APPROVEDBY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.approvedBy</code> attribute.
	 * @return the approvedBy
	 */
	public String getApprovedBy()
	{
		return getApprovedBy( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @return the localized approvedBy
	 */
	public Map<Language,String> getAllApprovedBy(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,APPROVEDBY,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @return the localized approvedBy
	 */
	public Map<Language,String> getAllApprovedBy()
	{
		return getAllApprovedBy( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @param value the approvedBy
	 */
	public void setApprovedBy(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMTaxDocument.setApprovedBy requires a session language", 0 );
		}
		setLocalizedProperty(ctx, APPROVEDBY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @param value the approvedBy
	 */
	public void setApprovedBy(final String value)
	{
		setApprovedBy( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @param value the approvedBy
	 */
	public void setAllApprovedBy(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,APPROVEDBY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.approvedBy</code> attribute. 
	 * @param value the approvedBy
	 */
	public void setAllApprovedBy(final Map<Language,String> value)
	{
		setAllApprovedBy( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		FMCUSTOMERACCOUNTUNITHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.docname</code> attribute.
	 * @return the docname
	 */
	public String getDocname(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMTaxDocument.getDocname requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, DOCNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.docname</code> attribute.
	 * @return the docname
	 */
	public String getDocname()
	{
		return getDocname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @return the localized docname
	 */
	public Map<Language,String> getAllDocname(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,DOCNAME,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @return the localized docname
	 */
	public Map<Language,String> getAllDocname()
	{
		return getAllDocname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @param value the docname
	 */
	public void setDocname(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMTaxDocument.setDocname requires a session language", 0 );
		}
		setLocalizedProperty(ctx, DOCNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @param value the docname
	 */
	public void setDocname(final String value)
	{
		setDocname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @param value the docname
	 */
	public void setAllDocname(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,DOCNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.docname</code> attribute. 
	 * @param value the docname
	 */
	public void setAllDocname(final Map<Language,String> value)
	{
		setAllDocname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.fmCustomerAccountUnit</code> attribute.
	 * @return the fmCustomerAccountUnit
	 */
	public FMCustomerAccount getFmCustomerAccountUnit(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMCUSTOMERACCOUNTUNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.fmCustomerAccountUnit</code> attribute.
	 * @return the fmCustomerAccountUnit
	 */
	public FMCustomerAccount getFmCustomerAccountUnit()
	{
		return getFmCustomerAccountUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.fmCustomerAccountUnit</code> attribute. 
	 * @param value the fmCustomerAccountUnit
	 */
	public void setFmCustomerAccountUnit(final SessionContext ctx, final FMCustomerAccount value)
	{
		FMCUSTOMERACCOUNTUNITHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.fmCustomerAccountUnit</code> attribute. 
	 * @param value the fmCustomerAccountUnit
	 */
	public void setFmCustomerAccountUnit(final FMCustomerAccount value)
	{
		setFmCustomerAccountUnit( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.state</code> attribute.
	 * @return the state
	 */
	public Region getState(final SessionContext ctx)
	{
		return (Region)getProperty( ctx, STATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.state</code> attribute.
	 * @return the state
	 */
	public Region getState()
	{
		return getState( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.state</code> attribute. 
	 * @param value the state
	 */
	public void setState(final SessionContext ctx, final Region value)
	{
		setProperty(ctx, STATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.state</code> attribute. 
	 * @param value the state
	 */
	public void setState(final Region value)
	{
		setState( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.uploadedBy</code> attribute.
	 * @return the uploadedBy
	 */
	public FMCustomer getUploadedBy(final SessionContext ctx)
	{
		return (FMCustomer)getProperty( ctx, UPLOADEDBY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.uploadedBy</code> attribute.
	 * @return the uploadedBy
	 */
	public FMCustomer getUploadedBy()
	{
		return getUploadedBy( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.uploadedBy</code> attribute. 
	 * @param value the uploadedBy
	 */
	public void setUploadedBy(final SessionContext ctx, final FMCustomer value)
	{
		setProperty(ctx, UPLOADEDBY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.uploadedBy</code> attribute. 
	 * @param value the uploadedBy
	 */
	public void setUploadedBy(final FMCustomer value)
	{
		setUploadedBy( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.validate</code> attribute.
	 * @return the validate
	 */
	public EnumerationValue getValidate(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, VALIDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMTaxDocument.validate</code> attribute.
	 * @return the validate
	 */
	public EnumerationValue getValidate()
	{
		return getValidate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.validate</code> attribute. 
	 * @param value the validate
	 */
	public void setValidate(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, VALIDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMTaxDocument.validate</code> attribute. 
	 * @param value the validate
	 */
	public void setValidate(final EnumerationValue value)
	{
		setValidate( getSession().getSessionContext(), value );
	}
	
}
