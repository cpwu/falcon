/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMAdminAddNewUserProcess;
import com.federalmogul.core.jalo.FMB2BAddress;
import com.federalmogul.core.jalo.FMB2BBRegistrationApprovalProcess;
import com.federalmogul.core.jalo.FMB2SBTaxAdminApprovalProcess;
import com.federalmogul.core.jalo.FMB2SBTaxApprovalProcess;
import com.federalmogul.core.jalo.FMCustomer;
import com.federalmogul.core.jalo.FMCustomerAccount;
import com.federalmogul.core.jalo.FMCustomerAdminProcess;
import com.federalmogul.core.jalo.FMCustomerPartnerFunction;
import com.federalmogul.core.jalo.FMCustomerRegistrationProcess;
import com.federalmogul.core.jalo.FMCustomerReviewProcess;
import com.federalmogul.core.jalo.FMFileDownload;
import com.federalmogul.core.jalo.FMLeadGenerationCallBack;
import com.federalmogul.core.jalo.FMTaxDocument;
import com.federalmogul.core.jalo.LoyaltyOrderConfirmationProcess;
import com.federalmogul.core.jalo.MultipleCatalogsSyncCronJob;
import com.federalmogul.core.jalo.PowertoolsSizeVariantProduct;
import com.federalmogul.core.jalo.ReferAFriendEmailProcess;
import com.federalmogul.core.jalo.SalesChannel;
import com.federalmogul.core.jalo.SalesOrganization;
import com.federalmogul.core.jalo.TSCLocation;
import com.federalmogul.core.jalo.UploadOrderProcess;
import com.federalmogul.core.jalo.UploadOrderProcessEmailNotification;
import com.federalmogul.core.jalo.btg.BTGOrganizationTotalSpentInCurrencyLastYearOperand;
import com.federalmogul.core.jalo.btg.BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand;
import com.federalmogul.core.jalo.btg.OrganizationOrderStatistics;
import com.federalmogul.core.jalo.btg.OrganizationOrdersReportingCronJob;
import com.federalmogul.falcon.core.jalo.ApparelSizeVariantProduct;
import com.federalmogul.falcon.core.jalo.ApparelStyleVariantProduct;
import com.federalmogul.falcon.core.jalo.CPL1Customer;
import com.federalmogul.falcon.core.jalo.CSBCustomerGroup;
import com.federalmogul.falcon.core.jalo.CSBPercents3612;
import com.federalmogul.falcon.core.jalo.CategorySalesBenchmarkCustomer;
import com.federalmogul.falcon.core.jalo.CategorySalesBenchmarkPercents;
import com.federalmogul.falcon.core.jalo.DistrubtionCenter;
import com.federalmogul.falcon.core.jalo.FMCorporate;
import com.federalmogul.falcon.core.jalo.FMCsrAccountList;
import com.federalmogul.falcon.core.jalo.FMDCCenter;
import com.federalmogul.falcon.core.jalo.FMDCShipping;
import com.federalmogul.falcon.core.jalo.FMFitment;
import com.federalmogul.falcon.core.jalo.FMOrderTracking;
import com.federalmogul.falcon.core.jalo.FMPart;
import com.federalmogul.falcon.core.jalo.FMPartAlsoFits;
import com.federalmogul.falcon.core.jalo.FMReturnItems;
import com.federalmogul.falcon.core.jalo.FMYearMakeModelVehicleType;
import com.federalmogul.falcon.core.jalo.FMZones;
import com.federalmogul.falcon.core.jalo.PartInterchange;
import com.federalmogul.falcon.core.jalo.UploadOrder;
import com.federalmogul.falcon.core.jalo.UploadOrderEntry;
import com.federalmogul.falcon.core.jalo.UploadOrderHistory;
import com.fmo.wom.jalo.FMIPOOrder;
import com.fmo.wom.jalo.FMIPOOrderEntry;
import de.hybris.platform.b2b.jalo.B2BCustomer;
import de.hybris.platform.b2b.jalo.B2BUnit;
import de.hybris.platform.catalog.jalo.Company;
import de.hybris.platform.cms2.jalo.contents.components.CMSParagraphComponent;
import de.hybris.platform.cms2.jalo.contents.components.SimpleCMSComponent;
import de.hybris.platform.cms2lib.components.FMBannerComponent;
import de.hybris.platform.cms2lib.components.FMBrandCarouselComponent;
import de.hybris.platform.cms2lib.components.FMCMSImageComponent;
import de.hybris.platform.cms2lib.components.FMHomeCarouselComponent;
import de.hybris.platform.cms2lib.components.FMLearningcenterCarouselComponent;
import de.hybris.platform.cms2lib.components.FMParagraphComponent;
import de.hybris.platform.cms2lib.components.FMSupportAboutusComponent;
import de.hybris.platform.cms2lib.components.FMVideoCarouselComponent;
import de.hybris.platform.cms2lib.components.FmExtendedFooterComponent;
import de.hybris.platform.cms2lib.components.FmFooterComponent;
import de.hybris.platform.cms2lib.components.FmNavigationBarComponent;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.link.Link;
import de.hybris.platform.jalo.order.AbstractOrder;
import de.hybris.platform.jalo.order.AbstractOrderEntry;
import de.hybris.platform.jalo.order.delivery.DeliveryMode;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.jalo.security.Principal;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import de.hybris.platform.jalo.user.Customer;
import de.hybris.platform.jalo.user.User;
import de.hybris.platform.returns.jalo.FMReturnOrder;
import de.hybris.platform.storelocator.jalo.PointOfService;
import de.hybris.platform.util.Utilities;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type <code>FmCoreManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFmCoreManager extends Extension
{
	/** Relation ordering override parameter constants for HomeFMSupportAboutusComponent from ((fmcore))*/
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED = "relation.HomeFMSupportAboutusComponent.source.ordered";
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_TGT_ORDERED = "relation.HomeFMSupportAboutusComponent.target.ordered";
	/** Relation disable markmodifed parameter constants for HomeFMSupportAboutusComponent from ((fmcore))*/
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED = "relation.HomeFMSupportAboutusComponent.markmodified";
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put("loyaltyPoints", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.product.Product", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("Brand", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.Customer", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("carrier", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.delivery.DeliveryMode", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("ordercomments", AttributeMode.INITIAL);
		tmp.put("sapordernumber", AttributeMode.INITIAL);
		tmp.put("custponumber", AttributeMode.INITIAL);
		tmp.put("pocustid", AttributeMode.INITIAL);
		tmp.put("fmordertype", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrder", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("brand", AttributeMode.INITIAL);
		tmp.put("shopType", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.storelocator.jalo.PointOfService", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("fmusertype", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.user.User", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("customerType", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.b2b.jalo.B2BCustomer", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("unitType", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.b2b.jalo.B2BUnit", Collections.unmodifiableMap(tmp));
		tmp = new HashMap<String, AttributeMode>();
		tmp.put("saperrormsg", AttributeMode.INITIAL);
		tmp.put("distrubtionCenter", AttributeMode.INITIAL);
		ttmp.put("de.hybris.platform.jalo.order.AbstractOrderEntry", Collections.unmodifiableMap(tmp));
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.Brand</code> attribute.
	 * @return the Brand
	 */
	public List<FMCorporate> getBrand(final SessionContext ctx, final Customer item)
	{
		List<FMCorporate> coll = (List<FMCorporate>)item.getProperty( ctx, FmCoreConstants.Attributes.Customer.BRAND);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Customer.Brand</code> attribute.
	 * @return the Brand
	 */
	public List<FMCorporate> getBrand(final Customer item)
	{
		return getBrand( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.Brand</code> attribute. 
	 * @param value the Brand
	 */
	public void setBrand(final SessionContext ctx, final Customer item, final List<FMCorporate> value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.Customer.BRAND,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Customer.Brand</code> attribute. 
	 * @param value the Brand
	 */
	public void setBrand(final Customer item, final List<FMCorporate> value)
	{
		setBrand( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PointOfService.brand</code> attribute.
	 * @return the brand - Store locator Brand data
	 */
	public String getBrand(final SessionContext ctx, final PointOfService item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.PointOfService.BRAND);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PointOfService.brand</code> attribute.
	 * @return the brand - Store locator Brand data
	 */
	public String getBrand(final PointOfService item)
	{
		return getBrand( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PointOfService.brand</code> attribute. 
	 * @param value the brand - Store locator Brand data
	 */
	public void setBrand(final SessionContext ctx, final PointOfService item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.PointOfService.BRAND,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PointOfService.brand</code> attribute. 
	 * @param value the brand - Store locator Brand data
	 */
	public void setBrand(final PointOfService item, final String value)
	{
		setBrand( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>DeliveryMode.carrier</code> attribute.
	 * @return the carrier
	 */
	public String getCarrier(final SessionContext ctx, final DeliveryMode item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.DeliveryMode.CARRIER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>DeliveryMode.carrier</code> attribute.
	 * @return the carrier
	 */
	public String getCarrier(final DeliveryMode item)
	{
		return getCarrier( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>DeliveryMode.carrier</code> attribute. 
	 * @param value the carrier
	 */
	public void setCarrier(final SessionContext ctx, final DeliveryMode item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.DeliveryMode.CARRIER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>DeliveryMode.carrier</code> attribute. 
	 * @param value the carrier
	 */
	public void setCarrier(final DeliveryMode item, final String value)
	{
		setCarrier( getSession().getSessionContext(), item, value );
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.APPARELSIZEVARIANTPRODUCT );
			return (ApparelSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelSizeVariantProduct createApparelSizeVariantProduct(final Map attributeValues)
	{
		return createApparelSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.APPARELSTYLEVARIANTPRODUCT );
			return (ApparelStyleVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ApparelStyleVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public ApparelStyleVariantProduct createApparelStyleVariantProduct(final Map attributeValues)
	{
		return createApparelStyleVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public BTGOrganizationTotalSpentInCurrencyLastYearOperand createBTGOrganizationTotalSpentInCurrencyLastYearOperand(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.BTGORGANIZATIONTOTALSPENTINCURRENCYLASTYEAROPERAND );
			return (BTGOrganizationTotalSpentInCurrencyLastYearOperand)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BTGOrganizationTotalSpentInCurrencyLastYearOperand : "+e.getMessage(), 0 );
		}
	}
	
	public BTGOrganizationTotalSpentInCurrencyLastYearOperand createBTGOrganizationTotalSpentInCurrencyLastYearOperand(final Map attributeValues)
	{
		return createBTGOrganizationTotalSpentInCurrencyLastYearOperand( getSession().getSessionContext(), attributeValues );
	}
	
	public BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand createBTGOrganizationTotalSpentInCurrencyRelativeDatesOperand(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.BTGORGANIZATIONTOTALSPENTINCURRENCYRELATIVEDATESOPERAND );
			return (BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand : "+e.getMessage(), 0 );
		}
	}
	
	public BTGOrganizationTotalSpentInCurrencyRelativeDatesOperand createBTGOrganizationTotalSpentInCurrencyRelativeDatesOperand(final Map attributeValues)
	{
		return createBTGOrganizationTotalSpentInCurrencyRelativeDatesOperand( getSession().getSessionContext(), attributeValues );
	}
	
	public CategorySalesBenchmarkCustomer createCategorySalesBenchmarkCustomer(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.CATEGORYSALESBENCHMARKCUSTOMER );
			return (CategorySalesBenchmarkCustomer)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating CategorySalesBenchmarkCustomer : "+e.getMessage(), 0 );
		}
	}
	
	public CategorySalesBenchmarkCustomer createCategorySalesBenchmarkCustomer(final Map attributeValues)
	{
		return createCategorySalesBenchmarkCustomer( getSession().getSessionContext(), attributeValues );
	}
	
	public CategorySalesBenchmarkPercents createCategorySalesBenchmarkPercents(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.CATEGORYSALESBENCHMARKPERCENTS );
			return (CategorySalesBenchmarkPercents)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating CategorySalesBenchmarkPercents : "+e.getMessage(), 0 );
		}
	}
	
	public CategorySalesBenchmarkPercents createCategorySalesBenchmarkPercents(final Map attributeValues)
	{
		return createCategorySalesBenchmarkPercents( getSession().getSessionContext(), attributeValues );
	}
	
	public CPL1Customer createCPL1Customer(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.CPL1CUSTOMER );
			return (CPL1Customer)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating CPL1Customer : "+e.getMessage(), 0 );
		}
	}
	
	public CPL1Customer createCPL1Customer(final Map attributeValues)
	{
		return createCPL1Customer( getSession().getSessionContext(), attributeValues );
	}
	
	public CSBCustomerGroup createCSBCustomerGroup(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.CSBCUSTOMERGROUP );
			return (CSBCustomerGroup)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating CSBCustomerGroup : "+e.getMessage(), 0 );
		}
	}
	
	public CSBCustomerGroup createCSBCustomerGroup(final Map attributeValues)
	{
		return createCSBCustomerGroup( getSession().getSessionContext(), attributeValues );
	}
	
	public CSBPercents3612 createCSBPercents3612(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.CSBPERCENTS3612 );
			return (CSBPercents3612)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating CSBPercents3612 : "+e.getMessage(), 0 );
		}
	}
	
	public CSBPercents3612 createCSBPercents3612(final Map attributeValues)
	{
		return createCSBPercents3612( getSession().getSessionContext(), attributeValues );
	}
	
	public FMAdminAddNewUserProcess createFMAdminAddNewUserProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMADMINADDNEWUSERPROCESS );
			return (FMAdminAddNewUserProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMAdminAddNewUserProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMAdminAddNewUserProcess createFMAdminAddNewUserProcess(final Map attributeValues)
	{
		return createFMAdminAddNewUserProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMB2BAddress createFMB2BAddress(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMB2BADDRESS );
			return (FMB2BAddress)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMB2BAddress : "+e.getMessage(), 0 );
		}
	}
	
	public FMB2BAddress createFMB2BAddress(final Map attributeValues)
	{
		return createFMB2BAddress( getSession().getSessionContext(), attributeValues );
	}
	
	public FMB2BBRegistrationApprovalProcess createFMB2BBRegistrationApprovalProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMB2BBREGISTRATIONAPPROVALPROCESS );
			return (FMB2BBRegistrationApprovalProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMB2BBRegistrationApprovalProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMB2BBRegistrationApprovalProcess createFMB2BBRegistrationApprovalProcess(final Map attributeValues)
	{
		return createFMB2BBRegistrationApprovalProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMB2SBTaxAdminApprovalProcess createFMB2SBTaxAdminApprovalProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMB2SBTAXADMINAPPROVALPROCESS );
			return (FMB2SBTaxAdminApprovalProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMB2SBTaxAdminApprovalProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMB2SBTaxAdminApprovalProcess createFMB2SBTaxAdminApprovalProcess(final Map attributeValues)
	{
		return createFMB2SBTaxAdminApprovalProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMB2SBTaxApprovalProcess createFMB2SBTaxApprovalProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMB2SBTAXAPPROVALPROCESS );
			return (FMB2SBTaxApprovalProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMB2SBTaxApprovalProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMB2SBTaxApprovalProcess createFMB2SBTaxApprovalProcess(final Map attributeValues)
	{
		return createFMB2SBTaxApprovalProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMBannerComponent createFMBannerComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMBANNERCOMPONENT );
			return (FMBannerComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMBannerComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMBannerComponent createFMBannerComponent(final Map attributeValues)
	{
		return createFMBannerComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMBrandCarouselComponent createFMBrandCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMBRANDCAROUSELCOMPONENT );
			return (FMBrandCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMBrandCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMBrandCarouselComponent createFMBrandCarouselComponent(final Map attributeValues)
	{
		return createFMBrandCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCMSImageComponent createFMCMSImageComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCMSIMAGECOMPONENT );
			return (FMCMSImageComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCMSImageComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMCMSImageComponent createFMCMSImageComponent(final Map attributeValues)
	{
		return createFMCMSImageComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCorporate createFMCorporate(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCORPORATE );
			return (FMCorporate)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCorporate : "+e.getMessage(), 0 );
		}
	}
	
	public FMCorporate createFMCorporate(final Map attributeValues)
	{
		return createFMCorporate( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCsrAccountList createFMCsrAccountList(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCSRACCOUNTLIST );
			return (FMCsrAccountList)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCsrAccountList : "+e.getMessage(), 0 );
		}
	}
	
	public FMCsrAccountList createFMCsrAccountList(final Map attributeValues)
	{
		return createFMCsrAccountList( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomer createFMCustomer(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMER );
			return (FMCustomer)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomer : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomer createFMCustomer(final Map attributeValues)
	{
		return createFMCustomer( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomerAccount createFMCustomerAccount(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMERACCOUNT );
			return (FMCustomerAccount)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomerAccount : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomerAccount createFMCustomerAccount(final Map attributeValues)
	{
		return createFMCustomerAccount( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomerAdminProcess createFMCustomerAdminProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMERADMINPROCESS );
			return (FMCustomerAdminProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomerAdminProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomerAdminProcess createFMCustomerAdminProcess(final Map attributeValues)
	{
		return createFMCustomerAdminProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomerPartnerFunction createFMCustomerPartnerFunction(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMERPARTNERFUNCTION );
			return (FMCustomerPartnerFunction)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomerPartnerFunction : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomerPartnerFunction createFMCustomerPartnerFunction(final Map attributeValues)
	{
		return createFMCustomerPartnerFunction( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomerRegistrationProcess createFMCustomerRegistrationProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMERREGISTRATIONPROCESS );
			return (FMCustomerRegistrationProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomerRegistrationProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomerRegistrationProcess createFMCustomerRegistrationProcess(final Map attributeValues)
	{
		return createFMCustomerRegistrationProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMCustomerReviewProcess createFMCustomerReviewProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMCUSTOMERREVIEWPROCESS );
			return (FMCustomerReviewProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMCustomerReviewProcess : "+e.getMessage(), 0 );
		}
	}
	
	public FMCustomerReviewProcess createFMCustomerReviewProcess(final Map attributeValues)
	{
		return createFMCustomerReviewProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public FMDCCenter createFMDCCenter(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMDCCENTER );
			return (FMDCCenter)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMDCCenter : "+e.getMessage(), 0 );
		}
	}
	
	public FMDCCenter createFMDCCenter(final Map attributeValues)
	{
		return createFMDCCenter( getSession().getSessionContext(), attributeValues );
	}
	
	public FMDCShipping createFMDCShipping(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMDCSHIPPING );
			return (FMDCShipping)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMDCShipping : "+e.getMessage(), 0 );
		}
	}
	
	public FMDCShipping createFMDCShipping(final Map attributeValues)
	{
		return createFMDCShipping( getSession().getSessionContext(), attributeValues );
	}
	
	public DistrubtionCenter createFMDistrubtionCenter(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMDISTRUBTIONCENTER );
			return (DistrubtionCenter)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMDistrubtionCenter : "+e.getMessage(), 0 );
		}
	}
	
	public DistrubtionCenter createFMDistrubtionCenter(final Map attributeValues)
	{
		return createFMDistrubtionCenter( getSession().getSessionContext(), attributeValues );
	}
	
	public FmExtendedFooterComponent createFmExtendedFooterComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMEXTENDEDFOOTERCOMPONENT );
			return (FmExtendedFooterComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FmExtendedFooterComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FmExtendedFooterComponent createFmExtendedFooterComponent(final Map attributeValues)
	{
		return createFmExtendedFooterComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMFileDownload createFMFileDownload(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMFILEDOWNLOAD );
			return (FMFileDownload)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMFileDownload : "+e.getMessage(), 0 );
		}
	}
	
	public FMFileDownload createFMFileDownload(final Map attributeValues)
	{
		return createFMFileDownload( getSession().getSessionContext(), attributeValues );
	}
	
	public FMFitment createFMFitment(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMFITMENT );
			return (FMFitment)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMFitment : "+e.getMessage(), 0 );
		}
	}
	
	public FMFitment createFMFitment(final Map attributeValues)
	{
		return createFMFitment( getSession().getSessionContext(), attributeValues );
	}
	
	public FmFooterComponent createFmFooterComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMFOOTERCOMPONENT );
			return (FmFooterComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FmFooterComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FmFooterComponent createFmFooterComponent(final Map attributeValues)
	{
		return createFmFooterComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMHomeCarouselComponent createFMHomeCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMHOMECAROUSELCOMPONENT );
			return (FMHomeCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMHomeCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMHomeCarouselComponent createFMHomeCarouselComponent(final Map attributeValues)
	{
		return createFMHomeCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMIPOOrder createFMIPOOrder(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMIPOORDER );
			return (FMIPOOrder)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMIPOOrder : "+e.getMessage(), 0 );
		}
	}
	
	public FMIPOOrder createFMIPOOrder(final Map attributeValues)
	{
		return createFMIPOOrder( getSession().getSessionContext(), attributeValues );
	}
	
	public FMIPOOrderEntry createFMIPOOrderEntry(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMIPOORDERENTRY );
			return (FMIPOOrderEntry)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMIPOOrderEntry : "+e.getMessage(), 0 );
		}
	}
	
	public FMIPOOrderEntry createFMIPOOrderEntry(final Map attributeValues)
	{
		return createFMIPOOrderEntry( getSession().getSessionContext(), attributeValues );
	}
	
	public FMLeadGenerationCallBack createFMLeadGenerationCallBack(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMLEADGENERATIONCALLBACK );
			return (FMLeadGenerationCallBack)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMLeadGenerationCallBack : "+e.getMessage(), 0 );
		}
	}
	
	public FMLeadGenerationCallBack createFMLeadGenerationCallBack(final Map attributeValues)
	{
		return createFMLeadGenerationCallBack( getSession().getSessionContext(), attributeValues );
	}
	
	public FMLearningcenterCarouselComponent createFMLearningcenterCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMLEARNINGCENTERCAROUSELCOMPONENT );
			return (FMLearningcenterCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMLearningcenterCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMLearningcenterCarouselComponent createFMLearningcenterCarouselComponent(final Map attributeValues)
	{
		return createFMLearningcenterCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FmNavigationBarComponent createFmNavigationBarComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMNAVIGATIONBARCOMPONENT );
			return (FmNavigationBarComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FmNavigationBarComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FmNavigationBarComponent createFmNavigationBarComponent(final Map attributeValues)
	{
		return createFmNavigationBarComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMParagraphComponent createFMParagraphComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMPARAGRAPHCOMPONENT );
			return (FMParagraphComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMParagraphComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMParagraphComponent createFMParagraphComponent(final Map attributeValues)
	{
		return createFMParagraphComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMPart createFMPart(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMPART );
			return (FMPart)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMPart : "+e.getMessage(), 0 );
		}
	}
	
	public FMPart createFMPart(final Map attributeValues)
	{
		return createFMPart( getSession().getSessionContext(), attributeValues );
	}
	
	public FMPartAlsoFits createFMPartAlsoFits(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMPARTALSOFITS );
			return (FMPartAlsoFits)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMPartAlsoFits : "+e.getMessage(), 0 );
		}
	}
	
	public FMPartAlsoFits createFMPartAlsoFits(final Map attributeValues)
	{
		return createFMPartAlsoFits( getSession().getSessionContext(), attributeValues );
	}
	
	public FMReturnItems createFMReturnItems(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMRETURNITEMS );
			return (FMReturnItems)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMReturnItems : "+e.getMessage(), 0 );
		}
	}
	
	public FMReturnItems createFMReturnItems(final Map attributeValues)
	{
		return createFMReturnItems( getSession().getSessionContext(), attributeValues );
	}
	
	public FMReturnOrder createFMReturnOrder(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMRETURNORDER );
			return (FMReturnOrder)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMReturnOrder : "+e.getMessage(), 0 );
		}
	}
	
	public FMReturnOrder createFMReturnOrder(final Map attributeValues)
	{
		return createFMReturnOrder( getSession().getSessionContext(), attributeValues );
	}
	
	public FMOrderTracking createFMShipperOrderTracking(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMSHIPPERORDERTRACKING );
			return (FMOrderTracking)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMShipperOrderTracking : "+e.getMessage(), 0 );
		}
	}
	
	public FMOrderTracking createFMShipperOrderTracking(final Map attributeValues)
	{
		return createFMShipperOrderTracking( getSession().getSessionContext(), attributeValues );
	}
	
	public FMSupportAboutusComponent createFMSupportAboutusComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMSUPPORTABOUTUSCOMPONENT );
			return (FMSupportAboutusComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMSupportAboutusComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMSupportAboutusComponent createFMSupportAboutusComponent(final Map attributeValues)
	{
		return createFMSupportAboutusComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMTaxDocument createFMTaxDocument(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMTAXDOCUMENT );
			return (FMTaxDocument)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMTaxDocument : "+e.getMessage(), 0 );
		}
	}
	
	public FMTaxDocument createFMTaxDocument(final Map attributeValues)
	{
		return createFMTaxDocument( getSession().getSessionContext(), attributeValues );
	}
	
	public FMVideoCarouselComponent createFMVideoCarouselComponent(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMVIDEOCAROUSELCOMPONENT );
			return (FMVideoCarouselComponent)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMVideoCarouselComponent : "+e.getMessage(), 0 );
		}
	}
	
	public FMVideoCarouselComponent createFMVideoCarouselComponent(final Map attributeValues)
	{
		return createFMVideoCarouselComponent( getSession().getSessionContext(), attributeValues );
	}
	
	public FMYearMakeModelVehicleType createFMYearMakeModelVehicleType(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMYEARMAKEMODELVEHICLETYPE );
			return (FMYearMakeModelVehicleType)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMYearMakeModelVehicleType : "+e.getMessage(), 0 );
		}
	}
	
	public FMYearMakeModelVehicleType createFMYearMakeModelVehicleType(final Map attributeValues)
	{
		return createFMYearMakeModelVehicleType( getSession().getSessionContext(), attributeValues );
	}
	
	public FMZones createFMZones(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.FMZONES );
			return (FMZones)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMZones : "+e.getMessage(), 0 );
		}
	}
	
	public FMZones createFMZones(final Map attributeValues)
	{
		return createFMZones( getSession().getSessionContext(), attributeValues );
	}
	
	public LoyaltyOrderConfirmationProcess createLoyaltyOrderConfirmationProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.LOYALTYORDERCONFIRMATIONPROCESS );
			return (LoyaltyOrderConfirmationProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating LoyaltyOrderConfirmationProcess : "+e.getMessage(), 0 );
		}
	}
	
	public LoyaltyOrderConfirmationProcess createLoyaltyOrderConfirmationProcess(final Map attributeValues)
	{
		return createLoyaltyOrderConfirmationProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public MultipleCatalogsSyncCronJob createMultipleCatalogsSyncCronJob(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.MULTIPLECATALOGSSYNCCRONJOB );
			return (MultipleCatalogsSyncCronJob)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating MultipleCatalogsSyncCronJob : "+e.getMessage(), 0 );
		}
	}
	
	public MultipleCatalogsSyncCronJob createMultipleCatalogsSyncCronJob(final Map attributeValues)
	{
		return createMultipleCatalogsSyncCronJob( getSession().getSessionContext(), attributeValues );
	}
	
	public OrganizationOrdersReportingCronJob createOrganizationOrdersReportingCronJob(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.ORGANIZATIONORDERSREPORTINGCRONJOB );
			return (OrganizationOrdersReportingCronJob)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating OrganizationOrdersReportingCronJob : "+e.getMessage(), 0 );
		}
	}
	
	public OrganizationOrdersReportingCronJob createOrganizationOrdersReportingCronJob(final Map attributeValues)
	{
		return createOrganizationOrdersReportingCronJob( getSession().getSessionContext(), attributeValues );
	}
	
	public OrganizationOrderStatistics createOrganizationOrderStatistics(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.ORGANIZATIONORDERSTATISTICS );
			return (OrganizationOrderStatistics)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating OrganizationOrderStatistics : "+e.getMessage(), 0 );
		}
	}
	
	public OrganizationOrderStatistics createOrganizationOrderStatistics(final Map attributeValues)
	{
		return createOrganizationOrderStatistics( getSession().getSessionContext(), attributeValues );
	}
	
	public PartInterchange createPartInterchange(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.PARTINTERCHANGE );
			return (PartInterchange)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating PartInterchange : "+e.getMessage(), 0 );
		}
	}
	
	public PartInterchange createPartInterchange(final Map attributeValues)
	{
		return createPartInterchange( getSession().getSessionContext(), attributeValues );
	}
	
	public PowertoolsSizeVariantProduct createPowertoolsSizeVariantProduct(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.POWERTOOLSSIZEVARIANTPRODUCT );
			return (PowertoolsSizeVariantProduct)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating PowertoolsSizeVariantProduct : "+e.getMessage(), 0 );
		}
	}
	
	public PowertoolsSizeVariantProduct createPowertoolsSizeVariantProduct(final Map attributeValues)
	{
		return createPowertoolsSizeVariantProduct( getSession().getSessionContext(), attributeValues );
	}
	
	public ReferAFriendEmailProcess createReferAFriendEmailProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.REFERAFRIENDEMAILPROCESS );
			return (ReferAFriendEmailProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating ReferAFriendEmailProcess : "+e.getMessage(), 0 );
		}
	}
	
	public ReferAFriendEmailProcess createReferAFriendEmailProcess(final Map attributeValues)
	{
		return createReferAFriendEmailProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public SalesChannel createSalesChannel(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.SALESCHANNEL );
			return (SalesChannel)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SalesChannel : "+e.getMessage(), 0 );
		}
	}
	
	public SalesChannel createSalesChannel(final Map attributeValues)
	{
		return createSalesChannel( getSession().getSessionContext(), attributeValues );
	}
	
	public SalesOrganization createSalesOrganization(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.SALESORGANIZATION );
			return (SalesOrganization)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating SalesOrganization : "+e.getMessage(), 0 );
		}
	}
	
	public SalesOrganization createSalesOrganization(final Map attributeValues)
	{
		return createSalesOrganization( getSession().getSessionContext(), attributeValues );
	}
	
	public TSCLocation createTSCLocation(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.TSCLOCATION );
			return (TSCLocation)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating TSCLocation : "+e.getMessage(), 0 );
		}
	}
	
	public TSCLocation createTSCLocation(final Map attributeValues)
	{
		return createTSCLocation( getSession().getSessionContext(), attributeValues );
	}
	
	public UploadOrder createUploadOrder(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.UPLOADORDER );
			return (UploadOrder)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UploadOrder : "+e.getMessage(), 0 );
		}
	}
	
	public UploadOrder createUploadOrder(final Map attributeValues)
	{
		return createUploadOrder( getSession().getSessionContext(), attributeValues );
	}
	
	public UploadOrderEntry createUploadOrderEntry(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.UPLOADORDERENTRY );
			return (UploadOrderEntry)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UploadOrderEntry : "+e.getMessage(), 0 );
		}
	}
	
	public UploadOrderEntry createUploadOrderEntry(final Map attributeValues)
	{
		return createUploadOrderEntry( getSession().getSessionContext(), attributeValues );
	}
	
	public UploadOrderHistory createUploadOrderHistory(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.UPLOADORDERHISTORY );
			return (UploadOrderHistory)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UploadOrderHistory : "+e.getMessage(), 0 );
		}
	}
	
	public UploadOrderHistory createUploadOrderHistory(final Map attributeValues)
	{
		return createUploadOrderHistory( getSession().getSessionContext(), attributeValues );
	}
	
	public UploadOrderProcess createUploadOrderProcess(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.UPLOADORDERPROCESS );
			return (UploadOrderProcess)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UploadOrderProcess : "+e.getMessage(), 0 );
		}
	}
	
	public UploadOrderProcess createUploadOrderProcess(final Map attributeValues)
	{
		return createUploadOrderProcess( getSession().getSessionContext(), attributeValues );
	}
	
	public UploadOrderProcessEmailNotification createUploadOrderProcessEmailNotification(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmCoreConstants.TC.UPLOADORDERPROCESSEMAILNOTIFICATION );
			return (UploadOrderProcessEmailNotification)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating UploadOrderProcessEmailNotification : "+e.getMessage(), 0 );
		}
	}
	
	public UploadOrderProcessEmailNotification createUploadOrderProcessEmailNotification(final Map attributeValues)
	{
		return createUploadOrderProcessEmailNotification( getSession().getSessionContext(), attributeValues );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>B2BCustomer.customerType</code> attribute.
	 * @return the customerType
	 */
	public EnumerationValue getCustomerType(final SessionContext ctx, final B2BCustomer item)
	{
		return (EnumerationValue)item.getProperty( ctx, FmCoreConstants.Attributes.B2BCustomer.CUSTOMERTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>B2BCustomer.customerType</code> attribute.
	 * @return the customerType
	 */
	public EnumerationValue getCustomerType(final B2BCustomer item)
	{
		return getCustomerType( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>B2BCustomer.customerType</code> attribute. 
	 * @param value the customerType
	 */
	public void setCustomerType(final SessionContext ctx, final B2BCustomer item, final EnumerationValue value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.B2BCustomer.CUSTOMERTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>B2BCustomer.customerType</code> attribute. 
	 * @param value the customerType
	 */
	public void setCustomerType(final B2BCustomer item, final EnumerationValue value)
	{
		setCustomerType( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.custponumber</code> attribute.
	 * @return the custponumber - SAP Purchase Order Number
	 */
	public String getCustponumber(final SessionContext ctx, final AbstractOrder item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrder.CUSTPONUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.custponumber</code> attribute.
	 * @return the custponumber - SAP Purchase Order Number
	 */
	public String getCustponumber(final AbstractOrder item)
	{
		return getCustponumber( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.custponumber</code> attribute. 
	 * @param value the custponumber - SAP Purchase Order Number
	 */
	public void setCustponumber(final SessionContext ctx, final AbstractOrder item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrder.CUSTPONUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.custponumber</code> attribute. 
	 * @param value the custponumber - SAP Purchase Order Number
	 */
	public void setCustponumber(final AbstractOrder item, final String value)
	{
		setCustponumber( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.distrubtionCenter</code> attribute.
	 * @return the distrubtionCenter
	 */
	public List<DistrubtionCenter> getDistrubtionCenter(final SessionContext ctx, final AbstractOrderEntry item)
	{
		List<DistrubtionCenter> coll = (List<DistrubtionCenter>)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrderEntry.DISTRUBTIONCENTER);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.distrubtionCenter</code> attribute.
	 * @return the distrubtionCenter
	 */
	public List<DistrubtionCenter> getDistrubtionCenter(final AbstractOrderEntry item)
	{
		return getDistrubtionCenter( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.distrubtionCenter</code> attribute. 
	 * @param value the distrubtionCenter
	 */
	public void setDistrubtionCenter(final SessionContext ctx, final AbstractOrderEntry item, final List<DistrubtionCenter> value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrderEntry.DISTRUBTIONCENTER,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.distrubtionCenter</code> attribute. 
	 * @param value the distrubtionCenter
	 */
	public void setDistrubtionCenter(final AbstractOrderEntry item, final List<DistrubtionCenter> value)
	{
		setDistrubtionCenter( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.fmordertype</code> attribute.
	 * @return the fmordertype - Order Type method
	 */
	public String getFmordertype(final SessionContext ctx, final AbstractOrder item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrder.FMORDERTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.fmordertype</code> attribute.
	 * @return the fmordertype - Order Type method
	 */
	public String getFmordertype(final AbstractOrder item)
	{
		return getFmordertype( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.fmordertype</code> attribute. 
	 * @param value the fmordertype - Order Type method
	 */
	public void setFmordertype(final SessionContext ctx, final AbstractOrder item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrder.FMORDERTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.fmordertype</code> attribute. 
	 * @param value the fmordertype - Order Type method
	 */
	public void setFmordertype(final AbstractOrder item, final String value)
	{
		setFmordertype( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSParagraphComponent.fmSupportAboutusComponent</code> attribute.
	 * @return the fmSupportAboutusComponent
	 */
	public Collection<FMSupportAboutusComponent> getFmSupportAboutusComponent(final SessionContext ctx, final CMSParagraphComponent item)
	{
		final List<FMSupportAboutusComponent> items = item.getLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CMSParagraphComponent.fmSupportAboutusComponent</code> attribute.
	 * @return the fmSupportAboutusComponent
	 */
	public Collection<FMSupportAboutusComponent> getFmSupportAboutusComponent(final CMSParagraphComponent item)
	{
		return getFmSupportAboutusComponent( getSession().getSessionContext(), item );
	}
	
	public long getFmSupportAboutusComponentCount(final SessionContext ctx, final CMSParagraphComponent item)
	{
		return item.getLinkedItemsCount(
			ctx,
			false,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null
		);
	}
	
	public long getFmSupportAboutusComponentCount(final CMSParagraphComponent item)
	{
		return getFmSupportAboutusComponentCount( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSParagraphComponent.fmSupportAboutusComponent</code> attribute. 
	 * @param value the fmSupportAboutusComponent
	 */
	public void setFmSupportAboutusComponent(final SessionContext ctx, final CMSParagraphComponent item, final Collection<FMSupportAboutusComponent> value)
	{
		item.setLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CMSParagraphComponent.fmSupportAboutusComponent</code> attribute. 
	 * @param value the fmSupportAboutusComponent
	 */
	public void setFmSupportAboutusComponent(final CMSParagraphComponent item, final Collection<FMSupportAboutusComponent> value)
	{
		setFmSupportAboutusComponent( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmSupportAboutusComponent. 
	 * @param value the item to add to fmSupportAboutusComponent
	 */
	public void addToFmSupportAboutusComponent(final SessionContext ctx, final CMSParagraphComponent item, final FMSupportAboutusComponent value)
	{
		item.addLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmSupportAboutusComponent. 
	 * @param value the item to add to fmSupportAboutusComponent
	 */
	public void addToFmSupportAboutusComponent(final CMSParagraphComponent item, final FMSupportAboutusComponent value)
	{
		addToFmSupportAboutusComponent( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmSupportAboutusComponent. 
	 * @param value the item to remove from fmSupportAboutusComponent
	 */
	public void removeFromFmSupportAboutusComponent(final SessionContext ctx, final CMSParagraphComponent item, final FMSupportAboutusComponent value)
	{
		item.removeLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmSupportAboutusComponent. 
	 * @param value the item to remove from fmSupportAboutusComponent
	 */
	public void removeFromFmSupportAboutusComponent(final CMSParagraphComponent item, final FMSupportAboutusComponent value)
	{
		removeFromFmSupportAboutusComponent( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>User.fmusertype</code> attribute.
	 * @return the fmusertype
	 */
	public EnumerationValue getFmusertype(final SessionContext ctx, final User item)
	{
		return (EnumerationValue)item.getProperty( ctx, FmCoreConstants.Attributes.User.FMUSERTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>User.fmusertype</code> attribute.
	 * @return the fmusertype
	 */
	public EnumerationValue getFmusertype(final User item)
	{
		return getFmusertype( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>User.fmusertype</code> attribute. 
	 * @param value the fmusertype
	 */
	public void setFmusertype(final SessionContext ctx, final User item, final EnumerationValue value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.User.FMUSERTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>User.fmusertype</code> attribute. 
	 * @param value the fmusertype
	 */
	public void setFmusertype(final User item, final EnumerationValue value)
	{
		setFmusertype( getSession().getSessionContext(), item, value );
	}
	
	@Override
	public String getName()
	{
		return FmCoreConstants.EXTENSIONNAME;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.loyaltyPoints</code> attribute.
	 * @return the loyaltyPoints
	 */
	public String getLoyaltyPoints(final SessionContext ctx, final Product item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.Product.LOYALTYPOINTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>Product.loyaltyPoints</code> attribute.
	 * @return the loyaltyPoints
	 */
	public String getLoyaltyPoints(final Product item)
	{
		return getLoyaltyPoints( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.loyaltyPoints</code> attribute. 
	 * @param value the loyaltyPoints
	 */
	public void setLoyaltyPoints(final SessionContext ctx, final Product item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.Product.LOYALTYPOINTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>Product.loyaltyPoints</code> attribute. 
	 * @param value the loyaltyPoints
	 */
	public void setLoyaltyPoints(final Product item, final String value)
	{
		setLoyaltyPoints( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments(final SessionContext ctx, final AbstractOrder item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrder.ORDERCOMMENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments(final AbstractOrder item)
	{
		return getOrdercomments( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final SessionContext ctx, final AbstractOrder item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrder.ORDERCOMMENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final AbstractOrder item, final String value)
	{
		setOrdercomments( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.pocustid</code> attribute.
	 * @return the pocustid - SAP purchase order comments
	 */
	public String getPocustid(final SessionContext ctx, final AbstractOrder item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrder.POCUSTID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.pocustid</code> attribute.
	 * @return the pocustid - SAP purchase order comments
	 */
	public String getPocustid(final AbstractOrder item)
	{
		return getPocustid( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.pocustid</code> attribute. 
	 * @param value the pocustid - SAP purchase order comments
	 */
	public void setPocustid(final SessionContext ctx, final AbstractOrder item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrder.POCUSTID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.pocustid</code> attribute. 
	 * @param value the pocustid - SAP purchase order comments
	 */
	public void setPocustid(final AbstractOrder item, final String value)
	{
		setPocustid( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.saperrormsg</code> attribute.
	 * @return the saperrormsg
	 */
	public String getSaperrormsg(final SessionContext ctx, final AbstractOrderEntry item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrderEntry.SAPERRORMSG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrderEntry.saperrormsg</code> attribute.
	 * @return the saperrormsg
	 */
	public String getSaperrormsg(final AbstractOrderEntry item)
	{
		return getSaperrormsg( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.saperrormsg</code> attribute. 
	 * @param value the saperrormsg
	 */
	public void setSaperrormsg(final SessionContext ctx, final AbstractOrderEntry item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrderEntry.SAPERRORMSG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrderEntry.saperrormsg</code> attribute. 
	 * @param value the saperrormsg
	 */
	public void setSaperrormsg(final AbstractOrderEntry item, final String value)
	{
		setSaperrormsg( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.sapordernumber</code> attribute.
	 * @return the sapordernumber - SAP Order Number
	 */
	public String getSapordernumber(final SessionContext ctx, final AbstractOrder item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.AbstractOrder.SAPORDERNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>AbstractOrder.sapordernumber</code> attribute.
	 * @return the sapordernumber - SAP Order Number
	 */
	public String getSapordernumber(final AbstractOrder item)
	{
		return getSapordernumber( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.sapordernumber</code> attribute. 
	 * @param value the sapordernumber - SAP Order Number
	 */
	public void setSapordernumber(final SessionContext ctx, final AbstractOrder item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.AbstractOrder.SAPORDERNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>AbstractOrder.sapordernumber</code> attribute. 
	 * @param value the sapordernumber - SAP Order Number
	 */
	public void setSapordernumber(final AbstractOrder item, final String value)
	{
		setSapordernumber( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PointOfService.shopType</code> attribute.
	 * @return the shopType - Shop/Store Type data
	 */
	public String getShopType(final SessionContext ctx, final PointOfService item)
	{
		return (String)item.getProperty( ctx, FmCoreConstants.Attributes.PointOfService.SHOPTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PointOfService.shopType</code> attribute.
	 * @return the shopType - Shop/Store Type data
	 */
	public String getShopType(final PointOfService item)
	{
		return getShopType( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PointOfService.shopType</code> attribute. 
	 * @param value the shopType - Shop/Store Type data
	 */
	public void setShopType(final SessionContext ctx, final PointOfService item, final String value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.PointOfService.SHOPTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PointOfService.shopType</code> attribute. 
	 * @param value the shopType - Shop/Store Type data
	 */
	public void setShopType(final PointOfService item, final String value)
	{
		setShopType( getSession().getSessionContext(), item, value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>B2BUnit.unitType</code> attribute.
	 * @return the unitType
	 */
	public EnumerationValue getUnitType(final SessionContext ctx, final B2BUnit item)
	{
		return (EnumerationValue)item.getProperty( ctx, FmCoreConstants.Attributes.B2BUnit.UNITTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>B2BUnit.unitType</code> attribute.
	 * @return the unitType
	 */
	public EnumerationValue getUnitType(final B2BUnit item)
	{
		return getUnitType( getSession().getSessionContext(), item );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>B2BUnit.unitType</code> attribute. 
	 * @param value the unitType
	 */
	public void setUnitType(final SessionContext ctx, final B2BUnit item, final EnumerationValue value)
	{
		item.setProperty(ctx, FmCoreConstants.Attributes.B2BUnit.UNITTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>B2BUnit.unitType</code> attribute. 
	 * @param value the unitType
	 */
	public void setUnitType(final B2BUnit item, final EnumerationValue value)
	{
		setUnitType( getSession().getSessionContext(), item, value );
	}
	
}
