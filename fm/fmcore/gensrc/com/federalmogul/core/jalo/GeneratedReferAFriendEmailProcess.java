/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomer;
import de.hybris.platform.commerceservices.jalo.process.StoreFrontCustomerProcess;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.commerceservices.jalo.process.StoreFrontCustomerProcess ReferAFriendEmailProcess}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedReferAFriendEmailProcess extends StoreFrontCustomerProcess
{
	/** Qualifier of the <code>ReferAFriendEmailProcess.refereeLastName</code> attribute **/
	public static final String REFEREELASTNAME = "refereeLastName";
	/** Qualifier of the <code>ReferAFriendEmailProcess.refereeEmail</code> attribute **/
	public static final String REFEREEEMAIL = "refereeEmail";
	/** Qualifier of the <code>ReferAFriendEmailProcess.refereeFirstName</code> attribute **/
	public static final String REFEREEFIRSTNAME = "refereeFirstName";
	/** Qualifier of the <code>ReferAFriendEmailProcess.referee</code> attribute **/
	public static final String REFEREE = "referee";
	/** Qualifier of the <code>ReferAFriendEmailProcess.referer</code> attribute **/
	public static final String REFERER = "referer";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(StoreFrontCustomerProcess.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(REFEREELASTNAME, AttributeMode.INITIAL);
		tmp.put(REFEREEEMAIL, AttributeMode.INITIAL);
		tmp.put(REFEREEFIRSTNAME, AttributeMode.INITIAL);
		tmp.put(REFEREE, AttributeMode.INITIAL);
		tmp.put(REFERER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.referee</code> attribute.
	 * @return the referee
	 */
	public FMCustomer getReferee(final SessionContext ctx)
	{
		return (FMCustomer)getProperty( ctx, REFEREE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.referee</code> attribute.
	 * @return the referee
	 */
	public FMCustomer getReferee()
	{
		return getReferee( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.referee</code> attribute. 
	 * @param value the referee
	 */
	public void setReferee(final SessionContext ctx, final FMCustomer value)
	{
		setProperty(ctx, REFEREE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.referee</code> attribute. 
	 * @param value the referee
	 */
	public void setReferee(final FMCustomer value)
	{
		setReferee( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeEmail</code> attribute.
	 * @return the refereeEmail
	 */
	public String getRefereeEmail(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REFEREEEMAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeEmail</code> attribute.
	 * @return the refereeEmail
	 */
	public String getRefereeEmail()
	{
		return getRefereeEmail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeEmail</code> attribute. 
	 * @param value the refereeEmail
	 */
	public void setRefereeEmail(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REFEREEEMAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeEmail</code> attribute. 
	 * @param value the refereeEmail
	 */
	public void setRefereeEmail(final String value)
	{
		setRefereeEmail( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeFirstName</code> attribute.
	 * @return the refereeFirstName
	 */
	public String getRefereeFirstName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REFEREEFIRSTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeFirstName</code> attribute.
	 * @return the refereeFirstName
	 */
	public String getRefereeFirstName()
	{
		return getRefereeFirstName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeFirstName</code> attribute. 
	 * @param value the refereeFirstName
	 */
	public void setRefereeFirstName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REFEREEFIRSTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeFirstName</code> attribute. 
	 * @param value the refereeFirstName
	 */
	public void setRefereeFirstName(final String value)
	{
		setRefereeFirstName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeLastName</code> attribute.
	 * @return the refereeLastName
	 */
	public String getRefereeLastName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REFEREELASTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.refereeLastName</code> attribute.
	 * @return the refereeLastName
	 */
	public String getRefereeLastName()
	{
		return getRefereeLastName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeLastName</code> attribute. 
	 * @param value the refereeLastName
	 */
	public void setRefereeLastName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REFEREELASTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.refereeLastName</code> attribute. 
	 * @param value the refereeLastName
	 */
	public void setRefereeLastName(final String value)
	{
		setRefereeLastName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.referer</code> attribute.
	 * @return the referer
	 */
	public FMCustomer getReferer(final SessionContext ctx)
	{
		return (FMCustomer)getProperty( ctx, REFERER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>ReferAFriendEmailProcess.referer</code> attribute.
	 * @return the referer
	 */
	public FMCustomer getReferer()
	{
		return getReferer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.referer</code> attribute. 
	 * @param value the referer
	 */
	public void setReferer(final SessionContext ctx, final FMCustomer value)
	{
		setProperty(ctx, REFERER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>ReferAFriendEmailProcess.referer</code> attribute. 
	 * @param value the referer
	 */
	public void setReferer(final FMCustomer value)
	{
		setReferer( getSession().getSessionContext(), value );
	}
	
}
