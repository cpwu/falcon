/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem SalesChannel}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedSalesChannel extends GenericItem
{
	/** Qualifier of the <code>SalesChannel.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>SalesChannel.name</code> attribute **/
	public static final String NAME = "name";
	/** Qualifier of the <code>SalesChannel.fmCustAccSalesChannel</code> attribute **/
	public static final String FMCUSTACCSALESCHANNEL = "fmCustAccSalesChannel";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n FMCUSTACCSALESCHANNEL's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSalesChannel> FMCUSTACCSALESCHANNELHANDLER = new BidirectionalOneToManyHandler<GeneratedSalesChannel>(
	FmCoreConstants.TC.SALESCHANNEL,
	false,
	"fmCustAccSalesChannel",
	null,
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(NAME, AttributeMode.INITIAL);
		tmp.put(FMCUSTACCSALESCHANNEL, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.code</code> attribute.
	 * @return the code - Sales channel code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.code</code> attribute.
	 * @return the code - Sales channel code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.code</code> attribute. 
	 * @param value the code - Sales channel code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.code</code> attribute. 
	 * @param value the code - Sales channel code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		FMCUSTACCSALESCHANNELHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.fmCustAccSalesChannel</code> attribute.
	 * @return the fmCustAccSalesChannel
	 */
	public FMCustomerAccount getFmCustAccSalesChannel(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMCUSTACCSALESCHANNEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.fmCustAccSalesChannel</code> attribute.
	 * @return the fmCustAccSalesChannel
	 */
	public FMCustomerAccount getFmCustAccSalesChannel()
	{
		return getFmCustAccSalesChannel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.fmCustAccSalesChannel</code> attribute. 
	 * @param value the fmCustAccSalesChannel
	 */
	public void setFmCustAccSalesChannel(final SessionContext ctx, final FMCustomerAccount value)
	{
		FMCUSTACCSALESCHANNELHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.fmCustAccSalesChannel</code> attribute. 
	 * @param value the fmCustAccSalesChannel
	 */
	public void setFmCustAccSalesChannel(final FMCustomerAccount value)
	{
		setFmCustAccSalesChannel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.name</code> attribute.
	 * @return the name - Sales channel description
	 */
	public String getName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesChannel.name</code> attribute.
	 * @return the name - Sales channel description
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.name</code> attribute. 
	 * @param value the name - Sales channel description
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesChannel.name</code> attribute. 
	 * @param value the name - Sales channel description
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
}
