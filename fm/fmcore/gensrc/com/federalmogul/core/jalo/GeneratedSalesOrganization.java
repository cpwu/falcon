/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.jalo.GenericItem SalesOrganization}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedSalesOrganization extends GenericItem
{
	/** Qualifier of the <code>SalesOrganization.soldToAccount</code> attribute **/
	public static final String SOLDTOACCOUNT = "soldToAccount";
	/** Qualifier of the <code>SalesOrganization.salesChannelCode</code> attribute **/
	public static final String SALESCHANNELCODE = "salesChannelCode";
	/** Qualifier of the <code>SalesOrganization.fmCustAccount</code> attribute **/
	public static final String FMCUSTACCOUNT = "fmCustAccount";
	/** Qualifier of the <code>SalesOrganization.salesDivisionCode</code> attribute **/
	public static final String SALESDIVISIONCODE = "salesDivisionCode";
	/** Qualifier of the <code>SalesOrganization.salesOrgCode</code> attribute **/
	public static final String SALESORGCODE = "salesOrgCode";
	/** Qualifier of the <code>SalesOrganization.salesOrgsId</code> attribute **/
	public static final String SALESORGSID = "salesOrgsId";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n FMCUSTACCOUNT's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedSalesOrganization> FMCUSTACCOUNTHANDLER = new BidirectionalOneToManyHandler<GeneratedSalesOrganization>(
	FmCoreConstants.TC.SALESORGANIZATION,
	false,
	"fmCustAccount",
	null,
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(SOLDTOACCOUNT, AttributeMode.INITIAL);
		tmp.put(SALESCHANNELCODE, AttributeMode.INITIAL);
		tmp.put(FMCUSTACCOUNT, AttributeMode.INITIAL);
		tmp.put(SALESDIVISIONCODE, AttributeMode.INITIAL);
		tmp.put(SALESORGCODE, AttributeMode.INITIAL);
		tmp.put(SALESORGSID, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		FMCUSTACCOUNTHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.fmCustAccount</code> attribute.
	 * @return the fmCustAccount
	 */
	public FMCustomerAccount getFmCustAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, FMCUSTACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.fmCustAccount</code> attribute.
	 * @return the fmCustAccount
	 */
	public FMCustomerAccount getFmCustAccount()
	{
		return getFmCustAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.fmCustAccount</code> attribute. 
	 * @param value the fmCustAccount
	 */
	public void setFmCustAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		FMCUSTACCOUNTHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.fmCustAccount</code> attribute. 
	 * @param value the fmCustAccount
	 */
	public void setFmCustAccount(final FMCustomerAccount value)
	{
		setFmCustAccount( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesChannelCode</code> attribute.
	 * @return the salesChannelCode - Sales channel code
	 */
	public String getSalesChannelCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESCHANNELCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesChannelCode</code> attribute.
	 * @return the salesChannelCode - Sales channel code
	 */
	public String getSalesChannelCode()
	{
		return getSalesChannelCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesChannelCode</code> attribute. 
	 * @param value the salesChannelCode - Sales channel code
	 */
	public void setSalesChannelCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESCHANNELCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesChannelCode</code> attribute. 
	 * @param value the salesChannelCode - Sales channel code
	 */
	public void setSalesChannelCode(final String value)
	{
		setSalesChannelCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesDivisionCode</code> attribute.
	 * @return the salesDivisionCode - Sales division code
	 */
	public String getSalesDivisionCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESDIVISIONCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesDivisionCode</code> attribute.
	 * @return the salesDivisionCode - Sales division code
	 */
	public String getSalesDivisionCode()
	{
		return getSalesDivisionCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesDivisionCode</code> attribute. 
	 * @param value the salesDivisionCode - Sales division code
	 */
	public void setSalesDivisionCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESDIVISIONCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesDivisionCode</code> attribute. 
	 * @param value the salesDivisionCode - Sales division code
	 */
	public void setSalesDivisionCode(final String value)
	{
		setSalesDivisionCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesOrgCode</code> attribute.
	 * @return the salesOrgCode - Sales organization code
	 */
	public String getSalesOrgCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESORGCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesOrgCode</code> attribute.
	 * @return the salesOrgCode - Sales organization code
	 */
	public String getSalesOrgCode()
	{
		return getSalesOrgCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesOrgCode</code> attribute. 
	 * @param value the salesOrgCode - Sales organization code
	 */
	public void setSalesOrgCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESORGCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesOrgCode</code> attribute. 
	 * @param value the salesOrgCode - Sales organization code
	 */
	public void setSalesOrgCode(final String value)
	{
		setSalesOrgCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesOrgsId</code> attribute.
	 * @return the salesOrgsId - Unique id for sales organization
	 */
	public String getSalesOrgsId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESORGSID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.salesOrgsId</code> attribute.
	 * @return the salesOrgsId - Unique id for sales organization
	 */
	public String getSalesOrgsId()
	{
		return getSalesOrgsId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesOrgsId</code> attribute. 
	 * @param value the salesOrgsId - Unique id for sales organization
	 */
	public void setSalesOrgsId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESORGSID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.salesOrgsId</code> attribute. 
	 * @param value the salesOrgsId - Unique id for sales organization
	 */
	public void setSalesOrgsId(final String value)
	{
		setSalesOrgsId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.soldToAccount</code> attribute.
	 * @return the soldToAccount - Customer Sold to account number
	 */
	public FMCustomerAccount getSoldToAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, SOLDTOACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>SalesOrganization.soldToAccount</code> attribute.
	 * @return the soldToAccount - Customer Sold to account number
	 */
	public FMCustomerAccount getSoldToAccount()
	{
		return getSoldToAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.soldToAccount</code> attribute. 
	 * @param value the soldToAccount - Customer Sold to account number
	 */
	public void setSoldToAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, SOLDTOACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>SalesOrganization.soldToAccount</code> attribute. 
	 * @param value the soldToAccount - Customer Sold to account number
	 */
	public void setSoldToAccount(final FMCustomerAccount value)
	{
		setSoldToAccount( getSession().getSessionContext(), value );
	}
	
}
