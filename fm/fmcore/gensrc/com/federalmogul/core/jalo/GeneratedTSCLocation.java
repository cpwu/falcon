/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.core.jalo.TSCLocation TSCLocation}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedTSCLocation extends GenericItem
{
	/** Qualifier of the <code>TSCLocation.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>TSCLocation.distance</code> attribute **/
	public static final String DISTANCE = "distance";
	/** Qualifier of the <code>TSCLocation.longitude</code> attribute **/
	public static final String LONGITUDE = "longitude";
	/** Qualifier of the <code>TSCLocation.addressLine1</code> attribute **/
	public static final String ADDRESSLINE1 = "addressLine1";
	/** Qualifier of the <code>TSCLocation.country</code> attribute **/
	public static final String COUNTRY = "country";
	/** Qualifier of the <code>TSCLocation.town</code> attribute **/
	public static final String TOWN = "town";
	/** Qualifier of the <code>TSCLocation.addressLine2</code> attribute **/
	public static final String ADDRESSLINE2 = "addressLine2";
	/** Qualifier of the <code>TSCLocation.TSCLocId</code> attribute **/
	public static final String TSCLOCID = "TSCLocId";
	/** Qualifier of the <code>TSCLocation.state</code> attribute **/
	public static final String STATE = "state";
	/** Qualifier of the <code>TSCLocation.latitude</code> attribute **/
	public static final String LATITUDE = "latitude";
	/** Qualifier of the <code>TSCLocation.zipcode</code> attribute **/
	public static final String ZIPCODE = "zipcode";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(DISTANCE, AttributeMode.INITIAL);
		tmp.put(LONGITUDE, AttributeMode.INITIAL);
		tmp.put(ADDRESSLINE1, AttributeMode.INITIAL);
		tmp.put(COUNTRY, AttributeMode.INITIAL);
		tmp.put(TOWN, AttributeMode.INITIAL);
		tmp.put(ADDRESSLINE2, AttributeMode.INITIAL);
		tmp.put(TSCLOCID, AttributeMode.INITIAL);
		tmp.put(STATE, AttributeMode.INITIAL);
		tmp.put(LATITUDE, AttributeMode.INITIAL);
		tmp.put(ZIPCODE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.addressLine1</code> attribute.
	 * @return the addressLine1 - Address Line 1
	 */
	public String getAddressLine1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ADDRESSLINE1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.addressLine1</code> attribute.
	 * @return the addressLine1 - Address Line 1
	 */
	public String getAddressLine1()
	{
		return getAddressLine1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.addressLine1</code> attribute. 
	 * @param value the addressLine1 - Address Line 1
	 */
	public void setAddressLine1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ADDRESSLINE1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.addressLine1</code> attribute. 
	 * @param value the addressLine1 - Address Line 1
	 */
	public void setAddressLine1(final String value)
	{
		setAddressLine1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.addressLine2</code> attribute.
	 * @return the addressLine2 - Address Line 2
	 */
	public String getAddressLine2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ADDRESSLINE2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.addressLine2</code> attribute.
	 * @return the addressLine2 - Address Line 2
	 */
	public String getAddressLine2()
	{
		return getAddressLine2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.addressLine2</code> attribute. 
	 * @param value the addressLine2 - Address Line 2
	 */
	public void setAddressLine2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ADDRESSLINE2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.addressLine2</code> attribute. 
	 * @param value the addressLine2 - Address Line 2
	 */
	public void setAddressLine2(final String value)
	{
		setAddressLine2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.code</code> attribute.
	 * @return the code - code for TSC
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.code</code> attribute.
	 * @return the code - code for TSC
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.code</code> attribute. 
	 * @param value the code - code for TSC
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.code</code> attribute. 
	 * @param value the code - code for TSC
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.country</code> attribute.
	 * @return the country - Country
	 */
	public String getCountry(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COUNTRY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.country</code> attribute.
	 * @return the country - Country
	 */
	public String getCountry()
	{
		return getCountry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.country</code> attribute. 
	 * @param value the country - Country
	 */
	public void setCountry(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COUNTRY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.country</code> attribute. 
	 * @param value the country - Country
	 */
	public void setCountry(final String value)
	{
		setCountry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.distance</code> attribute.
	 * @return the distance - Distance of the Store
	 */
	public Double getDistance(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, DISTANCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.distance</code> attribute.
	 * @return the distance - Distance of the Store
	 */
	public Double getDistance()
	{
		return getDistance( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.distance</code> attribute. 
	 * @return the distance - Distance of the Store
	 */
	public double getDistanceAsPrimitive(final SessionContext ctx)
	{
		Double value = getDistance( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.distance</code> attribute. 
	 * @return the distance - Distance of the Store
	 */
	public double getDistanceAsPrimitive()
	{
		return getDistanceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.distance</code> attribute. 
	 * @param value the distance - Distance of the Store
	 */
	public void setDistance(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, DISTANCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.distance</code> attribute. 
	 * @param value the distance - Distance of the Store
	 */
	public void setDistance(final Double value)
	{
		setDistance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.distance</code> attribute. 
	 * @param value the distance - Distance of the Store
	 */
	public void setDistance(final SessionContext ctx, final double value)
	{
		setDistance( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.distance</code> attribute. 
	 * @param value the distance - Distance of the Store
	 */
	public void setDistance(final double value)
	{
		setDistance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.latitude</code> attribute.
	 * @return the latitude - Latitude of the Store
	 */
	public Double getLatitude(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, LATITUDE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.latitude</code> attribute.
	 * @return the latitude - Latitude of the Store
	 */
	public Double getLatitude()
	{
		return getLatitude( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.latitude</code> attribute. 
	 * @return the latitude - Latitude of the Store
	 */
	public double getLatitudeAsPrimitive(final SessionContext ctx)
	{
		Double value = getLatitude( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.latitude</code> attribute. 
	 * @return the latitude - Latitude of the Store
	 */
	public double getLatitudeAsPrimitive()
	{
		return getLatitudeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.latitude</code> attribute. 
	 * @param value the latitude - Latitude of the Store
	 */
	public void setLatitude(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, LATITUDE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.latitude</code> attribute. 
	 * @param value the latitude - Latitude of the Store
	 */
	public void setLatitude(final Double value)
	{
		setLatitude( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.latitude</code> attribute. 
	 * @param value the latitude - Latitude of the Store
	 */
	public void setLatitude(final SessionContext ctx, final double value)
	{
		setLatitude( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.latitude</code> attribute. 
	 * @param value the latitude - Latitude of the Store
	 */
	public void setLatitude(final double value)
	{
		setLatitude( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.longitude</code> attribute.
	 * @return the longitude - Longitude of the Store
	 */
	public Double getLongitude(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, LONGITUDE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.longitude</code> attribute.
	 * @return the longitude - Longitude of the Store
	 */
	public Double getLongitude()
	{
		return getLongitude( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.longitude</code> attribute. 
	 * @return the longitude - Longitude of the Store
	 */
	public double getLongitudeAsPrimitive(final SessionContext ctx)
	{
		Double value = getLongitude( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.longitude</code> attribute. 
	 * @return the longitude - Longitude of the Store
	 */
	public double getLongitudeAsPrimitive()
	{
		return getLongitudeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.longitude</code> attribute. 
	 * @param value the longitude - Longitude of the Store
	 */
	public void setLongitude(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, LONGITUDE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.longitude</code> attribute. 
	 * @param value the longitude - Longitude of the Store
	 */
	public void setLongitude(final Double value)
	{
		setLongitude( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.longitude</code> attribute. 
	 * @param value the longitude - Longitude of the Store
	 */
	public void setLongitude(final SessionContext ctx, final double value)
	{
		setLongitude( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.longitude</code> attribute. 
	 * @param value the longitude - Longitude of the Store
	 */
	public void setLongitude(final double value)
	{
		setLongitude( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.state</code> attribute.
	 * @return the state - State
	 */
	public String getState(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.state</code> attribute.
	 * @return the state - State
	 */
	public String getState()
	{
		return getState( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.state</code> attribute. 
	 * @param value the state - State
	 */
	public void setState(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.state</code> attribute. 
	 * @param value the state - State
	 */
	public void setState(final String value)
	{
		setState( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.town</code> attribute.
	 * @return the town - Zipcode
	 */
	public String getTown(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TOWN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.town</code> attribute.
	 * @return the town - Zipcode
	 */
	public String getTown()
	{
		return getTown( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.town</code> attribute. 
	 * @param value the town - Zipcode
	 */
	public void setTown(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TOWN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.town</code> attribute. 
	 * @param value the town - Zipcode
	 */
	public void setTown(final String value)
	{
		setTown( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.TSCLocId</code> attribute.
	 * @return the TSCLocId - Unique id for TSC Location
	 */
	public String getTSCLocId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TSCLOCID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.TSCLocId</code> attribute.
	 * @return the TSCLocId - Unique id for TSC Location
	 */
	public String getTSCLocId()
	{
		return getTSCLocId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.TSCLocId</code> attribute. 
	 * @param value the TSCLocId - Unique id for TSC Location
	 */
	public void setTSCLocId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TSCLOCID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.TSCLocId</code> attribute. 
	 * @param value the TSCLocId - Unique id for TSC Location
	 */
	public void setTSCLocId(final String value)
	{
		setTSCLocId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.zipcode</code> attribute.
	 * @return the zipcode - Zipcode
	 */
	public String getZipcode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ZIPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>TSCLocation.zipcode</code> attribute.
	 * @return the zipcode - Zipcode
	 */
	public String getZipcode()
	{
		return getZipcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.zipcode</code> attribute. 
	 * @param value the zipcode - Zipcode
	 */
	public void setZipcode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ZIPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>TSCLocation.zipcode</code> attribute. 
	 * @param value the zipcode - Zipcode
	 */
	public void setZipcode(final String value)
	{
		setZipcode( getSession().getSessionContext(), value );
	}
	
}
