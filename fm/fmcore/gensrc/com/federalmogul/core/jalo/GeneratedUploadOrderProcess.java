/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.falcon.core.jalo.UploadOrder;
import de.hybris.platform.commerceservices.jalo.process.StoreFrontProcess;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.commerceservices.jalo.process.StoreFrontProcess UploadOrderProcess}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUploadOrderProcess extends StoreFrontProcess
{
	/** Qualifier of the <code>UploadOrderProcess.order</code> attribute **/
	public static final String ORDER = "order";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(StoreFrontProcess.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(ORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderProcess.order</code> attribute.
	 * @return the order - Upload order Model
	 */
	public UploadOrder getOrder(final SessionContext ctx)
	{
		return (UploadOrder)getProperty( ctx, ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderProcess.order</code> attribute.
	 * @return the order - Upload order Model
	 */
	public UploadOrder getOrder()
	{
		return getOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderProcess.order</code> attribute. 
	 * @param value the order - Upload order Model
	 */
	public void setOrder(final SessionContext ctx, final UploadOrder value)
	{
		setProperty(ctx, ORDER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderProcess.order</code> attribute. 
	 * @param value the order - Upload order Model
	 */
	public void setOrder(final UploadOrder value)
	{
		setOrder( getSession().getSessionContext(), value );
	}
	
}
