/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.CPL1Customer CPL1Customer}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCPL1Customer extends GenericItem
{
	/** Qualifier of the <code>CPL1Customer.cpl1SoldToAccount</code> attribute **/
	public static final String CPL1SOLDTOACCOUNT = "cpl1SoldToAccount";
	/** Qualifier of the <code>CPL1Customer.cpl1ProductDescription</code> attribute **/
	public static final String CPL1PRODUCTDESCRIPTION = "cpl1ProductDescription";
	/** Qualifier of the <code>CPL1Customer.cpl1ProductCode</code> attribute **/
	public static final String CPL1PRODUCTCODE = "cpl1ProductCode";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CPL1SOLDTOACCOUNT, AttributeMode.INITIAL);
		tmp.put(CPL1PRODUCTDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(CPL1PRODUCTCODE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1ProductCode</code> attribute.
	 * @return the cpl1ProductCode - CPL1 Product Code
	 */
	public String getCpl1ProductCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1PRODUCTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1ProductCode</code> attribute.
	 * @return the cpl1ProductCode - CPL1 Product Code
	 */
	public String getCpl1ProductCode()
	{
		return getCpl1ProductCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1ProductCode</code> attribute. 
	 * @param value the cpl1ProductCode - CPL1 Product Code
	 */
	public void setCpl1ProductCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1PRODUCTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1ProductCode</code> attribute. 
	 * @param value the cpl1ProductCode - CPL1 Product Code
	 */
	public void setCpl1ProductCode(final String value)
	{
		setCpl1ProductCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1ProductDescription</code> attribute.
	 * @return the cpl1ProductDescription - CPL1 Product Description
	 */
	public String getCpl1ProductDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1PRODUCTDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1ProductDescription</code> attribute.
	 * @return the cpl1ProductDescription - CPL1 Product Description
	 */
	public String getCpl1ProductDescription()
	{
		return getCpl1ProductDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1ProductDescription</code> attribute. 
	 * @param value the cpl1ProductDescription - CPL1 Product Description
	 */
	public void setCpl1ProductDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1PRODUCTDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1ProductDescription</code> attribute. 
	 * @param value the cpl1ProductDescription - CPL1 Product Description
	 */
	public void setCpl1ProductDescription(final String value)
	{
		setCpl1ProductDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1SoldToAccount</code> attribute.
	 * @return the cpl1SoldToAccount - CPL1 Sold To Account
	 */
	public String getCpl1SoldToAccount(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1SOLDTOACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CPL1Customer.cpl1SoldToAccount</code> attribute.
	 * @return the cpl1SoldToAccount - CPL1 Sold To Account
	 */
	public String getCpl1SoldToAccount()
	{
		return getCpl1SoldToAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1SoldToAccount</code> attribute. 
	 * @param value the cpl1SoldToAccount - CPL1 Sold To Account
	 */
	public void setCpl1SoldToAccount(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1SOLDTOACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CPL1Customer.cpl1SoldToAccount</code> attribute. 
	 * @param value the cpl1SoldToAccount - CPL1 Sold To Account
	 */
	public void setCpl1SoldToAccount(final String value)
	{
		setCpl1SoldToAccount( getSession().getSessionContext(), value );
	}
	
}
