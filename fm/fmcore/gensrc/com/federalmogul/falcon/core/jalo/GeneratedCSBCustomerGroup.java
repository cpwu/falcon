/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.CSBCustomerGroup CSBCustomerGroup}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCSBCustomerGroup extends GenericItem
{
	/** Qualifier of the <code>CSBCustomerGroup.csbCustomerGroupText</code> attribute **/
	public static final String CSBCUSTOMERGROUPTEXT = "csbCustomerGroupText";
	/** Qualifier of the <code>CSBCustomerGroup.posUnits</code> attribute **/
	public static final String POSUNITS = "posUnits";
	/** Qualifier of the <code>CSBCustomerGroup.csbCustomerGroupCode</code> attribute **/
	public static final String CSBCUSTOMERGROUPCODE = "csbCustomerGroupCode";
	/** Qualifier of the <code>CSBCustomerGroup.posDollars</code> attribute **/
	public static final String POSDOLLARS = "posDollars";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CSBCUSTOMERGROUPTEXT, AttributeMode.INITIAL);
		tmp.put(POSUNITS, AttributeMode.INITIAL);
		tmp.put(CSBCUSTOMERGROUPCODE, AttributeMode.INITIAL);
		tmp.put(POSDOLLARS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.csbCustomerGroupCode</code> attribute.
	 * @return the csbCustomerGroupCode - CSB Customer Group Code
	 */
	public String getCsbCustomerGroupCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBCUSTOMERGROUPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.csbCustomerGroupCode</code> attribute.
	 * @return the csbCustomerGroupCode - CSB Customer Group Code
	 */
	public String getCsbCustomerGroupCode()
	{
		return getCsbCustomerGroupCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.csbCustomerGroupCode</code> attribute. 
	 * @param value the csbCustomerGroupCode - CSB Customer Group Code
	 */
	public void setCsbCustomerGroupCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBCUSTOMERGROUPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.csbCustomerGroupCode</code> attribute. 
	 * @param value the csbCustomerGroupCode - CSB Customer Group Code
	 */
	public void setCsbCustomerGroupCode(final String value)
	{
		setCsbCustomerGroupCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.csbCustomerGroupText</code> attribute.
	 * @return the csbCustomerGroupText - CSB Customer Group Text
	 */
	public String getCsbCustomerGroupText(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBCUSTOMERGROUPTEXT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.csbCustomerGroupText</code> attribute.
	 * @return the csbCustomerGroupText - CSB Customer Group Text
	 */
	public String getCsbCustomerGroupText()
	{
		return getCsbCustomerGroupText( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.csbCustomerGroupText</code> attribute. 
	 * @param value the csbCustomerGroupText - CSB Customer Group Text
	 */
	public void setCsbCustomerGroupText(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBCUSTOMERGROUPTEXT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.csbCustomerGroupText</code> attribute. 
	 * @param value the csbCustomerGroupText - CSB Customer Group Text
	 */
	public void setCsbCustomerGroupText(final String value)
	{
		setCsbCustomerGroupText( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posDollars</code> attribute.
	 * @return the posDollars - CSB customergroup Dollars
	 */
	public Boolean isPosDollars(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, POSDOLLARS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posDollars</code> attribute.
	 * @return the posDollars - CSB customergroup Dollars
	 */
	public Boolean isPosDollars()
	{
		return isPosDollars( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @return the posDollars - CSB customergroup Dollars
	 */
	public boolean isPosDollarsAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isPosDollars( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @return the posDollars - CSB customergroup Dollars
	 */
	public boolean isPosDollarsAsPrimitive()
	{
		return isPosDollarsAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @param value the posDollars - CSB customergroup Dollars
	 */
	public void setPosDollars(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, POSDOLLARS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @param value the posDollars - CSB customergroup Dollars
	 */
	public void setPosDollars(final Boolean value)
	{
		setPosDollars( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @param value the posDollars - CSB customergroup Dollars
	 */
	public void setPosDollars(final SessionContext ctx, final boolean value)
	{
		setPosDollars( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posDollars</code> attribute. 
	 * @param value the posDollars - CSB customergroup Dollars
	 */
	public void setPosDollars(final boolean value)
	{
		setPosDollars( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posUnits</code> attribute.
	 * @return the posUnits - CSB customergroup Units
	 */
	public Boolean isPosUnits(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, POSUNITS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posUnits</code> attribute.
	 * @return the posUnits - CSB customergroup Units
	 */
	public Boolean isPosUnits()
	{
		return isPosUnits( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @return the posUnits - CSB customergroup Units
	 */
	public boolean isPosUnitsAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isPosUnits( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @return the posUnits - CSB customergroup Units
	 */
	public boolean isPosUnitsAsPrimitive()
	{
		return isPosUnitsAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @param value the posUnits - CSB customergroup Units
	 */
	public void setPosUnits(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, POSUNITS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @param value the posUnits - CSB customergroup Units
	 */
	public void setPosUnits(final Boolean value)
	{
		setPosUnits( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @param value the posUnits - CSB customergroup Units
	 */
	public void setPosUnits(final SessionContext ctx, final boolean value)
	{
		setPosUnits( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBCustomerGroup.posUnits</code> attribute. 
	 * @param value the posUnits - CSB customergroup Units
	 */
	public void setPosUnits(final boolean value)
	{
		setPosUnits( getSession().getSessionContext(), value );
	}
	
}
