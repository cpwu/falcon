/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.CSBPercents3612 CSBPercents3612}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCSBPercents3612 extends GenericItem
{
	/** Qualifier of the <code>CSBPercents3612.cpl1CustomerCode</code> attribute **/
	public static final String CPL1CUSTOMERCODE = "cpl1CustomerCode";
	/** Qualifier of the <code>CSBPercents3612.salechange_3months</code> attribute **/
	public static final String SALECHANGE_3MONTHS = "salechange_3months";
	/** Qualifier of the <code>CSBPercents3612.unitchange_6months</code> attribute **/
	public static final String UNITCHANGE_6MONTHS = "unitchange_6months";
	/** Qualifier of the <code>CSBPercents3612.unitchange_3months</code> attribute **/
	public static final String UNITCHANGE_3MONTHS = "unitchange_3months";
	/** Qualifier of the <code>CSBPercents3612.salechange_6months</code> attribute **/
	public static final String SALECHANGE_6MONTHS = "salechange_6months";
	/** Qualifier of the <code>CSBPercents3612.unitchange_12months</code> attribute **/
	public static final String UNITCHANGE_12MONTHS = "unitchange_12months";
	/** Qualifier of the <code>CSBPercents3612.salechange_12months</code> attribute **/
	public static final String SALECHANGE_12MONTHS = "salechange_12months";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CPL1CUSTOMERCODE, AttributeMode.INITIAL);
		tmp.put(SALECHANGE_3MONTHS, AttributeMode.INITIAL);
		tmp.put(UNITCHANGE_6MONTHS, AttributeMode.INITIAL);
		tmp.put(UNITCHANGE_3MONTHS, AttributeMode.INITIAL);
		tmp.put(SALECHANGE_6MONTHS, AttributeMode.INITIAL);
		tmp.put(UNITCHANGE_12MONTHS, AttributeMode.INITIAL);
		tmp.put(SALECHANGE_12MONTHS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1CUSTOMERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode()
	{
		return getCpl1CustomerCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1CUSTOMERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final String value)
	{
		setCpl1CustomerCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_12months</code> attribute.
	 * @return the salechange_12months - 12 months Sale change
	 */
	public String getSalechange_12months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALECHANGE_12MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_12months</code> attribute.
	 * @return the salechange_12months - 12 months Sale change
	 */
	public String getSalechange_12months()
	{
		return getSalechange_12months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_12months</code> attribute. 
	 * @param value the salechange_12months - 12 months Sale change
	 */
	public void setSalechange_12months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALECHANGE_12MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_12months</code> attribute. 
	 * @param value the salechange_12months - 12 months Sale change
	 */
	public void setSalechange_12months(final String value)
	{
		setSalechange_12months( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_3months</code> attribute.
	 * @return the salechange_3months - 3 months Sale change
	 */
	public String getSalechange_3months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALECHANGE_3MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_3months</code> attribute.
	 * @return the salechange_3months - 3 months Sale change
	 */
	public String getSalechange_3months()
	{
		return getSalechange_3months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_3months</code> attribute. 
	 * @param value the salechange_3months - 3 months Sale change
	 */
	public void setSalechange_3months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALECHANGE_3MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_3months</code> attribute. 
	 * @param value the salechange_3months - 3 months Sale change
	 */
	public void setSalechange_3months(final String value)
	{
		setSalechange_3months( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_6months</code> attribute.
	 * @return the salechange_6months - 6 months Sale change
	 */
	public String getSalechange_6months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALECHANGE_6MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.salechange_6months</code> attribute.
	 * @return the salechange_6months - 6 months Sale change
	 */
	public String getSalechange_6months()
	{
		return getSalechange_6months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_6months</code> attribute. 
	 * @param value the salechange_6months - 6 months Sale change
	 */
	public void setSalechange_6months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALECHANGE_6MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.salechange_6months</code> attribute. 
	 * @param value the salechange_6months - 6 months Sale change
	 */
	public void setSalechange_6months(final String value)
	{
		setSalechange_6months( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_12months</code> attribute.
	 * @return the unitchange_12months - 12 months Unit change
	 */
	public String getUnitchange_12months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UNITCHANGE_12MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_12months</code> attribute.
	 * @return the unitchange_12months - 12 months Unit change
	 */
	public String getUnitchange_12months()
	{
		return getUnitchange_12months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_12months</code> attribute. 
	 * @param value the unitchange_12months - 12 months Unit change
	 */
	public void setUnitchange_12months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UNITCHANGE_12MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_12months</code> attribute. 
	 * @param value the unitchange_12months - 12 months Unit change
	 */
	public void setUnitchange_12months(final String value)
	{
		setUnitchange_12months( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_3months</code> attribute.
	 * @return the unitchange_3months - 3 months Unit change
	 */
	public String getUnitchange_3months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UNITCHANGE_3MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_3months</code> attribute.
	 * @return the unitchange_3months - 3 months Unit change
	 */
	public String getUnitchange_3months()
	{
		return getUnitchange_3months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_3months</code> attribute. 
	 * @param value the unitchange_3months - 3 months Unit change
	 */
	public void setUnitchange_3months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UNITCHANGE_3MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_3months</code> attribute. 
	 * @param value the unitchange_3months - 3 months Unit change
	 */
	public void setUnitchange_3months(final String value)
	{
		setUnitchange_3months( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_6months</code> attribute.
	 * @return the unitchange_6months - 6 months Unit change
	 */
	public String getUnitchange_6months(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UNITCHANGE_6MONTHS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CSBPercents3612.unitchange_6months</code> attribute.
	 * @return the unitchange_6months - 6 months Unit change
	 */
	public String getUnitchange_6months()
	{
		return getUnitchange_6months( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_6months</code> attribute. 
	 * @param value the unitchange_6months - 6 months Unit change
	 */
	public void setUnitchange_6months(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UNITCHANGE_6MONTHS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CSBPercents3612.unitchange_6months</code> attribute. 
	 * @param value the unitchange_6months - 6 months Unit change
	 */
	public void setUnitchange_6months(final String value)
	{
		setUnitchange_6months( getSession().getSessionContext(), value );
	}
	
}
