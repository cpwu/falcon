/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.CategorySalesBenchmarkCustomer CategorySalesBenchmarkCustomer}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCategorySalesBenchmarkCustomer extends GenericItem
{
	/** Qualifier of the <code>CategorySalesBenchmarkCustomer.cpl1CustomerCode</code> attribute **/
	public static final String CPL1CUSTOMERCODE = "cpl1CustomerCode";
	/** Qualifier of the <code>CategorySalesBenchmarkCustomer.csbCustomerId</code> attribute **/
	public static final String CSBCUSTOMERID = "csbCustomerId";
	/** Qualifier of the <code>CategorySalesBenchmarkCustomer.csbCustomerGroupcode</code> attribute **/
	public static final String CSBCUSTOMERGROUPCODE = "csbCustomerGroupcode";
	/** Qualifier of the <code>CategorySalesBenchmarkCustomer.csbSoldToAccount</code> attribute **/
	public static final String CSBSOLDTOACCOUNT = "csbSoldToAccount";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CPL1CUSTOMERCODE, AttributeMode.INITIAL);
		tmp.put(CSBCUSTOMERID, AttributeMode.INITIAL);
		tmp.put(CSBCUSTOMERGROUPCODE, AttributeMode.INITIAL);
		tmp.put(CSBSOLDTOACCOUNT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1CUSTOMERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode()
	{
		return getCpl1CustomerCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1CUSTOMERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final String value)
	{
		setCpl1CustomerCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbCustomerGroupcode</code> attribute.
	 * @return the csbCustomerGroupcode - Customer Group
	 */
	public String getCsbCustomerGroupcode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBCUSTOMERGROUPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbCustomerGroupcode</code> attribute.
	 * @return the csbCustomerGroupcode - Customer Group
	 */
	public String getCsbCustomerGroupcode()
	{
		return getCsbCustomerGroupcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbCustomerGroupcode</code> attribute. 
	 * @param value the csbCustomerGroupcode - Customer Group
	 */
	public void setCsbCustomerGroupcode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBCUSTOMERGROUPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbCustomerGroupcode</code> attribute. 
	 * @param value the csbCustomerGroupcode - Customer Group
	 */
	public void setCsbCustomerGroupcode(final String value)
	{
		setCsbCustomerGroupcode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbCustomerId</code> attribute.
	 * @return the csbCustomerId - CSB Customer Id
	 */
	public String getCsbCustomerId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBCUSTOMERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbCustomerId</code> attribute.
	 * @return the csbCustomerId - CSB Customer Id
	 */
	public String getCsbCustomerId()
	{
		return getCsbCustomerId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbCustomerId</code> attribute. 
	 * @param value the csbCustomerId - CSB Customer Id
	 */
	public void setCsbCustomerId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBCUSTOMERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbCustomerId</code> attribute. 
	 * @param value the csbCustomerId - CSB Customer Id
	 */
	public void setCsbCustomerId(final String value)
	{
		setCsbCustomerId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbSoldToAccount</code> attribute.
	 * @return the csbSoldToAccount - CSR Sold To Account
	 */
	public String getCsbSoldToAccount(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBSOLDTOACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkCustomer.csbSoldToAccount</code> attribute.
	 * @return the csbSoldToAccount - CSR Sold To Account
	 */
	public String getCsbSoldToAccount()
	{
		return getCsbSoldToAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbSoldToAccount</code> attribute. 
	 * @param value the csbSoldToAccount - CSR Sold To Account
	 */
	public void setCsbSoldToAccount(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBSOLDTOACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkCustomer.csbSoldToAccount</code> attribute. 
	 * @param value the csbSoldToAccount - CSR Sold To Account
	 */
	public void setCsbSoldToAccount(final String value)
	{
		setCsbSoldToAccount( getSession().getSessionContext(), value );
	}
	
}
