/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.CategorySalesBenchmarkPercents CategorySalesBenchmarkPercents}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedCategorySalesBenchmarkPercents extends GenericItem
{
	/** Qualifier of the <code>CategorySalesBenchmarkPercents.csbPercentsId</code> attribute **/
	public static final String CSBPERCENTSID = "csbPercentsId";
	/** Qualifier of the <code>CategorySalesBenchmarkPercents.csbSaleChangePercents</code> attribute **/
	public static final String CSBSALECHANGEPERCENTS = "csbSaleChangePercents";
	/** Qualifier of the <code>CategorySalesBenchmarkPercents.csbCalenderYearMonth</code> attribute **/
	public static final String CSBCALENDERYEARMONTH = "csbCalenderYearMonth";
	/** Qualifier of the <code>CategorySalesBenchmarkPercents.cpl1CustomerCode</code> attribute **/
	public static final String CPL1CUSTOMERCODE = "cpl1CustomerCode";
	/** Qualifier of the <code>CategorySalesBenchmarkPercents.csbUnitChangePercents</code> attribute **/
	public static final String CSBUNITCHANGEPERCENTS = "csbUnitChangePercents";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CSBPERCENTSID, AttributeMode.INITIAL);
		tmp.put(CSBSALECHANGEPERCENTS, AttributeMode.INITIAL);
		tmp.put(CSBCALENDERYEARMONTH, AttributeMode.INITIAL);
		tmp.put(CPL1CUSTOMERCODE, AttributeMode.INITIAL);
		tmp.put(CSBUNITCHANGEPERCENTS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CPL1CUSTOMERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.cpl1CustomerCode</code> attribute.
	 * @return the cpl1CustomerCode - CPL1 Customer
	 */
	public String getCpl1CustomerCode()
	{
		return getCpl1CustomerCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CPL1CUSTOMERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.cpl1CustomerCode</code> attribute. 
	 * @param value the cpl1CustomerCode - CPL1 Customer
	 */
	public void setCpl1CustomerCode(final String value)
	{
		setCpl1CustomerCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbCalenderYearMonth</code> attribute.
	 * @return the csbCalenderYearMonth - CSB Calender Year Month
	 */
	public String getCsbCalenderYearMonth(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBCALENDERYEARMONTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbCalenderYearMonth</code> attribute.
	 * @return the csbCalenderYearMonth - CSB Calender Year Month
	 */
	public String getCsbCalenderYearMonth()
	{
		return getCsbCalenderYearMonth( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbCalenderYearMonth</code> attribute. 
	 * @param value the csbCalenderYearMonth - CSB Calender Year Month
	 */
	public void setCsbCalenderYearMonth(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBCALENDERYEARMONTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbCalenderYearMonth</code> attribute. 
	 * @param value the csbCalenderYearMonth - CSB Calender Year Month
	 */
	public void setCsbCalenderYearMonth(final String value)
	{
		setCsbCalenderYearMonth( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbPercentsId</code> attribute.
	 * @return the csbPercentsId - CSB Percents Id
	 */
	public String getCsbPercentsId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBPERCENTSID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbPercentsId</code> attribute.
	 * @return the csbPercentsId - CSB Percents Id
	 */
	public String getCsbPercentsId()
	{
		return getCsbPercentsId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbPercentsId</code> attribute. 
	 * @param value the csbPercentsId - CSB Percents Id
	 */
	public void setCsbPercentsId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBPERCENTSID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbPercentsId</code> attribute. 
	 * @param value the csbPercentsId - CSB Percents Id
	 */
	public void setCsbPercentsId(final String value)
	{
		setCsbPercentsId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbSaleChangePercents</code> attribute.
	 * @return the csbSaleChangePercents - CSB Sale Change Percents
	 */
	public String getCsbSaleChangePercents(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBSALECHANGEPERCENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbSaleChangePercents</code> attribute.
	 * @return the csbSaleChangePercents - CSB Sale Change Percents
	 */
	public String getCsbSaleChangePercents()
	{
		return getCsbSaleChangePercents( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbSaleChangePercents</code> attribute. 
	 * @param value the csbSaleChangePercents - CSB Sale Change Percents
	 */
	public void setCsbSaleChangePercents(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBSALECHANGEPERCENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbSaleChangePercents</code> attribute. 
	 * @param value the csbSaleChangePercents - CSB Sale Change Percents
	 */
	public void setCsbSaleChangePercents(final String value)
	{
		setCsbSaleChangePercents( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbUnitChangePercents</code> attribute.
	 * @return the csbUnitChangePercents - CSB Unit Change Percents
	 */
	public String getCsbUnitChangePercents(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSBUNITCHANGEPERCENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>CategorySalesBenchmarkPercents.csbUnitChangePercents</code> attribute.
	 * @return the csbUnitChangePercents - CSB Unit Change Percents
	 */
	public String getCsbUnitChangePercents()
	{
		return getCsbUnitChangePercents( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbUnitChangePercents</code> attribute. 
	 * @param value the csbUnitChangePercents - CSB Unit Change Percents
	 */
	public void setCsbUnitChangePercents(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSBUNITCHANGEPERCENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>CategorySalesBenchmarkPercents.csbUnitChangePercents</code> attribute. 
	 * @param value the csbUnitChangePercents - CSB Unit Change Percents
	 */
	public void setCsbUnitChangePercents(final String value)
	{
		setCsbUnitChangePercents( getSession().getSessionContext(), value );
	}
	
}
