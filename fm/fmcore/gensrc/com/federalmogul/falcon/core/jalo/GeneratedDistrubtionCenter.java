/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.DistrubtionCenter FMDistrubtionCenter}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedDistrubtionCenter extends GenericItem
{
	/** Qualifier of the <code>FMDistrubtionCenter.rawpartNumber</code> attribute **/
	public static final String RAWPARTNUMBER = "rawpartNumber";
	/** Qualifier of the <code>FMDistrubtionCenter.backorderQTYAll</code> attribute **/
	public static final String BACKORDERQTYALL = "backorderQTYAll";
	/** Qualifier of the <code>FMDistrubtionCenter.distrubtionCenterDataCode</code> attribute **/
	public static final String DISTRUBTIONCENTERDATACODE = "distrubtionCenterDataCode";
	/** Qualifier of the <code>FMDistrubtionCenter.availableQTY</code> attribute **/
	public static final String AVAILABLEQTY = "availableQTY";
	/** Qualifier of the <code>FMDistrubtionCenter.CarrierAccountCode</code> attribute **/
	public static final String CARRIERACCOUNTCODE = "CarrierAccountCode";
	/** Qualifier of the <code>FMDistrubtionCenter.availableDate</code> attribute **/
	public static final String AVAILABLEDATE = "availableDate";
	/** Qualifier of the <code>FMDistrubtionCenter.backorderFlag</code> attribute **/
	public static final String BACKORDERFLAG = "backorderFlag";
	/** Qualifier of the <code>FMDistrubtionCenter.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMDistrubtionCenter.shippingMethod</code> attribute **/
	public static final String SHIPPINGMETHOD = "shippingMethod";
	/** Qualifier of the <code>FMDistrubtionCenter.carrierName</code> attribute **/
	public static final String CARRIERNAME = "carrierName";
	/** Qualifier of the <code>FMDistrubtionCenter.carrierDispName</code> attribute **/
	public static final String CARRIERDISPNAME = "carrierDispName";
	/** Qualifier of the <code>FMDistrubtionCenter.shippingMethodName</code> attribute **/
	public static final String SHIPPINGMETHODNAME = "shippingMethodName";
	/** Qualifier of the <code>FMDistrubtionCenter.distrubtionCenterDataName</code> attribute **/
	public static final String DISTRUBTIONCENTERDATANAME = "distrubtionCenterDataName";
	/** Qualifier of the <code>FMDistrubtionCenter.backorderQTY</code> attribute **/
	public static final String BACKORDERQTY = "backorderQTY";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(RAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(BACKORDERQTYALL, AttributeMode.INITIAL);
		tmp.put(DISTRUBTIONCENTERDATACODE, AttributeMode.INITIAL);
		tmp.put(AVAILABLEQTY, AttributeMode.INITIAL);
		tmp.put(CARRIERACCOUNTCODE, AttributeMode.INITIAL);
		tmp.put(AVAILABLEDATE, AttributeMode.INITIAL);
		tmp.put(BACKORDERFLAG, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(SHIPPINGMETHOD, AttributeMode.INITIAL);
		tmp.put(CARRIERNAME, AttributeMode.INITIAL);
		tmp.put(CARRIERDISPNAME, AttributeMode.INITIAL);
		tmp.put(SHIPPINGMETHODNAME, AttributeMode.INITIAL);
		tmp.put(DISTRUBTIONCENTERDATANAME, AttributeMode.INITIAL);
		tmp.put(BACKORDERQTY, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.availableDate</code> attribute.
	 * @return the availableDate - availableDate
	 */
	public Date getAvailableDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, AVAILABLEDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.availableDate</code> attribute.
	 * @return the availableDate - availableDate
	 */
	public Date getAvailableDate()
	{
		return getAvailableDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.availableDate</code> attribute. 
	 * @param value the availableDate - availableDate
	 */
	public void setAvailableDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, AVAILABLEDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.availableDate</code> attribute. 
	 * @param value the availableDate - availableDate
	 */
	public void setAvailableDate(final Date value)
	{
		setAvailableDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.availableQTY</code> attribute.
	 * @return the availableQTY - availableQTY
	 */
	public String getAvailableQTY(final SessionContext ctx)
	{
		return (String)getProperty( ctx, AVAILABLEQTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.availableQTY</code> attribute.
	 * @return the availableQTY - availableQTY
	 */
	public String getAvailableQTY()
	{
		return getAvailableQTY( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.availableQTY</code> attribute. 
	 * @param value the availableQTY - availableQTY
	 */
	public void setAvailableQTY(final SessionContext ctx, final String value)
	{
		setProperty(ctx, AVAILABLEQTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.availableQTY</code> attribute. 
	 * @param value the availableQTY - availableQTY
	 */
	public void setAvailableQTY(final String value)
	{
		setAvailableQTY( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderFlag</code> attribute.
	 * @return the backorderFlag - backorderFlag
	 */
	public String getBackorderFlag(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BACKORDERFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderFlag</code> attribute.
	 * @return the backorderFlag - backorderFlag
	 */
	public String getBackorderFlag()
	{
		return getBackorderFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderFlag</code> attribute. 
	 * @param value the backorderFlag - backorderFlag
	 */
	public void setBackorderFlag(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BACKORDERFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderFlag</code> attribute. 
	 * @param value the backorderFlag - backorderFlag
	 */
	public void setBackorderFlag(final String value)
	{
		setBackorderFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderQTY</code> attribute.
	 * @return the backorderQTY - backorderQTY
	 */
	public String getBackorderQTY(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BACKORDERQTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderQTY</code> attribute.
	 * @return the backorderQTY - backorderQTY
	 */
	public String getBackorderQTY()
	{
		return getBackorderQTY( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderQTY</code> attribute. 
	 * @param value the backorderQTY - backorderQTY
	 */
	public void setBackorderQTY(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BACKORDERQTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderQTY</code> attribute. 
	 * @param value the backorderQTY - backorderQTY
	 */
	public void setBackorderQTY(final String value)
	{
		setBackorderQTY( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderQTYAll</code> attribute.
	 * @return the backorderQTYAll - backorderQTYAll
	 */
	public String getBackorderQTYAll(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BACKORDERQTYALL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.backorderQTYAll</code> attribute.
	 * @return the backorderQTYAll - backorderQTYAll
	 */
	public String getBackorderQTYAll()
	{
		return getBackorderQTYAll( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderQTYAll</code> attribute. 
	 * @param value the backorderQTYAll - backorderQTYAll
	 */
	public void setBackorderQTYAll(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BACKORDERQTYALL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.backorderQTYAll</code> attribute. 
	 * @param value the backorderQTYAll - backorderQTYAll
	 */
	public void setBackorderQTYAll(final String value)
	{
		setBackorderQTYAll( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.CarrierAccountCode</code> attribute.
	 * @return the CarrierAccountCode - distrubtionCenterDataName
	 */
	public String getCarrierAccountCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIERACCOUNTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.CarrierAccountCode</code> attribute.
	 * @return the CarrierAccountCode - distrubtionCenterDataName
	 */
	public String getCarrierAccountCode()
	{
		return getCarrierAccountCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.CarrierAccountCode</code> attribute. 
	 * @param value the CarrierAccountCode - distrubtionCenterDataName
	 */
	public void setCarrierAccountCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIERACCOUNTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.CarrierAccountCode</code> attribute. 
	 * @param value the CarrierAccountCode - distrubtionCenterDataName
	 */
	public void setCarrierAccountCode(final String value)
	{
		setCarrierAccountCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.carrierDispName</code> attribute.
	 * @return the carrierDispName - distrubtionCenterDataName
	 */
	public String getCarrierDispName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIERDISPNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.carrierDispName</code> attribute.
	 * @return the carrierDispName - distrubtionCenterDataName
	 */
	public String getCarrierDispName()
	{
		return getCarrierDispName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.carrierDispName</code> attribute. 
	 * @param value the carrierDispName - distrubtionCenterDataName
	 */
	public void setCarrierDispName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIERDISPNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.carrierDispName</code> attribute. 
	 * @param value the carrierDispName - distrubtionCenterDataName
	 */
	public void setCarrierDispName(final String value)
	{
		setCarrierDispName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.carrierName</code> attribute.
	 * @return the carrierName - distrubtionCenterDataName
	 */
	public String getCarrierName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.carrierName</code> attribute.
	 * @return the carrierName - distrubtionCenterDataName
	 */
	public String getCarrierName()
	{
		return getCarrierName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.carrierName</code> attribute. 
	 * @param value the carrierName - distrubtionCenterDataName
	 */
	public void setCarrierName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.carrierName</code> attribute. 
	 * @param value the carrierName - distrubtionCenterDataName
	 */
	public void setCarrierName(final String value)
	{
		setCarrierName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.code</code> attribute.
	 * @return the code - Code DistrubtionCenter
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.code</code> attribute.
	 * @return the code - Code DistrubtionCenter
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.code</code> attribute. 
	 * @param value the code - Code DistrubtionCenter
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.code</code> attribute. 
	 * @param value the code - Code DistrubtionCenter
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.distrubtionCenterDataCode</code> attribute.
	 * @return the distrubtionCenterDataCode - distrubtionCenterDataCode
	 */
	public String getDistrubtionCenterDataCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DISTRUBTIONCENTERDATACODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.distrubtionCenterDataCode</code> attribute.
	 * @return the distrubtionCenterDataCode - distrubtionCenterDataCode
	 */
	public String getDistrubtionCenterDataCode()
	{
		return getDistrubtionCenterDataCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.distrubtionCenterDataCode</code> attribute. 
	 * @param value the distrubtionCenterDataCode - distrubtionCenterDataCode
	 */
	public void setDistrubtionCenterDataCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DISTRUBTIONCENTERDATACODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.distrubtionCenterDataCode</code> attribute. 
	 * @param value the distrubtionCenterDataCode - distrubtionCenterDataCode
	 */
	public void setDistrubtionCenterDataCode(final String value)
	{
		setDistrubtionCenterDataCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.distrubtionCenterDataName</code> attribute.
	 * @return the distrubtionCenterDataName - distrubtionCenterDataName
	 */
	public String getDistrubtionCenterDataName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DISTRUBTIONCENTERDATANAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.distrubtionCenterDataName</code> attribute.
	 * @return the distrubtionCenterDataName - distrubtionCenterDataName
	 */
	public String getDistrubtionCenterDataName()
	{
		return getDistrubtionCenterDataName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.distrubtionCenterDataName</code> attribute. 
	 * @param value the distrubtionCenterDataName - distrubtionCenterDataName
	 */
	public void setDistrubtionCenterDataName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DISTRUBTIONCENTERDATANAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.distrubtionCenterDataName</code> attribute. 
	 * @param value the distrubtionCenterDataName - distrubtionCenterDataName
	 */
	public void setDistrubtionCenterDataName(final String value)
	{
		setDistrubtionCenterDataName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.rawpartNumber</code> attribute.
	 * @return the rawpartNumber - rawpartNumber
	 */
	public String getRawpartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.rawpartNumber</code> attribute.
	 * @return the rawpartNumber - rawpartNumber
	 */
	public String getRawpartNumber()
	{
		return getRawpartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.rawpartNumber</code> attribute. 
	 * @param value the rawpartNumber - rawpartNumber
	 */
	public void setRawpartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.rawpartNumber</code> attribute. 
	 * @param value the rawpartNumber - rawpartNumber
	 */
	public void setRawpartNumber(final String value)
	{
		setRawpartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.shippingMethod</code> attribute.
	 * @return the shippingMethod - distrubtionCenterDataName
	 */
	public String getShippingMethod(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPINGMETHOD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.shippingMethod</code> attribute.
	 * @return the shippingMethod - distrubtionCenterDataName
	 */
	public String getShippingMethod()
	{
		return getShippingMethod( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.shippingMethod</code> attribute. 
	 * @param value the shippingMethod - distrubtionCenterDataName
	 */
	public void setShippingMethod(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPINGMETHOD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.shippingMethod</code> attribute. 
	 * @param value the shippingMethod - distrubtionCenterDataName
	 */
	public void setShippingMethod(final String value)
	{
		setShippingMethod( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.shippingMethodName</code> attribute.
	 * @return the shippingMethodName - distrubtionCenterDataName
	 */
	public String getShippingMethodName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPINGMETHODNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDistrubtionCenter.shippingMethodName</code> attribute.
	 * @return the shippingMethodName - distrubtionCenterDataName
	 */
	public String getShippingMethodName()
	{
		return getShippingMethodName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.shippingMethodName</code> attribute. 
	 * @param value the shippingMethodName - distrubtionCenterDataName
	 */
	public void setShippingMethodName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPINGMETHODNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDistrubtionCenter.shippingMethodName</code> attribute. 
	 * @param value the shippingMethodName - distrubtionCenterDataName
	 */
	public void setShippingMethodName(final String value)
	{
		setShippingMethodName( getSession().getSessionContext(), value );
	}
	
}
