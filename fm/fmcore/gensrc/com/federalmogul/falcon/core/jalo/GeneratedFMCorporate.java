/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.media.Media;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMCorporate FMCorporate}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCorporate extends GenericItem
{
	/** Qualifier of the <code>FMCorporate.thumbnail</code> attribute **/
	public static final String THUMBNAIL = "thumbnail";
	/** Qualifier of the <code>FMCorporate.corpname</code> attribute **/
	public static final String CORPNAME = "corpname";
	/** Qualifier of the <code>FMCorporate.corpcode</code> attribute **/
	public static final String CORPCODE = "corpcode";
	/** Qualifier of the <code>FMCorporate.corptype</code> attribute **/
	public static final String CORPTYPE = "corptype";
	/** Qualifier of the <code>FMCorporate.corpid</code> attribute **/
	public static final String CORPID = "corpid";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(THUMBNAIL, AttributeMode.INITIAL);
		tmp.put(CORPNAME, AttributeMode.INITIAL);
		tmp.put(CORPCODE, AttributeMode.INITIAL);
		tmp.put(CORPTYPE, AttributeMode.INITIAL);
		tmp.put(CORPID, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpcode</code> attribute.
	 * @return the corpcode - Corporate id - Brand id
	 */
	public String getCorpcode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpcode</code> attribute.
	 * @return the corpcode - Corporate id - Brand id
	 */
	public String getCorpcode()
	{
		return getCorpcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpcode</code> attribute. 
	 * @param value the corpcode - Corporate id - Brand id
	 */
	public void setCorpcode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpcode</code> attribute. 
	 * @param value the corpcode - Corporate id - Brand id
	 */
	public void setCorpcode(final String value)
	{
		setCorpcode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpid</code> attribute.
	 * @return the corpid
	 */
	public Integer getCorpid(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, CORPID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpid</code> attribute.
	 * @return the corpid
	 */
	public Integer getCorpid()
	{
		return getCorpid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpid</code> attribute. 
	 * @return the corpid
	 */
	public int getCorpidAsPrimitive(final SessionContext ctx)
	{
		Integer value = getCorpid( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpid</code> attribute. 
	 * @return the corpid
	 */
	public int getCorpidAsPrimitive()
	{
		return getCorpidAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpid</code> attribute. 
	 * @param value the corpid
	 */
	public void setCorpid(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, CORPID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpid</code> attribute. 
	 * @param value the corpid
	 */
	public void setCorpid(final Integer value)
	{
		setCorpid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpid</code> attribute. 
	 * @param value the corpid
	 */
	public void setCorpid(final SessionContext ctx, final int value)
	{
		setCorpid( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpid</code> attribute. 
	 * @param value the corpid
	 */
	public void setCorpid(final int value)
	{
		setCorpid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpname</code> attribute.
	 * @return the corpname
	 */
	public String getCorpname(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corpname</code> attribute.
	 * @return the corpname
	 */
	public String getCorpname()
	{
		return getCorpname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpname</code> attribute. 
	 * @param value the corpname
	 */
	public void setCorpname(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corpname</code> attribute. 
	 * @param value the corpname
	 */
	public void setCorpname(final String value)
	{
		setCorpname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corptype</code> attribute.
	 * @return the corptype
	 */
	public String getCorptype(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.corptype</code> attribute.
	 * @return the corptype
	 */
	public String getCorptype()
	{
		return getCorptype( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corptype</code> attribute. 
	 * @param value the corptype
	 */
	public void setCorptype(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.corptype</code> attribute. 
	 * @param value the corptype
	 */
	public void setCorptype(final String value)
	{
		setCorptype( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.thumbnail</code> attribute.
	 * @return the thumbnail
	 */
	public Media getThumbnail(final SessionContext ctx)
	{
		return (Media)getProperty( ctx, THUMBNAIL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCorporate.thumbnail</code> attribute.
	 * @return the thumbnail
	 */
	public Media getThumbnail()
	{
		return getThumbnail( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.thumbnail</code> attribute. 
	 * @param value the thumbnail
	 */
	public void setThumbnail(final SessionContext ctx, final Media value)
	{
		setProperty(ctx, THUMBNAIL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCorporate.thumbnail</code> attribute. 
	 * @param value the thumbnail
	 */
	public void setThumbnail(final Media value)
	{
		setThumbnail( getSession().getSessionContext(), value );
	}
	
}
