/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMCsrAccountList FMCsrAccountList}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCsrAccountList extends GenericItem
{
	/** Qualifier of the <code>FMCsrAccountList.csrUserID</code> attribute **/
	public static final String CSRUSERID = "csrUserID";
	/** Qualifier of the <code>FMCsrAccountList.accountnum</code> attribute **/
	public static final String ACCOUNTNUM = "accountnum";
	/** Qualifier of the <code>FMCsrAccountList.nabsAccountCode</code> attribute **/
	public static final String NABSACCOUNTCODE = "nabsAccountCode";
	/** Qualifier of the <code>FMCsrAccountList.date</code> attribute **/
	public static final String DATE = "date";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CSRUSERID, AttributeMode.INITIAL);
		tmp.put(ACCOUNTNUM, AttributeMode.INITIAL);
		tmp.put(NABSACCOUNTCODE, AttributeMode.INITIAL);
		tmp.put(DATE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.accountnum</code> attribute.
	 * @return the accountnum
	 */
	public String getAccountnum(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ACCOUNTNUM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.accountnum</code> attribute.
	 * @return the accountnum
	 */
	public String getAccountnum()
	{
		return getAccountnum( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.accountnum</code> attribute. 
	 * @param value the accountnum
	 */
	public void setAccountnum(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ACCOUNTNUM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.accountnum</code> attribute. 
	 * @param value the accountnum
	 */
	public void setAccountnum(final String value)
	{
		setAccountnum( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.csrUserID</code> attribute.
	 * @return the csrUserID - Emulated CSR user
	 */
	public String getCsrUserID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CSRUSERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.csrUserID</code> attribute.
	 * @return the csrUserID - Emulated CSR user
	 */
	public String getCsrUserID()
	{
		return getCsrUserID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.csrUserID</code> attribute. 
	 * @param value the csrUserID - Emulated CSR user
	 */
	public void setCsrUserID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CSRUSERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.csrUserID</code> attribute. 
	 * @param value the csrUserID - Emulated CSR user
	 */
	public void setCsrUserID(final String value)
	{
		setCsrUserID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.date</code> attribute.
	 * @return the date - The date for timestamp
	 */
	public Date getDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, DATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.date</code> attribute.
	 * @return the date - The date for timestamp
	 */
	public Date getDate()
	{
		return getDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.date</code> attribute. 
	 * @param value the date - The date for timestamp
	 */
	public void setDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, DATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.date</code> attribute. 
	 * @param value the date - The date for timestamp
	 */
	public void setDate(final Date value)
	{
		setDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.nabsAccountCode</code> attribute.
	 * @return the nabsAccountCode
	 */
	public String getNabsAccountCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NABSACCOUNTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCsrAccountList.nabsAccountCode</code> attribute.
	 * @return the nabsAccountCode
	 */
	public String getNabsAccountCode()
	{
		return getNabsAccountCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.nabsAccountCode</code> attribute. 
	 * @param value the nabsAccountCode
	 */
	public void setNabsAccountCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NABSACCOUNTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCsrAccountList.nabsAccountCode</code> attribute. 
	 * @param value the nabsAccountCode
	 */
	public void setNabsAccountCode(final String value)
	{
		setNabsAccountCode( getSession().getSessionContext(), value );
	}
	
}
