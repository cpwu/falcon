/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMDCCenter FMDCCenter}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMDCCenter extends GenericItem
{
	/** Qualifier of the <code>FMDCCenter.addressLine1</code> attribute **/
	public static final String ADDRESSLINE1 = "addressLine1";
	/** Qualifier of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute **/
	public static final String LOCTIMEZONEOFFSET = "locTimeZoneOffSet";
	/** Qualifier of the <code>FMDCCenter.contactPhone</code> attribute **/
	public static final String CONTACTPHONE = "contactPhone";
	/** Qualifier of the <code>FMDCCenter.marketCode</code> attribute **/
	public static final String MARKETCODE = "marketCode";
	/** Qualifier of the <code>FMDCCenter.participateFlag</code> attribute **/
	public static final String PARTICIPATEFLAG = "participateFlag";
	/** Qualifier of the <code>FMDCCenter.dcCode</code> attribute **/
	public static final String DCCODE = "dcCode";
	/** Qualifier of the <code>FMDCCenter.tscFlag</code> attribute **/
	public static final String TSCFLAG = "tscFlag";
	/** Qualifier of the <code>FMDCCenter.emergencyCutOffTime</code> attribute **/
	public static final String EMERGENCYCUTOFFTIME = "emergencyCutOffTime";
	/** Qualifier of the <code>FMDCCenter.dcName</code> attribute **/
	public static final String DCNAME = "dcName";
	/** Qualifier of the <code>FMDCCenter.state</code> attribute **/
	public static final String STATE = "state";
	/** Qualifier of the <code>FMDCCenter.extSysCode</code> attribute **/
	public static final String EXTSYSCODE = "extSysCode";
	/** Qualifier of the <code>FMDCCenter.isoCountryCode</code> attribute **/
	public static final String ISOCOUNTRYCODE = "isoCountryCode";
	/** Qualifier of the <code>FMDCCenter.city</code> attribute **/
	public static final String CITY = "city";
	/** Qualifier of the <code>FMDCCenter.zipCode</code> attribute **/
	public static final String ZIPCODE = "zipCode";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ADDRESSLINE1, AttributeMode.INITIAL);
		tmp.put(LOCTIMEZONEOFFSET, AttributeMode.INITIAL);
		tmp.put(CONTACTPHONE, AttributeMode.INITIAL);
		tmp.put(MARKETCODE, AttributeMode.INITIAL);
		tmp.put(PARTICIPATEFLAG, AttributeMode.INITIAL);
		tmp.put(DCCODE, AttributeMode.INITIAL);
		tmp.put(TSCFLAG, AttributeMode.INITIAL);
		tmp.put(EMERGENCYCUTOFFTIME, AttributeMode.INITIAL);
		tmp.put(DCNAME, AttributeMode.INITIAL);
		tmp.put(STATE, AttributeMode.INITIAL);
		tmp.put(EXTSYSCODE, AttributeMode.INITIAL);
		tmp.put(ISOCOUNTRYCODE, AttributeMode.INITIAL);
		tmp.put(CITY, AttributeMode.INITIAL);
		tmp.put(ZIPCODE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.addressLine1</code> attribute.
	 * @return the addressLine1 - Address Line1
	 */
	public String getAddressLine1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ADDRESSLINE1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.addressLine1</code> attribute.
	 * @return the addressLine1 - Address Line1
	 */
	public String getAddressLine1()
	{
		return getAddressLine1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.addressLine1</code> attribute. 
	 * @param value the addressLine1 - Address Line1
	 */
	public void setAddressLine1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ADDRESSLINE1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.addressLine1</code> attribute. 
	 * @param value the addressLine1 - Address Line1
	 */
	public void setAddressLine1(final String value)
	{
		setAddressLine1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.city</code> attribute.
	 * @return the city - City
	 */
	public String getCity(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.city</code> attribute.
	 * @return the city - City
	 */
	public String getCity()
	{
		return getCity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.city</code> attribute. 
	 * @param value the city - City
	 */
	public void setCity(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.city</code> attribute. 
	 * @param value the city - City
	 */
	public void setCity(final String value)
	{
		setCity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.contactPhone</code> attribute.
	 * @return the contactPhone - Contact Phone
	 */
	public Integer getContactPhone(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, CONTACTPHONE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.contactPhone</code> attribute.
	 * @return the contactPhone - Contact Phone
	 */
	public Integer getContactPhone()
	{
		return getContactPhone( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @return the contactPhone - Contact Phone
	 */
	public int getContactPhoneAsPrimitive(final SessionContext ctx)
	{
		Integer value = getContactPhone( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @return the contactPhone - Contact Phone
	 */
	public int getContactPhoneAsPrimitive()
	{
		return getContactPhoneAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @param value the contactPhone - Contact Phone
	 */
	public void setContactPhone(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, CONTACTPHONE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @param value the contactPhone - Contact Phone
	 */
	public void setContactPhone(final Integer value)
	{
		setContactPhone( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @param value the contactPhone - Contact Phone
	 */
	public void setContactPhone(final SessionContext ctx, final int value)
	{
		setContactPhone( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.contactPhone</code> attribute. 
	 * @param value the contactPhone - Contact Phone
	 */
	public void setContactPhone(final int value)
	{
		setContactPhone( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.dcCode</code> attribute.
	 * @return the dcCode - Distribution Center Code
	 */
	public String getDcCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DCCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.dcCode</code> attribute.
	 * @return the dcCode - Distribution Center Code
	 */
	public String getDcCode()
	{
		return getDcCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.dcCode</code> attribute. 
	 * @param value the dcCode - Distribution Center Code
	 */
	public void setDcCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DCCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.dcCode</code> attribute. 
	 * @param value the dcCode - Distribution Center Code
	 */
	public void setDcCode(final String value)
	{
		setDcCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.dcName</code> attribute.
	 * @return the dcName - Distribution Center Name
	 */
	public String getDcName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DCNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.dcName</code> attribute.
	 * @return the dcName - Distribution Center Name
	 */
	public String getDcName()
	{
		return getDcName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.dcName</code> attribute. 
	 * @param value the dcName - Distribution Center Name
	 */
	public void setDcName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DCNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.dcName</code> attribute. 
	 * @param value the dcName - Distribution Center Name
	 */
	public void setDcName(final String value)
	{
		setDcName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.emergencyCutOffTime</code> attribute.
	 * @return the emergencyCutOffTime - Emergency Cutt off Time
	 */
	public Date getEmergencyCutOffTime(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, EMERGENCYCUTOFFTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.emergencyCutOffTime</code> attribute.
	 * @return the emergencyCutOffTime - Emergency Cutt off Time
	 */
	public Date getEmergencyCutOffTime()
	{
		return getEmergencyCutOffTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.emergencyCutOffTime</code> attribute. 
	 * @param value the emergencyCutOffTime - Emergency Cutt off Time
	 */
	public void setEmergencyCutOffTime(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, EMERGENCYCUTOFFTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.emergencyCutOffTime</code> attribute. 
	 * @param value the emergencyCutOffTime - Emergency Cutt off Time
	 */
	public void setEmergencyCutOffTime(final Date value)
	{
		setEmergencyCutOffTime( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.extSysCode</code> attribute.
	 * @return the extSysCode - External System Code
	 */
	public String getExtSysCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EXTSYSCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.extSysCode</code> attribute.
	 * @return the extSysCode - External System Code
	 */
	public String getExtSysCode()
	{
		return getExtSysCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.extSysCode</code> attribute. 
	 * @param value the extSysCode - External System Code
	 */
	public void setExtSysCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EXTSYSCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.extSysCode</code> attribute. 
	 * @param value the extSysCode - External System Code
	 */
	public void setExtSysCode(final String value)
	{
		setExtSysCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.isoCountryCode</code> attribute.
	 * @return the isoCountryCode - ISO Country Code
	 */
	public String getIsoCountryCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ISOCOUNTRYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.isoCountryCode</code> attribute.
	 * @return the isoCountryCode - ISO Country Code
	 */
	public String getIsoCountryCode()
	{
		return getIsoCountryCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.isoCountryCode</code> attribute. 
	 * @param value the isoCountryCode - ISO Country Code
	 */
	public void setIsoCountryCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ISOCOUNTRYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.isoCountryCode</code> attribute. 
	 * @param value the isoCountryCode - ISO Country Code
	 */
	public void setIsoCountryCode(final String value)
	{
		setIsoCountryCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute.
	 * @return the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public Integer getLocTimeZoneOffSet(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, LOCTIMEZONEOFFSET);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute.
	 * @return the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public Integer getLocTimeZoneOffSet()
	{
		return getLocTimeZoneOffSet( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @return the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public int getLocTimeZoneOffSetAsPrimitive(final SessionContext ctx)
	{
		Integer value = getLocTimeZoneOffSet( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @return the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public int getLocTimeZoneOffSetAsPrimitive()
	{
		return getLocTimeZoneOffSetAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @param value the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public void setLocTimeZoneOffSet(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, LOCTIMEZONEOFFSET,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @param value the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public void setLocTimeZoneOffSet(final Integer value)
	{
		setLocTimeZoneOffSet( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @param value the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public void setLocTimeZoneOffSet(final SessionContext ctx, final int value)
	{
		setLocTimeZoneOffSet( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.locTimeZoneOffSet</code> attribute. 
	 * @param value the locTimeZoneOffSet - Local Time Zone OffSet
	 */
	public void setLocTimeZoneOffSet(final int value)
	{
		setLocTimeZoneOffSet( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.marketCode</code> attribute.
	 * @return the marketCode - Market Code
	 */
	public String getMarketCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MARKETCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.marketCode</code> attribute.
	 * @return the marketCode - Market Code
	 */
	public String getMarketCode()
	{
		return getMarketCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.marketCode</code> attribute. 
	 * @param value the marketCode - Market Code
	 */
	public void setMarketCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MARKETCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.marketCode</code> attribute. 
	 * @param value the marketCode - Market Code
	 */
	public void setMarketCode(final String value)
	{
		setMarketCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.participateFlag</code> attribute.
	 * @return the participateFlag - Participation Flag
	 */
	public Boolean isParticipateFlag(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, PARTICIPATEFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.participateFlag</code> attribute.
	 * @return the participateFlag - Participation Flag
	 */
	public Boolean isParticipateFlag()
	{
		return isParticipateFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @return the participateFlag - Participation Flag
	 */
	public boolean isParticipateFlagAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isParticipateFlag( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @return the participateFlag - Participation Flag
	 */
	public boolean isParticipateFlagAsPrimitive()
	{
		return isParticipateFlagAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @param value the participateFlag - Participation Flag
	 */
	public void setParticipateFlag(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, PARTICIPATEFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @param value the participateFlag - Participation Flag
	 */
	public void setParticipateFlag(final Boolean value)
	{
		setParticipateFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @param value the participateFlag - Participation Flag
	 */
	public void setParticipateFlag(final SessionContext ctx, final boolean value)
	{
		setParticipateFlag( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.participateFlag</code> attribute. 
	 * @param value the participateFlag - Participation Flag
	 */
	public void setParticipateFlag(final boolean value)
	{
		setParticipateFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.state</code> attribute.
	 * @return the state - State
	 */
	public String getState(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.state</code> attribute.
	 * @return the state - State
	 */
	public String getState()
	{
		return getState( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.state</code> attribute. 
	 * @param value the state - State
	 */
	public void setState(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.state</code> attribute. 
	 * @param value the state - State
	 */
	public void setState(final String value)
	{
		setState( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.tscFlag</code> attribute.
	 * @return the tscFlag - TSC Flag
	 */
	public Boolean isTscFlag(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, TSCFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.tscFlag</code> attribute.
	 * @return the tscFlag - TSC Flag
	 */
	public Boolean isTscFlag()
	{
		return isTscFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @return the tscFlag - TSC Flag
	 */
	public boolean isTscFlagAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isTscFlag( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @return the tscFlag - TSC Flag
	 */
	public boolean isTscFlagAsPrimitive()
	{
		return isTscFlagAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @param value the tscFlag - TSC Flag
	 */
	public void setTscFlag(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, TSCFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @param value the tscFlag - TSC Flag
	 */
	public void setTscFlag(final Boolean value)
	{
		setTscFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @param value the tscFlag - TSC Flag
	 */
	public void setTscFlag(final SessionContext ctx, final boolean value)
	{
		setTscFlag( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.tscFlag</code> attribute. 
	 * @param value the tscFlag - TSC Flag
	 */
	public void setTscFlag(final boolean value)
	{
		setTscFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.zipCode</code> attribute.
	 * @return the zipCode - Zip Code
	 */
	public Integer getZipCode(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, ZIPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.zipCode</code> attribute.
	 * @return the zipCode - Zip Code
	 */
	public Integer getZipCode()
	{
		return getZipCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @return the zipCode - Zip Code
	 */
	public int getZipCodeAsPrimitive(final SessionContext ctx)
	{
		Integer value = getZipCode( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @return the zipCode - Zip Code
	 */
	public int getZipCodeAsPrimitive()
	{
		return getZipCodeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @param value the zipCode - Zip Code
	 */
	public void setZipCode(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, ZIPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @param value the zipCode - Zip Code
	 */
	public void setZipCode(final Integer value)
	{
		setZipCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @param value the zipCode - Zip Code
	 */
	public void setZipCode(final SessionContext ctx, final int value)
	{
		setZipCode( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCCenter.zipCode</code> attribute. 
	 * @param value the zipCode - Zip Code
	 */
	public void setZipCode(final int value)
	{
		setZipCode( getSession().getSessionContext(), value );
	}
	
}
