/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMDCShipping FMDCShipping}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMDCShipping extends GenericItem
{
	/** Qualifier of the <code>FMDCShipping.CARRIER_CD</code> attribute **/
	public static final String CARRIER_CD = "CARRIER_CD";
	/** Qualifier of the <code>FMDCShipping.STOCK_SHIP_CD</code> attribute **/
	public static final String STOCK_SHIP_CD = "STOCK_SHIP_CD";
	/** Qualifier of the <code>FMDCShipping.SORT_ORDER</code> attribute **/
	public static final String SORT_ORDER = "SORT_ORDER";
	/** Qualifier of the <code>FMDCShipping.NORMAL_SHIP_CD</code> attribute **/
	public static final String NORMAL_SHIP_CD = "NORMAL_SHIP_CD";
	/** Qualifier of the <code>FMDCShipping.SAP_CARRIER_CD</code> attribute **/
	public static final String SAP_CARRIER_CD = "SAP_CARRIER_CD";
	/** Qualifier of the <code>FMDCShipping.DIST_CNTR_ID</code> attribute **/
	public static final String DIST_CNTR_ID = "DIST_CNTR_ID";
	/** Qualifier of the <code>FMDCShipping.SHIP_MTHD_CD</code> attribute **/
	public static final String SHIP_MTHD_CD = "SHIP_MTHD_CD";
	/** Qualifier of the <code>FMDCShipping.ROUTE</code> attribute **/
	public static final String ROUTE = "ROUTE";
	/** Qualifier of the <code>FMDCShipping.INCO_TERMS</code> attribute **/
	public static final String INCO_TERMS = "INCO_TERMS";
	/** Qualifier of the <code>FMDCShipping.DC_CARRIER_SHIPMTHD_ID</code> attribute **/
	public static final String DC_CARRIER_SHIPMTHD_ID = "DC_CARRIER_SHIPMTHD_ID";
	/** Qualifier of the <code>FMDCShipping.CARRIER_ID</code> attribute **/
	public static final String CARRIER_ID = "CARRIER_ID";
	/** Qualifier of the <code>FMDCShipping.FROM_ISO_CNTRY_CD</code> attribute **/
	public static final String FROM_ISO_CNTRY_CD = "FROM_ISO_CNTRY_CD";
	/** Qualifier of the <code>FMDCShipping.DIST_CNTR_CD</code> attribute **/
	public static final String DIST_CNTR_CD = "DIST_CNTR_CD";
	/** Qualifier of the <code>FMDCShipping.SHIP_MTHD_DESC</code> attribute **/
	public static final String SHIP_MTHD_DESC = "SHIP_MTHD_DESC";
	/** Qualifier of the <code>FMDCShipping.ISO_CNTRY_CD</code> attribute **/
	public static final String ISO_CNTRY_CD = "ISO_CNTRY_CD";
	/** Qualifier of the <code>FMDCShipping.CARRIER_SHIPMTHD_ID</code> attribute **/
	public static final String CARRIER_SHIPMTHD_ID = "CARRIER_SHIPMTHD_ID";
	/** Qualifier of the <code>FMDCShipping.TO_ISO_CNTRY_CD</code> attribute **/
	public static final String TO_ISO_CNTRY_CD = "TO_ISO_CNTRY_CD";
	/** Qualifier of the <code>FMDCShipping.EMERG_SHIP_CD</code> attribute **/
	public static final String EMERG_SHIP_CD = "EMERG_SHIP_CD";
	/** Qualifier of the <code>FMDCShipping.DIST_CNTR_NAME</code> attribute **/
	public static final String DIST_CNTR_NAME = "DIST_CNTR_NAME";
	/** Qualifier of the <code>FMDCShipping.CARRIER_NAME</code> attribute **/
	public static final String CARRIER_NAME = "CARRIER_NAME";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CARRIER_CD, AttributeMode.INITIAL);
		tmp.put(STOCK_SHIP_CD, AttributeMode.INITIAL);
		tmp.put(SORT_ORDER, AttributeMode.INITIAL);
		tmp.put(NORMAL_SHIP_CD, AttributeMode.INITIAL);
		tmp.put(SAP_CARRIER_CD, AttributeMode.INITIAL);
		tmp.put(DIST_CNTR_ID, AttributeMode.INITIAL);
		tmp.put(SHIP_MTHD_CD, AttributeMode.INITIAL);
		tmp.put(ROUTE, AttributeMode.INITIAL);
		tmp.put(INCO_TERMS, AttributeMode.INITIAL);
		tmp.put(DC_CARRIER_SHIPMTHD_ID, AttributeMode.INITIAL);
		tmp.put(CARRIER_ID, AttributeMode.INITIAL);
		tmp.put(FROM_ISO_CNTRY_CD, AttributeMode.INITIAL);
		tmp.put(DIST_CNTR_CD, AttributeMode.INITIAL);
		tmp.put(SHIP_MTHD_DESC, AttributeMode.INITIAL);
		tmp.put(ISO_CNTRY_CD, AttributeMode.INITIAL);
		tmp.put(CARRIER_SHIPMTHD_ID, AttributeMode.INITIAL);
		tmp.put(TO_ISO_CNTRY_CD, AttributeMode.INITIAL);
		tmp.put(EMERG_SHIP_CD, AttributeMode.INITIAL);
		tmp.put(DIST_CNTR_NAME, AttributeMode.INITIAL);
		tmp.put(CARRIER_NAME, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_CD</code> attribute.
	 * @return the CARRIER_CD - Distribution Center Code
	 */
	public String getCARRIER_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_CD</code> attribute.
	 * @return the CARRIER_CD - Distribution Center Code
	 */
	public String getCARRIER_CD()
	{
		return getCARRIER_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_CD</code> attribute. 
	 * @param value the CARRIER_CD - Distribution Center Code
	 */
	public void setCARRIER_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_CD</code> attribute. 
	 * @param value the CARRIER_CD - Distribution Center Code
	 */
	public void setCARRIER_CD(final String value)
	{
		setCARRIER_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_ID</code> attribute.
	 * @return the CARRIER_ID - Distribution Center Code
	 */
	public String getCARRIER_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_ID</code> attribute.
	 * @return the CARRIER_ID - Distribution Center Code
	 */
	public String getCARRIER_ID()
	{
		return getCARRIER_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_ID</code> attribute. 
	 * @param value the CARRIER_ID - Distribution Center Code
	 */
	public void setCARRIER_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_ID</code> attribute. 
	 * @param value the CARRIER_ID - Distribution Center Code
	 */
	public void setCARRIER_ID(final String value)
	{
		setCARRIER_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_NAME</code> attribute.
	 * @return the CARRIER_NAME - Distribution Center Code
	 */
	public String getCARRIER_NAME(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_NAME</code> attribute.
	 * @return the CARRIER_NAME - Distribution Center Code
	 */
	public String getCARRIER_NAME()
	{
		return getCARRIER_NAME( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_NAME</code> attribute. 
	 * @param value the CARRIER_NAME - Distribution Center Code
	 */
	public void setCARRIER_NAME(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_NAME</code> attribute. 
	 * @param value the CARRIER_NAME - Distribution Center Code
	 */
	public void setCARRIER_NAME(final String value)
	{
		setCARRIER_NAME( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public String getCARRIER_SHIPMTHD_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_SHIPMTHD_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public String getCARRIER_SHIPMTHD_ID()
	{
		return getCARRIER_SHIPMTHD_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public void setCARRIER_SHIPMTHD_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_SHIPMTHD_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public void setCARRIER_SHIPMTHD_ID(final String value)
	{
		setCARRIER_SHIPMTHD_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DC_CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the DC_CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public String getDC_CARRIER_SHIPMTHD_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DC_CARRIER_SHIPMTHD_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DC_CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the DC_CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public String getDC_CARRIER_SHIPMTHD_ID()
	{
		return getDC_CARRIER_SHIPMTHD_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DC_CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the DC_CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public void setDC_CARRIER_SHIPMTHD_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DC_CARRIER_SHIPMTHD_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DC_CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the DC_CARRIER_SHIPMTHD_ID - Distribution Center Code
	 */
	public void setDC_CARRIER_SHIPMTHD_ID(final String value)
	{
		setDC_CARRIER_SHIPMTHD_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_CD</code> attribute.
	 * @return the DIST_CNTR_CD - Distribution Center Code
	 */
	public String getDIST_CNTR_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIST_CNTR_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_CD</code> attribute.
	 * @return the DIST_CNTR_CD - Distribution Center Code
	 */
	public String getDIST_CNTR_CD()
	{
		return getDIST_CNTR_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_CD</code> attribute. 
	 * @param value the DIST_CNTR_CD - Distribution Center Code
	 */
	public void setDIST_CNTR_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIST_CNTR_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_CD</code> attribute. 
	 * @param value the DIST_CNTR_CD - Distribution Center Code
	 */
	public void setDIST_CNTR_CD(final String value)
	{
		setDIST_CNTR_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_ID</code> attribute.
	 * @return the DIST_CNTR_ID - Distribution Center Code
	 */
	public String getDIST_CNTR_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIST_CNTR_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_ID</code> attribute.
	 * @return the DIST_CNTR_ID - Distribution Center Code
	 */
	public String getDIST_CNTR_ID()
	{
		return getDIST_CNTR_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_ID</code> attribute. 
	 * @param value the DIST_CNTR_ID - Distribution Center Code
	 */
	public void setDIST_CNTR_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIST_CNTR_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_ID</code> attribute. 
	 * @param value the DIST_CNTR_ID - Distribution Center Code
	 */
	public void setDIST_CNTR_ID(final String value)
	{
		setDIST_CNTR_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_NAME</code> attribute.
	 * @return the DIST_CNTR_NAME - Distribution Center Code
	 */
	public String getDIST_CNTR_NAME(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIST_CNTR_NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.DIST_CNTR_NAME</code> attribute.
	 * @return the DIST_CNTR_NAME - Distribution Center Code
	 */
	public String getDIST_CNTR_NAME()
	{
		return getDIST_CNTR_NAME( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_NAME</code> attribute. 
	 * @param value the DIST_CNTR_NAME - Distribution Center Code
	 */
	public void setDIST_CNTR_NAME(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIST_CNTR_NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.DIST_CNTR_NAME</code> attribute. 
	 * @param value the DIST_CNTR_NAME - Distribution Center Code
	 */
	public void setDIST_CNTR_NAME(final String value)
	{
		setDIST_CNTR_NAME( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.EMERG_SHIP_CD</code> attribute.
	 * @return the EMERG_SHIP_CD - Distribution Center Code
	 */
	public String getEMERG_SHIP_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EMERG_SHIP_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.EMERG_SHIP_CD</code> attribute.
	 * @return the EMERG_SHIP_CD - Distribution Center Code
	 */
	public String getEMERG_SHIP_CD()
	{
		return getEMERG_SHIP_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.EMERG_SHIP_CD</code> attribute. 
	 * @param value the EMERG_SHIP_CD - Distribution Center Code
	 */
	public void setEMERG_SHIP_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EMERG_SHIP_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.EMERG_SHIP_CD</code> attribute. 
	 * @param value the EMERG_SHIP_CD - Distribution Center Code
	 */
	public void setEMERG_SHIP_CD(final String value)
	{
		setEMERG_SHIP_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.FROM_ISO_CNTRY_CD</code> attribute.
	 * @return the FROM_ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getFROM_ISO_CNTRY_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FROM_ISO_CNTRY_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.FROM_ISO_CNTRY_CD</code> attribute.
	 * @return the FROM_ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getFROM_ISO_CNTRY_CD()
	{
		return getFROM_ISO_CNTRY_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.FROM_ISO_CNTRY_CD</code> attribute. 
	 * @param value the FROM_ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setFROM_ISO_CNTRY_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FROM_ISO_CNTRY_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.FROM_ISO_CNTRY_CD</code> attribute. 
	 * @param value the FROM_ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setFROM_ISO_CNTRY_CD(final String value)
	{
		setFROM_ISO_CNTRY_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.INCO_TERMS</code> attribute.
	 * @return the INCO_TERMS - Distribution Center Code
	 */
	public String getINCO_TERMS(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INCO_TERMS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.INCO_TERMS</code> attribute.
	 * @return the INCO_TERMS - Distribution Center Code
	 */
	public String getINCO_TERMS()
	{
		return getINCO_TERMS( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.INCO_TERMS</code> attribute. 
	 * @param value the INCO_TERMS - Distribution Center Code
	 */
	public void setINCO_TERMS(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INCO_TERMS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.INCO_TERMS</code> attribute. 
	 * @param value the INCO_TERMS - Distribution Center Code
	 */
	public void setINCO_TERMS(final String value)
	{
		setINCO_TERMS( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.ISO_CNTRY_CD</code> attribute.
	 * @return the ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getISO_CNTRY_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ISO_CNTRY_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.ISO_CNTRY_CD</code> attribute.
	 * @return the ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getISO_CNTRY_CD()
	{
		return getISO_CNTRY_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.ISO_CNTRY_CD</code> attribute. 
	 * @param value the ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setISO_CNTRY_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ISO_CNTRY_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.ISO_CNTRY_CD</code> attribute. 
	 * @param value the ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setISO_CNTRY_CD(final String value)
	{
		setISO_CNTRY_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.NORMAL_SHIP_CD</code> attribute.
	 * @return the NORMAL_SHIP_CD - Distribution Center Code
	 */
	public String getNORMAL_SHIP_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NORMAL_SHIP_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.NORMAL_SHIP_CD</code> attribute.
	 * @return the NORMAL_SHIP_CD - Distribution Center Code
	 */
	public String getNORMAL_SHIP_CD()
	{
		return getNORMAL_SHIP_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.NORMAL_SHIP_CD</code> attribute. 
	 * @param value the NORMAL_SHIP_CD - Distribution Center Code
	 */
	public void setNORMAL_SHIP_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NORMAL_SHIP_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.NORMAL_SHIP_CD</code> attribute. 
	 * @param value the NORMAL_SHIP_CD - Distribution Center Code
	 */
	public void setNORMAL_SHIP_CD(final String value)
	{
		setNORMAL_SHIP_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.ROUTE</code> attribute.
	 * @return the ROUTE - Distribution Center Code
	 */
	public String getROUTE(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ROUTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.ROUTE</code> attribute.
	 * @return the ROUTE - Distribution Center Code
	 */
	public String getROUTE()
	{
		return getROUTE( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.ROUTE</code> attribute. 
	 * @param value the ROUTE - Distribution Center Code
	 */
	public void setROUTE(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ROUTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.ROUTE</code> attribute. 
	 * @param value the ROUTE - Distribution Center Code
	 */
	public void setROUTE(final String value)
	{
		setROUTE( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SAP_CARRIER_CD</code> attribute.
	 * @return the SAP_CARRIER_CD - Distribution Center Code
	 */
	public String getSAP_CARRIER_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SAP_CARRIER_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SAP_CARRIER_CD</code> attribute.
	 * @return the SAP_CARRIER_CD - Distribution Center Code
	 */
	public String getSAP_CARRIER_CD()
	{
		return getSAP_CARRIER_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SAP_CARRIER_CD</code> attribute. 
	 * @param value the SAP_CARRIER_CD - Distribution Center Code
	 */
	public void setSAP_CARRIER_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SAP_CARRIER_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SAP_CARRIER_CD</code> attribute. 
	 * @param value the SAP_CARRIER_CD - Distribution Center Code
	 */
	public void setSAP_CARRIER_CD(final String value)
	{
		setSAP_CARRIER_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SHIP_MTHD_CD</code> attribute.
	 * @return the SHIP_MTHD_CD - Distribution Center Code
	 */
	public String getSHIP_MTHD_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIP_MTHD_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SHIP_MTHD_CD</code> attribute.
	 * @return the SHIP_MTHD_CD - Distribution Center Code
	 */
	public String getSHIP_MTHD_CD()
	{
		return getSHIP_MTHD_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SHIP_MTHD_CD</code> attribute. 
	 * @param value the SHIP_MTHD_CD - Distribution Center Code
	 */
	public void setSHIP_MTHD_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIP_MTHD_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SHIP_MTHD_CD</code> attribute. 
	 * @param value the SHIP_MTHD_CD - Distribution Center Code
	 */
	public void setSHIP_MTHD_CD(final String value)
	{
		setSHIP_MTHD_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SHIP_MTHD_DESC</code> attribute.
	 * @return the SHIP_MTHD_DESC - Distribution Center Code
	 */
	public String getSHIP_MTHD_DESC(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIP_MTHD_DESC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SHIP_MTHD_DESC</code> attribute.
	 * @return the SHIP_MTHD_DESC - Distribution Center Code
	 */
	public String getSHIP_MTHD_DESC()
	{
		return getSHIP_MTHD_DESC( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SHIP_MTHD_DESC</code> attribute. 
	 * @param value the SHIP_MTHD_DESC - Distribution Center Code
	 */
	public void setSHIP_MTHD_DESC(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIP_MTHD_DESC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SHIP_MTHD_DESC</code> attribute. 
	 * @param value the SHIP_MTHD_DESC - Distribution Center Code
	 */
	public void setSHIP_MTHD_DESC(final String value)
	{
		setSHIP_MTHD_DESC( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SORT_ORDER</code> attribute.
	 * @return the SORT_ORDER - Distribution Center Code
	 */
	public String getSORT_ORDER(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SORT_ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.SORT_ORDER</code> attribute.
	 * @return the SORT_ORDER - Distribution Center Code
	 */
	public String getSORT_ORDER()
	{
		return getSORT_ORDER( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SORT_ORDER</code> attribute. 
	 * @param value the SORT_ORDER - Distribution Center Code
	 */
	public void setSORT_ORDER(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SORT_ORDER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.SORT_ORDER</code> attribute. 
	 * @param value the SORT_ORDER - Distribution Center Code
	 */
	public void setSORT_ORDER(final String value)
	{
		setSORT_ORDER( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.STOCK_SHIP_CD</code> attribute.
	 * @return the STOCK_SHIP_CD - Distribution Center Code
	 */
	public String getSTOCK_SHIP_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STOCK_SHIP_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.STOCK_SHIP_CD</code> attribute.
	 * @return the STOCK_SHIP_CD - Distribution Center Code
	 */
	public String getSTOCK_SHIP_CD()
	{
		return getSTOCK_SHIP_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.STOCK_SHIP_CD</code> attribute. 
	 * @param value the STOCK_SHIP_CD - Distribution Center Code
	 */
	public void setSTOCK_SHIP_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STOCK_SHIP_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.STOCK_SHIP_CD</code> attribute. 
	 * @param value the STOCK_SHIP_CD - Distribution Center Code
	 */
	public void setSTOCK_SHIP_CD(final String value)
	{
		setSTOCK_SHIP_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.TO_ISO_CNTRY_CD</code> attribute.
	 * @return the TO_ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getTO_ISO_CNTRY_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TO_ISO_CNTRY_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMDCShipping.TO_ISO_CNTRY_CD</code> attribute.
	 * @return the TO_ISO_CNTRY_CD - Distribution Center Code
	 */
	public String getTO_ISO_CNTRY_CD()
	{
		return getTO_ISO_CNTRY_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.TO_ISO_CNTRY_CD</code> attribute. 
	 * @param value the TO_ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setTO_ISO_CNTRY_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TO_ISO_CNTRY_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMDCShipping.TO_ISO_CNTRY_CD</code> attribute. 
	 * @param value the TO_ISO_CNTRY_CD - Distribution Center Code
	 */
	public void setTO_ISO_CNTRY_CD(final String value)
	{
		setTO_ISO_CNTRY_CD( getSession().getSessionContext(), value );
	}
	
}
