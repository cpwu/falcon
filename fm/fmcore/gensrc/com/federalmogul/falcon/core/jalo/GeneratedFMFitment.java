/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.falcon.core.jalo.FMPart;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMFitment FMFitment}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMFitment extends GenericItem
{
	/** Qualifier of the <code>FMFitment.parts</code> attribute **/
	public static final String PARTS = "parts";
	/** Relation ordering override parameter constants for FitmentPartRelation from ((fmcore))*/
	protected static String FITMENTPARTRELATION_SRC_ORDERED = "relation.FitmentPartRelation.source.ordered";
	protected static String FITMENTPARTRELATION_TGT_ORDERED = "relation.FitmentPartRelation.target.ordered";
	/** Relation disable markmodifed parameter constants for FitmentPartRelation from ((fmcore))*/
	protected static String FITMENTPARTRELATION_MARKMODIFIED = "relation.FitmentPartRelation.markmodified";
	/** Qualifier of the <code>FMFitment.product</code> attribute **/
	public static final String PRODUCT = "product";
	/** Qualifier of the <code>FMFitment.assetItemRef</code> attribute **/
	public static final String ASSETITEMREF = "assetItemRef";
	/** Qualifier of the <code>FMFitment.comment10</code> attribute **/
	public static final String COMMENT10 = "comment10";
	/** Qualifier of the <code>FMFitment.applicationNote1</code> attribute **/
	public static final String APPLICATIONNOTE1 = "applicationNote1";
	/** Qualifier of the <code>FMFitment.transmissionControlType</code> attribute **/
	public static final String TRANSMISSIONCONTROLTYPE = "transmissionControlType";
	/** Qualifier of the <code>FMFitment.applicationNote4</code> attribute **/
	public static final String APPLICATIONNOTE4 = "applicationNote4";
	/** Qualifier of the <code>FMFitment.comment8</code> attribute **/
	public static final String COMMENT8 = "comment8";
	/** Qualifier of the <code>FMFitment.region</code> attribute **/
	public static final String REGION = "region";
	/** Qualifier of the <code>FMFitment.bodyType</code> attribute **/
	public static final String BODYTYPE = "bodyType";
	/** Qualifier of the <code>FMFitment.engineSerialNumber</code> attribute **/
	public static final String ENGINESERIALNUMBER = "engineSerialNumber";
	/** Qualifier of the <code>FMFitment.engineVIN</code> attribute **/
	public static final String ENGINEVIN = "engineVIN";
	/** Qualifier of the <code>FMFitment.transmissionType</code> attribute **/
	public static final String TRANSMISSIONTYPE = "transmissionType";
	/** Qualifier of the <code>FMFitment.engineBase</code> attribute **/
	public static final String ENGINEBASE = "engineBase";
	/** Qualifier of the <code>FMFitment.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMFitment.brakeSystem</code> attribute **/
	public static final String BRAKESYSTEM = "brakeSystem";
	/** Qualifier of the <code>FMFitment.applicationNote2</code> attribute **/
	public static final String APPLICATIONNOTE2 = "applicationNote2";
	/** Qualifier of the <code>FMFitment.interchangeNote8</code> attribute **/
	public static final String INTERCHANGENOTE8 = "interchangeNote8";
	/** Qualifier of the <code>FMFitment.fuelDeliveryType</code> attribute **/
	public static final String FUELDELIVERYTYPE = "fuelDeliveryType";
	/** Qualifier of the <code>FMFitment.interchangeNote1</code> attribute **/
	public static final String INTERCHANGENOTE1 = "interchangeNote1";
	/** Qualifier of the <code>FMFitment.engineCPLNumber</code> attribute **/
	public static final String ENGINECPLNUMBER = "engineCPLNumber";
	/** Qualifier of the <code>FMFitment.applicationNote8</code> attribute **/
	public static final String APPLICATIONNOTE8 = "applicationNote8";
	/** Qualifier of the <code>FMFitment.ignitionSystemType</code> attribute **/
	public static final String IGNITIONSYSTEMTYPE = "ignitionSystemType";
	/** Qualifier of the <code>FMFitment.make</code> attribute **/
	public static final String MAKE = "make";
	/** Qualifier of the <code>FMFitment.rearBrakeType</code> attribute **/
	public static final String REARBRAKETYPE = "rearBrakeType";
	/** Qualifier of the <code>FMFitment.mfrLabel</code> attribute **/
	public static final String MFRLABEL = "mfrLabel";
	/** Qualifier of the <code>FMFitment.engineVersion</code> attribute **/
	public static final String ENGINEVERSION = "engineVersion";
	/** Qualifier of the <code>FMFitment.comment9</code> attribute **/
	public static final String COMMENT9 = "comment9";
	/** Qualifier of the <code>FMFitment.engineValves</code> attribute **/
	public static final String ENGINEVALVES = "engineValves";
	/** Qualifier of the <code>FMFitment.mfrBodyCode</code> attribute **/
	public static final String MFRBODYCODE = "mfrBodyCode";
	/** Qualifier of the <code>FMFitment.assetFileName</code> attribute **/
	public static final String ASSETFILENAME = "assetFileName";
	/** Qualifier of the <code>FMFitment.transmissionNumSpeeds</code> attribute **/
	public static final String TRANSMISSIONNUMSPEEDS = "transmissionNumSpeeds";
	/** Qualifier of the <code>FMFitment.powerOutput</code> attribute **/
	public static final String POWEROUTPUT = "powerOutput";
	/** Qualifier of the <code>FMFitment.fuelType</code> attribute **/
	public static final String FUELTYPE = "fuelType";
	/** Qualifier of the <code>FMFitment.transmissionMfrCode</code> attribute **/
	public static final String TRANSMISSIONMFRCODE = "transmissionMfrCode";
	/** Qualifier of the <code>FMFitment.interchangeNote4</code> attribute **/
	public static final String INTERCHANGENOTE4 = "interchangeNote4";
	/** Qualifier of the <code>FMFitment.brakeABS</code> attribute **/
	public static final String BRAKEABS = "brakeABS";
	/** Qualifier of the <code>FMFitment.vehiclesegment</code> attribute **/
	public static final String VEHICLESEGMENT = "vehiclesegment";
	/** Qualifier of the <code>FMFitment.comment2</code> attribute **/
	public static final String COMMENT2 = "comment2";
	/** Qualifier of the <code>FMFitment.applicationNote3</code> attribute **/
	public static final String APPLICATIONNOTE3 = "applicationNote3";
	/** Qualifier of the <code>FMFitment.fuelSystemDesign</code> attribute **/
	public static final String FUELSYSTEMDESIGN = "fuelSystemDesign";
	/** Qualifier of the <code>FMFitment.interchangeNote5</code> attribute **/
	public static final String INTERCHANGENOTE5 = "interchangeNote5";
	/** Qualifier of the <code>FMFitment.position</code> attribute **/
	public static final String POSITION = "position";
	/** Qualifier of the <code>FMFitment.interchangeNote2</code> attribute **/
	public static final String INTERCHANGENOTE2 = "interchangeNote2";
	/** Qualifier of the <code>FMFitment.comment5</code> attribute **/
	public static final String COMMENT5 = "comment5";
	/** Qualifier of the <code>FMFitment.bedLength</code> attribute **/
	public static final String BEDLENGTH = "bedLength";
	/** Qualifier of the <code>FMFitment.fuelSystemControlType</code> attribute **/
	public static final String FUELSYSTEMCONTROLTYPE = "fuelSystemControlType";
	/** Qualifier of the <code>FMFitment.partNumber</code> attribute **/
	public static final String PARTNUMBER = "partNumber";
	/** Qualifier of the <code>FMFitment.frontBrakeType</code> attribute **/
	public static final String FRONTBRAKETYPE = "frontBrakeType";
	/** Qualifier of the <code>FMFitment.steeringType</code> attribute **/
	public static final String STEERINGTYPE = "steeringType";
	/** Qualifier of the <code>FMFitment.cylinderHeadType</code> attribute **/
	public static final String CYLINDERHEADTYPE = "cylinderHeadType";
	/** Qualifier of the <code>FMFitment.applicationNote10</code> attribute **/
	public static final String APPLICATIONNOTE10 = "applicationNote10";
	/** Qualifier of the <code>FMFitment.interchangeNote10</code> attribute **/
	public static final String INTERCHANGENOTE10 = "interchangeNote10";
	/** Qualifier of the <code>FMFitment.comment4</code> attribute **/
	public static final String COMMENT4 = "comment4";
	/** Qualifier of the <code>FMFitment.driveType</code> attribute **/
	public static final String DRIVETYPE = "driveType";
	/** Qualifier of the <code>FMFitment.steeringSystem</code> attribute **/
	public static final String STEERINGSYSTEM = "steeringSystem";
	/** Qualifier of the <code>FMFitment.bodyNumDoors</code> attribute **/
	public static final String BODYNUMDOORS = "bodyNumDoors";
	/** Qualifier of the <code>FMFitment.applicationNote6</code> attribute **/
	public static final String APPLICATIONNOTE6 = "applicationNote6";
	/** Qualifier of the <code>FMFitment.interchangeNote7</code> attribute **/
	public static final String INTERCHANGENOTE7 = "interchangeNote7";
	/** Qualifier of the <code>FMFitment.comment3</code> attribute **/
	public static final String COMMENT3 = "comment3";
	/** Qualifier of the <code>FMFitment.wheelBase</code> attribute **/
	public static final String WHEELBASE = "wheelBase";
	/** Qualifier of the <code>FMFitment.appQty</code> attribute **/
	public static final String APPQTY = "appQty";
	/** Qualifier of the <code>FMFitment.applicationNote9</code> attribute **/
	public static final String APPLICATIONNOTE9 = "applicationNote9";
	/** Qualifier of the <code>FMFitment.vehicleType</code> attribute **/
	public static final String VEHICLETYPE = "vehicleType";
	/** Qualifier of the <code>FMFitment.bedType</code> attribute **/
	public static final String BEDTYPE = "bedType";
	/** Qualifier of the <code>FMFitment.submodel</code> attribute **/
	public static final String SUBMODEL = "submodel";
	/** Qualifier of the <code>FMFitment.assetLogicalName</code> attribute **/
	public static final String ASSETLOGICALNAME = "assetLogicalName";
	/** Qualifier of the <code>FMFitment.engineMfr</code> attribute **/
	public static final String ENGINEMFR = "engineMfr";
	/** Qualifier of the <code>FMFitment.applicationNote7</code> attribute **/
	public static final String APPLICATIONNOTE7 = "applicationNote7";
	/** Qualifier of the <code>FMFitment.frontSpringType</code> attribute **/
	public static final String FRONTSPRINGTYPE = "frontSpringType";
	/** Qualifier of the <code>FMFitment.comment1</code> attribute **/
	public static final String COMMENT1 = "comment1";
	/** Qualifier of the <code>FMFitment.interchangeNote9</code> attribute **/
	public static final String INTERCHANGENOTE9 = "interchangeNote9";
	/** Qualifier of the <code>FMFitment.interchangeNote6</code> attribute **/
	public static final String INTERCHANGENOTE6 = "interchangeNote6";
	/** Qualifier of the <code>FMFitment.engineDesignation</code> attribute **/
	public static final String ENGINEDESIGNATION = "engineDesignation";
	/** Qualifier of the <code>FMFitment.comment7</code> attribute **/
	public static final String COMMENT7 = "comment7";
	/** Qualifier of the <code>FMFitment.engineArrangementNumber</code> attribute **/
	public static final String ENGINEARRANGEMENTNUMBER = "engineArrangementNumber";
	/** Qualifier of the <code>FMFitment.aspiration</code> attribute **/
	public static final String ASPIRATION = "aspiration";
	/** Qualifier of the <code>FMFitment.year</code> attribute **/
	public static final String YEAR = "year";
	/** Qualifier of the <code>FMFitment.rearSpringType</code> attribute **/
	public static final String REARSPRINGTYPE = "rearSpringType";
	/** Qualifier of the <code>FMFitment.interchangeNote3</code> attribute **/
	public static final String INTERCHANGENOTE3 = "interchangeNote3";
	/** Qualifier of the <code>FMFitment.comment6</code> attribute **/
	public static final String COMMENT6 = "comment6";
	/** Qualifier of the <code>FMFitment.model</code> attribute **/
	public static final String MODEL = "model";
	/** Qualifier of the <code>FMFitment.fuelDeliverySubType</code> attribute **/
	public static final String FUELDELIVERYSUBTYPE = "fuelDeliverySubType";
	/** Qualifier of the <code>FMFitment.applicationNote5</code> attribute **/
	public static final String APPLICATIONNOTE5 = "applicationNote5";
	/** Qualifier of the <code>FMFitment.ymmcode</code> attribute **/
	public static final String YMMCODE = "ymmcode";
	/** Qualifier of the <code>FMFitment.vehicleSeries</code> attribute **/
	public static final String VEHICLESERIES = "vehicleSeries";
	/** Qualifier of the <code>FMFitment.assetItemOrder</code> attribute **/
	public static final String ASSETITEMORDER = "assetItemOrder";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(PRODUCT, AttributeMode.INITIAL);
		tmp.put(ASSETITEMREF, AttributeMode.INITIAL);
		tmp.put(COMMENT10, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE1, AttributeMode.INITIAL);
		tmp.put(TRANSMISSIONCONTROLTYPE, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE4, AttributeMode.INITIAL);
		tmp.put(COMMENT8, AttributeMode.INITIAL);
		tmp.put(REGION, AttributeMode.INITIAL);
		tmp.put(BODYTYPE, AttributeMode.INITIAL);
		tmp.put(ENGINESERIALNUMBER, AttributeMode.INITIAL);
		tmp.put(ENGINEVIN, AttributeMode.INITIAL);
		tmp.put(TRANSMISSIONTYPE, AttributeMode.INITIAL);
		tmp.put(ENGINEBASE, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(BRAKESYSTEM, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE2, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE8, AttributeMode.INITIAL);
		tmp.put(FUELDELIVERYTYPE, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE1, AttributeMode.INITIAL);
		tmp.put(ENGINECPLNUMBER, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE8, AttributeMode.INITIAL);
		tmp.put(IGNITIONSYSTEMTYPE, AttributeMode.INITIAL);
		tmp.put(MAKE, AttributeMode.INITIAL);
		tmp.put(REARBRAKETYPE, AttributeMode.INITIAL);
		tmp.put(MFRLABEL, AttributeMode.INITIAL);
		tmp.put(ENGINEVERSION, AttributeMode.INITIAL);
		tmp.put(COMMENT9, AttributeMode.INITIAL);
		tmp.put(ENGINEVALVES, AttributeMode.INITIAL);
		tmp.put(MFRBODYCODE, AttributeMode.INITIAL);
		tmp.put(ASSETFILENAME, AttributeMode.INITIAL);
		tmp.put(TRANSMISSIONNUMSPEEDS, AttributeMode.INITIAL);
		tmp.put(POWEROUTPUT, AttributeMode.INITIAL);
		tmp.put(FUELTYPE, AttributeMode.INITIAL);
		tmp.put(TRANSMISSIONMFRCODE, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE4, AttributeMode.INITIAL);
		tmp.put(BRAKEABS, AttributeMode.INITIAL);
		tmp.put(VEHICLESEGMENT, AttributeMode.INITIAL);
		tmp.put(COMMENT2, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE3, AttributeMode.INITIAL);
		tmp.put(FUELSYSTEMDESIGN, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE5, AttributeMode.INITIAL);
		tmp.put(POSITION, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE2, AttributeMode.INITIAL);
		tmp.put(COMMENT5, AttributeMode.INITIAL);
		tmp.put(BEDLENGTH, AttributeMode.INITIAL);
		tmp.put(FUELSYSTEMCONTROLTYPE, AttributeMode.INITIAL);
		tmp.put(PARTNUMBER, AttributeMode.INITIAL);
		tmp.put(FRONTBRAKETYPE, AttributeMode.INITIAL);
		tmp.put(STEERINGTYPE, AttributeMode.INITIAL);
		tmp.put(CYLINDERHEADTYPE, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE10, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE10, AttributeMode.INITIAL);
		tmp.put(COMMENT4, AttributeMode.INITIAL);
		tmp.put(DRIVETYPE, AttributeMode.INITIAL);
		tmp.put(STEERINGSYSTEM, AttributeMode.INITIAL);
		tmp.put(BODYNUMDOORS, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE6, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE7, AttributeMode.INITIAL);
		tmp.put(COMMENT3, AttributeMode.INITIAL);
		tmp.put(WHEELBASE, AttributeMode.INITIAL);
		tmp.put(APPQTY, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE9, AttributeMode.INITIAL);
		tmp.put(VEHICLETYPE, AttributeMode.INITIAL);
		tmp.put(BEDTYPE, AttributeMode.INITIAL);
		tmp.put(SUBMODEL, AttributeMode.INITIAL);
		tmp.put(ASSETLOGICALNAME, AttributeMode.INITIAL);
		tmp.put(ENGINEMFR, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE7, AttributeMode.INITIAL);
		tmp.put(FRONTSPRINGTYPE, AttributeMode.INITIAL);
		tmp.put(COMMENT1, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE9, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE6, AttributeMode.INITIAL);
		tmp.put(ENGINEDESIGNATION, AttributeMode.INITIAL);
		tmp.put(COMMENT7, AttributeMode.INITIAL);
		tmp.put(ENGINEARRANGEMENTNUMBER, AttributeMode.INITIAL);
		tmp.put(ASPIRATION, AttributeMode.INITIAL);
		tmp.put(YEAR, AttributeMode.INITIAL);
		tmp.put(REARSPRINGTYPE, AttributeMode.INITIAL);
		tmp.put(INTERCHANGENOTE3, AttributeMode.INITIAL);
		tmp.put(COMMENT6, AttributeMode.INITIAL);
		tmp.put(MODEL, AttributeMode.INITIAL);
		tmp.put(FUELDELIVERYSUBTYPE, AttributeMode.INITIAL);
		tmp.put(APPLICATIONNOTE5, AttributeMode.INITIAL);
		tmp.put(YMMCODE, AttributeMode.INITIAL);
		tmp.put(VEHICLESERIES, AttributeMode.INITIAL);
		tmp.put(ASSETITEMORDER, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote1</code> attribute.
	 * @return the applicationNote1
	 */
	public String getApplicationNote1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote1</code> attribute.
	 * @return the applicationNote1
	 */
	public String getApplicationNote1()
	{
		return getApplicationNote1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote1</code> attribute. 
	 * @param value the applicationNote1
	 */
	public void setApplicationNote1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote1</code> attribute. 
	 * @param value the applicationNote1
	 */
	public void setApplicationNote1(final String value)
	{
		setApplicationNote1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote10</code> attribute.
	 * @return the applicationNote10
	 */
	public String getApplicationNote10(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE10);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote10</code> attribute.
	 * @return the applicationNote10
	 */
	public String getApplicationNote10()
	{
		return getApplicationNote10( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote10</code> attribute. 
	 * @param value the applicationNote10
	 */
	public void setApplicationNote10(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE10,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote10</code> attribute. 
	 * @param value the applicationNote10
	 */
	public void setApplicationNote10(final String value)
	{
		setApplicationNote10( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote2</code> attribute.
	 * @return the applicationNote2
	 */
	public String getApplicationNote2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote2</code> attribute.
	 * @return the applicationNote2
	 */
	public String getApplicationNote2()
	{
		return getApplicationNote2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote2</code> attribute. 
	 * @param value the applicationNote2
	 */
	public void setApplicationNote2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote2</code> attribute. 
	 * @param value the applicationNote2
	 */
	public void setApplicationNote2(final String value)
	{
		setApplicationNote2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote3</code> attribute.
	 * @return the applicationNote3
	 */
	public String getApplicationNote3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote3</code> attribute.
	 * @return the applicationNote3
	 */
	public String getApplicationNote3()
	{
		return getApplicationNote3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote3</code> attribute. 
	 * @param value the applicationNote3
	 */
	public void setApplicationNote3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote3</code> attribute. 
	 * @param value the applicationNote3
	 */
	public void setApplicationNote3(final String value)
	{
		setApplicationNote3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote4</code> attribute.
	 * @return the applicationNote4
	 */
	public String getApplicationNote4(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE4);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote4</code> attribute.
	 * @return the applicationNote4
	 */
	public String getApplicationNote4()
	{
		return getApplicationNote4( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote4</code> attribute. 
	 * @param value the applicationNote4
	 */
	public void setApplicationNote4(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE4,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote4</code> attribute. 
	 * @param value the applicationNote4
	 */
	public void setApplicationNote4(final String value)
	{
		setApplicationNote4( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote5</code> attribute.
	 * @return the applicationNote5
	 */
	public String getApplicationNote5(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE5);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote5</code> attribute.
	 * @return the applicationNote5
	 */
	public String getApplicationNote5()
	{
		return getApplicationNote5( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote5</code> attribute. 
	 * @param value the applicationNote5
	 */
	public void setApplicationNote5(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE5,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote5</code> attribute. 
	 * @param value the applicationNote5
	 */
	public void setApplicationNote5(final String value)
	{
		setApplicationNote5( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote6</code> attribute.
	 * @return the applicationNote6
	 */
	public String getApplicationNote6(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE6);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote6</code> attribute.
	 * @return the applicationNote6
	 */
	public String getApplicationNote6()
	{
		return getApplicationNote6( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote6</code> attribute. 
	 * @param value the applicationNote6
	 */
	public void setApplicationNote6(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE6,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote6</code> attribute. 
	 * @param value the applicationNote6
	 */
	public void setApplicationNote6(final String value)
	{
		setApplicationNote6( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote7</code> attribute.
	 * @return the applicationNote7
	 */
	public String getApplicationNote7(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE7);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote7</code> attribute.
	 * @return the applicationNote7
	 */
	public String getApplicationNote7()
	{
		return getApplicationNote7( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote7</code> attribute. 
	 * @param value the applicationNote7
	 */
	public void setApplicationNote7(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE7,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote7</code> attribute. 
	 * @param value the applicationNote7
	 */
	public void setApplicationNote7(final String value)
	{
		setApplicationNote7( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote8</code> attribute.
	 * @return the applicationNote8
	 */
	public String getApplicationNote8(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE8);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote8</code> attribute.
	 * @return the applicationNote8
	 */
	public String getApplicationNote8()
	{
		return getApplicationNote8( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote8</code> attribute. 
	 * @param value the applicationNote8
	 */
	public void setApplicationNote8(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE8,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote8</code> attribute. 
	 * @param value the applicationNote8
	 */
	public void setApplicationNote8(final String value)
	{
		setApplicationNote8( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote9</code> attribute.
	 * @return the applicationNote9
	 */
	public String getApplicationNote9(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONNOTE9);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.applicationNote9</code> attribute.
	 * @return the applicationNote9
	 */
	public String getApplicationNote9()
	{
		return getApplicationNote9( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote9</code> attribute. 
	 * @param value the applicationNote9
	 */
	public void setApplicationNote9(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONNOTE9,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.applicationNote9</code> attribute. 
	 * @param value the applicationNote9
	 */
	public void setApplicationNote9(final String value)
	{
		setApplicationNote9( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.appQty</code> attribute.
	 * @return the appQty
	 */
	public String getAppQty(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPQTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.appQty</code> attribute.
	 * @return the appQty
	 */
	public String getAppQty()
	{
		return getAppQty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.appQty</code> attribute. 
	 * @param value the appQty
	 */
	public void setAppQty(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPQTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.appQty</code> attribute. 
	 * @param value the appQty
	 */
	public void setAppQty(final String value)
	{
		setAppQty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.aspiration</code> attribute.
	 * @return the aspiration
	 */
	public String getAspiration(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASPIRATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.aspiration</code> attribute.
	 * @return the aspiration
	 */
	public String getAspiration()
	{
		return getAspiration( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.aspiration</code> attribute. 
	 * @param value the aspiration
	 */
	public void setAspiration(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASPIRATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.aspiration</code> attribute. 
	 * @param value the aspiration
	 */
	public void setAspiration(final String value)
	{
		setAspiration( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetFileName</code> attribute.
	 * @return the assetFileName
	 */
	public String getAssetFileName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASSETFILENAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetFileName</code> attribute.
	 * @return the assetFileName
	 */
	public String getAssetFileName()
	{
		return getAssetFileName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetFileName</code> attribute. 
	 * @param value the assetFileName
	 */
	public void setAssetFileName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASSETFILENAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetFileName</code> attribute. 
	 * @param value the assetFileName
	 */
	public void setAssetFileName(final String value)
	{
		setAssetFileName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetItemOrder</code> attribute.
	 * @return the assetItemOrder
	 */
	public String getAssetItemOrder(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASSETITEMORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetItemOrder</code> attribute.
	 * @return the assetItemOrder
	 */
	public String getAssetItemOrder()
	{
		return getAssetItemOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetItemOrder</code> attribute. 
	 * @param value the assetItemOrder
	 */
	public void setAssetItemOrder(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASSETITEMORDER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetItemOrder</code> attribute. 
	 * @param value the assetItemOrder
	 */
	public void setAssetItemOrder(final String value)
	{
		setAssetItemOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetItemRef</code> attribute.
	 * @return the assetItemRef
	 */
	public String getAssetItemRef(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASSETITEMREF);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetItemRef</code> attribute.
	 * @return the assetItemRef
	 */
	public String getAssetItemRef()
	{
		return getAssetItemRef( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetItemRef</code> attribute. 
	 * @param value the assetItemRef
	 */
	public void setAssetItemRef(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASSETITEMREF,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetItemRef</code> attribute. 
	 * @param value the assetItemRef
	 */
	public void setAssetItemRef(final String value)
	{
		setAssetItemRef( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetLogicalName</code> attribute.
	 * @return the assetLogicalName
	 */
	public String getAssetLogicalName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ASSETLOGICALNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.assetLogicalName</code> attribute.
	 * @return the assetLogicalName
	 */
	public String getAssetLogicalName()
	{
		return getAssetLogicalName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetLogicalName</code> attribute. 
	 * @param value the assetLogicalName
	 */
	public void setAssetLogicalName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ASSETLOGICALNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.assetLogicalName</code> attribute. 
	 * @param value the assetLogicalName
	 */
	public void setAssetLogicalName(final String value)
	{
		setAssetLogicalName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bedLength</code> attribute.
	 * @return the bedLength
	 */
	public String getBedLength(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BEDLENGTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bedLength</code> attribute.
	 * @return the bedLength
	 */
	public String getBedLength()
	{
		return getBedLength( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bedLength</code> attribute. 
	 * @param value the bedLength
	 */
	public void setBedLength(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BEDLENGTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bedLength</code> attribute. 
	 * @param value the bedLength
	 */
	public void setBedLength(final String value)
	{
		setBedLength( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bedType</code> attribute.
	 * @return the bedType
	 */
	public String getBedType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BEDTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bedType</code> attribute.
	 * @return the bedType
	 */
	public String getBedType()
	{
		return getBedType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bedType</code> attribute. 
	 * @param value the bedType
	 */
	public void setBedType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BEDTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bedType</code> attribute. 
	 * @param value the bedType
	 */
	public void setBedType(final String value)
	{
		setBedType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bodyNumDoors</code> attribute.
	 * @return the bodyNumDoors
	 */
	public String getBodyNumDoors(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BODYNUMDOORS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bodyNumDoors</code> attribute.
	 * @return the bodyNumDoors
	 */
	public String getBodyNumDoors()
	{
		return getBodyNumDoors( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bodyNumDoors</code> attribute. 
	 * @param value the bodyNumDoors
	 */
	public void setBodyNumDoors(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BODYNUMDOORS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bodyNumDoors</code> attribute. 
	 * @param value the bodyNumDoors
	 */
	public void setBodyNumDoors(final String value)
	{
		setBodyNumDoors( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bodyType</code> attribute.
	 * @return the bodyType
	 */
	public String getBodyType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BODYTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.bodyType</code> attribute.
	 * @return the bodyType
	 */
	public String getBodyType()
	{
		return getBodyType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bodyType</code> attribute. 
	 * @param value the bodyType
	 */
	public void setBodyType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BODYTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.bodyType</code> attribute. 
	 * @param value the bodyType
	 */
	public void setBodyType(final String value)
	{
		setBodyType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.brakeABS</code> attribute.
	 * @return the brakeABS
	 */
	public String getBrakeABS(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRAKEABS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.brakeABS</code> attribute.
	 * @return the brakeABS
	 */
	public String getBrakeABS()
	{
		return getBrakeABS( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.brakeABS</code> attribute. 
	 * @param value the brakeABS
	 */
	public void setBrakeABS(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRAKEABS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.brakeABS</code> attribute. 
	 * @param value the brakeABS
	 */
	public void setBrakeABS(final String value)
	{
		setBrakeABS( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.brakeSystem</code> attribute.
	 * @return the brakeSystem
	 */
	public String getBrakeSystem(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRAKESYSTEM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.brakeSystem</code> attribute.
	 * @return the brakeSystem
	 */
	public String getBrakeSystem()
	{
		return getBrakeSystem( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.brakeSystem</code> attribute. 
	 * @param value the brakeSystem
	 */
	public void setBrakeSystem(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRAKESYSTEM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.brakeSystem</code> attribute. 
	 * @param value the brakeSystem
	 */
	public void setBrakeSystem(final String value)
	{
		setBrakeSystem( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.code</code> attribute.
	 * @return the code - Code of FMYearMakeModelSearch
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.code</code> attribute.
	 * @return the code - Code of FMYearMakeModelSearch
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.code</code> attribute. 
	 * @param value the code - Code of FMYearMakeModelSearch
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.code</code> attribute. 
	 * @param value the code - Code of FMYearMakeModelSearch
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment1</code> attribute.
	 * @return the comment1
	 */
	public String getComment1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment1</code> attribute.
	 * @return the comment1
	 */
	public String getComment1()
	{
		return getComment1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment1</code> attribute. 
	 * @param value the comment1
	 */
	public void setComment1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment1</code> attribute. 
	 * @param value the comment1
	 */
	public void setComment1(final String value)
	{
		setComment1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment10</code> attribute.
	 * @return the comment10
	 */
	public String getComment10(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT10);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment10</code> attribute.
	 * @return the comment10
	 */
	public String getComment10()
	{
		return getComment10( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment10</code> attribute. 
	 * @param value the comment10
	 */
	public void setComment10(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT10,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment10</code> attribute. 
	 * @param value the comment10
	 */
	public void setComment10(final String value)
	{
		setComment10( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment2</code> attribute.
	 * @return the comment2
	 */
	public String getComment2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment2</code> attribute.
	 * @return the comment2
	 */
	public String getComment2()
	{
		return getComment2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment2</code> attribute. 
	 * @param value the comment2
	 */
	public void setComment2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment2</code> attribute. 
	 * @param value the comment2
	 */
	public void setComment2(final String value)
	{
		setComment2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment3</code> attribute.
	 * @return the comment3
	 */
	public String getComment3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment3</code> attribute.
	 * @return the comment3
	 */
	public String getComment3()
	{
		return getComment3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment3</code> attribute. 
	 * @param value the comment3
	 */
	public void setComment3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment3</code> attribute. 
	 * @param value the comment3
	 */
	public void setComment3(final String value)
	{
		setComment3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment4</code> attribute.
	 * @return the comment4
	 */
	public String getComment4(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT4);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment4</code> attribute.
	 * @return the comment4
	 */
	public String getComment4()
	{
		return getComment4( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment4</code> attribute. 
	 * @param value the comment4
	 */
	public void setComment4(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT4,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment4</code> attribute. 
	 * @param value the comment4
	 */
	public void setComment4(final String value)
	{
		setComment4( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment5</code> attribute.
	 * @return the comment5
	 */
	public String getComment5(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT5);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment5</code> attribute.
	 * @return the comment5
	 */
	public String getComment5()
	{
		return getComment5( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment5</code> attribute. 
	 * @param value the comment5
	 */
	public void setComment5(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT5,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment5</code> attribute. 
	 * @param value the comment5
	 */
	public void setComment5(final String value)
	{
		setComment5( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment6</code> attribute.
	 * @return the comment6
	 */
	public String getComment6(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT6);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment6</code> attribute.
	 * @return the comment6
	 */
	public String getComment6()
	{
		return getComment6( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment6</code> attribute. 
	 * @param value the comment6
	 */
	public void setComment6(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT6,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment6</code> attribute. 
	 * @param value the comment6
	 */
	public void setComment6(final String value)
	{
		setComment6( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment7</code> attribute.
	 * @return the comment7
	 */
	public String getComment7(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT7);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment7</code> attribute.
	 * @return the comment7
	 */
	public String getComment7()
	{
		return getComment7( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment7</code> attribute. 
	 * @param value the comment7
	 */
	public void setComment7(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT7,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment7</code> attribute. 
	 * @param value the comment7
	 */
	public void setComment7(final String value)
	{
		setComment7( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment8</code> attribute.
	 * @return the comment8
	 */
	public String getComment8(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT8);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment8</code> attribute.
	 * @return the comment8
	 */
	public String getComment8()
	{
		return getComment8( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment8</code> attribute. 
	 * @param value the comment8
	 */
	public void setComment8(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT8,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment8</code> attribute. 
	 * @param value the comment8
	 */
	public void setComment8(final String value)
	{
		setComment8( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment9</code> attribute.
	 * @return the comment9
	 */
	public String getComment9(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMENT9);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.comment9</code> attribute.
	 * @return the comment9
	 */
	public String getComment9()
	{
		return getComment9( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment9</code> attribute. 
	 * @param value the comment9
	 */
	public void setComment9(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMENT9,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.comment9</code> attribute. 
	 * @param value the comment9
	 */
	public void setComment9(final String value)
	{
		setComment9( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.cylinderHeadType</code> attribute.
	 * @return the cylinderHeadType
	 */
	public String getCylinderHeadType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CYLINDERHEADTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.cylinderHeadType</code> attribute.
	 * @return the cylinderHeadType
	 */
	public String getCylinderHeadType()
	{
		return getCylinderHeadType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.cylinderHeadType</code> attribute. 
	 * @param value the cylinderHeadType
	 */
	public void setCylinderHeadType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CYLINDERHEADTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.cylinderHeadType</code> attribute. 
	 * @param value the cylinderHeadType
	 */
	public void setCylinderHeadType(final String value)
	{
		setCylinderHeadType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.driveType</code> attribute.
	 * @return the driveType
	 */
	public String getDriveType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DRIVETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.driveType</code> attribute.
	 * @return the driveType
	 */
	public String getDriveType()
	{
		return getDriveType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.driveType</code> attribute. 
	 * @param value the driveType
	 */
	public void setDriveType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DRIVETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.driveType</code> attribute. 
	 * @param value the driveType
	 */
	public void setDriveType(final String value)
	{
		setDriveType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineArrangementNumber</code> attribute.
	 * @return the engineArrangementNumber
	 */
	public String getEngineArrangementNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEARRANGEMENTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineArrangementNumber</code> attribute.
	 * @return the engineArrangementNumber
	 */
	public String getEngineArrangementNumber()
	{
		return getEngineArrangementNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineArrangementNumber</code> attribute. 
	 * @param value the engineArrangementNumber
	 */
	public void setEngineArrangementNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEARRANGEMENTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineArrangementNumber</code> attribute. 
	 * @param value the engineArrangementNumber
	 */
	public void setEngineArrangementNumber(final String value)
	{
		setEngineArrangementNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineBase</code> attribute.
	 * @return the engineBase
	 */
	public String getEngineBase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEBASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineBase</code> attribute.
	 * @return the engineBase
	 */
	public String getEngineBase()
	{
		return getEngineBase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineBase</code> attribute. 
	 * @param value the engineBase
	 */
	public void setEngineBase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEBASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineBase</code> attribute. 
	 * @param value the engineBase
	 */
	public void setEngineBase(final String value)
	{
		setEngineBase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineCPLNumber</code> attribute.
	 * @return the engineCPLNumber
	 */
	public String getEngineCPLNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINECPLNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineCPLNumber</code> attribute.
	 * @return the engineCPLNumber
	 */
	public String getEngineCPLNumber()
	{
		return getEngineCPLNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineCPLNumber</code> attribute. 
	 * @param value the engineCPLNumber
	 */
	public void setEngineCPLNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINECPLNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineCPLNumber</code> attribute. 
	 * @param value the engineCPLNumber
	 */
	public void setEngineCPLNumber(final String value)
	{
		setEngineCPLNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineDesignation</code> attribute.
	 * @return the engineDesignation
	 */
	public String getEngineDesignation(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEDESIGNATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineDesignation</code> attribute.
	 * @return the engineDesignation
	 */
	public String getEngineDesignation()
	{
		return getEngineDesignation( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineDesignation</code> attribute. 
	 * @param value the engineDesignation
	 */
	public void setEngineDesignation(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEDESIGNATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineDesignation</code> attribute. 
	 * @param value the engineDesignation
	 */
	public void setEngineDesignation(final String value)
	{
		setEngineDesignation( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineMfr</code> attribute.
	 * @return the engineMfr
	 */
	public String getEngineMfr(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEMFR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineMfr</code> attribute.
	 * @return the engineMfr
	 */
	public String getEngineMfr()
	{
		return getEngineMfr( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineMfr</code> attribute. 
	 * @param value the engineMfr
	 */
	public void setEngineMfr(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEMFR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineMfr</code> attribute. 
	 * @param value the engineMfr
	 */
	public void setEngineMfr(final String value)
	{
		setEngineMfr( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineSerialNumber</code> attribute.
	 * @return the engineSerialNumber
	 */
	public String getEngineSerialNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINESERIALNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineSerialNumber</code> attribute.
	 * @return the engineSerialNumber
	 */
	public String getEngineSerialNumber()
	{
		return getEngineSerialNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineSerialNumber</code> attribute. 
	 * @param value the engineSerialNumber
	 */
	public void setEngineSerialNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINESERIALNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineSerialNumber</code> attribute. 
	 * @param value the engineSerialNumber
	 */
	public void setEngineSerialNumber(final String value)
	{
		setEngineSerialNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineValves</code> attribute.
	 * @return the engineValves
	 */
	public String getEngineValves(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEVALVES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineValves</code> attribute.
	 * @return the engineValves
	 */
	public String getEngineValves()
	{
		return getEngineValves( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineValves</code> attribute. 
	 * @param value the engineValves
	 */
	public void setEngineValves(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEVALVES,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineValves</code> attribute. 
	 * @param value the engineValves
	 */
	public void setEngineValves(final String value)
	{
		setEngineValves( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineVersion</code> attribute.
	 * @return the engineVersion
	 */
	public String getEngineVersion(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEVERSION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineVersion</code> attribute.
	 * @return the engineVersion
	 */
	public String getEngineVersion()
	{
		return getEngineVersion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineVersion</code> attribute. 
	 * @param value the engineVersion
	 */
	public void setEngineVersion(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEVERSION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineVersion</code> attribute. 
	 * @param value the engineVersion
	 */
	public void setEngineVersion(final String value)
	{
		setEngineVersion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineVIN</code> attribute.
	 * @return the engineVIN
	 */
	public String getEngineVIN(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEVIN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.engineVIN</code> attribute.
	 * @return the engineVIN
	 */
	public String getEngineVIN()
	{
		return getEngineVIN( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineVIN</code> attribute. 
	 * @param value the engineVIN
	 */
	public void setEngineVIN(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEVIN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.engineVIN</code> attribute. 
	 * @param value the engineVIN
	 */
	public void setEngineVIN(final String value)
	{
		setEngineVIN( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.frontBrakeType</code> attribute.
	 * @return the frontBrakeType
	 */
	public String getFrontBrakeType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FRONTBRAKETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.frontBrakeType</code> attribute.
	 * @return the frontBrakeType
	 */
	public String getFrontBrakeType()
	{
		return getFrontBrakeType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.frontBrakeType</code> attribute. 
	 * @param value the frontBrakeType
	 */
	public void setFrontBrakeType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FRONTBRAKETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.frontBrakeType</code> attribute. 
	 * @param value the frontBrakeType
	 */
	public void setFrontBrakeType(final String value)
	{
		setFrontBrakeType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.frontSpringType</code> attribute.
	 * @return the frontSpringType
	 */
	public String getFrontSpringType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FRONTSPRINGTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.frontSpringType</code> attribute.
	 * @return the frontSpringType
	 */
	public String getFrontSpringType()
	{
		return getFrontSpringType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.frontSpringType</code> attribute. 
	 * @param value the frontSpringType
	 */
	public void setFrontSpringType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FRONTSPRINGTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.frontSpringType</code> attribute. 
	 * @param value the frontSpringType
	 */
	public void setFrontSpringType(final String value)
	{
		setFrontSpringType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelDeliverySubType</code> attribute.
	 * @return the fuelDeliverySubType
	 */
	public String getFuelDeliverySubType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FUELDELIVERYSUBTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelDeliverySubType</code> attribute.
	 * @return the fuelDeliverySubType
	 */
	public String getFuelDeliverySubType()
	{
		return getFuelDeliverySubType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelDeliverySubType</code> attribute. 
	 * @param value the fuelDeliverySubType
	 */
	public void setFuelDeliverySubType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FUELDELIVERYSUBTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelDeliverySubType</code> attribute. 
	 * @param value the fuelDeliverySubType
	 */
	public void setFuelDeliverySubType(final String value)
	{
		setFuelDeliverySubType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelDeliveryType</code> attribute.
	 * @return the fuelDeliveryType
	 */
	public String getFuelDeliveryType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FUELDELIVERYTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelDeliveryType</code> attribute.
	 * @return the fuelDeliveryType
	 */
	public String getFuelDeliveryType()
	{
		return getFuelDeliveryType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelDeliveryType</code> attribute. 
	 * @param value the fuelDeliveryType
	 */
	public void setFuelDeliveryType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FUELDELIVERYTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelDeliveryType</code> attribute. 
	 * @param value the fuelDeliveryType
	 */
	public void setFuelDeliveryType(final String value)
	{
		setFuelDeliveryType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelSystemControlType</code> attribute.
	 * @return the fuelSystemControlType
	 */
	public String getFuelSystemControlType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FUELSYSTEMCONTROLTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelSystemControlType</code> attribute.
	 * @return the fuelSystemControlType
	 */
	public String getFuelSystemControlType()
	{
		return getFuelSystemControlType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelSystemControlType</code> attribute. 
	 * @param value the fuelSystemControlType
	 */
	public void setFuelSystemControlType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FUELSYSTEMCONTROLTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelSystemControlType</code> attribute. 
	 * @param value the fuelSystemControlType
	 */
	public void setFuelSystemControlType(final String value)
	{
		setFuelSystemControlType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelSystemDesign</code> attribute.
	 * @return the fuelSystemDesign
	 */
	public String getFuelSystemDesign(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FUELSYSTEMDESIGN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelSystemDesign</code> attribute.
	 * @return the fuelSystemDesign
	 */
	public String getFuelSystemDesign()
	{
		return getFuelSystemDesign( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelSystemDesign</code> attribute. 
	 * @param value the fuelSystemDesign
	 */
	public void setFuelSystemDesign(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FUELSYSTEMDESIGN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelSystemDesign</code> attribute. 
	 * @param value the fuelSystemDesign
	 */
	public void setFuelSystemDesign(final String value)
	{
		setFuelSystemDesign( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelType</code> attribute.
	 * @return the fuelType
	 */
	public String getFuelType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FUELTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.fuelType</code> attribute.
	 * @return the fuelType
	 */
	public String getFuelType()
	{
		return getFuelType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelType</code> attribute. 
	 * @param value the fuelType
	 */
	public void setFuelType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FUELTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.fuelType</code> attribute. 
	 * @param value the fuelType
	 */
	public void setFuelType(final String value)
	{
		setFuelType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.ignitionSystemType</code> attribute.
	 * @return the ignitionSystemType
	 */
	public String getIgnitionSystemType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, IGNITIONSYSTEMTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.ignitionSystemType</code> attribute.
	 * @return the ignitionSystemType
	 */
	public String getIgnitionSystemType()
	{
		return getIgnitionSystemType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.ignitionSystemType</code> attribute. 
	 * @param value the ignitionSystemType
	 */
	public void setIgnitionSystemType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, IGNITIONSYSTEMTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.ignitionSystemType</code> attribute. 
	 * @param value the ignitionSystemType
	 */
	public void setIgnitionSystemType(final String value)
	{
		setIgnitionSystemType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote1</code> attribute.
	 * @return the interchangeNote1
	 */
	public String getInterchangeNote1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote1</code> attribute.
	 * @return the interchangeNote1
	 */
	public String getInterchangeNote1()
	{
		return getInterchangeNote1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote1</code> attribute. 
	 * @param value the interchangeNote1
	 */
	public void setInterchangeNote1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote1</code> attribute. 
	 * @param value the interchangeNote1
	 */
	public void setInterchangeNote1(final String value)
	{
		setInterchangeNote1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote10</code> attribute.
	 * @return the interchangeNote10
	 */
	public String getInterchangeNote10(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE10);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote10</code> attribute.
	 * @return the interchangeNote10
	 */
	public String getInterchangeNote10()
	{
		return getInterchangeNote10( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote10</code> attribute. 
	 * @param value the interchangeNote10
	 */
	public void setInterchangeNote10(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE10,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote10</code> attribute. 
	 * @param value the interchangeNote10
	 */
	public void setInterchangeNote10(final String value)
	{
		setInterchangeNote10( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote2</code> attribute.
	 * @return the interchangeNote2
	 */
	public String getInterchangeNote2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote2</code> attribute.
	 * @return the interchangeNote2
	 */
	public String getInterchangeNote2()
	{
		return getInterchangeNote2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote2</code> attribute. 
	 * @param value the interchangeNote2
	 */
	public void setInterchangeNote2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote2</code> attribute. 
	 * @param value the interchangeNote2
	 */
	public void setInterchangeNote2(final String value)
	{
		setInterchangeNote2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote3</code> attribute.
	 * @return the interchangeNote3
	 */
	public String getInterchangeNote3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote3</code> attribute.
	 * @return the interchangeNote3
	 */
	public String getInterchangeNote3()
	{
		return getInterchangeNote3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote3</code> attribute. 
	 * @param value the interchangeNote3
	 */
	public void setInterchangeNote3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote3</code> attribute. 
	 * @param value the interchangeNote3
	 */
	public void setInterchangeNote3(final String value)
	{
		setInterchangeNote3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote4</code> attribute.
	 * @return the interchangeNote4
	 */
	public String getInterchangeNote4(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE4);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote4</code> attribute.
	 * @return the interchangeNote4
	 */
	public String getInterchangeNote4()
	{
		return getInterchangeNote4( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote4</code> attribute. 
	 * @param value the interchangeNote4
	 */
	public void setInterchangeNote4(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE4,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote4</code> attribute. 
	 * @param value the interchangeNote4
	 */
	public void setInterchangeNote4(final String value)
	{
		setInterchangeNote4( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote5</code> attribute.
	 * @return the interchangeNote5
	 */
	public String getInterchangeNote5(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE5);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote5</code> attribute.
	 * @return the interchangeNote5
	 */
	public String getInterchangeNote5()
	{
		return getInterchangeNote5( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote5</code> attribute. 
	 * @param value the interchangeNote5
	 */
	public void setInterchangeNote5(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE5,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote5</code> attribute. 
	 * @param value the interchangeNote5
	 */
	public void setInterchangeNote5(final String value)
	{
		setInterchangeNote5( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote6</code> attribute.
	 * @return the interchangeNote6
	 */
	public String getInterchangeNote6(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE6);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote6</code> attribute.
	 * @return the interchangeNote6
	 */
	public String getInterchangeNote6()
	{
		return getInterchangeNote6( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote6</code> attribute. 
	 * @param value the interchangeNote6
	 */
	public void setInterchangeNote6(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE6,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote6</code> attribute. 
	 * @param value the interchangeNote6
	 */
	public void setInterchangeNote6(final String value)
	{
		setInterchangeNote6( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote7</code> attribute.
	 * @return the interchangeNote7
	 */
	public String getInterchangeNote7(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE7);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote7</code> attribute.
	 * @return the interchangeNote7
	 */
	public String getInterchangeNote7()
	{
		return getInterchangeNote7( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote7</code> attribute. 
	 * @param value the interchangeNote7
	 */
	public void setInterchangeNote7(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE7,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote7</code> attribute. 
	 * @param value the interchangeNote7
	 */
	public void setInterchangeNote7(final String value)
	{
		setInterchangeNote7( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote8</code> attribute.
	 * @return the interchangeNote8
	 */
	public String getInterchangeNote8(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE8);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote8</code> attribute.
	 * @return the interchangeNote8
	 */
	public String getInterchangeNote8()
	{
		return getInterchangeNote8( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote8</code> attribute. 
	 * @param value the interchangeNote8
	 */
	public void setInterchangeNote8(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE8,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote8</code> attribute. 
	 * @param value the interchangeNote8
	 */
	public void setInterchangeNote8(final String value)
	{
		setInterchangeNote8( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote9</code> attribute.
	 * @return the interchangeNote9
	 */
	public String getInterchangeNote9(final SessionContext ctx)
	{
		return (String)getProperty( ctx, INTERCHANGENOTE9);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.interchangeNote9</code> attribute.
	 * @return the interchangeNote9
	 */
	public String getInterchangeNote9()
	{
		return getInterchangeNote9( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote9</code> attribute. 
	 * @param value the interchangeNote9
	 */
	public void setInterchangeNote9(final SessionContext ctx, final String value)
	{
		setProperty(ctx, INTERCHANGENOTE9,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.interchangeNote9</code> attribute. 
	 * @param value the interchangeNote9
	 */
	public void setInterchangeNote9(final String value)
	{
		setInterchangeNote9( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.make</code> attribute.
	 * @return the make - component Id
	 */
	public String getMake(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MAKE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.make</code> attribute.
	 * @return the make - component Id
	 */
	public String getMake()
	{
		return getMake( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.make</code> attribute. 
	 * @param value the make - component Id
	 */
	public void setMake(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MAKE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.make</code> attribute. 
	 * @param value the make - component Id
	 */
	public void setMake(final String value)
	{
		setMake( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.mfrBodyCode</code> attribute.
	 * @return the mfrBodyCode
	 */
	public String getMfrBodyCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MFRBODYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.mfrBodyCode</code> attribute.
	 * @return the mfrBodyCode
	 */
	public String getMfrBodyCode()
	{
		return getMfrBodyCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.mfrBodyCode</code> attribute. 
	 * @param value the mfrBodyCode
	 */
	public void setMfrBodyCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MFRBODYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.mfrBodyCode</code> attribute. 
	 * @param value the mfrBodyCode
	 */
	public void setMfrBodyCode(final String value)
	{
		setMfrBodyCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.mfrLabel</code> attribute.
	 * @return the mfrLabel
	 */
	public String getMfrLabel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MFRLABEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.mfrLabel</code> attribute.
	 * @return the mfrLabel
	 */
	public String getMfrLabel()
	{
		return getMfrLabel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.mfrLabel</code> attribute. 
	 * @param value the mfrLabel
	 */
	public void setMfrLabel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MFRLABEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.mfrLabel</code> attribute. 
	 * @param value the mfrLabel
	 */
	public void setMfrLabel(final String value)
	{
		setMfrLabel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.model</code> attribute.
	 * @return the model - connection for Reason
	 */
	public String getModel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.model</code> attribute.
	 * @return the model - connection for Reason
	 */
	public String getModel()
	{
		return getModel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.model</code> attribute. 
	 * @param value the model - connection for Reason
	 */
	public void setModel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.model</code> attribute. 
	 * @param value the model - connection for Reason
	 */
	public void setModel(final String value)
	{
		setModel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.partNumber</code> attribute.
	 * @return the partNumber
	 */
	public String getPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.partNumber</code> attribute.
	 * @return the partNumber
	 */
	public String getPartNumber()
	{
		return getPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.partNumber</code> attribute. 
	 * @param value the partNumber
	 */
	public void setPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.partNumber</code> attribute. 
	 * @param value the partNumber
	 */
	public void setPartNumber(final String value)
	{
		setPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.parts</code> attribute.
	 * @return the parts - Products
	 */
	public List<FMPart> getParts(final SessionContext ctx)
	{
		final List<FMPart> items = getLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.parts</code> attribute.
	 * @return the parts - Products
	 */
	public List<FMPart> getParts()
	{
		return getParts( getSession().getSessionContext() );
	}
	
	public long getPartsCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			true,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null
		);
	}
	
	public long getPartsCount()
	{
		return getPartsCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.parts</code> attribute. 
	 * @param value the parts - Products
	 */
	public void setParts(final SessionContext ctx, final List<FMPart> value)
	{
		setLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			value,
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.parts</code> attribute. 
	 * @param value the parts - Products
	 */
	public void setParts(final List<FMPart> value)
	{
		setParts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to parts. 
	 * @param value the item to add to parts - Products
	 */
	public void addToParts(final SessionContext ctx, final FMPart value)
	{
		addLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to parts. 
	 * @param value the item to add to parts - Products
	 */
	public void addToParts(final FMPart value)
	{
		addToParts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from parts. 
	 * @param value the item to remove from parts - Products
	 */
	public void removeFromParts(final SessionContext ctx, final FMPart value)
	{
		removeLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from parts. 
	 * @param value the item to remove from parts - Products
	 */
	public void removeFromParts(final FMPart value)
	{
		removeFromParts( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.position</code> attribute.
	 * @return the position
	 */
	public String getPosition(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POSITION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.position</code> attribute.
	 * @return the position
	 */
	public String getPosition()
	{
		return getPosition( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.position</code> attribute. 
	 * @param value the position
	 */
	public void setPosition(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POSITION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.position</code> attribute. 
	 * @param value the position
	 */
	public void setPosition(final String value)
	{
		setPosition( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.powerOutput</code> attribute.
	 * @return the powerOutput
	 */
	public String getPowerOutput(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POWEROUTPUT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.powerOutput</code> attribute.
	 * @return the powerOutput
	 */
	public String getPowerOutput()
	{
		return getPowerOutput( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.powerOutput</code> attribute. 
	 * @param value the powerOutput
	 */
	public void setPowerOutput(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POWEROUTPUT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.powerOutput</code> attribute. 
	 * @param value the powerOutput
	 */
	public void setPowerOutput(final String value)
	{
		setPowerOutput( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.product</code> attribute.
	 * @return the product - component InstId
	 */
	public String getProduct(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.product</code> attribute.
	 * @return the product - component InstId
	 */
	public String getProduct()
	{
		return getProduct( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.product</code> attribute. 
	 * @param value the product - component InstId
	 */
	public void setProduct(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.product</code> attribute. 
	 * @param value the product - component InstId
	 */
	public void setProduct(final String value)
	{
		setProduct( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.rearBrakeType</code> attribute.
	 * @return the rearBrakeType
	 */
	public String getRearBrakeType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REARBRAKETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.rearBrakeType</code> attribute.
	 * @return the rearBrakeType
	 */
	public String getRearBrakeType()
	{
		return getRearBrakeType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.rearBrakeType</code> attribute. 
	 * @param value the rearBrakeType
	 */
	public void setRearBrakeType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REARBRAKETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.rearBrakeType</code> attribute. 
	 * @param value the rearBrakeType
	 */
	public void setRearBrakeType(final String value)
	{
		setRearBrakeType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.rearSpringType</code> attribute.
	 * @return the rearSpringType
	 */
	public String getRearSpringType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REARSPRINGTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.rearSpringType</code> attribute.
	 * @return the rearSpringType
	 */
	public String getRearSpringType()
	{
		return getRearSpringType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.rearSpringType</code> attribute. 
	 * @param value the rearSpringType
	 */
	public void setRearSpringType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REARSPRINGTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.rearSpringType</code> attribute. 
	 * @param value the rearSpringType
	 */
	public void setRearSpringType(final String value)
	{
		setRearSpringType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.region</code> attribute.
	 * @return the region
	 */
	public String getRegion(final SessionContext ctx)
	{
		return (String)getProperty( ctx, REGION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.region</code> attribute.
	 * @return the region
	 */
	public String getRegion()
	{
		return getRegion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.region</code> attribute. 
	 * @param value the region
	 */
	public void setRegion(final SessionContext ctx, final String value)
	{
		setProperty(ctx, REGION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.region</code> attribute. 
	 * @param value the region
	 */
	public void setRegion(final String value)
	{
		setRegion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.steeringSystem</code> attribute.
	 * @return the steeringSystem
	 */
	public String getSteeringSystem(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STEERINGSYSTEM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.steeringSystem</code> attribute.
	 * @return the steeringSystem
	 */
	public String getSteeringSystem()
	{
		return getSteeringSystem( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.steeringSystem</code> attribute. 
	 * @param value the steeringSystem
	 */
	public void setSteeringSystem(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STEERINGSYSTEM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.steeringSystem</code> attribute. 
	 * @param value the steeringSystem
	 */
	public void setSteeringSystem(final String value)
	{
		setSteeringSystem( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.steeringType</code> attribute.
	 * @return the steeringType
	 */
	public String getSteeringType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STEERINGTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.steeringType</code> attribute.
	 * @return the steeringType
	 */
	public String getSteeringType()
	{
		return getSteeringType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.steeringType</code> attribute. 
	 * @param value the steeringType
	 */
	public void setSteeringType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STEERINGTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.steeringType</code> attribute. 
	 * @param value the steeringType
	 */
	public void setSteeringType(final String value)
	{
		setSteeringType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.submodel</code> attribute.
	 * @return the submodel
	 */
	public String getSubmodel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUBMODEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.submodel</code> attribute.
	 * @return the submodel
	 */
	public String getSubmodel()
	{
		return getSubmodel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.submodel</code> attribute. 
	 * @param value the submodel
	 */
	public void setSubmodel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUBMODEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.submodel</code> attribute. 
	 * @param value the submodel
	 */
	public void setSubmodel(final String value)
	{
		setSubmodel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionControlType</code> attribute.
	 * @return the transmissionControlType
	 */
	public String getTransmissionControlType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSMISSIONCONTROLTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionControlType</code> attribute.
	 * @return the transmissionControlType
	 */
	public String getTransmissionControlType()
	{
		return getTransmissionControlType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionControlType</code> attribute. 
	 * @param value the transmissionControlType
	 */
	public void setTransmissionControlType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSMISSIONCONTROLTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionControlType</code> attribute. 
	 * @param value the transmissionControlType
	 */
	public void setTransmissionControlType(final String value)
	{
		setTransmissionControlType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionMfrCode</code> attribute.
	 * @return the transmissionMfrCode
	 */
	public String getTransmissionMfrCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSMISSIONMFRCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionMfrCode</code> attribute.
	 * @return the transmissionMfrCode
	 */
	public String getTransmissionMfrCode()
	{
		return getTransmissionMfrCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionMfrCode</code> attribute. 
	 * @param value the transmissionMfrCode
	 */
	public void setTransmissionMfrCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSMISSIONMFRCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionMfrCode</code> attribute. 
	 * @param value the transmissionMfrCode
	 */
	public void setTransmissionMfrCode(final String value)
	{
		setTransmissionMfrCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionNumSpeeds</code> attribute.
	 * @return the transmissionNumSpeeds
	 */
	public String getTransmissionNumSpeeds(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSMISSIONNUMSPEEDS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionNumSpeeds</code> attribute.
	 * @return the transmissionNumSpeeds
	 */
	public String getTransmissionNumSpeeds()
	{
		return getTransmissionNumSpeeds( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionNumSpeeds</code> attribute. 
	 * @param value the transmissionNumSpeeds
	 */
	public void setTransmissionNumSpeeds(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSMISSIONNUMSPEEDS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionNumSpeeds</code> attribute. 
	 * @param value the transmissionNumSpeeds
	 */
	public void setTransmissionNumSpeeds(final String value)
	{
		setTransmissionNumSpeeds( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionType</code> attribute.
	 * @return the transmissionType
	 */
	public String getTransmissionType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSMISSIONTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.transmissionType</code> attribute.
	 * @return the transmissionType
	 */
	public String getTransmissionType()
	{
		return getTransmissionType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionType</code> attribute. 
	 * @param value the transmissionType
	 */
	public void setTransmissionType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSMISSIONTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.transmissionType</code> attribute. 
	 * @param value the transmissionType
	 */
	public void setTransmissionType(final String value)
	{
		setTransmissionType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehiclesegment</code> attribute.
	 * @return the vehiclesegment - component InstId
	 */
	public String getVehiclesegment(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VEHICLESEGMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehiclesegment</code> attribute.
	 * @return the vehiclesegment - component InstId
	 */
	public String getVehiclesegment()
	{
		return getVehiclesegment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehiclesegment</code> attribute. 
	 * @param value the vehiclesegment - component InstId
	 */
	public void setVehiclesegment(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VEHICLESEGMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehiclesegment</code> attribute. 
	 * @param value the vehiclesegment - component InstId
	 */
	public void setVehiclesegment(final String value)
	{
		setVehiclesegment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehicleSeries</code> attribute.
	 * @return the vehicleSeries
	 */
	public String getVehicleSeries(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VEHICLESERIES);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehicleSeries</code> attribute.
	 * @return the vehicleSeries
	 */
	public String getVehicleSeries()
	{
		return getVehicleSeries( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehicleSeries</code> attribute. 
	 * @param value the vehicleSeries
	 */
	public void setVehicleSeries(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VEHICLESERIES,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehicleSeries</code> attribute. 
	 * @param value the vehicleSeries
	 */
	public void setVehicleSeries(final String value)
	{
		setVehicleSeries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehicleType</code> attribute.
	 * @return the vehicleType
	 */
	public String getVehicleType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VEHICLETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.vehicleType</code> attribute.
	 * @return the vehicleType
	 */
	public String getVehicleType()
	{
		return getVehicleType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehicleType</code> attribute. 
	 * @param value the vehicleType
	 */
	public void setVehicleType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VEHICLETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.vehicleType</code> attribute. 
	 * @param value the vehicleType
	 */
	public void setVehicleType(final String value)
	{
		setVehicleType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.wheelBase</code> attribute.
	 * @return the wheelBase
	 */
	public String getWheelBase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WHEELBASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.wheelBase</code> attribute.
	 * @return the wheelBase
	 */
	public String getWheelBase()
	{
		return getWheelBase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.wheelBase</code> attribute. 
	 * @param value the wheelBase
	 */
	public void setWheelBase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WHEELBASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.wheelBase</code> attribute. 
	 * @param value the wheelBase
	 */
	public void setWheelBase(final String value)
	{
		setWheelBase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.year</code> attribute.
	 * @return the year - component InstId
	 */
	public String getYear(final SessionContext ctx)
	{
		return (String)getProperty( ctx, YEAR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.year</code> attribute.
	 * @return the year - component InstId
	 */
	public String getYear()
	{
		return getYear( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.year</code> attribute. 
	 * @param value the year - component InstId
	 */
	public void setYear(final SessionContext ctx, final String value)
	{
		setProperty(ctx, YEAR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.year</code> attribute. 
	 * @param value the year - component InstId
	 */
	public void setYear(final String value)
	{
		setYear( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.ymmcode</code> attribute.
	 * @return the ymmcode - YearMakeModel
	 */
	public String getYmmcode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, YMMCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMFitment.ymmcode</code> attribute.
	 * @return the ymmcode - YearMakeModel
	 */
	public String getYmmcode()
	{
		return getYmmcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.ymmcode</code> attribute. 
	 * @param value the ymmcode - YearMakeModel
	 */
	public void setYmmcode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, YMMCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMFitment.ymmcode</code> attribute. 
	 * @param value the ymmcode - YearMakeModel
	 */
	public void setYmmcode(final String value)
	{
		setYmmcode( getSession().getSessionContext(), value );
	}
	
}
