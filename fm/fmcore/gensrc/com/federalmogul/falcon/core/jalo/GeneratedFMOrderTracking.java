/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMOrderTracking FMShipperOrderTracking}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMOrderTracking extends GenericItem
{
	/** Qualifier of the <code>FMShipperOrderTracking.activeFlag</code> attribute **/
	public static final String ACTIVEFLAG = "activeFlag";
	/** Qualifier of the <code>FMShipperOrderTracking.ShipperName</code> attribute **/
	public static final String SHIPPERNAME = "ShipperName";
	/** Qualifier of the <code>FMShipperOrderTracking.shipperURL</code> attribute **/
	public static final String SHIPPERURL = "shipperURL";
	/** Qualifier of the <code>FMShipperOrderTracking.shipperId</code> attribute **/
	public static final String SHIPPERID = "shipperId";
	/** Qualifier of the <code>FMShipperOrderTracking.shipperCode</code> attribute **/
	public static final String SHIPPERCODE = "shipperCode";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ACTIVEFLAG, AttributeMode.INITIAL);
		tmp.put(SHIPPERNAME, AttributeMode.INITIAL);
		tmp.put(SHIPPERURL, AttributeMode.INITIAL);
		tmp.put(SHIPPERID, AttributeMode.INITIAL);
		tmp.put(SHIPPERCODE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.activeFlag</code> attribute.
	 * @return the activeFlag - activeFlag
	 */
	public Boolean isActiveFlag(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, ACTIVEFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.activeFlag</code> attribute.
	 * @return the activeFlag - activeFlag
	 */
	public Boolean isActiveFlag()
	{
		return isActiveFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @return the activeFlag - activeFlag
	 */
	public boolean isActiveFlagAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isActiveFlag( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @return the activeFlag - activeFlag
	 */
	public boolean isActiveFlagAsPrimitive()
	{
		return isActiveFlagAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @param value the activeFlag - activeFlag
	 */
	public void setActiveFlag(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, ACTIVEFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @param value the activeFlag - activeFlag
	 */
	public void setActiveFlag(final Boolean value)
	{
		setActiveFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @param value the activeFlag - activeFlag
	 */
	public void setActiveFlag(final SessionContext ctx, final boolean value)
	{
		setActiveFlag( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.activeFlag</code> attribute. 
	 * @param value the activeFlag - activeFlag
	 */
	public void setActiveFlag(final boolean value)
	{
		setActiveFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperCode</code> attribute.
	 * @return the shipperCode - EXTERNAL_SYSTEM_SHIPPER_CD
	 */
	public String getShipperCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperCode</code> attribute.
	 * @return the shipperCode - EXTERNAL_SYSTEM_SHIPPER_CD
	 */
	public String getShipperCode()
	{
		return getShipperCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperCode</code> attribute. 
	 * @param value the shipperCode - EXTERNAL_SYSTEM_SHIPPER_CD
	 */
	public void setShipperCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperCode</code> attribute. 
	 * @param value the shipperCode - EXTERNAL_SYSTEM_SHIPPER_CD
	 */
	public void setShipperCode(final String value)
	{
		setShipperCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperId</code> attribute.
	 * @return the shipperId - SHIPPER_ID
	 */
	public String getShipperId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperId</code> attribute.
	 * @return the shipperId - SHIPPER_ID
	 */
	public String getShipperId()
	{
		return getShipperId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperId</code> attribute. 
	 * @param value the shipperId - SHIPPER_ID
	 */
	public void setShipperId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperId</code> attribute. 
	 * @param value the shipperId - SHIPPER_ID
	 */
	public void setShipperId(final String value)
	{
		setShipperId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.ShipperName</code> attribute.
	 * @return the ShipperName - ShipperName
	 */
	public String getShipperName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.ShipperName</code> attribute.
	 * @return the ShipperName - ShipperName
	 */
	public String getShipperName()
	{
		return getShipperName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.ShipperName</code> attribute. 
	 * @param value the ShipperName - ShipperName
	 */
	public void setShipperName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.ShipperName</code> attribute. 
	 * @param value the ShipperName - ShipperName
	 */
	public void setShipperName(final String value)
	{
		setShipperName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperURL</code> attribute.
	 * @return the shipperURL - SHIPPER_URL
	 */
	public String getShipperURL(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPERURL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMShipperOrderTracking.shipperURL</code> attribute.
	 * @return the shipperURL - SHIPPER_URL
	 */
	public String getShipperURL()
	{
		return getShipperURL( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperURL</code> attribute. 
	 * @param value the shipperURL - SHIPPER_URL
	 */
	public void setShipperURL(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPERURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMShipperOrderTracking.shipperURL</code> attribute. 
	 * @param value the shipperURL - SHIPPER_URL
	 */
	public void setShipperURL(final String value)
	{
		setShipperURL( getSession().getSessionContext(), value );
	}
	
}
