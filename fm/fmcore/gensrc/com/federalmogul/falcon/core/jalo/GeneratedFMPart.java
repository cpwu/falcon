/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.falcon.core.jalo.FMCorporate;
import com.federalmogul.falcon.core.jalo.FMFitment;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.product.Product;
import de.hybris.platform.util.Utilities;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMPart FMPart}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMPart extends Product
{
	/** Qualifier of the <code>FMPart.squashedPartNumber</code> attribute **/
	public static final String SQUASHEDPARTNUMBER = "squashedPartNumber";
	/** Qualifier of the <code>FMPart.partSequence</code> attribute **/
	public static final String PARTSEQUENCE = "partSequence";
	/** Qualifier of the <code>FMPart.packageEachWeight</code> attribute **/
	public static final String PACKAGEEACHWEIGHT = "packageEachWeight";
	/** Qualifier of the <code>FMPart.faqAnswer1</code> attribute **/
	public static final String FAQANSWER1 = "faqAnswer1";
	/** Qualifier of the <code>FMPart.weightInnerPackage</code> attribute **/
	public static final String WEIGHTINNERPACKAGE = "weightInnerPackage";
	/** Qualifier of the <code>FMPart.advanceBuys</code> attribute **/
	public static final String ADVANCEBUYS = "advanceBuys";
	/** Qualifier of the <code>FMPart.palletFootprintSize</code> attribute **/
	public static final String PALLETFOOTPRINTSIZE = "palletFootprintSize";
	/** Qualifier of the <code>FMPart.weightUOM</code> attribute **/
	public static final String WEIGHTUOM = "weightUOM";
	/** Qualifier of the <code>FMPart.highPerformance</code> attribute **/
	public static final String HIGHPERFORMANCE = "highPerformance";
	/** Qualifier of the <code>FMPart.pid</code> attribute **/
	public static final String PID = "pid";
	/** Qualifier of the <code>FMPart.warrantydescription</code> attribute **/
	public static final String WARRANTYDESCRIPTION = "warrantydescription";
	/** Qualifier of the <code>FMPart.faqAnswer2</code> attribute **/
	public static final String FAQANSWER2 = "faqAnswer2";
	/** Qualifier of the <code>FMPart.snowMobile</code> attribute **/
	public static final String SNOWMOBILE = "snowMobile";
	/** Qualifier of the <code>FMPart.faqAnswer3</code> attribute **/
	public static final String FAQANSWER3 = "faqAnswer3";
	/** Qualifier of the <code>FMPart.priceSheetNumber</code> attribute **/
	public static final String PRICESHEETNUMBER = "priceSheetNumber";
	/** Qualifier of the <code>FMPart.mediumDutyTruck</code> attribute **/
	public static final String MEDIUMDUTYTRUCK = "mediumDutyTruck";
	/** Qualifier of the <code>FMPart.itemQuantitySizeUOM</code> attribute **/
	public static final String ITEMQUANTITYSIZEUOM = "itemQuantitySizeUOM";
	/** Qualifier of the <code>FMPart.itemLevelGTINUPC</code> attribute **/
	public static final String ITEMLEVELGTINUPC = "itemLevelGTINUPC";
	/** Qualifier of the <code>FMPart.widthCase</code> attribute **/
	public static final String WIDTHCASE = "widthCase";
	/** Qualifier of the <code>FMPart.lengthInnerPackage</code> attribute **/
	public static final String LENGTHINNERPACKAGE = "lengthInnerPackage";
	/** Qualifier of the <code>FMPart.aAPPLANumberAN3</code> attribute **/
	public static final String AAPPLANUMBERAN3 = "aAPPLANumberAN3";
	/** Qualifier of the <code>FMPart.minimumOrderQuantity</code> attribute **/
	public static final String MINIMUMORDERQUANTITY = "minimumOrderQuantity";
	/** Qualifier of the <code>FMPart.quantityOfEachesEachPackage</code> attribute **/
	public static final String QUANTITYOFEACHESEACHPACKAGE = "quantityOfEachesEachPackage";
	/** Qualifier of the <code>FMPart.productCategoryCode</code> attribute **/
	public static final String PRODUCTCATEGORYCODE = "productCategoryCode";
	/** Qualifier of the <code>FMPart.faqQestion1</code> attribute **/
	public static final String FAQQESTION1 = "faqQestion1";
	/** Qualifier of the <code>FMPart.nAPABuys</code> attribute **/
	public static final String NAPABUYS = "nAPABuys";
	/** Qualifier of the <code>FMPart.faqAnswer4</code> attribute **/
	public static final String FAQANSWER4 = "faqAnswer4";
	/** Qualifier of the <code>FMPart.brandAAIAID</code> attribute **/
	public static final String BRANDAAIAID = "brandAAIAID";
	/** Qualifier of the <code>FMPart.packageEachLevelGTINUPC</code> attribute **/
	public static final String PACKAGEEACHLEVELGTINUPC = "packageEachLevelGTINUPC";
	/** Qualifier of the <code>FMPart.palletLayerMax</code> attribute **/
	public static final String PALLETLAYERMAX = "palletLayerMax";
	/** Qualifier of the <code>FMPart.packageEachLength</code> attribute **/
	public static final String PACKAGEEACHLENGTH = "packageEachLength";
	/** Qualifier of the <code>FMPart.faqQestion5</code> attribute **/
	public static final String FAQQESTION5 = "faqQestion5";
	/** Qualifier of the <code>FMPart.nationalPopularityCode</code> attribute **/
	public static final String NATIONALPOPULARITYCODE = "nationalPopularityCode";
	/** Qualifier of the <code>FMPart.faqQestion2</code> attribute **/
	public static final String FAQQESTION2 = "faqQestion2";
	/** Qualifier of the <code>FMPart.qtyOfEachesinPkgInnerPackage</code> attribute **/
	public static final String QTYOFEACHESINPKGINNERPACKAGE = "qtyOfEachesinPkgInnerPackage";
	/** Qualifier of the <code>FMPart.harmonizedTariffCode</code> attribute **/
	public static final String HARMONIZEDTARIFFCODE = "harmonizedTariffCode";
	/** Qualifier of the <code>FMPart.packageUOM</code> attribute **/
	public static final String PACKAGEUOM = "packageUOM";
	/** Qualifier of the <code>FMPart.upc</code> attribute **/
	public static final String UPC = "upc";
	/** Qualifier of the <code>FMPart.countryOfOriginPrimary</code> attribute **/
	public static final String COUNTRYOFORIGINPRIMARY = "countryOfOriginPrimary";
	/** Qualifier of the <code>FMPart.textfootnote</code> attribute **/
	public static final String TEXTFOOTNOTE = "textfootnote";
	/** Qualifier of the <code>FMPart.sqshPtNbr</code> attribute **/
	public static final String SQSHPTNBR = "sqshPtNbr";
	/** Qualifier of the <code>FMPart.itemLevelGTINQualifier</code> attribute **/
	public static final String ITEMLEVELGTINQUALIFIER = "itemLevelGTINQualifier";
	/** Qualifier of the <code>FMPart.textcomment</code> attribute **/
	public static final String TEXTCOMMENT = "textcomment";
	/** Qualifier of the <code>FMPart.heightInnerPackage</code> attribute **/
	public static final String HEIGHTINNERPACKAGE = "heightInnerPackage";
	/** Qualifier of the <code>FMPart.faqQestion4</code> attribute **/
	public static final String FAQQESTION4 = "faqQestion4";
	/** Qualifier of the <code>FMPart.quantityPerApplication</code> attribute **/
	public static final String QUANTITYPERAPPLICATION = "quantityPerApplication";
	/** Qualifier of the <code>FMPart.autoZoneBuys</code> attribute **/
	public static final String AUTOZONEBUYS = "autoZoneBuys";
	/** Qualifier of the <code>FMPart.dimensionsUOM</code> attribute **/
	public static final String DIMENSIONSUOM = "dimensionsUOM";
	/** Qualifier of the <code>FMPart.priceSheetEffectiveDate</code> attribute **/
	public static final String PRICESHEETEFFECTIVEDATE = "priceSheetEffectiveDate";
	/** Qualifier of the <code>FMPart.quantityOfEachesinPackageCase</code> attribute **/
	public static final String QUANTITYOFEACHESINPACKAGECASE = "quantityOfEachesinPackageCase";
	/** Qualifier of the <code>FMPart.partNumberOld</code> attribute **/
	public static final String PARTNUMBEROLD = "partNumberOld";
	/** Qualifier of the <code>FMPart.hazardousClassCode</code> attribute **/
	public static final String HAZARDOUSCLASSCODE = "hazardousClassCode";
	/** Qualifier of the <code>FMPart.mSDSSheetAvailable</code> attribute **/
	public static final String MSDSSHEETAVAILABLE = "mSDSSheetAvailable";
	/** Qualifier of the <code>FMPart.sentImagetoAdvance</code> attribute **/
	public static final String SENTIMAGETOADVANCE = "sentImagetoAdvance";
	/** Qualifier of the <code>FMPart.partNumberSupersededTo</code> attribute **/
	public static final String PARTNUMBERSUPERSEDEDTO = "partNumberSupersededTo";
	/** Qualifier of the <code>FMPart.itemQuantitySize</code> attribute **/
	public static final String ITEMQUANTITYSIZE = "itemQuantitySize";
	/** Qualifier of the <code>FMPart.fitments</code> attribute **/
	public static final String FITMENTS = "fitments";
	/** Relation ordering override parameter constants for FitmentPartRelation from ((fmcore))*/
	protected static String FITMENTPARTRELATION_SRC_ORDERED = "relation.FitmentPartRelation.source.ordered";
	protected static String FITMENTPARTRELATION_TGT_ORDERED = "relation.FitmentPartRelation.target.ordered";
	/** Relation disable markmodifed parameter constants for FitmentPartRelation from ((fmcore))*/
	protected static String FITMENTPARTRELATION_MARKMODIFIED = "relation.FitmentPartRelation.markmodified";
	/** Qualifier of the <code>FMPart.nAPAPartNumber</code> attribute **/
	public static final String NAPAPARTNUMBER = "nAPAPartNumber";
	/** Qualifier of the <code>FMPart.alternatePartNumber</code> attribute **/
	public static final String ALTERNATEPARTNUMBER = "alternatePartNumber";
	/** Qualifier of the <code>FMPart.maxCasePerPalletLayer</code> attribute **/
	public static final String MAXCASEPERPALLETLAYER = "maxCasePerPalletLayer";
	/** Qualifier of the <code>FMPart.packageEachHeight</code> attribute **/
	public static final String PACKAGEEACHHEIGHT = "packageEachHeight";
	/** Qualifier of the <code>FMPart.faqAnswer5</code> attribute **/
	public static final String FAQANSWER5 = "faqAnswer5";
	/** Qualifier of the <code>FMPart.weightCase</code> attribute **/
	public static final String WEIGHTCASE = "weightCase";
	/** Qualifier of the <code>FMPart.productGroupCode</code> attribute **/
	public static final String PRODUCTGROUPCODE = "productGroupCode";
	/** Qualifier of the <code>FMPart.palletFootprintSizeUOM</code> attribute **/
	public static final String PALLETFOOTPRINTSIZEUOM = "palletFootprintSizeUOM";
	/** Qualifier of the <code>FMPart.taxable</code> attribute **/
	public static final String TAXABLE = "taxable";
	/** Qualifier of the <code>FMPart.marine</code> attribute **/
	public static final String MARINE = "marine";
	/** Qualifier of the <code>FMPart.faqQestion3</code> attribute **/
	public static final String FAQQESTION3 = "faqQestion3";
	/** Qualifier of the <code>FMPart.vehicleRegistration</code> attribute **/
	public static final String VEHICLEREGISTRATION = "vehicleRegistration";
	/** Qualifier of the <code>FMPart.popularityCode</code> attribute **/
	public static final String POPULARITYCODE = "popularityCode";
	/** Qualifier of the <code>FMPart.flagcode</code> attribute **/
	public static final String FLAGCODE = "flagcode";
	/** Qualifier of the <code>FMPart.quantityPerApplicationUOM</code> attribute **/
	public static final String QUANTITYPERAPPLICATIONUOM = "quantityPerApplicationUOM";
	/** Qualifier of the <code>FMPart.smallEngine</code> attribute **/
	public static final String SMALLENGINE = "smallEngine";
	/** Qualifier of the <code>FMPart.longDescription</code> attribute **/
	public static final String LONGDESCRIPTION = "longDescription";
	/** Qualifier of the <code>FMPart.mSDSSheetNumber</code> attribute **/
	public static final String MSDSSHEETNUMBER = "mSDSSheetNumber";
	/** Qualifier of the <code>FMPart.autoZonePartNumber</code> attribute **/
	public static final String AUTOZONEPARTNUMBER = "autoZonePartNumber";
	/** Qualifier of the <code>FMPart.jobberPrice</code> attribute **/
	public static final String JOBBERPRICE = "jobberPrice";
	/** Qualifier of the <code>FMPart.motorcycleATV</code> attribute **/
	public static final String MOTORCYCLEATV = "motorcycleATV";
	/** Qualifier of the <code>FMPart.supersededPriceSheetNumber</code> attribute **/
	public static final String SUPERSEDEDPRICESHEETNUMBER = "supersededPriceSheetNumber";
	/** Qualifier of the <code>FMPart.caseBarCodeCharacters</code> attribute **/
	public static final String CASEBARCODECHARACTERS = "caseBarCodeCharacters";
	/** Qualifier of the <code>FMPart.eachPackageBarCodeCharacters</code> attribute **/
	public static final String EACHPACKAGEBARCODECHARACTERS = "eachPackageBarCodeCharacters";
	/** Qualifier of the <code>FMPart.industrialOffHighwayEquipment</code> attribute **/
	public static final String INDUSTRIALOFFHIGHWAYEQUIPMENT = "industrialOffHighwayEquipment";
	/** Qualifier of the <code>FMPart.partstatus</code> attribute **/
	public static final String PARTSTATUS = "partstatus";
	/** Qualifier of the <code>FMPart.heightCase</code> attribute **/
	public static final String HEIGHTCASE = "heightCase";
	/** Qualifier of the <code>FMPart.lengthCase</code> attribute **/
	public static final String LENGTHCASE = "lengthCase";
	/** Qualifier of the <code>FMPart.dateRecordAddedorChanged</code> attribute **/
	public static final String DATERECORDADDEDORCHANGED = "dateRecordAddedorChanged";
	/** Qualifier of the <code>FMPart.personalWatercraft</code> attribute **/
	public static final String PERSONALWATERCRAFT = "personalWatercraft";
	/** Qualifier of the <code>FMPart.warranty</code> attribute **/
	public static final String WARRANTY = "warranty";
	/** Qualifier of the <code>FMPart.carLightTruck</code> attribute **/
	public static final String CARLIGHTTRUCK = "carLightTruck";
	/** Qualifier of the <code>FMPart.oReillyBuys</code> attribute **/
	public static final String OREILLYBUYS = "oReillyBuys";
	/** Qualifier of the <code>FMPart.widthInnerPackage</code> attribute **/
	public static final String WIDTHINNERPACKAGE = "widthInnerPackage";
	/** Qualifier of the <code>FMPart.quantityPerApplicationQualifier</code> attribute **/
	public static final String QUANTITYPERAPPLICATIONQUALIFIER = "quantityPerApplicationQualifier";
	/** Qualifier of the <code>FMPart.emissionCode</code> attribute **/
	public static final String EMISSIONCODE = "emissionCode";
	/** Qualifier of the <code>FMPart.rawPartNumber</code> attribute **/
	public static final String RAWPARTNUMBER = "rawPartNumber";
	/** Qualifier of the <code>FMPart.brandLabel</code> attribute **/
	public static final String BRANDLABEL = "brandLabel";
	/** Qualifier of the <code>FMPart.aaiaPartTerminologyID</code> attribute **/
	public static final String AAIAPARTTERMINOLOGYID = "aaiaPartTerminologyID";
	/** Qualifier of the <code>FMPart.engineSpecific</code> attribute **/
	public static final String ENGINESPECIFIC = "engineSpecific";
	/** Qualifier of the <code>FMPart.alphaNumeric</code> attribute **/
	public static final String ALPHANUMERIC = "alphaNumeric";
	/** Qualifier of the <code>FMPart.allianceBuys</code> attribute **/
	public static final String ALLIANCEBUYS = "allianceBuys";
	/** Qualifier of the <code>FMPart.wdNetPrice</code> attribute **/
	public static final String WDNETPRICE = "wdNetPrice";
	/** Qualifier of the <code>FMPart.pkgLevelGTINUPCCase</code> attribute **/
	public static final String PKGLEVELGTINUPCCASE = "pkgLevelGTINUPCCase";
	/** Qualifier of the <code>FMPart.partNumber</code> attribute **/
	public static final String PARTNUMBER = "partNumber";
	/** Qualifier of the <code>FMPart.listPrice</code> attribute **/
	public static final String LISTPRICE = "listPrice";
	/** Qualifier of the <code>FMPart.pkgLevelGTINUPCInnerPackage</code> attribute **/
	public static final String PKGLEVELGTINUPCINNERPACKAGE = "pkgLevelGTINUPCInnerPackage";
	/** Qualifier of the <code>FMPart.containerType</code> attribute **/
	public static final String CONTAINERTYPE = "containerType";
	/** Qualifier of the <code>FMPart.availableDate</code> attribute **/
	public static final String AVAILABLEDATE = "availableDate";
	/** Qualifier of the <code>FMPart.agriculturalEquipment</code> attribute **/
	public static final String AGRICULTURALEQUIPMENT = "agriculturalEquipment";
	/** Qualifier of the <code>FMPart.aAPSKUNumberAN68</code> attribute **/
	public static final String AAPSKUNUMBERAN68 = "aAPSKUNumberAN68";
	/** Qualifier of the <code>FMPart.digitalAssetURL</code> attribute **/
	public static final String DIGITALASSETURL = "digitalAssetURL";
	/** Qualifier of the <code>FMPart.transmissionSpecific</code> attribute **/
	public static final String TRANSMISSIONSPECIFIC = "transmissionSpecific";
	/** Qualifier of the <code>FMPart.textnote</code> attribute **/
	public static final String TEXTNOTE = "textnote";
	/** Qualifier of the <code>FMPart.productFlag</code> attribute **/
	public static final String PRODUCTFLAG = "productFlag";
	/** Qualifier of the <code>FMPart.lifeCycleStatusCode</code> attribute **/
	public static final String LIFECYCLESTATUSCODE = "lifeCycleStatusCode";
	/** Qualifier of the <code>FMPart.minimumOrderQuantityUOM</code> attribute **/
	public static final String MINIMUMORDERQUANTITYUOM = "minimumOrderQuantityUOM";
	/** Qualifier of the <code>FMPart.heavyDutyTruck</code> attribute **/
	public static final String HEAVYDUTYTRUCK = "heavyDutyTruck";
	/** Qualifier of the <code>FMPart.remanufacturedPart</code> attribute **/
	public static final String REMANUFACTUREDPART = "remanufacturedPart";
	/** Qualifier of the <code>FMPart.packageEachWidth</code> attribute **/
	public static final String PACKAGEEACHWIDTH = "packageEachWidth";
	/** Qualifier of the <code>FMPart.otherCustomersifany</code> attribute **/
	public static final String OTHERCUSTOMERSIFANY = "otherCustomersifany";
	/** Qualifier of the <code>FMPart.applicationSummaryDescription</code> attribute **/
	public static final String APPLICATIONSUMMARYDESCRIPTION = "applicationSummaryDescription";
	/** Qualifier of the <code>FMPart.freightClass</code> attribute **/
	public static final String FREIGHTCLASS = "freightClass";
	/** Qualifier of the <code>FMPart.nonVehicleSpecific</code> attribute **/
	public static final String NONVEHICLESPECIFIC = "nonVehicleSpecific";
	/** Qualifier of the <code>FMPart.flagname</code> attribute **/
	public static final String FLAGNAME = "flagname";
	/** Qualifier of the <code>FMPart.priceSheetExpirationDate</code> attribute **/
	public static final String PRICESHEETEXPIRATIONDATE = "priceSheetExpirationDate";
	/** Qualifier of the <code>FMPart.lifeCycleStatusDescription</code> attribute **/
	public static final String LIFECYCLESTATUSDESCRIPTION = "lifeCycleStatusDescription";
	/** Qualifier of the <code>FMPart.productSubGroupCode</code> attribute **/
	public static final String PRODUCTSUBGROUPCODE = "productSubGroupCode";
	/** Qualifier of the <code>FMPart.aAPPartTypeIDsmandatoryAN250</code> attribute **/
	public static final String AAPPARTTYPEIDSMANDATORYAN250 = "aAPPartTypeIDsmandatoryAN250";
	/** Qualifier of the <code>FMPart.hazardousMaterialDescription</code> attribute **/
	public static final String HAZARDOUSMATERIALDESCRIPTION = "hazardousMaterialDescription";
	/** Qualifier of the <code>FMPart.corporate</code> attribute **/
	public static final String CORPORATE = "corporate";
	/** Qualifier of the <code>FMPart.hazardousIndicator</code> attribute **/
	public static final String HAZARDOUSINDICATOR = "hazardousIndicator";
	/** Qualifier of the <code>FMPart.applicationSpecific</code> attribute **/
	public static final String APPLICATIONSPECIFIC = "applicationSpecific";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Product.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(SQUASHEDPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(PARTSEQUENCE, AttributeMode.INITIAL);
		tmp.put(PACKAGEEACHWEIGHT, AttributeMode.INITIAL);
		tmp.put(FAQANSWER1, AttributeMode.INITIAL);
		tmp.put(WEIGHTINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(ADVANCEBUYS, AttributeMode.INITIAL);
		tmp.put(PALLETFOOTPRINTSIZE, AttributeMode.INITIAL);
		tmp.put(WEIGHTUOM, AttributeMode.INITIAL);
		tmp.put(HIGHPERFORMANCE, AttributeMode.INITIAL);
		tmp.put(PID, AttributeMode.INITIAL);
		tmp.put(WARRANTYDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(FAQANSWER2, AttributeMode.INITIAL);
		tmp.put(SNOWMOBILE, AttributeMode.INITIAL);
		tmp.put(FAQANSWER3, AttributeMode.INITIAL);
		tmp.put(PRICESHEETNUMBER, AttributeMode.INITIAL);
		tmp.put(MEDIUMDUTYTRUCK, AttributeMode.INITIAL);
		tmp.put(ITEMQUANTITYSIZEUOM, AttributeMode.INITIAL);
		tmp.put(ITEMLEVELGTINUPC, AttributeMode.INITIAL);
		tmp.put(WIDTHCASE, AttributeMode.INITIAL);
		tmp.put(LENGTHINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(AAPPLANUMBERAN3, AttributeMode.INITIAL);
		tmp.put(MINIMUMORDERQUANTITY, AttributeMode.INITIAL);
		tmp.put(QUANTITYOFEACHESEACHPACKAGE, AttributeMode.INITIAL);
		tmp.put(PRODUCTCATEGORYCODE, AttributeMode.INITIAL);
		tmp.put(FAQQESTION1, AttributeMode.INITIAL);
		tmp.put(NAPABUYS, AttributeMode.INITIAL);
		tmp.put(FAQANSWER4, AttributeMode.INITIAL);
		tmp.put(BRANDAAIAID, AttributeMode.INITIAL);
		tmp.put(PACKAGEEACHLEVELGTINUPC, AttributeMode.INITIAL);
		tmp.put(PALLETLAYERMAX, AttributeMode.INITIAL);
		tmp.put(PACKAGEEACHLENGTH, AttributeMode.INITIAL);
		tmp.put(FAQQESTION5, AttributeMode.INITIAL);
		tmp.put(NATIONALPOPULARITYCODE, AttributeMode.INITIAL);
		tmp.put(FAQQESTION2, AttributeMode.INITIAL);
		tmp.put(QTYOFEACHESINPKGINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(HARMONIZEDTARIFFCODE, AttributeMode.INITIAL);
		tmp.put(PACKAGEUOM, AttributeMode.INITIAL);
		tmp.put(UPC, AttributeMode.INITIAL);
		tmp.put(COUNTRYOFORIGINPRIMARY, AttributeMode.INITIAL);
		tmp.put(TEXTFOOTNOTE, AttributeMode.INITIAL);
		tmp.put(SQSHPTNBR, AttributeMode.INITIAL);
		tmp.put(ITEMLEVELGTINQUALIFIER, AttributeMode.INITIAL);
		tmp.put(TEXTCOMMENT, AttributeMode.INITIAL);
		tmp.put(HEIGHTINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(FAQQESTION4, AttributeMode.INITIAL);
		tmp.put(QUANTITYPERAPPLICATION, AttributeMode.INITIAL);
		tmp.put(AUTOZONEBUYS, AttributeMode.INITIAL);
		tmp.put(DIMENSIONSUOM, AttributeMode.INITIAL);
		tmp.put(PRICESHEETEFFECTIVEDATE, AttributeMode.INITIAL);
		tmp.put(QUANTITYOFEACHESINPACKAGECASE, AttributeMode.INITIAL);
		tmp.put(PARTNUMBEROLD, AttributeMode.INITIAL);
		tmp.put(HAZARDOUSCLASSCODE, AttributeMode.INITIAL);
		tmp.put(MSDSSHEETAVAILABLE, AttributeMode.INITIAL);
		tmp.put(SENTIMAGETOADVANCE, AttributeMode.INITIAL);
		tmp.put(PARTNUMBERSUPERSEDEDTO, AttributeMode.INITIAL);
		tmp.put(ITEMQUANTITYSIZE, AttributeMode.INITIAL);
		tmp.put(NAPAPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(ALTERNATEPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(MAXCASEPERPALLETLAYER, AttributeMode.INITIAL);
		tmp.put(PACKAGEEACHHEIGHT, AttributeMode.INITIAL);
		tmp.put(FAQANSWER5, AttributeMode.INITIAL);
		tmp.put(WEIGHTCASE, AttributeMode.INITIAL);
		tmp.put(PRODUCTGROUPCODE, AttributeMode.INITIAL);
		tmp.put(PALLETFOOTPRINTSIZEUOM, AttributeMode.INITIAL);
		tmp.put(TAXABLE, AttributeMode.INITIAL);
		tmp.put(MARINE, AttributeMode.INITIAL);
		tmp.put(FAQQESTION3, AttributeMode.INITIAL);
		tmp.put(VEHICLEREGISTRATION, AttributeMode.INITIAL);
		tmp.put(POPULARITYCODE, AttributeMode.INITIAL);
		tmp.put(FLAGCODE, AttributeMode.INITIAL);
		tmp.put(QUANTITYPERAPPLICATIONUOM, AttributeMode.INITIAL);
		tmp.put(SMALLENGINE, AttributeMode.INITIAL);
		tmp.put(LONGDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(MSDSSHEETNUMBER, AttributeMode.INITIAL);
		tmp.put(AUTOZONEPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(JOBBERPRICE, AttributeMode.INITIAL);
		tmp.put(MOTORCYCLEATV, AttributeMode.INITIAL);
		tmp.put(SUPERSEDEDPRICESHEETNUMBER, AttributeMode.INITIAL);
		tmp.put(CASEBARCODECHARACTERS, AttributeMode.INITIAL);
		tmp.put(EACHPACKAGEBARCODECHARACTERS, AttributeMode.INITIAL);
		tmp.put(INDUSTRIALOFFHIGHWAYEQUIPMENT, AttributeMode.INITIAL);
		tmp.put(PARTSTATUS, AttributeMode.INITIAL);
		tmp.put(HEIGHTCASE, AttributeMode.INITIAL);
		tmp.put(LENGTHCASE, AttributeMode.INITIAL);
		tmp.put(DATERECORDADDEDORCHANGED, AttributeMode.INITIAL);
		tmp.put(PERSONALWATERCRAFT, AttributeMode.INITIAL);
		tmp.put(WARRANTY, AttributeMode.INITIAL);
		tmp.put(CARLIGHTTRUCK, AttributeMode.INITIAL);
		tmp.put(OREILLYBUYS, AttributeMode.INITIAL);
		tmp.put(WIDTHINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(QUANTITYPERAPPLICATIONQUALIFIER, AttributeMode.INITIAL);
		tmp.put(EMISSIONCODE, AttributeMode.INITIAL);
		tmp.put(RAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(BRANDLABEL, AttributeMode.INITIAL);
		tmp.put(AAIAPARTTERMINOLOGYID, AttributeMode.INITIAL);
		tmp.put(ENGINESPECIFIC, AttributeMode.INITIAL);
		tmp.put(ALPHANUMERIC, AttributeMode.INITIAL);
		tmp.put(ALLIANCEBUYS, AttributeMode.INITIAL);
		tmp.put(WDNETPRICE, AttributeMode.INITIAL);
		tmp.put(PKGLEVELGTINUPCCASE, AttributeMode.INITIAL);
		tmp.put(PARTNUMBER, AttributeMode.INITIAL);
		tmp.put(LISTPRICE, AttributeMode.INITIAL);
		tmp.put(PKGLEVELGTINUPCINNERPACKAGE, AttributeMode.INITIAL);
		tmp.put(CONTAINERTYPE, AttributeMode.INITIAL);
		tmp.put(AVAILABLEDATE, AttributeMode.INITIAL);
		tmp.put(AGRICULTURALEQUIPMENT, AttributeMode.INITIAL);
		tmp.put(AAPSKUNUMBERAN68, AttributeMode.INITIAL);
		tmp.put(DIGITALASSETURL, AttributeMode.INITIAL);
		tmp.put(TRANSMISSIONSPECIFIC, AttributeMode.INITIAL);
		tmp.put(TEXTNOTE, AttributeMode.INITIAL);
		tmp.put(PRODUCTFLAG, AttributeMode.INITIAL);
		tmp.put(LIFECYCLESTATUSCODE, AttributeMode.INITIAL);
		tmp.put(MINIMUMORDERQUANTITYUOM, AttributeMode.INITIAL);
		tmp.put(HEAVYDUTYTRUCK, AttributeMode.INITIAL);
		tmp.put(REMANUFACTUREDPART, AttributeMode.INITIAL);
		tmp.put(PACKAGEEACHWIDTH, AttributeMode.INITIAL);
		tmp.put(OTHERCUSTOMERSIFANY, AttributeMode.INITIAL);
		tmp.put(APPLICATIONSUMMARYDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(FREIGHTCLASS, AttributeMode.INITIAL);
		tmp.put(NONVEHICLESPECIFIC, AttributeMode.INITIAL);
		tmp.put(FLAGNAME, AttributeMode.INITIAL);
		tmp.put(PRICESHEETEXPIRATIONDATE, AttributeMode.INITIAL);
		tmp.put(LIFECYCLESTATUSDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(PRODUCTSUBGROUPCODE, AttributeMode.INITIAL);
		tmp.put(AAPPARTTYPEIDSMANDATORYAN250, AttributeMode.INITIAL);
		tmp.put(HAZARDOUSMATERIALDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(CORPORATE, AttributeMode.INITIAL);
		tmp.put(HAZARDOUSINDICATOR, AttributeMode.INITIAL);
		tmp.put(APPLICATIONSPECIFIC, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aaiaPartTerminologyID</code> attribute.
	 * @return the aaiaPartTerminologyID - AAIA Part Terminology ID
	 */
	public String getAaiaPartTerminologyID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, AAIAPARTTERMINOLOGYID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aaiaPartTerminologyID</code> attribute.
	 * @return the aaiaPartTerminologyID - AAIA Part Terminology ID
	 */
	public String getAaiaPartTerminologyID()
	{
		return getAaiaPartTerminologyID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aaiaPartTerminologyID</code> attribute. 
	 * @param value the aaiaPartTerminologyID - AAIA Part Terminology ID
	 */
	public void setAaiaPartTerminologyID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, AAIAPARTTERMINOLOGYID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aaiaPartTerminologyID</code> attribute. 
	 * @param value the aaiaPartTerminologyID - AAIA Part Terminology ID
	 */
	public void setAaiaPartTerminologyID(final String value)
	{
		setAaiaPartTerminologyID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPPartTypeIDsmandatoryAN250</code> attribute.
	 * @return the aAPPartTypeIDsmandatoryAN250 - AAP Part Type IDs (mandatory) AN250
	 */
	public String getAAPPartTypeIDsmandatoryAN250(final SessionContext ctx)
	{
		return (String)getProperty( ctx, AAPPARTTYPEIDSMANDATORYAN250);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPPartTypeIDsmandatoryAN250</code> attribute.
	 * @return the aAPPartTypeIDsmandatoryAN250 - AAP Part Type IDs (mandatory) AN250
	 */
	public String getAAPPartTypeIDsmandatoryAN250()
	{
		return getAAPPartTypeIDsmandatoryAN250( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPPartTypeIDsmandatoryAN250</code> attribute. 
	 * @param value the aAPPartTypeIDsmandatoryAN250 - AAP Part Type IDs (mandatory) AN250
	 */
	public void setAAPPartTypeIDsmandatoryAN250(final SessionContext ctx, final String value)
	{
		setProperty(ctx, AAPPARTTYPEIDSMANDATORYAN250,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPPartTypeIDsmandatoryAN250</code> attribute. 
	 * @param value the aAPPartTypeIDsmandatoryAN250 - AAP Part Type IDs (mandatory) AN250
	 */
	public void setAAPPartTypeIDsmandatoryAN250(final String value)
	{
		setAAPPartTypeIDsmandatoryAN250( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPPLANumberAN3</code> attribute.
	 * @return the aAPPLANumberAN3 - AAP PLA Number (AN3)
	 */
	public String getAAPPLANumberAN3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, AAPPLANUMBERAN3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPPLANumberAN3</code> attribute.
	 * @return the aAPPLANumberAN3 - AAP PLA Number (AN3)
	 */
	public String getAAPPLANumberAN3()
	{
		return getAAPPLANumberAN3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPPLANumberAN3</code> attribute. 
	 * @param value the aAPPLANumberAN3 - AAP PLA Number (AN3)
	 */
	public void setAAPPLANumberAN3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, AAPPLANUMBERAN3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPPLANumberAN3</code> attribute. 
	 * @param value the aAPPLANumberAN3 - AAP PLA Number (AN3)
	 */
	public void setAAPPLANumberAN3(final String value)
	{
		setAAPPLANumberAN3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPSKUNumberAN68</code> attribute.
	 * @return the aAPSKUNumberAN68 - AAP SKU Number (AN6/8)
	 */
	public String getAAPSKUNumberAN68(final SessionContext ctx)
	{
		return (String)getProperty( ctx, AAPSKUNUMBERAN68);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.aAPSKUNumberAN68</code> attribute.
	 * @return the aAPSKUNumberAN68 - AAP SKU Number (AN6/8)
	 */
	public String getAAPSKUNumberAN68()
	{
		return getAAPSKUNumberAN68( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPSKUNumberAN68</code> attribute. 
	 * @param value the aAPSKUNumberAN68 - AAP SKU Number (AN6/8)
	 */
	public void setAAPSKUNumberAN68(final SessionContext ctx, final String value)
	{
		setProperty(ctx, AAPSKUNUMBERAN68,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.aAPSKUNumberAN68</code> attribute. 
	 * @param value the aAPSKUNumberAN68 - AAP SKU Number (AN6/8)
	 */
	public void setAAPSKUNumberAN68(final String value)
	{
		setAAPSKUNumberAN68( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.advanceBuys</code> attribute.
	 * @return the advanceBuys - Advance Buys? Y/N
	 */
	public Boolean isAdvanceBuys(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, ADVANCEBUYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.advanceBuys</code> attribute.
	 * @return the advanceBuys - Advance Buys? Y/N
	 */
	public Boolean isAdvanceBuys()
	{
		return isAdvanceBuys( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @return the advanceBuys - Advance Buys? Y/N
	 */
	public boolean isAdvanceBuysAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isAdvanceBuys( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @return the advanceBuys - Advance Buys? Y/N
	 */
	public boolean isAdvanceBuysAsPrimitive()
	{
		return isAdvanceBuysAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @param value the advanceBuys - Advance Buys? Y/N
	 */
	public void setAdvanceBuys(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, ADVANCEBUYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @param value the advanceBuys - Advance Buys? Y/N
	 */
	public void setAdvanceBuys(final Boolean value)
	{
		setAdvanceBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @param value the advanceBuys - Advance Buys? Y/N
	 */
	public void setAdvanceBuys(final SessionContext ctx, final boolean value)
	{
		setAdvanceBuys( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.advanceBuys</code> attribute. 
	 * @param value the advanceBuys - Advance Buys? Y/N
	 */
	public void setAdvanceBuys(final boolean value)
	{
		setAdvanceBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.agriculturalEquipment</code> attribute.
	 * @return the agriculturalEquipment - Agricultural Equipment
	 */
	public Boolean isAgriculturalEquipment(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, AGRICULTURALEQUIPMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.agriculturalEquipment</code> attribute.
	 * @return the agriculturalEquipment - Agricultural Equipment
	 */
	public Boolean isAgriculturalEquipment()
	{
		return isAgriculturalEquipment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @return the agriculturalEquipment - Agricultural Equipment
	 */
	public boolean isAgriculturalEquipmentAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isAgriculturalEquipment( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @return the agriculturalEquipment - Agricultural Equipment
	 */
	public boolean isAgriculturalEquipmentAsPrimitive()
	{
		return isAgriculturalEquipmentAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @param value the agriculturalEquipment - Agricultural Equipment
	 */
	public void setAgriculturalEquipment(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, AGRICULTURALEQUIPMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @param value the agriculturalEquipment - Agricultural Equipment
	 */
	public void setAgriculturalEquipment(final Boolean value)
	{
		setAgriculturalEquipment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @param value the agriculturalEquipment - Agricultural Equipment
	 */
	public void setAgriculturalEquipment(final SessionContext ctx, final boolean value)
	{
		setAgriculturalEquipment( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.agriculturalEquipment</code> attribute. 
	 * @param value the agriculturalEquipment - Agricultural Equipment
	 */
	public void setAgriculturalEquipment(final boolean value)
	{
		setAgriculturalEquipment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.allianceBuys</code> attribute.
	 * @return the allianceBuys - Alliance Buys? Y/N
	 */
	public Boolean isAllianceBuys(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, ALLIANCEBUYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.allianceBuys</code> attribute.
	 * @return the allianceBuys - Alliance Buys? Y/N
	 */
	public Boolean isAllianceBuys()
	{
		return isAllianceBuys( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @return the allianceBuys - Alliance Buys? Y/N
	 */
	public boolean isAllianceBuysAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isAllianceBuys( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @return the allianceBuys - Alliance Buys? Y/N
	 */
	public boolean isAllianceBuysAsPrimitive()
	{
		return isAllianceBuysAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @param value the allianceBuys - Alliance Buys? Y/N
	 */
	public void setAllianceBuys(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, ALLIANCEBUYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @param value the allianceBuys - Alliance Buys? Y/N
	 */
	public void setAllianceBuys(final Boolean value)
	{
		setAllianceBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @param value the allianceBuys - Alliance Buys? Y/N
	 */
	public void setAllianceBuys(final SessionContext ctx, final boolean value)
	{
		setAllianceBuys( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.allianceBuys</code> attribute. 
	 * @param value the allianceBuys - Alliance Buys? Y/N
	 */
	public void setAllianceBuys(final boolean value)
	{
		setAllianceBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.alphaNumeric</code> attribute.
	 * @return the alphaNumeric - alphaNumeric
	 */
	public String getAlphaNumeric(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ALPHANUMERIC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.alphaNumeric</code> attribute.
	 * @return the alphaNumeric - alphaNumeric
	 */
	public String getAlphaNumeric()
	{
		return getAlphaNumeric( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.alphaNumeric</code> attribute. 
	 * @param value the alphaNumeric - alphaNumeric
	 */
	public void setAlphaNumeric(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ALPHANUMERIC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.alphaNumeric</code> attribute. 
	 * @param value the alphaNumeric - alphaNumeric
	 */
	public void setAlphaNumeric(final String value)
	{
		setAlphaNumeric( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.alternatePartNumber</code> attribute.
	 * @return the alternatePartNumber - Alternate Part Number
	 */
	public String getAlternatePartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ALTERNATEPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.alternatePartNumber</code> attribute.
	 * @return the alternatePartNumber - Alternate Part Number
	 */
	public String getAlternatePartNumber()
	{
		return getAlternatePartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.alternatePartNumber</code> attribute. 
	 * @param value the alternatePartNumber - Alternate Part Number
	 */
	public void setAlternatePartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ALTERNATEPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.alternatePartNumber</code> attribute. 
	 * @param value the alternatePartNumber - Alternate Part Number
	 */
	public void setAlternatePartNumber(final String value)
	{
		setAlternatePartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSpecific</code> attribute.
	 * @return the applicationSpecific - Application Specific
	 */
	public Boolean isApplicationSpecific(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, APPLICATIONSPECIFIC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSpecific</code> attribute.
	 * @return the applicationSpecific - Application Specific
	 */
	public Boolean isApplicationSpecific()
	{
		return isApplicationSpecific( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @return the applicationSpecific - Application Specific
	 */
	public boolean isApplicationSpecificAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isApplicationSpecific( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @return the applicationSpecific - Application Specific
	 */
	public boolean isApplicationSpecificAsPrimitive()
	{
		return isApplicationSpecificAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @param value the applicationSpecific - Application Specific
	 */
	public void setApplicationSpecific(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, APPLICATIONSPECIFIC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @param value the applicationSpecific - Application Specific
	 */
	public void setApplicationSpecific(final Boolean value)
	{
		setApplicationSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @param value the applicationSpecific - Application Specific
	 */
	public void setApplicationSpecific(final SessionContext ctx, final boolean value)
	{
		setApplicationSpecific( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSpecific</code> attribute. 
	 * @param value the applicationSpecific - Application Specific
	 */
	public void setApplicationSpecific(final boolean value)
	{
		setApplicationSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSummaryDescription</code> attribute.
	 * @return the applicationSummaryDescription - Application Summary Description
	 */
	public String getApplicationSummaryDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, APPLICATIONSUMMARYDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.applicationSummaryDescription</code> attribute.
	 * @return the applicationSummaryDescription - Application Summary Description
	 */
	public String getApplicationSummaryDescription()
	{
		return getApplicationSummaryDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSummaryDescription</code> attribute. 
	 * @param value the applicationSummaryDescription - Application Summary Description
	 */
	public void setApplicationSummaryDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, APPLICATIONSUMMARYDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.applicationSummaryDescription</code> attribute. 
	 * @param value the applicationSummaryDescription - Application Summary Description
	 */
	public void setApplicationSummaryDescription(final String value)
	{
		setApplicationSummaryDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZoneBuys</code> attribute.
	 * @return the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public Boolean isAutoZoneBuys(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, AUTOZONEBUYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZoneBuys</code> attribute.
	 * @return the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public Boolean isAutoZoneBuys()
	{
		return isAutoZoneBuys( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @return the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public boolean isAutoZoneBuysAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isAutoZoneBuys( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @return the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public boolean isAutoZoneBuysAsPrimitive()
	{
		return isAutoZoneBuysAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @param value the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public void setAutoZoneBuys(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, AUTOZONEBUYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @param value the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public void setAutoZoneBuys(final Boolean value)
	{
		setAutoZoneBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @param value the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public void setAutoZoneBuys(final SessionContext ctx, final boolean value)
	{
		setAutoZoneBuys( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZoneBuys</code> attribute. 
	 * @param value the autoZoneBuys - AutoZone Buys? Y/N
	 */
	public void setAutoZoneBuys(final boolean value)
	{
		setAutoZoneBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZonePartNumber</code> attribute.
	 * @return the autoZonePartNumber - AutoZone Part Number
	 */
	public Boolean isAutoZonePartNumber(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, AUTOZONEPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZonePartNumber</code> attribute.
	 * @return the autoZonePartNumber - AutoZone Part Number
	 */
	public Boolean isAutoZonePartNumber()
	{
		return isAutoZonePartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @return the autoZonePartNumber - AutoZone Part Number
	 */
	public boolean isAutoZonePartNumberAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isAutoZonePartNumber( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @return the autoZonePartNumber - AutoZone Part Number
	 */
	public boolean isAutoZonePartNumberAsPrimitive()
	{
		return isAutoZonePartNumberAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @param value the autoZonePartNumber - AutoZone Part Number
	 */
	public void setAutoZonePartNumber(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, AUTOZONEPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @param value the autoZonePartNumber - AutoZone Part Number
	 */
	public void setAutoZonePartNumber(final Boolean value)
	{
		setAutoZonePartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @param value the autoZonePartNumber - AutoZone Part Number
	 */
	public void setAutoZonePartNumber(final SessionContext ctx, final boolean value)
	{
		setAutoZonePartNumber( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.autoZonePartNumber</code> attribute. 
	 * @param value the autoZonePartNumber - AutoZone Part Number
	 */
	public void setAutoZonePartNumber(final boolean value)
	{
		setAutoZonePartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.availableDate</code> attribute.
	 * @return the availableDate - Available Date
	 */
	public Date getAvailableDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, AVAILABLEDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.availableDate</code> attribute.
	 * @return the availableDate - Available Date
	 */
	public Date getAvailableDate()
	{
		return getAvailableDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.availableDate</code> attribute. 
	 * @param value the availableDate - Available Date
	 */
	public void setAvailableDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, AVAILABLEDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.availableDate</code> attribute. 
	 * @param value the availableDate - Available Date
	 */
	public void setAvailableDate(final Date value)
	{
		setAvailableDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.brandAAIAID</code> attribute.
	 * @return the brandAAIAID - Brand AAIAID
	 */
	public String getBrandAAIAID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRANDAAIAID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.brandAAIAID</code> attribute.
	 * @return the brandAAIAID - Brand AAIAID
	 */
	public String getBrandAAIAID()
	{
		return getBrandAAIAID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.brandAAIAID</code> attribute. 
	 * @param value the brandAAIAID - Brand AAIAID
	 */
	public void setBrandAAIAID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRANDAAIAID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.brandAAIAID</code> attribute. 
	 * @param value the brandAAIAID - Brand AAIAID
	 */
	public void setBrandAAIAID(final String value)
	{
		setBrandAAIAID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.brandLabel</code> attribute.
	 * @return the brandLabel - Brand Label
	 */
	public String getBrandLabel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRANDLABEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.brandLabel</code> attribute.
	 * @return the brandLabel - Brand Label
	 */
	public String getBrandLabel()
	{
		return getBrandLabel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.brandLabel</code> attribute. 
	 * @param value the brandLabel - Brand Label
	 */
	public void setBrandLabel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRANDLABEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.brandLabel</code> attribute. 
	 * @param value the brandLabel - Brand Label
	 */
	public void setBrandLabel(final String value)
	{
		setBrandLabel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.carLightTruck</code> attribute.
	 * @return the carLightTruck - Car Light Truck
	 */
	public Boolean isCarLightTruck(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, CARLIGHTTRUCK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.carLightTruck</code> attribute.
	 * @return the carLightTruck - Car Light Truck
	 */
	public Boolean isCarLightTruck()
	{
		return isCarLightTruck( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @return the carLightTruck - Car Light Truck
	 */
	public boolean isCarLightTruckAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isCarLightTruck( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @return the carLightTruck - Car Light Truck
	 */
	public boolean isCarLightTruckAsPrimitive()
	{
		return isCarLightTruckAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @param value the carLightTruck - Car Light Truck
	 */
	public void setCarLightTruck(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, CARLIGHTTRUCK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @param value the carLightTruck - Car Light Truck
	 */
	public void setCarLightTruck(final Boolean value)
	{
		setCarLightTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @param value the carLightTruck - Car Light Truck
	 */
	public void setCarLightTruck(final SessionContext ctx, final boolean value)
	{
		setCarLightTruck( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.carLightTruck</code> attribute. 
	 * @param value the carLightTruck - Car Light Truck
	 */
	public void setCarLightTruck(final boolean value)
	{
		setCarLightTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.caseBarCodeCharacters</code> attribute.
	 * @return the caseBarCodeCharacters - Case Bar Code Characters
	 */
	public String getCaseBarCodeCharacters(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CASEBARCODECHARACTERS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.caseBarCodeCharacters</code> attribute.
	 * @return the caseBarCodeCharacters - Case Bar Code Characters
	 */
	public String getCaseBarCodeCharacters()
	{
		return getCaseBarCodeCharacters( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.caseBarCodeCharacters</code> attribute. 
	 * @param value the caseBarCodeCharacters - Case Bar Code Characters
	 */
	public void setCaseBarCodeCharacters(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CASEBARCODECHARACTERS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.caseBarCodeCharacters</code> attribute. 
	 * @param value the caseBarCodeCharacters - Case Bar Code Characters
	 */
	public void setCaseBarCodeCharacters(final String value)
	{
		setCaseBarCodeCharacters( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.containerType</code> attribute.
	 * @return the containerType - Container Type
	 */
	public String getContainerType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CONTAINERTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.containerType</code> attribute.
	 * @return the containerType - Container Type
	 */
	public String getContainerType()
	{
		return getContainerType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.containerType</code> attribute. 
	 * @param value the containerType - Container Type
	 */
	public void setContainerType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CONTAINERTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.containerType</code> attribute. 
	 * @param value the containerType - Container Type
	 */
	public void setContainerType(final String value)
	{
		setContainerType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.corporate</code> attribute.
	 * @return the corporate
	 */
	public List<FMCorporate> getCorporate(final SessionContext ctx)
	{
		List<FMCorporate> coll = (List<FMCorporate>)getProperty( ctx, CORPORATE);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.corporate</code> attribute.
	 * @return the corporate
	 */
	public List<FMCorporate> getCorporate()
	{
		return getCorporate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.corporate</code> attribute. 
	 * @param value the corporate
	 */
	public void setCorporate(final SessionContext ctx, final List<FMCorporate> value)
	{
		setProperty(ctx, CORPORATE,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.corporate</code> attribute. 
	 * @param value the corporate
	 */
	public void setCorporate(final List<FMCorporate> value)
	{
		setCorporate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.countryOfOriginPrimary</code> attribute.
	 * @return the countryOfOriginPrimary - Country Of Origin Primary
	 */
	public String getCountryOfOriginPrimary(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COUNTRYOFORIGINPRIMARY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.countryOfOriginPrimary</code> attribute.
	 * @return the countryOfOriginPrimary - Country Of Origin Primary
	 */
	public String getCountryOfOriginPrimary()
	{
		return getCountryOfOriginPrimary( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.countryOfOriginPrimary</code> attribute. 
	 * @param value the countryOfOriginPrimary - Country Of Origin Primary
	 */
	public void setCountryOfOriginPrimary(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COUNTRYOFORIGINPRIMARY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.countryOfOriginPrimary</code> attribute. 
	 * @param value the countryOfOriginPrimary - Country Of Origin Primary
	 */
	public void setCountryOfOriginPrimary(final String value)
	{
		setCountryOfOriginPrimary( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.dateRecordAddedorChanged</code> attribute.
	 * @return the dateRecordAddedorChanged - Date Record Added or Changed
	 */
	public Date getDateRecordAddedorChanged(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, DATERECORDADDEDORCHANGED);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.dateRecordAddedorChanged</code> attribute.
	 * @return the dateRecordAddedorChanged - Date Record Added or Changed
	 */
	public Date getDateRecordAddedorChanged()
	{
		return getDateRecordAddedorChanged( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.dateRecordAddedorChanged</code> attribute. 
	 * @param value the dateRecordAddedorChanged - Date Record Added or Changed
	 */
	public void setDateRecordAddedorChanged(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, DATERECORDADDEDORCHANGED,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.dateRecordAddedorChanged</code> attribute. 
	 * @param value the dateRecordAddedorChanged - Date Record Added or Changed
	 */
	public void setDateRecordAddedorChanged(final Date value)
	{
		setDateRecordAddedorChanged( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.digitalAssetURL</code> attribute.
	 * @return the digitalAssetURL - Digital Asset Segement URL
	 */
	public String getDigitalAssetURL(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIGITALASSETURL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.digitalAssetURL</code> attribute.
	 * @return the digitalAssetURL - Digital Asset Segement URL
	 */
	public String getDigitalAssetURL()
	{
		return getDigitalAssetURL( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.digitalAssetURL</code> attribute. 
	 * @param value the digitalAssetURL - Digital Asset Segement URL
	 */
	public void setDigitalAssetURL(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIGITALASSETURL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.digitalAssetURL</code> attribute. 
	 * @param value the digitalAssetURL - Digital Asset Segement URL
	 */
	public void setDigitalAssetURL(final String value)
	{
		setDigitalAssetURL( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.dimensionsUOM</code> attribute.
	 * @return the dimensionsUOM - Dimensions UOM
	 */
	public String getDimensionsUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DIMENSIONSUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.dimensionsUOM</code> attribute.
	 * @return the dimensionsUOM - Dimensions UOM
	 */
	public String getDimensionsUOM()
	{
		return getDimensionsUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.dimensionsUOM</code> attribute. 
	 * @param value the dimensionsUOM - Dimensions UOM
	 */
	public void setDimensionsUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DIMENSIONSUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.dimensionsUOM</code> attribute. 
	 * @param value the dimensionsUOM - Dimensions UOM
	 */
	public void setDimensionsUOM(final String value)
	{
		setDimensionsUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.eachPackageBarCodeCharacters</code> attribute.
	 * @return the eachPackageBarCodeCharacters - Each Package Bar Code Characters
	 */
	public String getEachPackageBarCodeCharacters(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EACHPACKAGEBARCODECHARACTERS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.eachPackageBarCodeCharacters</code> attribute.
	 * @return the eachPackageBarCodeCharacters - Each Package Bar Code Characters
	 */
	public String getEachPackageBarCodeCharacters()
	{
		return getEachPackageBarCodeCharacters( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.eachPackageBarCodeCharacters</code> attribute. 
	 * @param value the eachPackageBarCodeCharacters - Each Package Bar Code Characters
	 */
	public void setEachPackageBarCodeCharacters(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EACHPACKAGEBARCODECHARACTERS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.eachPackageBarCodeCharacters</code> attribute. 
	 * @param value the eachPackageBarCodeCharacters - Each Package Bar Code Characters
	 */
	public void setEachPackageBarCodeCharacters(final String value)
	{
		setEachPackageBarCodeCharacters( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.emissionCode</code> attribute.
	 * @return the emissionCode - Emission Code
	 */
	public Integer getEmissionCode(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, EMISSIONCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.emissionCode</code> attribute.
	 * @return the emissionCode - Emission Code
	 */
	public Integer getEmissionCode()
	{
		return getEmissionCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.emissionCode</code> attribute. 
	 * @return the emissionCode - Emission Code
	 */
	public int getEmissionCodeAsPrimitive(final SessionContext ctx)
	{
		Integer value = getEmissionCode( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.emissionCode</code> attribute. 
	 * @return the emissionCode - Emission Code
	 */
	public int getEmissionCodeAsPrimitive()
	{
		return getEmissionCodeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.emissionCode</code> attribute. 
	 * @param value the emissionCode - Emission Code
	 */
	public void setEmissionCode(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, EMISSIONCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.emissionCode</code> attribute. 
	 * @param value the emissionCode - Emission Code
	 */
	public void setEmissionCode(final Integer value)
	{
		setEmissionCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.emissionCode</code> attribute. 
	 * @param value the emissionCode - Emission Code
	 */
	public void setEmissionCode(final SessionContext ctx, final int value)
	{
		setEmissionCode( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.emissionCode</code> attribute. 
	 * @param value the emissionCode - Emission Code
	 */
	public void setEmissionCode(final int value)
	{
		setEmissionCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.engineSpecific</code> attribute.
	 * @return the engineSpecific - Engine Specific
	 */
	public Boolean isEngineSpecific(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, ENGINESPECIFIC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.engineSpecific</code> attribute.
	 * @return the engineSpecific - Engine Specific
	 */
	public Boolean isEngineSpecific()
	{
		return isEngineSpecific( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @return the engineSpecific - Engine Specific
	 */
	public boolean isEngineSpecificAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isEngineSpecific( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @return the engineSpecific - Engine Specific
	 */
	public boolean isEngineSpecificAsPrimitive()
	{
		return isEngineSpecificAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @param value the engineSpecific - Engine Specific
	 */
	public void setEngineSpecific(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, ENGINESPECIFIC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @param value the engineSpecific - Engine Specific
	 */
	public void setEngineSpecific(final Boolean value)
	{
		setEngineSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @param value the engineSpecific - Engine Specific
	 */
	public void setEngineSpecific(final SessionContext ctx, final boolean value)
	{
		setEngineSpecific( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.engineSpecific</code> attribute. 
	 * @param value the engineSpecific - Engine Specific
	 */
	public void setEngineSpecific(final boolean value)
	{
		setEngineSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer1</code> attribute.
	 * @return the faqAnswer1
	 */
	public String getFaqAnswer1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQANSWER1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer1</code> attribute.
	 * @return the faqAnswer1
	 */
	public String getFaqAnswer1()
	{
		return getFaqAnswer1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer1</code> attribute. 
	 * @param value the faqAnswer1
	 */
	public void setFaqAnswer1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQANSWER1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer1</code> attribute. 
	 * @param value the faqAnswer1
	 */
	public void setFaqAnswer1(final String value)
	{
		setFaqAnswer1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer2</code> attribute.
	 * @return the faqAnswer2
	 */
	public String getFaqAnswer2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQANSWER2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer2</code> attribute.
	 * @return the faqAnswer2
	 */
	public String getFaqAnswer2()
	{
		return getFaqAnswer2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer2</code> attribute. 
	 * @param value the faqAnswer2
	 */
	public void setFaqAnswer2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQANSWER2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer2</code> attribute. 
	 * @param value the faqAnswer2
	 */
	public void setFaqAnswer2(final String value)
	{
		setFaqAnswer2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer3</code> attribute.
	 * @return the faqAnswer3
	 */
	public String getFaqAnswer3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQANSWER3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer3</code> attribute.
	 * @return the faqAnswer3
	 */
	public String getFaqAnswer3()
	{
		return getFaqAnswer3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer3</code> attribute. 
	 * @param value the faqAnswer3
	 */
	public void setFaqAnswer3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQANSWER3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer3</code> attribute. 
	 * @param value the faqAnswer3
	 */
	public void setFaqAnswer3(final String value)
	{
		setFaqAnswer3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer4</code> attribute.
	 * @return the faqAnswer4
	 */
	public String getFaqAnswer4(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQANSWER4);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer4</code> attribute.
	 * @return the faqAnswer4
	 */
	public String getFaqAnswer4()
	{
		return getFaqAnswer4( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer4</code> attribute. 
	 * @param value the faqAnswer4
	 */
	public void setFaqAnswer4(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQANSWER4,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer4</code> attribute. 
	 * @param value the faqAnswer4
	 */
	public void setFaqAnswer4(final String value)
	{
		setFaqAnswer4( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer5</code> attribute.
	 * @return the faqAnswer5
	 */
	public String getFaqAnswer5(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQANSWER5);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqAnswer5</code> attribute.
	 * @return the faqAnswer5
	 */
	public String getFaqAnswer5()
	{
		return getFaqAnswer5( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer5</code> attribute. 
	 * @param value the faqAnswer5
	 */
	public void setFaqAnswer5(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQANSWER5,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqAnswer5</code> attribute. 
	 * @param value the faqAnswer5
	 */
	public void setFaqAnswer5(final String value)
	{
		setFaqAnswer5( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion1</code> attribute.
	 * @return the faqQestion1
	 */
	public String getFaqQestion1(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQQESTION1);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion1</code> attribute.
	 * @return the faqQestion1
	 */
	public String getFaqQestion1()
	{
		return getFaqQestion1( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion1</code> attribute. 
	 * @param value the faqQestion1
	 */
	public void setFaqQestion1(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQQESTION1,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion1</code> attribute. 
	 * @param value the faqQestion1
	 */
	public void setFaqQestion1(final String value)
	{
		setFaqQestion1( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion2</code> attribute.
	 * @return the faqQestion2
	 */
	public String getFaqQestion2(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQQESTION2);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion2</code> attribute.
	 * @return the faqQestion2
	 */
	public String getFaqQestion2()
	{
		return getFaqQestion2( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion2</code> attribute. 
	 * @param value the faqQestion2
	 */
	public void setFaqQestion2(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQQESTION2,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion2</code> attribute. 
	 * @param value the faqQestion2
	 */
	public void setFaqQestion2(final String value)
	{
		setFaqQestion2( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion3</code> attribute.
	 * @return the faqQestion3
	 */
	public String getFaqQestion3(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQQESTION3);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion3</code> attribute.
	 * @return the faqQestion3
	 */
	public String getFaqQestion3()
	{
		return getFaqQestion3( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion3</code> attribute. 
	 * @param value the faqQestion3
	 */
	public void setFaqQestion3(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQQESTION3,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion3</code> attribute. 
	 * @param value the faqQestion3
	 */
	public void setFaqQestion3(final String value)
	{
		setFaqQestion3( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion4</code> attribute.
	 * @return the faqQestion4
	 */
	public String getFaqQestion4(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQQESTION4);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion4</code> attribute.
	 * @return the faqQestion4
	 */
	public String getFaqQestion4()
	{
		return getFaqQestion4( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion4</code> attribute. 
	 * @param value the faqQestion4
	 */
	public void setFaqQestion4(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQQESTION4,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion4</code> attribute. 
	 * @param value the faqQestion4
	 */
	public void setFaqQestion4(final String value)
	{
		setFaqQestion4( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion5</code> attribute.
	 * @return the faqQestion5
	 */
	public String getFaqQestion5(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FAQQESTION5);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.faqQestion5</code> attribute.
	 * @return the faqQestion5
	 */
	public String getFaqQestion5()
	{
		return getFaqQestion5( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion5</code> attribute. 
	 * @param value the faqQestion5
	 */
	public void setFaqQestion5(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FAQQESTION5,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.faqQestion5</code> attribute. 
	 * @param value the faqQestion5
	 */
	public void setFaqQestion5(final String value)
	{
		setFaqQestion5( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.fitments</code> attribute.
	 * @return the fitments - Fitment
	 */
	public Collection<FMFitment> getFitments(final SessionContext ctx)
	{
		final List<FMFitment> items = getLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.fitments</code> attribute.
	 * @return the fitments - Fitment
	 */
	public Collection<FMFitment> getFitments()
	{
		return getFitments( getSession().getSessionContext() );
	}
	
	public long getFitmentsCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			false,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null
		);
	}
	
	public long getFitmentsCount()
	{
		return getFitmentsCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.fitments</code> attribute. 
	 * @param value the fitments - Fitment
	 */
	public void setFitments(final SessionContext ctx, final Collection<FMFitment> value)
	{
		setLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			value,
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.fitments</code> attribute. 
	 * @param value the fitments - Fitment
	 */
	public void setFitments(final Collection<FMFitment> value)
	{
		setFitments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fitments. 
	 * @param value the item to add to fitments - Fitment
	 */
	public void addToFitments(final SessionContext ctx, final FMFitment value)
	{
		addLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fitments. 
	 * @param value the item to add to fitments - Fitment
	 */
	public void addToFitments(final FMFitment value)
	{
		addToFitments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fitments. 
	 * @param value the item to remove from fitments - Fitment
	 */
	public void removeFromFitments(final SessionContext ctx, final FMFitment value)
	{
		removeLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.FITMENTPARTRELATION,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(FITMENTPARTRELATION_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(FITMENTPARTRELATION_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fitments. 
	 * @param value the item to remove from fitments - Fitment
	 */
	public void removeFromFitments(final FMFitment value)
	{
		removeFromFitments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.flagcode</code> attribute.
	 * @return the flagcode
	 */
	public String getFlagcode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FLAGCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.flagcode</code> attribute.
	 * @return the flagcode
	 */
	public String getFlagcode()
	{
		return getFlagcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.flagcode</code> attribute. 
	 * @param value the flagcode
	 */
	public void setFlagcode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FLAGCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.flagcode</code> attribute. 
	 * @param value the flagcode
	 */
	public void setFlagcode(final String value)
	{
		setFlagcode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.flagname</code> attribute.
	 * @return the flagname
	 */
	public String getFlagname(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FLAGNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.flagname</code> attribute.
	 * @return the flagname
	 */
	public String getFlagname()
	{
		return getFlagname( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.flagname</code> attribute. 
	 * @param value the flagname
	 */
	public void setFlagname(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FLAGNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.flagname</code> attribute. 
	 * @param value the flagname
	 */
	public void setFlagname(final String value)
	{
		setFlagname( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.freightClass</code> attribute.
	 * @return the freightClass - Freight Class UOM
	 */
	public String getFreightClass(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FREIGHTCLASS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.freightClass</code> attribute.
	 * @return the freightClass - Freight Class UOM
	 */
	public String getFreightClass()
	{
		return getFreightClass( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.freightClass</code> attribute. 
	 * @param value the freightClass - Freight Class UOM
	 */
	public void setFreightClass(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FREIGHTCLASS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.freightClass</code> attribute. 
	 * @param value the freightClass - Freight Class UOM
	 */
	public void setFreightClass(final String value)
	{
		setFreightClass( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.harmonizedTariffCode</code> attribute.
	 * @return the harmonizedTariffCode - Harmonized Tariff Code
	 */
	public String getHarmonizedTariffCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HARMONIZEDTARIFFCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.harmonizedTariffCode</code> attribute.
	 * @return the harmonizedTariffCode - Harmonized Tariff Code
	 */
	public String getHarmonizedTariffCode()
	{
		return getHarmonizedTariffCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.harmonizedTariffCode</code> attribute. 
	 * @param value the harmonizedTariffCode - Harmonized Tariff Code
	 */
	public void setHarmonizedTariffCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HARMONIZEDTARIFFCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.harmonizedTariffCode</code> attribute. 
	 * @param value the harmonizedTariffCode - Harmonized Tariff Code
	 */
	public void setHarmonizedTariffCode(final String value)
	{
		setHarmonizedTariffCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousClassCode</code> attribute.
	 * @return the hazardousClassCode - Hazardous Class Code
	 */
	public String getHazardousClassCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HAZARDOUSCLASSCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousClassCode</code> attribute.
	 * @return the hazardousClassCode - Hazardous Class Code
	 */
	public String getHazardousClassCode()
	{
		return getHazardousClassCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousClassCode</code> attribute. 
	 * @param value the hazardousClassCode - Hazardous Class Code
	 */
	public void setHazardousClassCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HAZARDOUSCLASSCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousClassCode</code> attribute. 
	 * @param value the hazardousClassCode - Hazardous Class Code
	 */
	public void setHazardousClassCode(final String value)
	{
		setHazardousClassCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousIndicator</code> attribute.
	 * @return the hazardousIndicator - hazardous Indicator
	 */
	public Boolean isHazardousIndicator(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, HAZARDOUSINDICATOR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousIndicator</code> attribute.
	 * @return the hazardousIndicator - hazardous Indicator
	 */
	public Boolean isHazardousIndicator()
	{
		return isHazardousIndicator( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @return the hazardousIndicator - hazardous Indicator
	 */
	public boolean isHazardousIndicatorAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isHazardousIndicator( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @return the hazardousIndicator - hazardous Indicator
	 */
	public boolean isHazardousIndicatorAsPrimitive()
	{
		return isHazardousIndicatorAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @param value the hazardousIndicator - hazardous Indicator
	 */
	public void setHazardousIndicator(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, HAZARDOUSINDICATOR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @param value the hazardousIndicator - hazardous Indicator
	 */
	public void setHazardousIndicator(final Boolean value)
	{
		setHazardousIndicator( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @param value the hazardousIndicator - hazardous Indicator
	 */
	public void setHazardousIndicator(final SessionContext ctx, final boolean value)
	{
		setHazardousIndicator( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousIndicator</code> attribute. 
	 * @param value the hazardousIndicator - hazardous Indicator
	 */
	public void setHazardousIndicator(final boolean value)
	{
		setHazardousIndicator( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousMaterialDescription</code> attribute.
	 * @return the hazardousMaterialDescription - Hazardous Material Description
	 */
	public String getHazardousMaterialDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HAZARDOUSMATERIALDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.hazardousMaterialDescription</code> attribute.
	 * @return the hazardousMaterialDescription - Hazardous Material Description
	 */
	public String getHazardousMaterialDescription()
	{
		return getHazardousMaterialDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousMaterialDescription</code> attribute. 
	 * @param value the hazardousMaterialDescription - Hazardous Material Description
	 */
	public void setHazardousMaterialDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HAZARDOUSMATERIALDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.hazardousMaterialDescription</code> attribute. 
	 * @param value the hazardousMaterialDescription - Hazardous Material Description
	 */
	public void setHazardousMaterialDescription(final String value)
	{
		setHazardousMaterialDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heavyDutyTruck</code> attribute.
	 * @return the heavyDutyTruck - Heavy Duty Truck
	 */
	public Boolean isHeavyDutyTruck(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, HEAVYDUTYTRUCK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heavyDutyTruck</code> attribute.
	 * @return the heavyDutyTruck - Heavy Duty Truck
	 */
	public Boolean isHeavyDutyTruck()
	{
		return isHeavyDutyTruck( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @return the heavyDutyTruck - Heavy Duty Truck
	 */
	public boolean isHeavyDutyTruckAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isHeavyDutyTruck( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @return the heavyDutyTruck - Heavy Duty Truck
	 */
	public boolean isHeavyDutyTruckAsPrimitive()
	{
		return isHeavyDutyTruckAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @param value the heavyDutyTruck - Heavy Duty Truck
	 */
	public void setHeavyDutyTruck(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, HEAVYDUTYTRUCK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @param value the heavyDutyTruck - Heavy Duty Truck
	 */
	public void setHeavyDutyTruck(final Boolean value)
	{
		setHeavyDutyTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @param value the heavyDutyTruck - Heavy Duty Truck
	 */
	public void setHeavyDutyTruck(final SessionContext ctx, final boolean value)
	{
		setHeavyDutyTruck( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heavyDutyTruck</code> attribute. 
	 * @param value the heavyDutyTruck - Heavy Duty Truck
	 */
	public void setHeavyDutyTruck(final boolean value)
	{
		setHeavyDutyTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heightCase</code> attribute.
	 * @return the heightCase - Height (Case)
	 */
	public String getHeightCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HEIGHTCASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heightCase</code> attribute.
	 * @return the heightCase - Height (Case)
	 */
	public String getHeightCase()
	{
		return getHeightCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heightCase</code> attribute. 
	 * @param value the heightCase - Height (Case)
	 */
	public void setHeightCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HEIGHTCASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heightCase</code> attribute. 
	 * @param value the heightCase - Height (Case)
	 */
	public void setHeightCase(final String value)
	{
		setHeightCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heightInnerPackage</code> attribute.
	 * @return the heightInnerPackage - Height (Inner Package)
	 */
	public String getHeightInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HEIGHTINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.heightInnerPackage</code> attribute.
	 * @return the heightInnerPackage - Height (Inner Package)
	 */
	public String getHeightInnerPackage()
	{
		return getHeightInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heightInnerPackage</code> attribute. 
	 * @param value the heightInnerPackage - Height (Inner Package)
	 */
	public void setHeightInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HEIGHTINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.heightInnerPackage</code> attribute. 
	 * @param value the heightInnerPackage - Height (Inner Package)
	 */
	public void setHeightInnerPackage(final String value)
	{
		setHeightInnerPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.highPerformance</code> attribute.
	 * @return the highPerformance - High Performance
	 */
	public Boolean isHighPerformance(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, HIGHPERFORMANCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.highPerformance</code> attribute.
	 * @return the highPerformance - High Performance
	 */
	public Boolean isHighPerformance()
	{
		return isHighPerformance( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.highPerformance</code> attribute. 
	 * @return the highPerformance - High Performance
	 */
	public boolean isHighPerformanceAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isHighPerformance( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.highPerformance</code> attribute. 
	 * @return the highPerformance - High Performance
	 */
	public boolean isHighPerformanceAsPrimitive()
	{
		return isHighPerformanceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.highPerformance</code> attribute. 
	 * @param value the highPerformance - High Performance
	 */
	public void setHighPerformance(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, HIGHPERFORMANCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.highPerformance</code> attribute. 
	 * @param value the highPerformance - High Performance
	 */
	public void setHighPerformance(final Boolean value)
	{
		setHighPerformance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.highPerformance</code> attribute. 
	 * @param value the highPerformance - High Performance
	 */
	public void setHighPerformance(final SessionContext ctx, final boolean value)
	{
		setHighPerformance( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.highPerformance</code> attribute. 
	 * @param value the highPerformance - High Performance
	 */
	public void setHighPerformance(final boolean value)
	{
		setHighPerformance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute.
	 * @return the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public Boolean isIndustrialOffHighwayEquipment(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, INDUSTRIALOFFHIGHWAYEQUIPMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute.
	 * @return the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public Boolean isIndustrialOffHighwayEquipment()
	{
		return isIndustrialOffHighwayEquipment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @return the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public boolean isIndustrialOffHighwayEquipmentAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isIndustrialOffHighwayEquipment( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @return the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public boolean isIndustrialOffHighwayEquipmentAsPrimitive()
	{
		return isIndustrialOffHighwayEquipmentAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @param value the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public void setIndustrialOffHighwayEquipment(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, INDUSTRIALOFFHIGHWAYEQUIPMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @param value the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public void setIndustrialOffHighwayEquipment(final Boolean value)
	{
		setIndustrialOffHighwayEquipment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @param value the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public void setIndustrialOffHighwayEquipment(final SessionContext ctx, final boolean value)
	{
		setIndustrialOffHighwayEquipment( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.industrialOffHighwayEquipment</code> attribute. 
	 * @param value the industrialOffHighwayEquipment - Industrial Off Highway Equipment
	 */
	public void setIndustrialOffHighwayEquipment(final boolean value)
	{
		setIndustrialOffHighwayEquipment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINQualifier</code> attribute.
	 * @return the itemLevelGTINQualifier - ItemLevel GTIN Qualifier
	 */
	public String getItemLevelGTINQualifier(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ITEMLEVELGTINQUALIFIER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINQualifier</code> attribute.
	 * @return the itemLevelGTINQualifier - ItemLevel GTIN Qualifier
	 */
	public String getItemLevelGTINQualifier()
	{
		return getItemLevelGTINQualifier( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINQualifier</code> attribute. 
	 * @param value the itemLevelGTINQualifier - ItemLevel GTIN Qualifier
	 */
	public void setItemLevelGTINQualifier(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ITEMLEVELGTINQUALIFIER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINQualifier</code> attribute. 
	 * @param value the itemLevelGTINQualifier - ItemLevel GTIN Qualifier
	 */
	public void setItemLevelGTINQualifier(final String value)
	{
		setItemLevelGTINQualifier( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINUPC</code> attribute.
	 * @return the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public Long getItemLevelGTINUPC(final SessionContext ctx)
	{
		return (Long)getProperty( ctx, ITEMLEVELGTINUPC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINUPC</code> attribute.
	 * @return the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public Long getItemLevelGTINUPC()
	{
		return getItemLevelGTINUPC( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @return the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public long getItemLevelGTINUPCAsPrimitive(final SessionContext ctx)
	{
		Long value = getItemLevelGTINUPC( ctx );
		return value != null ? value.longValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @return the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public long getItemLevelGTINUPCAsPrimitive()
	{
		return getItemLevelGTINUPCAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @param value the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public void setItemLevelGTINUPC(final SessionContext ctx, final Long value)
	{
		setProperty(ctx, ITEMLEVELGTINUPC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @param value the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public void setItemLevelGTINUPC(final Long value)
	{
		setItemLevelGTINUPC( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @param value the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public void setItemLevelGTINUPC(final SessionContext ctx, final long value)
	{
		setItemLevelGTINUPC( ctx,Long.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemLevelGTINUPC</code> attribute. 
	 * @param value the itemLevelGTINUPC - ItemLevel GTIN (UPC)
	 */
	public void setItemLevelGTINUPC(final long value)
	{
		setItemLevelGTINUPC( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySize</code> attribute.
	 * @return the itemQuantitySize - Item Quantity Size
	 */
	public Integer getItemQuantitySize(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, ITEMQUANTITYSIZE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySize</code> attribute.
	 * @return the itemQuantitySize - Item Quantity Size
	 */
	public Integer getItemQuantitySize()
	{
		return getItemQuantitySize( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @return the itemQuantitySize - Item Quantity Size
	 */
	public int getItemQuantitySizeAsPrimitive(final SessionContext ctx)
	{
		Integer value = getItemQuantitySize( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @return the itemQuantitySize - Item Quantity Size
	 */
	public int getItemQuantitySizeAsPrimitive()
	{
		return getItemQuantitySizeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @param value the itemQuantitySize - Item Quantity Size
	 */
	public void setItemQuantitySize(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, ITEMQUANTITYSIZE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @param value the itemQuantitySize - Item Quantity Size
	 */
	public void setItemQuantitySize(final Integer value)
	{
		setItemQuantitySize( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @param value the itemQuantitySize - Item Quantity Size
	 */
	public void setItemQuantitySize(final SessionContext ctx, final int value)
	{
		setItemQuantitySize( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySize</code> attribute. 
	 * @param value the itemQuantitySize - Item Quantity Size
	 */
	public void setItemQuantitySize(final int value)
	{
		setItemQuantitySize( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySizeUOM</code> attribute.
	 * @return the itemQuantitySizeUOM - Item Quantity Size UOM
	 */
	public String getItemQuantitySizeUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ITEMQUANTITYSIZEUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.itemQuantitySizeUOM</code> attribute.
	 * @return the itemQuantitySizeUOM - Item Quantity Size UOM
	 */
	public String getItemQuantitySizeUOM()
	{
		return getItemQuantitySizeUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySizeUOM</code> attribute. 
	 * @param value the itemQuantitySizeUOM - Item Quantity Size UOM
	 */
	public void setItemQuantitySizeUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ITEMQUANTITYSIZEUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.itemQuantitySizeUOM</code> attribute. 
	 * @param value the itemQuantitySizeUOM - Item Quantity Size UOM
	 */
	public void setItemQuantitySizeUOM(final String value)
	{
		setItemQuantitySizeUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.jobberPrice</code> attribute.
	 * @return the jobberPrice - Jobber Price
	 */
	public Double getJobberPrice(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, JOBBERPRICE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.jobberPrice</code> attribute.
	 * @return the jobberPrice - Jobber Price
	 */
	public Double getJobberPrice()
	{
		return getJobberPrice( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @return the jobberPrice - Jobber Price
	 */
	public double getJobberPriceAsPrimitive(final SessionContext ctx)
	{
		Double value = getJobberPrice( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @return the jobberPrice - Jobber Price
	 */
	public double getJobberPriceAsPrimitive()
	{
		return getJobberPriceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @param value the jobberPrice - Jobber Price
	 */
	public void setJobberPrice(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, JOBBERPRICE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @param value the jobberPrice - Jobber Price
	 */
	public void setJobberPrice(final Double value)
	{
		setJobberPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @param value the jobberPrice - Jobber Price
	 */
	public void setJobberPrice(final SessionContext ctx, final double value)
	{
		setJobberPrice( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.jobberPrice</code> attribute. 
	 * @param value the jobberPrice - Jobber Price
	 */
	public void setJobberPrice(final double value)
	{
		setJobberPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lengthCase</code> attribute.
	 * @return the lengthCase - Length (Case)
	 */
	public String getLengthCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LENGTHCASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lengthCase</code> attribute.
	 * @return the lengthCase - Length (Case)
	 */
	public String getLengthCase()
	{
		return getLengthCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lengthCase</code> attribute. 
	 * @param value the lengthCase - Length (Case)
	 */
	public void setLengthCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LENGTHCASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lengthCase</code> attribute. 
	 * @param value the lengthCase - Length (Case)
	 */
	public void setLengthCase(final String value)
	{
		setLengthCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lengthInnerPackage</code> attribute.
	 * @return the lengthInnerPackage - Length (Inner Package)
	 */
	public String getLengthInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LENGTHINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lengthInnerPackage</code> attribute.
	 * @return the lengthInnerPackage - Length (Inner Package)
	 */
	public String getLengthInnerPackage()
	{
		return getLengthInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lengthInnerPackage</code> attribute. 
	 * @param value the lengthInnerPackage - Length (Inner Package)
	 */
	public void setLengthInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LENGTHINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lengthInnerPackage</code> attribute. 
	 * @param value the lengthInnerPackage - Length (Inner Package)
	 */
	public void setLengthInnerPackage(final String value)
	{
		setLengthInnerPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusCode</code> attribute.
	 * @return the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public Integer getLifeCycleStatusCode(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, LIFECYCLESTATUSCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusCode</code> attribute.
	 * @return the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public Integer getLifeCycleStatusCode()
	{
		return getLifeCycleStatusCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @return the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public int getLifeCycleStatusCodeAsPrimitive(final SessionContext ctx)
	{
		Integer value = getLifeCycleStatusCode( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @return the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public int getLifeCycleStatusCodeAsPrimitive()
	{
		return getLifeCycleStatusCodeAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @param value the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public void setLifeCycleStatusCode(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, LIFECYCLESTATUSCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @param value the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public void setLifeCycleStatusCode(final Integer value)
	{
		setLifeCycleStatusCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @param value the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public void setLifeCycleStatusCode(final SessionContext ctx, final int value)
	{
		setLifeCycleStatusCode( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusCode</code> attribute. 
	 * @param value the lifeCycleStatusCode - Life Cycle Status Code
	 */
	public void setLifeCycleStatusCode(final int value)
	{
		setLifeCycleStatusCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusDescription</code> attribute.
	 * @return the lifeCycleStatusDescription - Life Cycle Status Description
	 */
	public String getLifeCycleStatusDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LIFECYCLESTATUSDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.lifeCycleStatusDescription</code> attribute.
	 * @return the lifeCycleStatusDescription - Life Cycle Status Description
	 */
	public String getLifeCycleStatusDescription()
	{
		return getLifeCycleStatusDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusDescription</code> attribute. 
	 * @param value the lifeCycleStatusDescription - Life Cycle Status Description
	 */
	public void setLifeCycleStatusDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LIFECYCLESTATUSDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.lifeCycleStatusDescription</code> attribute. 
	 * @param value the lifeCycleStatusDescription - Life Cycle Status Description
	 */
	public void setLifeCycleStatusDescription(final String value)
	{
		setLifeCycleStatusDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.listPrice</code> attribute.
	 * @return the listPrice - List Price
	 */
	public Double getListPrice(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, LISTPRICE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.listPrice</code> attribute.
	 * @return the listPrice - List Price
	 */
	public Double getListPrice()
	{
		return getListPrice( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.listPrice</code> attribute. 
	 * @return the listPrice - List Price
	 */
	public double getListPriceAsPrimitive(final SessionContext ctx)
	{
		Double value = getListPrice( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.listPrice</code> attribute. 
	 * @return the listPrice - List Price
	 */
	public double getListPriceAsPrimitive()
	{
		return getListPriceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.listPrice</code> attribute. 
	 * @param value the listPrice - List Price
	 */
	public void setListPrice(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, LISTPRICE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.listPrice</code> attribute. 
	 * @param value the listPrice - List Price
	 */
	public void setListPrice(final Double value)
	{
		setListPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.listPrice</code> attribute. 
	 * @param value the listPrice - List Price
	 */
	public void setListPrice(final SessionContext ctx, final double value)
	{
		setListPrice( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.listPrice</code> attribute. 
	 * @param value the listPrice - List Price
	 */
	public void setListPrice(final double value)
	{
		setListPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.longDescription</code> attribute.
	 * @return the longDescription - Long Description
	 */
	public String getLongDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LONGDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.longDescription</code> attribute.
	 * @return the longDescription - Long Description
	 */
	public String getLongDescription()
	{
		return getLongDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.longDescription</code> attribute. 
	 * @param value the longDescription - Long Description
	 */
	public void setLongDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LONGDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.longDescription</code> attribute. 
	 * @param value the longDescription - Long Description
	 */
	public void setLongDescription(final String value)
	{
		setLongDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.marine</code> attribute.
	 * @return the marine - Marine
	 */
	public Boolean isMarine(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, MARINE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.marine</code> attribute.
	 * @return the marine - Marine
	 */
	public Boolean isMarine()
	{
		return isMarine( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.marine</code> attribute. 
	 * @return the marine - Marine
	 */
	public boolean isMarineAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isMarine( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.marine</code> attribute. 
	 * @return the marine - Marine
	 */
	public boolean isMarineAsPrimitive()
	{
		return isMarineAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.marine</code> attribute. 
	 * @param value the marine - Marine
	 */
	public void setMarine(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, MARINE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.marine</code> attribute. 
	 * @param value the marine - Marine
	 */
	public void setMarine(final Boolean value)
	{
		setMarine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.marine</code> attribute. 
	 * @param value the marine - Marine
	 */
	public void setMarine(final SessionContext ctx, final boolean value)
	{
		setMarine( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.marine</code> attribute. 
	 * @param value the marine - Marine
	 */
	public void setMarine(final boolean value)
	{
		setMarine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.maxCasePerPalletLayer</code> attribute.
	 * @return the maxCasePerPalletLayer - Max Case per Pallet Layer
	 */
	public String getMaxCasePerPalletLayer(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MAXCASEPERPALLETLAYER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.maxCasePerPalletLayer</code> attribute.
	 * @return the maxCasePerPalletLayer - Max Case per Pallet Layer
	 */
	public String getMaxCasePerPalletLayer()
	{
		return getMaxCasePerPalletLayer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.maxCasePerPalletLayer</code> attribute. 
	 * @param value the maxCasePerPalletLayer - Max Case per Pallet Layer
	 */
	public void setMaxCasePerPalletLayer(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MAXCASEPERPALLETLAYER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.maxCasePerPalletLayer</code> attribute. 
	 * @param value the maxCasePerPalletLayer - Max Case per Pallet Layer
	 */
	public void setMaxCasePerPalletLayer(final String value)
	{
		setMaxCasePerPalletLayer( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mediumDutyTruck</code> attribute.
	 * @return the mediumDutyTruck - Medium Duty Truck
	 */
	public Boolean isMediumDutyTruck(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, MEDIUMDUTYTRUCK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mediumDutyTruck</code> attribute.
	 * @return the mediumDutyTruck - Medium Duty Truck
	 */
	public Boolean isMediumDutyTruck()
	{
		return isMediumDutyTruck( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @return the mediumDutyTruck - Medium Duty Truck
	 */
	public boolean isMediumDutyTruckAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isMediumDutyTruck( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @return the mediumDutyTruck - Medium Duty Truck
	 */
	public boolean isMediumDutyTruckAsPrimitive()
	{
		return isMediumDutyTruckAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @param value the mediumDutyTruck - Medium Duty Truck
	 */
	public void setMediumDutyTruck(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, MEDIUMDUTYTRUCK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @param value the mediumDutyTruck - Medium Duty Truck
	 */
	public void setMediumDutyTruck(final Boolean value)
	{
		setMediumDutyTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @param value the mediumDutyTruck - Medium Duty Truck
	 */
	public void setMediumDutyTruck(final SessionContext ctx, final boolean value)
	{
		setMediumDutyTruck( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mediumDutyTruck</code> attribute. 
	 * @param value the mediumDutyTruck - Medium Duty Truck
	 */
	public void setMediumDutyTruck(final boolean value)
	{
		setMediumDutyTruck( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantity</code> attribute.
	 * @return the minimumOrderQuantity - Minimum Order Quantity
	 */
	public Integer getMinimumOrderQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, MINIMUMORDERQUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantity</code> attribute.
	 * @return the minimumOrderQuantity - Minimum Order Quantity
	 */
	public Integer getMinimumOrderQuantity()
	{
		return getMinimumOrderQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @return the minimumOrderQuantity - Minimum Order Quantity
	 */
	public int getMinimumOrderQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getMinimumOrderQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @return the minimumOrderQuantity - Minimum Order Quantity
	 */
	public int getMinimumOrderQuantityAsPrimitive()
	{
		return getMinimumOrderQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @param value the minimumOrderQuantity - Minimum Order Quantity
	 */
	public void setMinimumOrderQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, MINIMUMORDERQUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @param value the minimumOrderQuantity - Minimum Order Quantity
	 */
	public void setMinimumOrderQuantity(final Integer value)
	{
		setMinimumOrderQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @param value the minimumOrderQuantity - Minimum Order Quantity
	 */
	public void setMinimumOrderQuantity(final SessionContext ctx, final int value)
	{
		setMinimumOrderQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantity</code> attribute. 
	 * @param value the minimumOrderQuantity - Minimum Order Quantity
	 */
	public void setMinimumOrderQuantity(final int value)
	{
		setMinimumOrderQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantityUOM</code> attribute.
	 * @return the minimumOrderQuantityUOM - Minimum Order Quantity UOM
	 */
	public String getMinimumOrderQuantityUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MINIMUMORDERQUANTITYUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.minimumOrderQuantityUOM</code> attribute.
	 * @return the minimumOrderQuantityUOM - Minimum Order Quantity UOM
	 */
	public String getMinimumOrderQuantityUOM()
	{
		return getMinimumOrderQuantityUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantityUOM</code> attribute. 
	 * @param value the minimumOrderQuantityUOM - Minimum Order Quantity UOM
	 */
	public void setMinimumOrderQuantityUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MINIMUMORDERQUANTITYUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.minimumOrderQuantityUOM</code> attribute. 
	 * @param value the minimumOrderQuantityUOM - Minimum Order Quantity UOM
	 */
	public void setMinimumOrderQuantityUOM(final String value)
	{
		setMinimumOrderQuantityUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.motorcycleATV</code> attribute.
	 * @return the motorcycleATV - Motorcycle ATV
	 */
	public Boolean isMotorcycleATV(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, MOTORCYCLEATV);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.motorcycleATV</code> attribute.
	 * @return the motorcycleATV - Motorcycle ATV
	 */
	public Boolean isMotorcycleATV()
	{
		return isMotorcycleATV( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @return the motorcycleATV - Motorcycle ATV
	 */
	public boolean isMotorcycleATVAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isMotorcycleATV( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @return the motorcycleATV - Motorcycle ATV
	 */
	public boolean isMotorcycleATVAsPrimitive()
	{
		return isMotorcycleATVAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @param value the motorcycleATV - Motorcycle ATV
	 */
	public void setMotorcycleATV(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, MOTORCYCLEATV,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @param value the motorcycleATV - Motorcycle ATV
	 */
	public void setMotorcycleATV(final Boolean value)
	{
		setMotorcycleATV( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @param value the motorcycleATV - Motorcycle ATV
	 */
	public void setMotorcycleATV(final SessionContext ctx, final boolean value)
	{
		setMotorcycleATV( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.motorcycleATV</code> attribute. 
	 * @param value the motorcycleATV - Motorcycle ATV
	 */
	public void setMotorcycleATV(final boolean value)
	{
		setMotorcycleATV( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetAvailable</code> attribute.
	 * @return the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public Boolean isMSDSSheetAvailable(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, MSDSSHEETAVAILABLE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetAvailable</code> attribute.
	 * @return the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public Boolean isMSDSSheetAvailable()
	{
		return isMSDSSheetAvailable( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @return the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public boolean isMSDSSheetAvailableAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isMSDSSheetAvailable( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @return the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public boolean isMSDSSheetAvailableAsPrimitive()
	{
		return isMSDSSheetAvailableAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @param value the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public void setMSDSSheetAvailable(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, MSDSSHEETAVAILABLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @param value the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public void setMSDSSheetAvailable(final Boolean value)
	{
		setMSDSSheetAvailable( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @param value the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public void setMSDSSheetAvailable(final SessionContext ctx, final boolean value)
	{
		setMSDSSheetAvailable( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetAvailable</code> attribute. 
	 * @param value the mSDSSheetAvailable - MSDS Sheet Available Y/N
	 */
	public void setMSDSSheetAvailable(final boolean value)
	{
		setMSDSSheetAvailable( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetNumber</code> attribute.
	 * @return the mSDSSheetNumber - MSDS Sheet Number
	 */
	public String getMSDSSheetNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MSDSSHEETNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.mSDSSheetNumber</code> attribute.
	 * @return the mSDSSheetNumber - MSDS Sheet Number
	 */
	public String getMSDSSheetNumber()
	{
		return getMSDSSheetNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetNumber</code> attribute. 
	 * @param value the mSDSSheetNumber - MSDS Sheet Number
	 */
	public void setMSDSSheetNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MSDSSHEETNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.mSDSSheetNumber</code> attribute. 
	 * @param value the mSDSSheetNumber - MSDS Sheet Number
	 */
	public void setMSDSSheetNumber(final String value)
	{
		setMSDSSheetNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPABuys</code> attribute.
	 * @return the nAPABuys - NAPA Buys? Y/N
	 */
	public Boolean isNAPABuys(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, NAPABUYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPABuys</code> attribute.
	 * @return the nAPABuys - NAPA Buys? Y/N
	 */
	public Boolean isNAPABuys()
	{
		return isNAPABuys( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @return the nAPABuys - NAPA Buys? Y/N
	 */
	public boolean isNAPABuysAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isNAPABuys( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @return the nAPABuys - NAPA Buys? Y/N
	 */
	public boolean isNAPABuysAsPrimitive()
	{
		return isNAPABuysAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @param value the nAPABuys - NAPA Buys? Y/N
	 */
	public void setNAPABuys(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, NAPABUYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @param value the nAPABuys - NAPA Buys? Y/N
	 */
	public void setNAPABuys(final Boolean value)
	{
		setNAPABuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @param value the nAPABuys - NAPA Buys? Y/N
	 */
	public void setNAPABuys(final SessionContext ctx, final boolean value)
	{
		setNAPABuys( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPABuys</code> attribute. 
	 * @param value the nAPABuys - NAPA Buys? Y/N
	 */
	public void setNAPABuys(final boolean value)
	{
		setNAPABuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPAPartNumber</code> attribute.
	 * @return the nAPAPartNumber - NAPA Part Number
	 */
	public Boolean isNAPAPartNumber(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, NAPAPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPAPartNumber</code> attribute.
	 * @return the nAPAPartNumber - NAPA Part Number
	 */
	public Boolean isNAPAPartNumber()
	{
		return isNAPAPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @return the nAPAPartNumber - NAPA Part Number
	 */
	public boolean isNAPAPartNumberAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isNAPAPartNumber( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @return the nAPAPartNumber - NAPA Part Number
	 */
	public boolean isNAPAPartNumberAsPrimitive()
	{
		return isNAPAPartNumberAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @param value the nAPAPartNumber - NAPA Part Number
	 */
	public void setNAPAPartNumber(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, NAPAPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @param value the nAPAPartNumber - NAPA Part Number
	 */
	public void setNAPAPartNumber(final Boolean value)
	{
		setNAPAPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @param value the nAPAPartNumber - NAPA Part Number
	 */
	public void setNAPAPartNumber(final SessionContext ctx, final boolean value)
	{
		setNAPAPartNumber( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nAPAPartNumber</code> attribute. 
	 * @param value the nAPAPartNumber - NAPA Part Number
	 */
	public void setNAPAPartNumber(final boolean value)
	{
		setNAPAPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nationalPopularityCode</code> attribute.
	 * @return the nationalPopularityCode - National Popularity Code
	 */
	public String getNationalPopularityCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NATIONALPOPULARITYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nationalPopularityCode</code> attribute.
	 * @return the nationalPopularityCode - National Popularity Code
	 */
	public String getNationalPopularityCode()
	{
		return getNationalPopularityCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nationalPopularityCode</code> attribute. 
	 * @param value the nationalPopularityCode - National Popularity Code
	 */
	public void setNationalPopularityCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NATIONALPOPULARITYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nationalPopularityCode</code> attribute. 
	 * @param value the nationalPopularityCode - National Popularity Code
	 */
	public void setNationalPopularityCode(final String value)
	{
		setNationalPopularityCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nonVehicleSpecific</code> attribute.
	 * @return the nonVehicleSpecific - Non Vehicle Specific
	 */
	public Boolean isNonVehicleSpecific(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, NONVEHICLESPECIFIC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nonVehicleSpecific</code> attribute.
	 * @return the nonVehicleSpecific - Non Vehicle Specific
	 */
	public Boolean isNonVehicleSpecific()
	{
		return isNonVehicleSpecific( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @return the nonVehicleSpecific - Non Vehicle Specific
	 */
	public boolean isNonVehicleSpecificAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isNonVehicleSpecific( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @return the nonVehicleSpecific - Non Vehicle Specific
	 */
	public boolean isNonVehicleSpecificAsPrimitive()
	{
		return isNonVehicleSpecificAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @param value the nonVehicleSpecific - Non Vehicle Specific
	 */
	public void setNonVehicleSpecific(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, NONVEHICLESPECIFIC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @param value the nonVehicleSpecific - Non Vehicle Specific
	 */
	public void setNonVehicleSpecific(final Boolean value)
	{
		setNonVehicleSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @param value the nonVehicleSpecific - Non Vehicle Specific
	 */
	public void setNonVehicleSpecific(final SessionContext ctx, final boolean value)
	{
		setNonVehicleSpecific( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.nonVehicleSpecific</code> attribute. 
	 * @param value the nonVehicleSpecific - Non Vehicle Specific
	 */
	public void setNonVehicleSpecific(final boolean value)
	{
		setNonVehicleSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.oReillyBuys</code> attribute.
	 * @return the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public Boolean isOReillyBuys(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, OREILLYBUYS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.oReillyBuys</code> attribute.
	 * @return the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public Boolean isOReillyBuys()
	{
		return isOReillyBuys( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @return the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public boolean isOReillyBuysAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isOReillyBuys( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @return the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public boolean isOReillyBuysAsPrimitive()
	{
		return isOReillyBuysAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @param value the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public void setOReillyBuys(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, OREILLYBUYS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @param value the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public void setOReillyBuys(final Boolean value)
	{
		setOReillyBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @param value the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public void setOReillyBuys(final SessionContext ctx, final boolean value)
	{
		setOReillyBuys( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.oReillyBuys</code> attribute. 
	 * @param value the oReillyBuys - O'Reilly Buys? Y/N
	 */
	public void setOReillyBuys(final boolean value)
	{
		setOReillyBuys( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.otherCustomersifany</code> attribute.
	 * @return the otherCustomersifany - Other Customers (if any)
	 */
	public String getOtherCustomersifany(final SessionContext ctx)
	{
		return (String)getProperty( ctx, OTHERCUSTOMERSIFANY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.otherCustomersifany</code> attribute.
	 * @return the otherCustomersifany - Other Customers (if any)
	 */
	public String getOtherCustomersifany()
	{
		return getOtherCustomersifany( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.otherCustomersifany</code> attribute. 
	 * @param value the otherCustomersifany - Other Customers (if any)
	 */
	public void setOtherCustomersifany(final SessionContext ctx, final String value)
	{
		setProperty(ctx, OTHERCUSTOMERSIFANY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.otherCustomersifany</code> attribute. 
	 * @param value the otherCustomersifany - Other Customers (if any)
	 */
	public void setOtherCustomersifany(final String value)
	{
		setOtherCustomersifany( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachHeight</code> attribute.
	 * @return the packageEachHeight - Package (Each) Height
	 */
	public String getPackageEachHeight(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEEACHHEIGHT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachHeight</code> attribute.
	 * @return the packageEachHeight - Package (Each) Height
	 */
	public String getPackageEachHeight()
	{
		return getPackageEachHeight( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachHeight</code> attribute. 
	 * @param value the packageEachHeight - Package (Each) Height
	 */
	public void setPackageEachHeight(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEEACHHEIGHT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachHeight</code> attribute. 
	 * @param value the packageEachHeight - Package (Each) Height
	 */
	public void setPackageEachHeight(final String value)
	{
		setPackageEachHeight( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachLength</code> attribute.
	 * @return the packageEachLength - Package (Each) Length
	 */
	public String getPackageEachLength(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEEACHLENGTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachLength</code> attribute.
	 * @return the packageEachLength - Package (Each) Length
	 */
	public String getPackageEachLength()
	{
		return getPackageEachLength( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachLength</code> attribute. 
	 * @param value the packageEachLength - Package (Each) Length
	 */
	public void setPackageEachLength(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEEACHLENGTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachLength</code> attribute. 
	 * @param value the packageEachLength - Package (Each) Length
	 */
	public void setPackageEachLength(final String value)
	{
		setPackageEachLength( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachLevelGTINUPC</code> attribute.
	 * @return the packageEachLevelGTINUPC - Package (Each) Level GTIN (UPC)
	 */
	public String getPackageEachLevelGTINUPC(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEEACHLEVELGTINUPC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachLevelGTINUPC</code> attribute.
	 * @return the packageEachLevelGTINUPC - Package (Each) Level GTIN (UPC)
	 */
	public String getPackageEachLevelGTINUPC()
	{
		return getPackageEachLevelGTINUPC( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachLevelGTINUPC</code> attribute. 
	 * @param value the packageEachLevelGTINUPC - Package (Each) Level GTIN (UPC)
	 */
	public void setPackageEachLevelGTINUPC(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEEACHLEVELGTINUPC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachLevelGTINUPC</code> attribute. 
	 * @param value the packageEachLevelGTINUPC - Package (Each) Level GTIN (UPC)
	 */
	public void setPackageEachLevelGTINUPC(final String value)
	{
		setPackageEachLevelGTINUPC( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachWeight</code> attribute.
	 * @return the packageEachWeight - Package (Each) Weight
	 */
	public String getPackageEachWeight(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEEACHWEIGHT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachWeight</code> attribute.
	 * @return the packageEachWeight - Package (Each) Weight
	 */
	public String getPackageEachWeight()
	{
		return getPackageEachWeight( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachWeight</code> attribute. 
	 * @param value the packageEachWeight - Package (Each) Weight
	 */
	public void setPackageEachWeight(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEEACHWEIGHT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachWeight</code> attribute. 
	 * @param value the packageEachWeight - Package (Each) Weight
	 */
	public void setPackageEachWeight(final String value)
	{
		setPackageEachWeight( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachWidth</code> attribute.
	 * @return the packageEachWidth - Package (Each) Width
	 */
	public String getPackageEachWidth(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEEACHWIDTH);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageEachWidth</code> attribute.
	 * @return the packageEachWidth - Package (Each) Width
	 */
	public String getPackageEachWidth()
	{
		return getPackageEachWidth( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachWidth</code> attribute. 
	 * @param value the packageEachWidth - Package (Each) Width
	 */
	public void setPackageEachWidth(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEEACHWIDTH,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageEachWidth</code> attribute. 
	 * @param value the packageEachWidth - Package (Each) Width
	 */
	public void setPackageEachWidth(final String value)
	{
		setPackageEachWidth( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageUOM</code> attribute.
	 * @return the packageUOM - Package UOM
	 */
	public String getPackageUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PACKAGEUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.packageUOM</code> attribute.
	 * @return the packageUOM - Package UOM
	 */
	public String getPackageUOM()
	{
		return getPackageUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageUOM</code> attribute. 
	 * @param value the packageUOM - Package UOM
	 */
	public void setPackageUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PACKAGEUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.packageUOM</code> attribute. 
	 * @param value the packageUOM - Package UOM
	 */
	public void setPackageUOM(final String value)
	{
		setPackageUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletFootprintSize</code> attribute.
	 * @return the palletFootprintSize - Pallet Footprint Size
	 */
	public String getPalletFootprintSize(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PALLETFOOTPRINTSIZE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletFootprintSize</code> attribute.
	 * @return the palletFootprintSize - Pallet Footprint Size
	 */
	public String getPalletFootprintSize()
	{
		return getPalletFootprintSize( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletFootprintSize</code> attribute. 
	 * @param value the palletFootprintSize - Pallet Footprint Size
	 */
	public void setPalletFootprintSize(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PALLETFOOTPRINTSIZE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletFootprintSize</code> attribute. 
	 * @param value the palletFootprintSize - Pallet Footprint Size
	 */
	public void setPalletFootprintSize(final String value)
	{
		setPalletFootprintSize( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletFootprintSizeUOM</code> attribute.
	 * @return the palletFootprintSizeUOM - Pallet Footprint Size UOM
	 */
	public String getPalletFootprintSizeUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PALLETFOOTPRINTSIZEUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletFootprintSizeUOM</code> attribute.
	 * @return the palletFootprintSizeUOM - Pallet Footprint Size UOM
	 */
	public String getPalletFootprintSizeUOM()
	{
		return getPalletFootprintSizeUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletFootprintSizeUOM</code> attribute. 
	 * @param value the palletFootprintSizeUOM - Pallet Footprint Size UOM
	 */
	public void setPalletFootprintSizeUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PALLETFOOTPRINTSIZEUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletFootprintSizeUOM</code> attribute. 
	 * @param value the palletFootprintSizeUOM - Pallet Footprint Size UOM
	 */
	public void setPalletFootprintSizeUOM(final String value)
	{
		setPalletFootprintSizeUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletLayerMax</code> attribute.
	 * @return the palletLayerMax - Max Case per Pallet Layer
	 */
	public String getPalletLayerMax(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PALLETLAYERMAX);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.palletLayerMax</code> attribute.
	 * @return the palletLayerMax - Max Case per Pallet Layer
	 */
	public String getPalletLayerMax()
	{
		return getPalletLayerMax( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletLayerMax</code> attribute. 
	 * @param value the palletLayerMax - Max Case per Pallet Layer
	 */
	public void setPalletLayerMax(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PALLETLAYERMAX,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.palletLayerMax</code> attribute. 
	 * @param value the palletLayerMax - Max Case per Pallet Layer
	 */
	public void setPalletLayerMax(final String value)
	{
		setPalletLayerMax( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber()
	{
		return getPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final String value)
	{
		setPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumberOld</code> attribute.
	 * @return the partNumberOld - Part Number - Old
	 */
	public String getPartNumberOld(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBEROLD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumberOld</code> attribute.
	 * @return the partNumberOld - Part Number - Old
	 */
	public String getPartNumberOld()
	{
		return getPartNumberOld( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumberOld</code> attribute. 
	 * @param value the partNumberOld - Part Number - Old
	 */
	public void setPartNumberOld(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBEROLD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumberOld</code> attribute. 
	 * @param value the partNumberOld - Part Number - Old
	 */
	public void setPartNumberOld(final String value)
	{
		setPartNumberOld( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumberSupersededTo</code> attribute.
	 * @return the partNumberSupersededTo - Part Number Superseded To
	 */
	public String getPartNumberSupersededTo(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBERSUPERSEDEDTO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partNumberSupersededTo</code> attribute.
	 * @return the partNumberSupersededTo - Part Number Superseded To
	 */
	public String getPartNumberSupersededTo()
	{
		return getPartNumberSupersededTo( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumberSupersededTo</code> attribute. 
	 * @param value the partNumberSupersededTo - Part Number Superseded To
	 */
	public void setPartNumberSupersededTo(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBERSUPERSEDEDTO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partNumberSupersededTo</code> attribute. 
	 * @param value the partNumberSupersededTo - Part Number Superseded To
	 */
	public void setPartNumberSupersededTo(final String value)
	{
		setPartNumberSupersededTo( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partSequence</code> attribute.
	 * @return the partSequence - Part Sequence
	 */
	public String getPartSequence(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTSEQUENCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partSequence</code> attribute.
	 * @return the partSequence - Part Sequence
	 */
	public String getPartSequence()
	{
		return getPartSequence( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partSequence</code> attribute. 
	 * @param value the partSequence - Part Sequence
	 */
	public void setPartSequence(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTSEQUENCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partSequence</code> attribute. 
	 * @param value the partSequence - Part Sequence
	 */
	public void setPartSequence(final String value)
	{
		setPartSequence( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partstatus</code> attribute.
	 * @return the partstatus
	 */
	public EnumerationValue getPartstatus(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, PARTSTATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.partstatus</code> attribute.
	 * @return the partstatus
	 */
	public EnumerationValue getPartstatus()
	{
		return getPartstatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partstatus</code> attribute. 
	 * @param value the partstatus
	 */
	public void setPartstatus(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, PARTSTATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.partstatus</code> attribute. 
	 * @param value the partstatus
	 */
	public void setPartstatus(final EnumerationValue value)
	{
		setPartstatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.personalWatercraft</code> attribute.
	 * @return the personalWatercraft - Personal Watercraft
	 */
	public Boolean isPersonalWatercraft(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, PERSONALWATERCRAFT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.personalWatercraft</code> attribute.
	 * @return the personalWatercraft - Personal Watercraft
	 */
	public Boolean isPersonalWatercraft()
	{
		return isPersonalWatercraft( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @return the personalWatercraft - Personal Watercraft
	 */
	public boolean isPersonalWatercraftAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isPersonalWatercraft( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @return the personalWatercraft - Personal Watercraft
	 */
	public boolean isPersonalWatercraftAsPrimitive()
	{
		return isPersonalWatercraftAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @param value the personalWatercraft - Personal Watercraft
	 */
	public void setPersonalWatercraft(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, PERSONALWATERCRAFT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @param value the personalWatercraft - Personal Watercraft
	 */
	public void setPersonalWatercraft(final Boolean value)
	{
		setPersonalWatercraft( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @param value the personalWatercraft - Personal Watercraft
	 */
	public void setPersonalWatercraft(final SessionContext ctx, final boolean value)
	{
		setPersonalWatercraft( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.personalWatercraft</code> attribute. 
	 * @param value the personalWatercraft - Personal Watercraft
	 */
	public void setPersonalWatercraft(final boolean value)
	{
		setPersonalWatercraft( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pid</code> attribute.
	 * @return the pid
	 */
	public Integer getPid(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, PID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pid</code> attribute.
	 * @return the pid
	 */
	public Integer getPid()
	{
		return getPid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pid</code> attribute. 
	 * @return the pid
	 */
	public int getPidAsPrimitive(final SessionContext ctx)
	{
		Integer value = getPid( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pid</code> attribute. 
	 * @return the pid
	 */
	public int getPidAsPrimitive()
	{
		return getPidAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pid</code> attribute. 
	 * @param value the pid
	 */
	public void setPid(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, PID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pid</code> attribute. 
	 * @param value the pid
	 */
	public void setPid(final Integer value)
	{
		setPid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pid</code> attribute. 
	 * @param value the pid
	 */
	public void setPid(final SessionContext ctx, final int value)
	{
		setPid( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pid</code> attribute. 
	 * @param value the pid
	 */
	public void setPid(final int value)
	{
		setPid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pkgLevelGTINUPCCase</code> attribute.
	 * @return the pkgLevelGTINUPCCase - Pkg LevelGTIN UPC  (Case)
	 */
	public String getPkgLevelGTINUPCCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PKGLEVELGTINUPCCASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pkgLevelGTINUPCCase</code> attribute.
	 * @return the pkgLevelGTINUPCCase - Pkg LevelGTIN UPC  (Case)
	 */
	public String getPkgLevelGTINUPCCase()
	{
		return getPkgLevelGTINUPCCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pkgLevelGTINUPCCase</code> attribute. 
	 * @param value the pkgLevelGTINUPCCase - Pkg LevelGTIN UPC  (Case)
	 */
	public void setPkgLevelGTINUPCCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PKGLEVELGTINUPCCASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pkgLevelGTINUPCCase</code> attribute. 
	 * @param value the pkgLevelGTINUPCCase - Pkg LevelGTIN UPC  (Case)
	 */
	public void setPkgLevelGTINUPCCase(final String value)
	{
		setPkgLevelGTINUPCCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pkgLevelGTINUPCInnerPackage</code> attribute.
	 * @return the pkgLevelGTINUPCInnerPackage - Pkg LevelGTIN_UPC (Inner Package)
	 */
	public String getPkgLevelGTINUPCInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PKGLEVELGTINUPCINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.pkgLevelGTINUPCInnerPackage</code> attribute.
	 * @return the pkgLevelGTINUPCInnerPackage - Pkg LevelGTIN_UPC (Inner Package)
	 */
	public String getPkgLevelGTINUPCInnerPackage()
	{
		return getPkgLevelGTINUPCInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pkgLevelGTINUPCInnerPackage</code> attribute. 
	 * @param value the pkgLevelGTINUPCInnerPackage - Pkg LevelGTIN_UPC (Inner Package)
	 */
	public void setPkgLevelGTINUPCInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PKGLEVELGTINUPCINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.pkgLevelGTINUPCInnerPackage</code> attribute. 
	 * @param value the pkgLevelGTINUPCInnerPackage - Pkg LevelGTIN_UPC (Inner Package)
	 */
	public void setPkgLevelGTINUPCInnerPackage(final String value)
	{
		setPkgLevelGTINUPCInnerPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.popularityCode</code> attribute.
	 * @return the popularityCode - Popularity Code
	 */
	public String getPopularityCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POPULARITYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.popularityCode</code> attribute.
	 * @return the popularityCode - Popularity Code
	 */
	public String getPopularityCode()
	{
		return getPopularityCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.popularityCode</code> attribute. 
	 * @param value the popularityCode - Popularity Code
	 */
	public void setPopularityCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POPULARITYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.popularityCode</code> attribute. 
	 * @param value the popularityCode - Popularity Code
	 */
	public void setPopularityCode(final String value)
	{
		setPopularityCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetEffectiveDate</code> attribute.
	 * @return the priceSheetEffectiveDate - Alternate Part Number
	 */
	public Date getPriceSheetEffectiveDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, PRICESHEETEFFECTIVEDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetEffectiveDate</code> attribute.
	 * @return the priceSheetEffectiveDate - Alternate Part Number
	 */
	public Date getPriceSheetEffectiveDate()
	{
		return getPriceSheetEffectiveDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetEffectiveDate</code> attribute. 
	 * @param value the priceSheetEffectiveDate - Alternate Part Number
	 */
	public void setPriceSheetEffectiveDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, PRICESHEETEFFECTIVEDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetEffectiveDate</code> attribute. 
	 * @param value the priceSheetEffectiveDate - Alternate Part Number
	 */
	public void setPriceSheetEffectiveDate(final Date value)
	{
		setPriceSheetEffectiveDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetExpirationDate</code> attribute.
	 * @return the priceSheetExpirationDate - Price Sheet Expiration Date
	 */
	public Date getPriceSheetExpirationDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, PRICESHEETEXPIRATIONDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetExpirationDate</code> attribute.
	 * @return the priceSheetExpirationDate - Price Sheet Expiration Date
	 */
	public Date getPriceSheetExpirationDate()
	{
		return getPriceSheetExpirationDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetExpirationDate</code> attribute. 
	 * @param value the priceSheetExpirationDate - Price Sheet Expiration Date
	 */
	public void setPriceSheetExpirationDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, PRICESHEETEXPIRATIONDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetExpirationDate</code> attribute. 
	 * @param value the priceSheetExpirationDate - Price Sheet Expiration Date
	 */
	public void setPriceSheetExpirationDate(final Date value)
	{
		setPriceSheetExpirationDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetNumber</code> attribute.
	 * @return the priceSheetNumber - Price Sheet Number
	 */
	public String getPriceSheetNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRICESHEETNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.priceSheetNumber</code> attribute.
	 * @return the priceSheetNumber - Price Sheet Number
	 */
	public String getPriceSheetNumber()
	{
		return getPriceSheetNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetNumber</code> attribute. 
	 * @param value the priceSheetNumber - Price Sheet Number
	 */
	public void setPriceSheetNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRICESHEETNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.priceSheetNumber</code> attribute. 
	 * @param value the priceSheetNumber - Price Sheet Number
	 */
	public void setPriceSheetNumber(final String value)
	{
		setPriceSheetNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productCategoryCode</code> attribute.
	 * @return the productCategoryCode - Product Category Code
	 */
	public String getProductCategoryCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTCATEGORYCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productCategoryCode</code> attribute.
	 * @return the productCategoryCode - Product Category Code
	 */
	public String getProductCategoryCode()
	{
		return getProductCategoryCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productCategoryCode</code> attribute. 
	 * @param value the productCategoryCode - Product Category Code
	 */
	public void setProductCategoryCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTCATEGORYCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productCategoryCode</code> attribute. 
	 * @param value the productCategoryCode - Product Category Code
	 */
	public void setProductCategoryCode(final String value)
	{
		setProductCategoryCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productFlag</code> attribute.
	 * @return the productFlag - Product Flag
	 */
	public String getProductFlag(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productFlag</code> attribute.
	 * @return the productFlag - Product Flag
	 */
	public String getProductFlag()
	{
		return getProductFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productFlag</code> attribute. 
	 * @param value the productFlag - Product Flag
	 */
	public void setProductFlag(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productFlag</code> attribute. 
	 * @param value the productFlag - Product Flag
	 */
	public void setProductFlag(final String value)
	{
		setProductFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productGroupCode</code> attribute.
	 * @return the productGroupCode - Product Group Code
	 */
	public String getProductGroupCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTGROUPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productGroupCode</code> attribute.
	 * @return the productGroupCode - Product Group Code
	 */
	public String getProductGroupCode()
	{
		return getProductGroupCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productGroupCode</code> attribute. 
	 * @param value the productGroupCode - Product Group Code
	 */
	public void setProductGroupCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTGROUPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productGroupCode</code> attribute. 
	 * @param value the productGroupCode - Product Group Code
	 */
	public void setProductGroupCode(final String value)
	{
		setProductGroupCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productSubGroupCode</code> attribute.
	 * @return the productSubGroupCode - Product Sub-Group Code
	 */
	public String getProductSubGroupCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PRODUCTSUBGROUPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.productSubGroupCode</code> attribute.
	 * @return the productSubGroupCode - Product Sub-Group Code
	 */
	public String getProductSubGroupCode()
	{
		return getProductSubGroupCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productSubGroupCode</code> attribute. 
	 * @param value the productSubGroupCode - Product Sub-Group Code
	 */
	public void setProductSubGroupCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PRODUCTSUBGROUPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.productSubGroupCode</code> attribute. 
	 * @param value the productSubGroupCode - Product Sub-Group Code
	 */
	public void setProductSubGroupCode(final String value)
	{
		setProductSubGroupCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.qtyOfEachesinPkgInnerPackage</code> attribute.
	 * @return the qtyOfEachesinPkgInnerPackage - Qty of Eaches in Pkg (Inner Package)
	 */
	public String getQtyOfEachesinPkgInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, QTYOFEACHESINPKGINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.qtyOfEachesinPkgInnerPackage</code> attribute.
	 * @return the qtyOfEachesinPkgInnerPackage - Qty of Eaches in Pkg (Inner Package)
	 */
	public String getQtyOfEachesinPkgInnerPackage()
	{
		return getQtyOfEachesinPkgInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.qtyOfEachesinPkgInnerPackage</code> attribute. 
	 * @param value the qtyOfEachesinPkgInnerPackage - Qty of Eaches in Pkg (Inner Package)
	 */
	public void setQtyOfEachesinPkgInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, QTYOFEACHESINPKGINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.qtyOfEachesinPkgInnerPackage</code> attribute. 
	 * @param value the qtyOfEachesinPkgInnerPackage - Qty of Eaches in Pkg (Inner Package)
	 */
	public void setQtyOfEachesinPkgInnerPackage(final String value)
	{
		setQtyOfEachesinPkgInnerPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute.
	 * @return the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public Integer getQuantityOfEachesEachPackage(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUANTITYOFEACHESEACHPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute.
	 * @return the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public Integer getQuantityOfEachesEachPackage()
	{
		return getQuantityOfEachesEachPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @return the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public int getQuantityOfEachesEachPackageAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQuantityOfEachesEachPackage( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @return the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public int getQuantityOfEachesEachPackageAsPrimitive()
	{
		return getQuantityOfEachesEachPackageAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @param value the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public void setQuantityOfEachesEachPackage(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUANTITYOFEACHESEACHPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @param value the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public void setQuantityOfEachesEachPackage(final Integer value)
	{
		setQuantityOfEachesEachPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @param value the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public void setQuantityOfEachesEachPackage(final SessionContext ctx, final int value)
	{
		setQuantityOfEachesEachPackage( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesEachPackage</code> attribute. 
	 * @param value the quantityOfEachesEachPackage - Quantity of Eaches (Each Package)
	 */
	public void setQuantityOfEachesEachPackage(final int value)
	{
		setQuantityOfEachesEachPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesinPackageCase</code> attribute.
	 * @return the quantityOfEachesinPackageCase - Quantity of Eaches in Package  (Case)
	 */
	public String getQuantityOfEachesinPackageCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, QUANTITYOFEACHESINPACKAGECASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityOfEachesinPackageCase</code> attribute.
	 * @return the quantityOfEachesinPackageCase - Quantity of Eaches in Package  (Case)
	 */
	public String getQuantityOfEachesinPackageCase()
	{
		return getQuantityOfEachesinPackageCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesinPackageCase</code> attribute. 
	 * @param value the quantityOfEachesinPackageCase - Quantity of Eaches in Package  (Case)
	 */
	public void setQuantityOfEachesinPackageCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, QUANTITYOFEACHESINPACKAGECASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityOfEachesinPackageCase</code> attribute. 
	 * @param value the quantityOfEachesinPackageCase - Quantity of Eaches in Package  (Case)
	 */
	public void setQuantityOfEachesinPackageCase(final String value)
	{
		setQuantityOfEachesinPackageCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplication</code> attribute.
	 * @return the quantityPerApplication - Quantity Per Application
	 */
	public Integer getQuantityPerApplication(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUANTITYPERAPPLICATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplication</code> attribute.
	 * @return the quantityPerApplication - Quantity Per Application
	 */
	public Integer getQuantityPerApplication()
	{
		return getQuantityPerApplication( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @return the quantityPerApplication - Quantity Per Application
	 */
	public int getQuantityPerApplicationAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQuantityPerApplication( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @return the quantityPerApplication - Quantity Per Application
	 */
	public int getQuantityPerApplicationAsPrimitive()
	{
		return getQuantityPerApplicationAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @param value the quantityPerApplication - Quantity Per Application
	 */
	public void setQuantityPerApplication(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUANTITYPERAPPLICATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @param value the quantityPerApplication - Quantity Per Application
	 */
	public void setQuantityPerApplication(final Integer value)
	{
		setQuantityPerApplication( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @param value the quantityPerApplication - Quantity Per Application
	 */
	public void setQuantityPerApplication(final SessionContext ctx, final int value)
	{
		setQuantityPerApplication( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplication</code> attribute. 
	 * @param value the quantityPerApplication - Quantity Per Application
	 */
	public void setQuantityPerApplication(final int value)
	{
		setQuantityPerApplication( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplicationQualifier</code> attribute.
	 * @return the quantityPerApplicationQualifier - Quantity Per Application Qualifier
	 */
	public String getQuantityPerApplicationQualifier(final SessionContext ctx)
	{
		return (String)getProperty( ctx, QUANTITYPERAPPLICATIONQUALIFIER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplicationQualifier</code> attribute.
	 * @return the quantityPerApplicationQualifier - Quantity Per Application Qualifier
	 */
	public String getQuantityPerApplicationQualifier()
	{
		return getQuantityPerApplicationQualifier( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplicationQualifier</code> attribute. 
	 * @param value the quantityPerApplicationQualifier - Quantity Per Application Qualifier
	 */
	public void setQuantityPerApplicationQualifier(final SessionContext ctx, final String value)
	{
		setProperty(ctx, QUANTITYPERAPPLICATIONQUALIFIER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplicationQualifier</code> attribute. 
	 * @param value the quantityPerApplicationQualifier - Quantity Per Application Qualifier
	 */
	public void setQuantityPerApplicationQualifier(final String value)
	{
		setQuantityPerApplicationQualifier( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplicationUOM</code> attribute.
	 * @return the quantityPerApplicationUOM - Quantity Per Application UOM
	 */
	public String getQuantityPerApplicationUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, QUANTITYPERAPPLICATIONUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.quantityPerApplicationUOM</code> attribute.
	 * @return the quantityPerApplicationUOM - Quantity Per Application UOM
	 */
	public String getQuantityPerApplicationUOM()
	{
		return getQuantityPerApplicationUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplicationUOM</code> attribute. 
	 * @param value the quantityPerApplicationUOM - Quantity Per Application UOM
	 */
	public void setQuantityPerApplicationUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, QUANTITYPERAPPLICATIONUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.quantityPerApplicationUOM</code> attribute. 
	 * @param value the quantityPerApplicationUOM - Quantity Per Application UOM
	 */
	public void setQuantityPerApplicationUOM(final String value)
	{
		setQuantityPerApplicationUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber()
	{
		return getRawPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final String value)
	{
		setRawPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.remanufacturedPart</code> attribute.
	 * @return the remanufacturedPart - Remanufactured Part
	 */
	public Boolean isRemanufacturedPart(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, REMANUFACTUREDPART);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.remanufacturedPart</code> attribute.
	 * @return the remanufacturedPart - Remanufactured Part
	 */
	public Boolean isRemanufacturedPart()
	{
		return isRemanufacturedPart( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @return the remanufacturedPart - Remanufactured Part
	 */
	public boolean isRemanufacturedPartAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isRemanufacturedPart( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @return the remanufacturedPart - Remanufactured Part
	 */
	public boolean isRemanufacturedPartAsPrimitive()
	{
		return isRemanufacturedPartAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @param value the remanufacturedPart - Remanufactured Part
	 */
	public void setRemanufacturedPart(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, REMANUFACTUREDPART,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @param value the remanufacturedPart - Remanufactured Part
	 */
	public void setRemanufacturedPart(final Boolean value)
	{
		setRemanufacturedPart( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @param value the remanufacturedPart - Remanufactured Part
	 */
	public void setRemanufacturedPart(final SessionContext ctx, final boolean value)
	{
		setRemanufacturedPart( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.remanufacturedPart</code> attribute. 
	 * @param value the remanufacturedPart - Remanufactured Part
	 */
	public void setRemanufacturedPart(final boolean value)
	{
		setRemanufacturedPart( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sentImagetoAdvance</code> attribute.
	 * @return the sentImagetoAdvance - Sent Image to Advance?
	 */
	public Boolean isSentImagetoAdvance(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, SENTIMAGETOADVANCE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sentImagetoAdvance</code> attribute.
	 * @return the sentImagetoAdvance - Sent Image to Advance?
	 */
	public Boolean isSentImagetoAdvance()
	{
		return isSentImagetoAdvance( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @return the sentImagetoAdvance - Sent Image to Advance?
	 */
	public boolean isSentImagetoAdvanceAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isSentImagetoAdvance( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @return the sentImagetoAdvance - Sent Image to Advance?
	 */
	public boolean isSentImagetoAdvanceAsPrimitive()
	{
		return isSentImagetoAdvanceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @param value the sentImagetoAdvance - Sent Image to Advance?
	 */
	public void setSentImagetoAdvance(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, SENTIMAGETOADVANCE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @param value the sentImagetoAdvance - Sent Image to Advance?
	 */
	public void setSentImagetoAdvance(final Boolean value)
	{
		setSentImagetoAdvance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @param value the sentImagetoAdvance - Sent Image to Advance?
	 */
	public void setSentImagetoAdvance(final SessionContext ctx, final boolean value)
	{
		setSentImagetoAdvance( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sentImagetoAdvance</code> attribute. 
	 * @param value the sentImagetoAdvance - Sent Image to Advance?
	 */
	public void setSentImagetoAdvance(final boolean value)
	{
		setSentImagetoAdvance( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.smallEngine</code> attribute.
	 * @return the smallEngine - Small Engine
	 */
	public Boolean isSmallEngine(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, SMALLENGINE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.smallEngine</code> attribute.
	 * @return the smallEngine - Small Engine
	 */
	public Boolean isSmallEngine()
	{
		return isSmallEngine( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.smallEngine</code> attribute. 
	 * @return the smallEngine - Small Engine
	 */
	public boolean isSmallEngineAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isSmallEngine( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.smallEngine</code> attribute. 
	 * @return the smallEngine - Small Engine
	 */
	public boolean isSmallEngineAsPrimitive()
	{
		return isSmallEngineAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.smallEngine</code> attribute. 
	 * @param value the smallEngine - Small Engine
	 */
	public void setSmallEngine(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, SMALLENGINE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.smallEngine</code> attribute. 
	 * @param value the smallEngine - Small Engine
	 */
	public void setSmallEngine(final Boolean value)
	{
		setSmallEngine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.smallEngine</code> attribute. 
	 * @param value the smallEngine - Small Engine
	 */
	public void setSmallEngine(final SessionContext ctx, final boolean value)
	{
		setSmallEngine( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.smallEngine</code> attribute. 
	 * @param value the smallEngine - Small Engine
	 */
	public void setSmallEngine(final boolean value)
	{
		setSmallEngine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.snowMobile</code> attribute.
	 * @return the snowMobile - Snow mobile
	 */
	public Boolean isSnowMobile(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, SNOWMOBILE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.snowMobile</code> attribute.
	 * @return the snowMobile - Snow mobile
	 */
	public Boolean isSnowMobile()
	{
		return isSnowMobile( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.snowMobile</code> attribute. 
	 * @return the snowMobile - Snow mobile
	 */
	public boolean isSnowMobileAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isSnowMobile( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.snowMobile</code> attribute. 
	 * @return the snowMobile - Snow mobile
	 */
	public boolean isSnowMobileAsPrimitive()
	{
		return isSnowMobileAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.snowMobile</code> attribute. 
	 * @param value the snowMobile - Snow mobile
	 */
	public void setSnowMobile(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, SNOWMOBILE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.snowMobile</code> attribute. 
	 * @param value the snowMobile - Snow mobile
	 */
	public void setSnowMobile(final Boolean value)
	{
		setSnowMobile( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.snowMobile</code> attribute. 
	 * @param value the snowMobile - Snow mobile
	 */
	public void setSnowMobile(final SessionContext ctx, final boolean value)
	{
		setSnowMobile( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.snowMobile</code> attribute. 
	 * @param value the snowMobile - Snow mobile
	 */
	public void setSnowMobile(final boolean value)
	{
		setSnowMobile( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sqshPtNbr</code> attribute.
	 * @return the sqshPtNbr - SQSH Part Number
	 */
	public String getSqshPtNbr(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SQSHPTNBR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.sqshPtNbr</code> attribute.
	 * @return the sqshPtNbr - SQSH Part Number
	 */
	public String getSqshPtNbr()
	{
		return getSqshPtNbr( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sqshPtNbr</code> attribute. 
	 * @param value the sqshPtNbr - SQSH Part Number
	 */
	public void setSqshPtNbr(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SQSHPTNBR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.sqshPtNbr</code> attribute. 
	 * @param value the sqshPtNbr - SQSH Part Number
	 */
	public void setSqshPtNbr(final String value)
	{
		setSqshPtNbr( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.squashedPartNumber</code> attribute.
	 * @return the squashedPartNumber - Squashed Part Number
	 */
	public String getSquashedPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SQUASHEDPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.squashedPartNumber</code> attribute.
	 * @return the squashedPartNumber - Squashed Part Number
	 */
	public String getSquashedPartNumber()
	{
		return getSquashedPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.squashedPartNumber</code> attribute. 
	 * @param value the squashedPartNumber - Squashed Part Number
	 */
	public void setSquashedPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SQUASHEDPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.squashedPartNumber</code> attribute. 
	 * @param value the squashedPartNumber - Squashed Part Number
	 */
	public void setSquashedPartNumber(final String value)
	{
		setSquashedPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.supersededPriceSheetNumber</code> attribute.
	 * @return the supersededPriceSheetNumber - Superseded Price Sheet Number
	 */
	public String getSupersededPriceSheetNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUPERSEDEDPRICESHEETNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.supersededPriceSheetNumber</code> attribute.
	 * @return the supersededPriceSheetNumber - Superseded Price Sheet Number
	 */
	public String getSupersededPriceSheetNumber()
	{
		return getSupersededPriceSheetNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.supersededPriceSheetNumber</code> attribute. 
	 * @param value the supersededPriceSheetNumber - Superseded Price Sheet Number
	 */
	public void setSupersededPriceSheetNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUPERSEDEDPRICESHEETNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.supersededPriceSheetNumber</code> attribute. 
	 * @param value the supersededPriceSheetNumber - Superseded Price Sheet Number
	 */
	public void setSupersededPriceSheetNumber(final String value)
	{
		setSupersededPriceSheetNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.taxable</code> attribute.
	 * @return the taxable - Taxable Y/N
	 */
	public Boolean isTaxable(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, TAXABLE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.taxable</code> attribute.
	 * @return the taxable - Taxable Y/N
	 */
	public Boolean isTaxable()
	{
		return isTaxable( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.taxable</code> attribute. 
	 * @return the taxable - Taxable Y/N
	 */
	public boolean isTaxableAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isTaxable( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.taxable</code> attribute. 
	 * @return the taxable - Taxable Y/N
	 */
	public boolean isTaxableAsPrimitive()
	{
		return isTaxableAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.taxable</code> attribute. 
	 * @param value the taxable - Taxable Y/N
	 */
	public void setTaxable(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, TAXABLE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.taxable</code> attribute. 
	 * @param value the taxable - Taxable Y/N
	 */
	public void setTaxable(final Boolean value)
	{
		setTaxable( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.taxable</code> attribute. 
	 * @param value the taxable - Taxable Y/N
	 */
	public void setTaxable(final SessionContext ctx, final boolean value)
	{
		setTaxable( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.taxable</code> attribute. 
	 * @param value the taxable - Taxable Y/N
	 */
	public void setTaxable(final boolean value)
	{
		setTaxable( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textcomment</code> attribute.
	 * @return the textcomment
	 */
	public String getTextcomment(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TEXTCOMMENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textcomment</code> attribute.
	 * @return the textcomment
	 */
	public String getTextcomment()
	{
		return getTextcomment( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textcomment</code> attribute. 
	 * @param value the textcomment
	 */
	public void setTextcomment(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TEXTCOMMENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textcomment</code> attribute. 
	 * @param value the textcomment
	 */
	public void setTextcomment(final String value)
	{
		setTextcomment( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textfootnote</code> attribute.
	 * @return the textfootnote
	 */
	public String getTextfootnote(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TEXTFOOTNOTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textfootnote</code> attribute.
	 * @return the textfootnote
	 */
	public String getTextfootnote()
	{
		return getTextfootnote( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textfootnote</code> attribute. 
	 * @param value the textfootnote
	 */
	public void setTextfootnote(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TEXTFOOTNOTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textfootnote</code> attribute. 
	 * @param value the textfootnote
	 */
	public void setTextfootnote(final String value)
	{
		setTextfootnote( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textnote</code> attribute.
	 * @return the textnote
	 */
	public String getTextnote(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TEXTNOTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.textnote</code> attribute.
	 * @return the textnote
	 */
	public String getTextnote()
	{
		return getTextnote( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textnote</code> attribute. 
	 * @param value the textnote
	 */
	public void setTextnote(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TEXTNOTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.textnote</code> attribute. 
	 * @param value the textnote
	 */
	public void setTextnote(final String value)
	{
		setTextnote( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.transmissionSpecific</code> attribute.
	 * @return the transmissionSpecific - Transmission Specific
	 */
	public Boolean isTransmissionSpecific(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, TRANSMISSIONSPECIFIC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.transmissionSpecific</code> attribute.
	 * @return the transmissionSpecific - Transmission Specific
	 */
	public Boolean isTransmissionSpecific()
	{
		return isTransmissionSpecific( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @return the transmissionSpecific - Transmission Specific
	 */
	public boolean isTransmissionSpecificAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isTransmissionSpecific( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @return the transmissionSpecific - Transmission Specific
	 */
	public boolean isTransmissionSpecificAsPrimitive()
	{
		return isTransmissionSpecificAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @param value the transmissionSpecific - Transmission Specific
	 */
	public void setTransmissionSpecific(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, TRANSMISSIONSPECIFIC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @param value the transmissionSpecific - Transmission Specific
	 */
	public void setTransmissionSpecific(final Boolean value)
	{
		setTransmissionSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @param value the transmissionSpecific - Transmission Specific
	 */
	public void setTransmissionSpecific(final SessionContext ctx, final boolean value)
	{
		setTransmissionSpecific( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.transmissionSpecific</code> attribute. 
	 * @param value the transmissionSpecific - Transmission Specific
	 */
	public void setTransmissionSpecific(final boolean value)
	{
		setTransmissionSpecific( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.upc</code> attribute.
	 * @return the upc
	 */
	public String getUpc(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UPC);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.upc</code> attribute.
	 * @return the upc
	 */
	public String getUpc()
	{
		return getUpc( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.upc</code> attribute. 
	 * @param value the upc
	 */
	public void setUpc(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UPC,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.upc</code> attribute. 
	 * @param value the upc
	 */
	public void setUpc(final String value)
	{
		setUpc( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.vehicleRegistration</code> attribute.
	 * @return the vehicleRegistration - Popularity Code
	 */
	public Long getVehicleRegistration(final SessionContext ctx)
	{
		return (Long)getProperty( ctx, VEHICLEREGISTRATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.vehicleRegistration</code> attribute.
	 * @return the vehicleRegistration - Popularity Code
	 */
	public Long getVehicleRegistration()
	{
		return getVehicleRegistration( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @return the vehicleRegistration - Popularity Code
	 */
	public long getVehicleRegistrationAsPrimitive(final SessionContext ctx)
	{
		Long value = getVehicleRegistration( ctx );
		return value != null ? value.longValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @return the vehicleRegistration - Popularity Code
	 */
	public long getVehicleRegistrationAsPrimitive()
	{
		return getVehicleRegistrationAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @param value the vehicleRegistration - Popularity Code
	 */
	public void setVehicleRegistration(final SessionContext ctx, final Long value)
	{
		setProperty(ctx, VEHICLEREGISTRATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @param value the vehicleRegistration - Popularity Code
	 */
	public void setVehicleRegistration(final Long value)
	{
		setVehicleRegistration( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @param value the vehicleRegistration - Popularity Code
	 */
	public void setVehicleRegistration(final SessionContext ctx, final long value)
	{
		setVehicleRegistration( ctx,Long.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.vehicleRegistration</code> attribute. 
	 * @param value the vehicleRegistration - Popularity Code
	 */
	public void setVehicleRegistration(final long value)
	{
		setVehicleRegistration( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.warranty</code> attribute.
	 * @return the warranty
	 */
	public String getWarranty(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WARRANTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.warranty</code> attribute.
	 * @return the warranty
	 */
	public String getWarranty()
	{
		return getWarranty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.warranty</code> attribute. 
	 * @param value the warranty
	 */
	public void setWarranty(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WARRANTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.warranty</code> attribute. 
	 * @param value the warranty
	 */
	public void setWarranty(final String value)
	{
		setWarranty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.warrantydescription</code> attribute.
	 * @return the warrantydescription
	 */
	public String getWarrantydescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WARRANTYDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.warrantydescription</code> attribute.
	 * @return the warrantydescription
	 */
	public String getWarrantydescription()
	{
		return getWarrantydescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.warrantydescription</code> attribute. 
	 * @param value the warrantydescription
	 */
	public void setWarrantydescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WARRANTYDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.warrantydescription</code> attribute. 
	 * @param value the warrantydescription
	 */
	public void setWarrantydescription(final String value)
	{
		setWarrantydescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.wdNetPrice</code> attribute.
	 * @return the wdNetPrice - WD Net Price
	 */
	public Double getWdNetPrice(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, WDNETPRICE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.wdNetPrice</code> attribute.
	 * @return the wdNetPrice - WD Net Price
	 */
	public Double getWdNetPrice()
	{
		return getWdNetPrice( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @return the wdNetPrice - WD Net Price
	 */
	public double getWdNetPriceAsPrimitive(final SessionContext ctx)
	{
		Double value = getWdNetPrice( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @return the wdNetPrice - WD Net Price
	 */
	public double getWdNetPriceAsPrimitive()
	{
		return getWdNetPriceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @param value the wdNetPrice - WD Net Price
	 */
	public void setWdNetPrice(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, WDNETPRICE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @param value the wdNetPrice - WD Net Price
	 */
	public void setWdNetPrice(final Double value)
	{
		setWdNetPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @param value the wdNetPrice - WD Net Price
	 */
	public void setWdNetPrice(final SessionContext ctx, final double value)
	{
		setWdNetPrice( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.wdNetPrice</code> attribute. 
	 * @param value the wdNetPrice - WD Net Price
	 */
	public void setWdNetPrice(final double value)
	{
		setWdNetPrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightCase</code> attribute.
	 * @return the weightCase - Weight (Case)
	 */
	public String getWeightCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WEIGHTCASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightCase</code> attribute.
	 * @return the weightCase - Weight (Case)
	 */
	public String getWeightCase()
	{
		return getWeightCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightCase</code> attribute. 
	 * @param value the weightCase - Weight (Case)
	 */
	public void setWeightCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WEIGHTCASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightCase</code> attribute. 
	 * @param value the weightCase - Weight (Case)
	 */
	public void setWeightCase(final String value)
	{
		setWeightCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightInnerPackage</code> attribute.
	 * @return the weightInnerPackage - Weight (Inner Package)
	 */
	public String getWeightInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WEIGHTINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightInnerPackage</code> attribute.
	 * @return the weightInnerPackage - Weight (Inner Package)
	 */
	public String getWeightInnerPackage()
	{
		return getWeightInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightInnerPackage</code> attribute. 
	 * @param value the weightInnerPackage - Weight (Inner Package)
	 */
	public void setWeightInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WEIGHTINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightInnerPackage</code> attribute. 
	 * @param value the weightInnerPackage - Weight (Inner Package)
	 */
	public void setWeightInnerPackage(final String value)
	{
		setWeightInnerPackage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightUOM</code> attribute.
	 * @return the weightUOM - weight UOM
	 */
	public String getWeightUOM(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WEIGHTUOM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.weightUOM</code> attribute.
	 * @return the weightUOM - weight UOM
	 */
	public String getWeightUOM()
	{
		return getWeightUOM( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightUOM</code> attribute. 
	 * @param value the weightUOM - weight UOM
	 */
	public void setWeightUOM(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WEIGHTUOM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.weightUOM</code> attribute. 
	 * @param value the weightUOM - weight UOM
	 */
	public void setWeightUOM(final String value)
	{
		setWeightUOM( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.widthCase</code> attribute.
	 * @return the widthCase - Width (Case)
	 */
	public String getWidthCase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WIDTHCASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.widthCase</code> attribute.
	 * @return the widthCase - Width (Case)
	 */
	public String getWidthCase()
	{
		return getWidthCase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.widthCase</code> attribute. 
	 * @param value the widthCase - Width (Case)
	 */
	public void setWidthCase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WIDTHCASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.widthCase</code> attribute. 
	 * @param value the widthCase - Width (Case)
	 */
	public void setWidthCase(final String value)
	{
		setWidthCase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.widthInnerPackage</code> attribute.
	 * @return the widthInnerPackage - Width (Inner Package)
	 */
	public String getWidthInnerPackage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, WIDTHINNERPACKAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPart.widthInnerPackage</code> attribute.
	 * @return the widthInnerPackage - Width (Inner Package)
	 */
	public String getWidthInnerPackage()
	{
		return getWidthInnerPackage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.widthInnerPackage</code> attribute. 
	 * @param value the widthInnerPackage - Width (Inner Package)
	 */
	public void setWidthInnerPackage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, WIDTHINNERPACKAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPart.widthInnerPackage</code> attribute. 
	 * @param value the widthInnerPackage - Width (Inner Package)
	 */
	public void setWidthInnerPackage(final String value)
	{
		setWidthInnerPackage( getSession().getSessionContext(), value );
	}
	
}
