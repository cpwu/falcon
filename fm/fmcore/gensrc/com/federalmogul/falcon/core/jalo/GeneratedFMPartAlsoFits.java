/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMPartAlsoFits FMPartAlsoFits}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMPartAlsoFits extends GenericItem
{
	/** Qualifier of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute **/
	public static final String VEHICLEQUANTITY = "vehicleQuantity";
	/** Qualifier of the <code>FMPartAlsoFits.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMPartAlsoFits.engineDesignation</code> attribute **/
	public static final String ENGINEDESIGNATION = "engineDesignation";
	/** Qualifier of the <code>FMPartAlsoFits.position</code> attribute **/
	public static final String POSITION = "position";
	/** Qualifier of the <code>FMPartAlsoFits.make</code> attribute **/
	public static final String MAKE = "make";
	/** Qualifier of the <code>FMPartAlsoFits.yearRange</code> attribute **/
	public static final String YEARRANGE = "yearRange";
	/** Qualifier of the <code>FMPartAlsoFits.uid</code> attribute **/
	public static final String UID = "uid";
	/** Qualifier of the <code>FMPartAlsoFits.driveWheel</code> attribute **/
	public static final String DRIVEWHEEL = "driveWheel";
	/** Qualifier of the <code>FMPartAlsoFits.model</code> attribute **/
	public static final String MODEL = "model";
	/** Qualifier of the <code>FMPartAlsoFits.engineVIN</code> attribute **/
	public static final String ENGINEVIN = "engineVIN";
	/** Qualifier of the <code>FMPartAlsoFits.engineBase</code> attribute **/
	public static final String ENGINEBASE = "engineBase";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(VEHICLEQUANTITY, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(ENGINEDESIGNATION, AttributeMode.INITIAL);
		tmp.put(POSITION, AttributeMode.INITIAL);
		tmp.put(MAKE, AttributeMode.INITIAL);
		tmp.put(YEARRANGE, AttributeMode.INITIAL);
		tmp.put(UID, AttributeMode.INITIAL);
		tmp.put(DRIVEWHEEL, AttributeMode.INITIAL);
		tmp.put(MODEL, AttributeMode.INITIAL);
		tmp.put(ENGINEVIN, AttributeMode.INITIAL);
		tmp.put(ENGINEBASE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.code</code> attribute.
	 * @return the code - Part Code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.code</code> attribute.
	 * @return the code - Part Code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.code</code> attribute. 
	 * @param value the code - Part Code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.code</code> attribute. 
	 * @param value the code - Part Code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.driveWheel</code> attribute.
	 * @return the driveWheel - Drive Wheel
	 */
	public String getDriveWheel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DRIVEWHEEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.driveWheel</code> attribute.
	 * @return the driveWheel - Drive Wheel
	 */
	public String getDriveWheel()
	{
		return getDriveWheel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.driveWheel</code> attribute. 
	 * @param value the driveWheel - Drive Wheel
	 */
	public void setDriveWheel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DRIVEWHEEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.driveWheel</code> attribute. 
	 * @param value the driveWheel - Drive Wheel
	 */
	public void setDriveWheel(final String value)
	{
		setDriveWheel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineBase</code> attribute.
	 * @return the engineBase - Engine Base
	 */
	public String getEngineBase(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEBASE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineBase</code> attribute.
	 * @return the engineBase - Engine Base
	 */
	public String getEngineBase()
	{
		return getEngineBase( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineBase</code> attribute. 
	 * @param value the engineBase - Engine Base
	 */
	public void setEngineBase(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEBASE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineBase</code> attribute. 
	 * @param value the engineBase - Engine Base
	 */
	public void setEngineBase(final String value)
	{
		setEngineBase( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineDesignation</code> attribute.
	 * @return the engineDesignation - Engine Designation
	 */
	public String getEngineDesignation(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEDESIGNATION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineDesignation</code> attribute.
	 * @return the engineDesignation - Engine Designation
	 */
	public String getEngineDesignation()
	{
		return getEngineDesignation( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineDesignation</code> attribute. 
	 * @param value the engineDesignation - Engine Designation
	 */
	public void setEngineDesignation(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEDESIGNATION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineDesignation</code> attribute. 
	 * @param value the engineDesignation - Engine Designation
	 */
	public void setEngineDesignation(final String value)
	{
		setEngineDesignation( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineVIN</code> attribute.
	 * @return the engineVIN - Engine VIN
	 */
	public String getEngineVIN(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ENGINEVIN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.engineVIN</code> attribute.
	 * @return the engineVIN - Engine VIN
	 */
	public String getEngineVIN()
	{
		return getEngineVIN( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineVIN</code> attribute. 
	 * @param value the engineVIN - Engine VIN
	 */
	public void setEngineVIN(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ENGINEVIN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.engineVIN</code> attribute. 
	 * @param value the engineVIN - Engine VIN
	 */
	public void setEngineVIN(final String value)
	{
		setEngineVIN( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.make</code> attribute.
	 * @return the make - Make
	 */
	public String getMake(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MAKE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.make</code> attribute.
	 * @return the make - Make
	 */
	public String getMake()
	{
		return getMake( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.make</code> attribute. 
	 * @param value the make - Make
	 */
	public void setMake(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MAKE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.make</code> attribute. 
	 * @param value the make - Make
	 */
	public void setMake(final String value)
	{
		setMake( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.model</code> attribute.
	 * @return the model - Model
	 */
	public String getModel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.model</code> attribute.
	 * @return the model - Model
	 */
	public String getModel()
	{
		return getModel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.model</code> attribute. 
	 * @param value the model - Model
	 */
	public void setModel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.model</code> attribute. 
	 * @param value the model - Model
	 */
	public void setModel(final String value)
	{
		setModel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.position</code> attribute.
	 * @return the position - Position
	 */
	public String getPosition(final SessionContext ctx)
	{
		return (String)getProperty( ctx, POSITION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.position</code> attribute.
	 * @return the position - Position
	 */
	public String getPosition()
	{
		return getPosition( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.position</code> attribute. 
	 * @param value the position - Position
	 */
	public void setPosition(final SessionContext ctx, final String value)
	{
		setProperty(ctx, POSITION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.position</code> attribute. 
	 * @param value the position - Position
	 */
	public void setPosition(final String value)
	{
		setPosition( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.uid</code> attribute.
	 * @return the uid - FM Part Also Fits
	 */
	public String getUid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.uid</code> attribute.
	 * @return the uid - FM Part Also Fits
	 */
	public String getUid()
	{
		return getUid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.uid</code> attribute. 
	 * @param value the uid - FM Part Also Fits
	 */
	public void setUid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.uid</code> attribute. 
	 * @param value the uid - FM Part Also Fits
	 */
	public void setUid(final String value)
	{
		setUid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute.
	 * @return the vehicleQuantity - Vehicle Quantity
	 */
	public Integer getVehicleQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, VEHICLEQUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute.
	 * @return the vehicleQuantity - Vehicle Quantity
	 */
	public Integer getVehicleQuantity()
	{
		return getVehicleQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @return the vehicleQuantity - Vehicle Quantity
	 */
	public int getVehicleQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getVehicleQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @return the vehicleQuantity - Vehicle Quantity
	 */
	public int getVehicleQuantityAsPrimitive()
	{
		return getVehicleQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @param value the vehicleQuantity - Vehicle Quantity
	 */
	public void setVehicleQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, VEHICLEQUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @param value the vehicleQuantity - Vehicle Quantity
	 */
	public void setVehicleQuantity(final Integer value)
	{
		setVehicleQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @param value the vehicleQuantity - Vehicle Quantity
	 */
	public void setVehicleQuantity(final SessionContext ctx, final int value)
	{
		setVehicleQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.vehicleQuantity</code> attribute. 
	 * @param value the vehicleQuantity - Vehicle Quantity
	 */
	public void setVehicleQuantity(final int value)
	{
		setVehicleQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.yearRange</code> attribute.
	 * @return the yearRange - Year Range
	 */
	public String getYearRange(final SessionContext ctx)
	{
		return (String)getProperty( ctx, YEARRANGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMPartAlsoFits.yearRange</code> attribute.
	 * @return the yearRange - Year Range
	 */
	public String getYearRange()
	{
		return getYearRange( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.yearRange</code> attribute. 
	 * @param value the yearRange - Year Range
	 */
	public void setYearRange(final SessionContext ctx, final String value)
	{
		setProperty(ctx, YEARRANGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMPartAlsoFits.yearRange</code> attribute. 
	 * @param value the yearRange - Year Range
	 */
	public void setYearRange(final String value)
	{
		setYearRange( getSession().getSessionContext(), value );
	}
	
}
