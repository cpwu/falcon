/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMReturnItems FMReturnItems}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMReturnItems extends GenericItem
{
	/** Qualifier of the <code>FMReturnItems.orderedqty</code> attribute **/
	public static final String ORDEREDQTY = "orderedqty";
	/** Qualifier of the <code>FMReturnItems.unit</code> attribute **/
	public static final String UNIT = "unit";
	/** Qualifier of the <code>FMReturnItems.returnqty</code> attribute **/
	public static final String RETURNQTY = "returnqty";
	/** Qualifier of the <code>FMReturnItems.itemDescription</code> attribute **/
	public static final String ITEMDESCRIPTION = "itemDescription";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(ORDEREDQTY, AttributeMode.INITIAL);
		tmp.put(UNIT, AttributeMode.INITIAL);
		tmp.put(RETURNQTY, AttributeMode.INITIAL);
		tmp.put(ITEMDESCRIPTION, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.itemDescription</code> attribute.
	 * @return the itemDescription
	 */
	public String getItemDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ITEMDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.itemDescription</code> attribute.
	 * @return the itemDescription
	 */
	public String getItemDescription()
	{
		return getItemDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.itemDescription</code> attribute. 
	 * @param value the itemDescription
	 */
	public void setItemDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ITEMDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.itemDescription</code> attribute. 
	 * @param value the itemDescription
	 */
	public void setItemDescription(final String value)
	{
		setItemDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.orderedqty</code> attribute.
	 * @return the orderedqty
	 */
	public String getOrderedqty(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDEREDQTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.orderedqty</code> attribute.
	 * @return the orderedqty
	 */
	public String getOrderedqty()
	{
		return getOrderedqty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.orderedqty</code> attribute. 
	 * @param value the orderedqty
	 */
	public void setOrderedqty(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDEREDQTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.orderedqty</code> attribute. 
	 * @param value the orderedqty
	 */
	public void setOrderedqty(final String value)
	{
		setOrderedqty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.returnqty</code> attribute.
	 * @return the returnqty
	 */
	public String getReturnqty(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RETURNQTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.returnqty</code> attribute.
	 * @return the returnqty
	 */
	public String getReturnqty()
	{
		return getReturnqty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.returnqty</code> attribute. 
	 * @param value the returnqty
	 */
	public void setReturnqty(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RETURNQTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.returnqty</code> attribute. 
	 * @param value the returnqty
	 */
	public void setReturnqty(final String value)
	{
		setReturnqty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.unit</code> attribute.
	 * @return the unit
	 */
	public EnumerationValue getUnit(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, UNIT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnItems.unit</code> attribute.
	 * @return the unit
	 */
	public EnumerationValue getUnit()
	{
		return getUnit( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, UNIT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnItems.unit</code> attribute. 
	 * @param value the unit
	 */
	public void setUnit(final EnumerationValue value)
	{
		setUnit( getSession().getSessionContext(), value );
	}
	
}
