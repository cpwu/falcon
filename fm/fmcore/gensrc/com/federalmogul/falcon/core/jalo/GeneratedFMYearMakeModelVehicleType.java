/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMYearMakeModelVehicleType FMYearMakeModelVehicleType}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMYearMakeModelVehicleType extends GenericItem
{
	/** Qualifier of the <code>FMYearMakeModelVehicleType.vehicleType</code> attribute **/
	public static final String VEHICLETYPE = "vehicleType";
	/** Qualifier of the <code>FMYearMakeModelVehicleType.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMYearMakeModelVehicleType.year</code> attribute **/
	public static final String YEAR = "year";
	/** Qualifier of the <code>FMYearMakeModelVehicleType.make</code> attribute **/
	public static final String MAKE = "make";
	/** Qualifier of the <code>FMYearMakeModelVehicleType.model</code> attribute **/
	public static final String MODEL = "model";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(VEHICLETYPE, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(YEAR, AttributeMode.INITIAL);
		tmp.put(MAKE, AttributeMode.INITIAL);
		tmp.put(MODEL, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.code</code> attribute.
	 * @return the code - YMM Code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.code</code> attribute.
	 * @return the code - YMM Code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.code</code> attribute. 
	 * @param value the code - YMM Code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.code</code> attribute. 
	 * @param value the code - YMM Code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.make</code> attribute.
	 * @return the make - Make
	 */
	public String getMake(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MAKE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.make</code> attribute.
	 * @return the make - Make
	 */
	public String getMake()
	{
		return getMake( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.make</code> attribute. 
	 * @param value the make - Make
	 */
	public void setMake(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MAKE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.make</code> attribute. 
	 * @param value the make - Make
	 */
	public void setMake(final String value)
	{
		setMake( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.model</code> attribute.
	 * @return the model - Model
	 */
	public String getModel(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODEL);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.model</code> attribute.
	 * @return the model - Model
	 */
	public String getModel()
	{
		return getModel( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.model</code> attribute. 
	 * @param value the model - Model
	 */
	public void setModel(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODEL,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.model</code> attribute. 
	 * @param value the model - Model
	 */
	public void setModel(final String value)
	{
		setModel( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.vehicleType</code> attribute.
	 * @return the vehicleType - Vehicle Type
	 */
	public String getVehicleType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, VEHICLETYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.vehicleType</code> attribute.
	 * @return the vehicleType - Vehicle Type
	 */
	public String getVehicleType()
	{
		return getVehicleType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.vehicleType</code> attribute. 
	 * @param value the vehicleType - Vehicle Type
	 */
	public void setVehicleType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, VEHICLETYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.vehicleType</code> attribute. 
	 * @param value the vehicleType - Vehicle Type
	 */
	public void setVehicleType(final String value)
	{
		setVehicleType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.year</code> attribute.
	 * @return the year - Year
	 */
	public String getYear(final SessionContext ctx)
	{
		return (String)getProperty( ctx, YEAR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMYearMakeModelVehicleType.year</code> attribute.
	 * @return the year - Year
	 */
	public String getYear()
	{
		return getYear( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.year</code> attribute. 
	 * @param value the year - Year
	 */
	public void setYear(final SessionContext ctx, final String value)
	{
		setProperty(ctx, YEAR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMYearMakeModelVehicleType.year</code> attribute. 
	 * @param value the year - Year
	 */
	public void setYear(final String value)
	{
		setYear( getSession().getSessionContext(), value );
	}
	
}
