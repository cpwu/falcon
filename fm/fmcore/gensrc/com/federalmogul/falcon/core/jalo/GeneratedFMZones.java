/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.Country;
import de.hybris.platform.jalo.c2l.Region;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.FMZones FMZones}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMZones extends GenericItem
{
	/** Qualifier of the <code>FMZones.Code</code> attribute **/
	public static final String CODE = "Code";
	/** Qualifier of the <code>FMZones.Country</code> attribute **/
	public static final String COUNTRY = "Country";
	/** Qualifier of the <code>FMZones.Region</code> attribute **/
	public static final String REGION = "Region";
	/** Qualifier of the <code>FMZones.Name</code> attribute **/
	public static final String NAME = "Name";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(COUNTRY, AttributeMode.INITIAL);
		tmp.put(REGION, AttributeMode.INITIAL);
		tmp.put(NAME, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Code</code> attribute.
	 * @return the Code - FM FMZone
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Code</code> attribute.
	 * @return the Code - FM FMZone
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Code</code> attribute. 
	 * @param value the Code - FM FMZone
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Code</code> attribute. 
	 * @param value the Code - FM FMZone
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Country</code> attribute.
	 * @return the Country - FM Countries
	 */
	public List<Country> getCountry(final SessionContext ctx)
	{
		List<Country> coll = (List<Country>)getProperty( ctx, COUNTRY);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Country</code> attribute.
	 * @return the Country - FM Countries
	 */
	public List<Country> getCountry()
	{
		return getCountry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Country</code> attribute. 
	 * @param value the Country - FM Countries
	 */
	public void setCountry(final SessionContext ctx, final List<Country> value)
	{
		setProperty(ctx, COUNTRY,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Country</code> attribute. 
	 * @param value the Country - FM Countries
	 */
	public void setCountry(final List<Country> value)
	{
		setCountry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Name</code> attribute.
	 * @return the Name - FMZone Name
	 */
	public String getName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, NAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Name</code> attribute.
	 * @return the Name - FMZone Name
	 */
	public String getName()
	{
		return getName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Name</code> attribute. 
	 * @param value the Name - FMZone Name
	 */
	public void setName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, NAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Name</code> attribute. 
	 * @param value the Name - FMZone Name
	 */
	public void setName(final String value)
	{
		setName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Region</code> attribute.
	 * @return the Region - FM Region
	 */
	public List<Region> getRegion(final SessionContext ctx)
	{
		List<Region> coll = (List<Region>)getProperty( ctx, REGION);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMZones.Region</code> attribute.
	 * @return the Region - FM Region
	 */
	public List<Region> getRegion()
	{
		return getRegion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Region</code> attribute. 
	 * @param value the Region - FM Region
	 */
	public void setRegion(final SessionContext ctx, final List<Region> value)
	{
		setProperty(ctx, REGION,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMZones.Region</code> attribute. 
	 * @param value the Region - FM Region
	 */
	public void setRegion(final List<Region> value)
	{
		setRegion( getSession().getSessionContext(), value );
	}
	
}
