/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.PartInterchange PartInterchange}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedPartInterchange extends GenericItem
{
	/** Qualifier of the <code>PartInterchange.rawPartNumber</code> attribute **/
	public static final String RAWPARTNUMBER = "rawPartNumber";
	/** Qualifier of the <code>PartInterchange.fmcorpName</code> attribute **/
	public static final String FMCORPNAME = "fmcorpName";
	/** Qualifier of the <code>PartInterchange.corpCode</code> attribute **/
	public static final String CORPCODE = "corpCode";
	/** Qualifier of the <code>PartInterchange.uid</code> attribute **/
	public static final String UID = "uid";
	/** Qualifier of the <code>PartInterchange.partTypeCode</code> attribute **/
	public static final String PARTTYPECODE = "partTypeCode";
	/** Qualifier of the <code>PartInterchange.fmRawPartNumber</code> attribute **/
	public static final String FMRAWPARTNUMBER = "fmRawPartNumber";
	/** Qualifier of the <code>PartInterchange.partNumber</code> attribute **/
	public static final String PARTNUMBER = "partNumber";
	/** Qualifier of the <code>PartInterchange.reboxQuantity</code> attribute **/
	public static final String REBOXQUANTITY = "reboxQuantity";
	/** Qualifier of the <code>PartInterchange.partFlagCode</code> attribute **/
	public static final String PARTFLAGCODE = "partFlagCode";
	/** Qualifier of the <code>PartInterchange.fmcorpCode</code> attribute **/
	public static final String FMCORPCODE = "fmcorpCode";
	/** Qualifier of the <code>PartInterchange.packageQuantity</code> attribute **/
	public static final String PACKAGEQUANTITY = "packageQuantity";
	/** Qualifier of the <code>PartInterchange.corpName</code> attribute **/
	public static final String CORPNAME = "corpName";
	/** Qualifier of the <code>PartInterchange.corpType</code> attribute **/
	public static final String CORPTYPE = "corpType";
	/** Qualifier of the <code>PartInterchange.fmPartNumber</code> attribute **/
	public static final String FMPARTNUMBER = "fmPartNumber";
	/** Qualifier of the <code>PartInterchange.partSort</code> attribute **/
	public static final String PARTSORT = "partSort";
	/** Qualifier of the <code>PartInterchange.partTypeName</code> attribute **/
	public static final String PARTTYPENAME = "partTypeName";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(RAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(FMCORPNAME, AttributeMode.INITIAL);
		tmp.put(CORPCODE, AttributeMode.INITIAL);
		tmp.put(UID, AttributeMode.INITIAL);
		tmp.put(PARTTYPECODE, AttributeMode.INITIAL);
		tmp.put(FMRAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(PARTNUMBER, AttributeMode.INITIAL);
		tmp.put(REBOXQUANTITY, AttributeMode.INITIAL);
		tmp.put(PARTFLAGCODE, AttributeMode.INITIAL);
		tmp.put(FMCORPCODE, AttributeMode.INITIAL);
		tmp.put(PACKAGEQUANTITY, AttributeMode.INITIAL);
		tmp.put(CORPNAME, AttributeMode.INITIAL);
		tmp.put(CORPTYPE, AttributeMode.INITIAL);
		tmp.put(FMPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(PARTSORT, AttributeMode.INITIAL);
		tmp.put(PARTTYPENAME, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpCode</code> attribute.
	 * @return the corpCode - From Corporate Code
	 */
	public String getCorpCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpCode</code> attribute.
	 * @return the corpCode - From Corporate Code
	 */
	public String getCorpCode()
	{
		return getCorpCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpCode</code> attribute. 
	 * @param value the corpCode - From Corporate Code
	 */
	public void setCorpCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpCode</code> attribute. 
	 * @param value the corpCode - From Corporate Code
	 */
	public void setCorpCode(final String value)
	{
		setCorpCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpName</code> attribute.
	 * @return the corpName - From Corporate name
	 */
	public String getCorpName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpName</code> attribute.
	 * @return the corpName - From Corporate name
	 */
	public String getCorpName()
	{
		return getCorpName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpName</code> attribute. 
	 * @param value the corpName - From Corporate name
	 */
	public void setCorpName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpName</code> attribute. 
	 * @param value the corpName - From Corporate name
	 */
	public void setCorpName(final String value)
	{
		setCorpName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpType</code> attribute.
	 * @return the corpType - From Corporate Type
	 */
	public String getCorpType(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CORPTYPE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.corpType</code> attribute.
	 * @return the corpType - From Corporate Type
	 */
	public String getCorpType()
	{
		return getCorpType( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpType</code> attribute. 
	 * @param value the corpType - From Corporate Type
	 */
	public void setCorpType(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CORPTYPE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.corpType</code> attribute. 
	 * @param value the corpType - From Corporate Type
	 */
	public void setCorpType(final String value)
	{
		setCorpType( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmcorpCode</code> attribute.
	 * @return the fmcorpCode - FM Brand name
	 */
	public String getFmcorpCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FMCORPCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmcorpCode</code> attribute.
	 * @return the fmcorpCode - FM Brand name
	 */
	public String getFmcorpCode()
	{
		return getFmcorpCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmcorpCode</code> attribute. 
	 * @param value the fmcorpCode - FM Brand name
	 */
	public void setFmcorpCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FMCORPCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmcorpCode</code> attribute. 
	 * @param value the fmcorpCode - FM Brand name
	 */
	public void setFmcorpCode(final String value)
	{
		setFmcorpCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmcorpName</code> attribute.
	 * @return the fmcorpName - FM Brand name
	 */
	public String getFmcorpName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FMCORPNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmcorpName</code> attribute.
	 * @return the fmcorpName - FM Brand name
	 */
	public String getFmcorpName()
	{
		return getFmcorpName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmcorpName</code> attribute. 
	 * @param value the fmcorpName - FM Brand name
	 */
	public void setFmcorpName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FMCORPNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmcorpName</code> attribute. 
	 * @param value the fmcorpName - FM Brand name
	 */
	public void setFmcorpName(final String value)
	{
		setFmcorpName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmPartNumber</code> attribute.
	 * @return the fmPartNumber - FM Part Number
	 */
	public String getFmPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FMPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmPartNumber</code> attribute.
	 * @return the fmPartNumber - FM Part Number
	 */
	public String getFmPartNumber()
	{
		return getFmPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmPartNumber</code> attribute. 
	 * @param value the fmPartNumber - FM Part Number
	 */
	public void setFmPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FMPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmPartNumber</code> attribute. 
	 * @param value the fmPartNumber - FM Part Number
	 */
	public void setFmPartNumber(final String value)
	{
		setFmPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmRawPartNumber</code> attribute.
	 * @return the fmRawPartNumber - FM Part Number
	 */
	public String getFmRawPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FMRAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.fmRawPartNumber</code> attribute.
	 * @return the fmRawPartNumber - FM Part Number
	 */
	public String getFmRawPartNumber()
	{
		return getFmRawPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmRawPartNumber</code> attribute. 
	 * @param value the fmRawPartNumber - FM Part Number
	 */
	public void setFmRawPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FMRAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.fmRawPartNumber</code> attribute. 
	 * @param value the fmRawPartNumber - FM Part Number
	 */
	public void setFmRawPartNumber(final String value)
	{
		setFmRawPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.packageQuantity</code> attribute.
	 * @return the packageQuantity - From Package Quantity
	 */
	public Integer getPackageQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, PACKAGEQUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.packageQuantity</code> attribute.
	 * @return the packageQuantity - From Package Quantity
	 */
	public Integer getPackageQuantity()
	{
		return getPackageQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @return the packageQuantity - From Package Quantity
	 */
	public int getPackageQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getPackageQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @return the packageQuantity - From Package Quantity
	 */
	public int getPackageQuantityAsPrimitive()
	{
		return getPackageQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @param value the packageQuantity - From Package Quantity
	 */
	public void setPackageQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, PACKAGEQUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @param value the packageQuantity - From Package Quantity
	 */
	public void setPackageQuantity(final Integer value)
	{
		setPackageQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @param value the packageQuantity - From Package Quantity
	 */
	public void setPackageQuantity(final SessionContext ctx, final int value)
	{
		setPackageQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.packageQuantity</code> attribute. 
	 * @param value the packageQuantity - From Package Quantity
	 */
	public void setPackageQuantity(final int value)
	{
		setPackageQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partFlagCode</code> attribute.
	 * @return the partFlagCode - From Part Flag Code
	 */
	public String getPartFlagCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTFLAGCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partFlagCode</code> attribute.
	 * @return the partFlagCode - From Part Flag Code
	 */
	public String getPartFlagCode()
	{
		return getPartFlagCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partFlagCode</code> attribute. 
	 * @param value the partFlagCode - From Part Flag Code
	 */
	public void setPartFlagCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTFLAGCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partFlagCode</code> attribute. 
	 * @param value the partFlagCode - From Part Flag Code
	 */
	public void setPartFlagCode(final String value)
	{
		setPartFlagCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partNumber</code> attribute.
	 * @return the partNumber - From Part number
	 */
	public String getPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partNumber</code> attribute.
	 * @return the partNumber - From Part number
	 */
	public String getPartNumber()
	{
		return getPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partNumber</code> attribute. 
	 * @param value the partNumber - From Part number
	 */
	public void setPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partNumber</code> attribute. 
	 * @param value the partNumber - From Part number
	 */
	public void setPartNumber(final String value)
	{
		setPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partSort</code> attribute.
	 * @return the partSort - From Part Sort
	 */
	public String getPartSort(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTSORT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partSort</code> attribute.
	 * @return the partSort - From Part Sort
	 */
	public String getPartSort()
	{
		return getPartSort( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partSort</code> attribute. 
	 * @param value the partSort - From Part Sort
	 */
	public void setPartSort(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTSORT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partSort</code> attribute. 
	 * @param value the partSort - From Part Sort
	 */
	public void setPartSort(final String value)
	{
		setPartSort( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partTypeCode</code> attribute.
	 * @return the partTypeCode - From Part type Code
	 */
	public String getPartTypeCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTTYPECODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partTypeCode</code> attribute.
	 * @return the partTypeCode - From Part type Code
	 */
	public String getPartTypeCode()
	{
		return getPartTypeCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partTypeCode</code> attribute. 
	 * @param value the partTypeCode - From Part type Code
	 */
	public void setPartTypeCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTTYPECODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partTypeCode</code> attribute. 
	 * @param value the partTypeCode - From Part type Code
	 */
	public void setPartTypeCode(final String value)
	{
		setPartTypeCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partTypeName</code> attribute.
	 * @return the partTypeName - From Part Type Name
	 */
	public String getPartTypeName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTTYPENAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.partTypeName</code> attribute.
	 * @return the partTypeName - From Part Type Name
	 */
	public String getPartTypeName()
	{
		return getPartTypeName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partTypeName</code> attribute. 
	 * @param value the partTypeName - From Part Type Name
	 */
	public void setPartTypeName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTTYPENAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.partTypeName</code> attribute. 
	 * @param value the partTypeName - From Part Type Name
	 */
	public void setPartTypeName(final String value)
	{
		setPartTypeName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - From Raw Part Number
	 */
	public String getRawPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - From Raw Part Number
	 */
	public String getRawPartNumber()
	{
		return getRawPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - From Raw Part Number
	 */
	public void setRawPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - From Raw Part Number
	 */
	public void setRawPartNumber(final String value)
	{
		setRawPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.reboxQuantity</code> attribute.
	 * @return the reboxQuantity - From Rebox Quantity
	 */
	public Integer getReboxQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, REBOXQUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.reboxQuantity</code> attribute.
	 * @return the reboxQuantity - From Rebox Quantity
	 */
	public Integer getReboxQuantity()
	{
		return getReboxQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @return the reboxQuantity - From Rebox Quantity
	 */
	public int getReboxQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getReboxQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @return the reboxQuantity - From Rebox Quantity
	 */
	public int getReboxQuantityAsPrimitive()
	{
		return getReboxQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @param value the reboxQuantity - From Rebox Quantity
	 */
	public void setReboxQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, REBOXQUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @param value the reboxQuantity - From Rebox Quantity
	 */
	public void setReboxQuantity(final Integer value)
	{
		setReboxQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @param value the reboxQuantity - From Rebox Quantity
	 */
	public void setReboxQuantity(final SessionContext ctx, final int value)
	{
		setReboxQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.reboxQuantity</code> attribute. 
	 * @param value the reboxQuantity - From Rebox Quantity
	 */
	public void setReboxQuantity(final int value)
	{
		setReboxQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.uid</code> attribute.
	 * @return the uid - Unique id
	 */
	public String getUid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>PartInterchange.uid</code> attribute.
	 * @return the uid - Unique id
	 */
	public String getUid()
	{
		return getUid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.uid</code> attribute. 
	 * @param value the uid - Unique id
	 */
	public void setUid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>PartInterchange.uid</code> attribute. 
	 * @param value the uid - Unique id
	 */
	public void setUid(final String value)
	{
		setUid( getSession().getSessionContext(), value );
	}
	
}
