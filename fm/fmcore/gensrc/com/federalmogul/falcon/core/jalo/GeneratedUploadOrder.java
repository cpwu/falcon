/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomer;
import com.federalmogul.core.jalo.FMCustomerAccount;
import com.federalmogul.falcon.core.jalo.UploadOrderEntry;
import com.federalmogul.falcon.core.jalo.UploadOrderHistory;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.media.Media;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.user.Address;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.UploadOrder UploadOrder}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUploadOrder extends GenericItem
{
	/** Qualifier of the <code>UploadOrder.SoldToAccount</code> attribute **/
	public static final String SOLDTOACCOUNT = "SoldToAccount";
	/** Qualifier of the <code>UploadOrder.ShipToAccount</code> attribute **/
	public static final String SHIPTOACCOUNT = "ShipToAccount";
	/** Qualifier of the <code>UploadOrder.user</code> attribute **/
	public static final String USER = "user";
	/** Qualifier of the <code>UploadOrder.userFirstName</code> attribute **/
	public static final String USERFIRSTNAME = "userFirstName";
	/** Qualifier of the <code>UploadOrder.sapordernumber</code> attribute **/
	public static final String SAPORDERNUMBER = "sapordernumber";
	/** Qualifier of the <code>UploadOrder.updatedTime</code> attribute **/
	public static final String UPDATEDTIME = "updatedTime";
	/** Qualifier of the <code>UploadOrder.entries</code> attribute **/
	public static final String ENTRIES = "entries";
	/** Qualifier of the <code>UploadOrder.userLastName</code> attribute **/
	public static final String USERLASTNAME = "userLastName";
	/** Qualifier of the <code>UploadOrder.uploadOrderStatus</code> attribute **/
	public static final String UPLOADORDERSTATUS = "uploadOrderStatus";
	/** Qualifier of the <code>UploadOrder.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>UploadOrder.BillToAddress</code> attribute **/
	public static final String BILLTOADDRESS = "BillToAddress";
	/** Qualifier of the <code>UploadOrder.ordercomments</code> attribute **/
	public static final String ORDERCOMMENTS = "ordercomments";
	/** Qualifier of the <code>UploadOrder.uploadOrderFile</code> attribute **/
	public static final String UPLOADORDERFILE = "uploadOrderFile";
	/** Qualifier of the <code>UploadOrder.PONumber</code> attribute **/
	public static final String PONUMBER = "PONumber";
	/** Qualifier of the <code>UploadOrder.ShipToAddress</code> attribute **/
	public static final String SHIPTOADDRESS = "ShipToAddress";
	/** Qualifier of the <code>UploadOrder.history</code> attribute **/
	public static final String HISTORY = "history";
	/**
	* {@link OneToManyHandler} for handling 1:n ENTRIES's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<UploadOrderEntry> ENTRIESHANDLER = new OneToManyHandler<UploadOrderEntry>(
	FmCoreConstants.TC.UPLOADORDERENTRY,
	true,
	"order",
	"orderentryNumber",
	false,
	true,
	CollectionType.LIST
	);
	/**
	* {@link OneToManyHandler} for handling 1:n HISTORY's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<UploadOrderHistory> HISTORYHANDLER = new OneToManyHandler<UploadOrderHistory>(
	FmCoreConstants.TC.UPLOADORDERHISTORY,
	true,
	"order",
	"updatedTime",
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(SOLDTOACCOUNT, AttributeMode.INITIAL);
		tmp.put(SHIPTOACCOUNT, AttributeMode.INITIAL);
		tmp.put(USER, AttributeMode.INITIAL);
		tmp.put(USERFIRSTNAME, AttributeMode.INITIAL);
		tmp.put(SAPORDERNUMBER, AttributeMode.INITIAL);
		tmp.put(UPDATEDTIME, AttributeMode.INITIAL);
		tmp.put(USERLASTNAME, AttributeMode.INITIAL);
		tmp.put(UPLOADORDERSTATUS, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(BILLTOADDRESS, AttributeMode.INITIAL);
		tmp.put(ORDERCOMMENTS, AttributeMode.INITIAL);
		tmp.put(UPLOADORDERFILE, AttributeMode.INITIAL);
		tmp.put(PONUMBER, AttributeMode.INITIAL);
		tmp.put(SHIPTOADDRESS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.BillToAddress</code> attribute.
	 * @return the BillToAddress
	 */
	public Address getBillToAddress(final SessionContext ctx)
	{
		return (Address)getProperty( ctx, BILLTOADDRESS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.BillToAddress</code> attribute.
	 * @return the BillToAddress
	 */
	public Address getBillToAddress()
	{
		return getBillToAddress( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.BillToAddress</code> attribute. 
	 * @param value the BillToAddress
	 */
	public void setBillToAddress(final SessionContext ctx, final Address value)
	{
		setProperty(ctx, BILLTOADDRESS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.BillToAddress</code> attribute. 
	 * @param value the BillToAddress
	 */
	public void setBillToAddress(final Address value)
	{
		setBillToAddress( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.code</code> attribute.
	 * @return the code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.code</code> attribute.
	 * @return the code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.entries</code> attribute.
	 * @return the entries
	 */
	public List<UploadOrderEntry> getEntries(final SessionContext ctx)
	{
		return (List<UploadOrderEntry>)ENTRIESHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.entries</code> attribute.
	 * @return the entries
	 */
	public List<UploadOrderEntry> getEntries()
	{
		return getEntries( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.entries</code> attribute. 
	 * @param value the entries
	 */
	public void setEntries(final SessionContext ctx, final List<UploadOrderEntry> value)
	{
		ENTRIESHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.entries</code> attribute. 
	 * @param value the entries
	 */
	public void setEntries(final List<UploadOrderEntry> value)
	{
		setEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to entries. 
	 * @param value the item to add to entries
	 */
	public void addToEntries(final SessionContext ctx, final UploadOrderEntry value)
	{
		ENTRIESHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to entries. 
	 * @param value the item to add to entries
	 */
	public void addToEntries(final UploadOrderEntry value)
	{
		addToEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from entries. 
	 * @param value the item to remove from entries
	 */
	public void removeFromEntries(final SessionContext ctx, final UploadOrderEntry value)
	{
		ENTRIESHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from entries. 
	 * @param value the item to remove from entries
	 */
	public void removeFromEntries(final UploadOrderEntry value)
	{
		removeFromEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.history</code> attribute.
	 * @return the history
	 */
	public List<UploadOrderHistory> getHistory(final SessionContext ctx)
	{
		return (List<UploadOrderHistory>)HISTORYHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.history</code> attribute.
	 * @return the history
	 */
	public List<UploadOrderHistory> getHistory()
	{
		return getHistory( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.history</code> attribute. 
	 * @param value the history
	 */
	public void setHistory(final SessionContext ctx, final List<UploadOrderHistory> value)
	{
		HISTORYHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.history</code> attribute. 
	 * @param value the history
	 */
	public void setHistory(final List<UploadOrderHistory> value)
	{
		setHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to history. 
	 * @param value the item to add to history
	 */
	public void addToHistory(final SessionContext ctx, final UploadOrderHistory value)
	{
		HISTORYHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to history. 
	 * @param value the item to add to history
	 */
	public void addToHistory(final UploadOrderHistory value)
	{
		addToHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from history. 
	 * @param value the item to remove from history
	 */
	public void removeFromHistory(final SessionContext ctx, final UploadOrderHistory value)
	{
		HISTORYHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from history. 
	 * @param value the item to remove from history
	 */
	public void removeFromHistory(final UploadOrderHistory value)
	{
		removeFromHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERCOMMENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments()
	{
		return getOrdercomments( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERCOMMENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final String value)
	{
		setOrdercomments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.PONumber</code> attribute.
	 * @return the PONumber - SAP Purchase Order Number
	 */
	public String getPONumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PONUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.PONumber</code> attribute.
	 * @return the PONumber - SAP Purchase Order Number
	 */
	public String getPONumber()
	{
		return getPONumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.PONumber</code> attribute. 
	 * @param value the PONumber - SAP Purchase Order Number
	 */
	public void setPONumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PONUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.PONumber</code> attribute. 
	 * @param value the PONumber - SAP Purchase Order Number
	 */
	public void setPONumber(final String value)
	{
		setPONumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.sapordernumber</code> attribute.
	 * @return the sapordernumber - SAP Order Number
	 */
	public String getSapordernumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SAPORDERNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.sapordernumber</code> attribute.
	 * @return the sapordernumber - SAP Order Number
	 */
	public String getSapordernumber()
	{
		return getSapordernumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.sapordernumber</code> attribute. 
	 * @param value the sapordernumber - SAP Order Number
	 */
	public void setSapordernumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SAPORDERNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.sapordernumber</code> attribute. 
	 * @param value the sapordernumber - SAP Order Number
	 */
	public void setSapordernumber(final String value)
	{
		setSapordernumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ShipToAccount</code> attribute.
	 * @return the ShipToAccount - Ship To account code
	 */
	public FMCustomerAccount getShipToAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, SHIPTOACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ShipToAccount</code> attribute.
	 * @return the ShipToAccount - Ship To account code
	 */
	public FMCustomerAccount getShipToAccount()
	{
		return getShipToAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ShipToAccount</code> attribute. 
	 * @param value the ShipToAccount - Ship To account code
	 */
	public void setShipToAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, SHIPTOACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ShipToAccount</code> attribute. 
	 * @param value the ShipToAccount - Ship To account code
	 */
	public void setShipToAccount(final FMCustomerAccount value)
	{
		setShipToAccount( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ShipToAddress</code> attribute.
	 * @return the ShipToAddress
	 */
	public Address getShipToAddress(final SessionContext ctx)
	{
		return (Address)getProperty( ctx, SHIPTOADDRESS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.ShipToAddress</code> attribute.
	 * @return the ShipToAddress
	 */
	public Address getShipToAddress()
	{
		return getShipToAddress( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ShipToAddress</code> attribute. 
	 * @param value the ShipToAddress
	 */
	public void setShipToAddress(final SessionContext ctx, final Address value)
	{
		setProperty(ctx, SHIPTOADDRESS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.ShipToAddress</code> attribute. 
	 * @param value the ShipToAddress
	 */
	public void setShipToAddress(final Address value)
	{
		setShipToAddress( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.SoldToAccount</code> attribute.
	 * @return the SoldToAccount - Sold To account code
	 */
	public FMCustomerAccount getSoldToAccount(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, SOLDTOACCOUNT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.SoldToAccount</code> attribute.
	 * @return the SoldToAccount - Sold To account code
	 */
	public FMCustomerAccount getSoldToAccount()
	{
		return getSoldToAccount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.SoldToAccount</code> attribute. 
	 * @param value the SoldToAccount - Sold To account code
	 */
	public void setSoldToAccount(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, SOLDTOACCOUNT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.SoldToAccount</code> attribute. 
	 * @param value the SoldToAccount - Sold To account code
	 */
	public void setSoldToAccount(final FMCustomerAccount value)
	{
		setSoldToAccount( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to gather Upload order
	 */
	public Date getUpdatedTime(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, UPDATEDTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to gather Upload order
	 */
	public Date getUpdatedTime()
	{
		return getUpdatedTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to gather Upload order
	 */
	public void setUpdatedTime(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, UPDATEDTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to gather Upload order
	 */
	public void setUpdatedTime(final Date value)
	{
		setUpdatedTime( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.uploadOrderFile</code> attribute.
	 * @return the uploadOrderFile
	 */
	public Media getUploadOrderFile(final SessionContext ctx)
	{
		return (Media)getProperty( ctx, UPLOADORDERFILE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.uploadOrderFile</code> attribute.
	 * @return the uploadOrderFile
	 */
	public Media getUploadOrderFile()
	{
		return getUploadOrderFile( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.uploadOrderFile</code> attribute. 
	 * @param value the uploadOrderFile
	 */
	public void setUploadOrderFile(final SessionContext ctx, final Media value)
	{
		setProperty(ctx, UPLOADORDERFILE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.uploadOrderFile</code> attribute. 
	 * @param value the uploadOrderFile
	 */
	public void setUploadOrderFile(final Media value)
	{
		setUploadOrderFile( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.uploadOrderStatus</code> attribute.
	 * @return the uploadOrderStatus
	 */
	public EnumerationValue getUploadOrderStatus(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, UPLOADORDERSTATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.uploadOrderStatus</code> attribute.
	 * @return the uploadOrderStatus
	 */
	public EnumerationValue getUploadOrderStatus()
	{
		return getUploadOrderStatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.uploadOrderStatus</code> attribute. 
	 * @param value the uploadOrderStatus
	 */
	public void setUploadOrderStatus(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, UPLOADORDERSTATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.uploadOrderStatus</code> attribute. 
	 * @param value the uploadOrderStatus
	 */
	public void setUploadOrderStatus(final EnumerationValue value)
	{
		setUploadOrderStatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.user</code> attribute.
	 * @return the user
	 */
	public FMCustomer getUser(final SessionContext ctx)
	{
		return (FMCustomer)getProperty( ctx, USER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.user</code> attribute.
	 * @return the user
	 */
	public FMCustomer getUser()
	{
		return getUser( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.user</code> attribute. 
	 * @param value the user
	 */
	public void setUser(final SessionContext ctx, final FMCustomer value)
	{
		setProperty(ctx, USER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.user</code> attribute. 
	 * @param value the user
	 */
	public void setUser(final FMCustomer value)
	{
		setUser( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.userFirstName</code> attribute.
	 * @return the userFirstName
	 */
	public String getUserFirstName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, USERFIRSTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.userFirstName</code> attribute.
	 * @return the userFirstName
	 */
	public String getUserFirstName()
	{
		return getUserFirstName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.userFirstName</code> attribute. 
	 * @param value the userFirstName
	 */
	public void setUserFirstName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, USERFIRSTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.userFirstName</code> attribute. 
	 * @param value the userFirstName
	 */
	public void setUserFirstName(final String value)
	{
		setUserFirstName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.userLastName</code> attribute.
	 * @return the userLastName
	 */
	public String getUserLastName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, USERLASTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrder.userLastName</code> attribute.
	 * @return the userLastName
	 */
	public String getUserLastName()
	{
		return getUserLastName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.userLastName</code> attribute. 
	 * @param value the userLastName
	 */
	public void setUserLastName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, USERLASTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrder.userLastName</code> attribute. 
	 * @param value the userLastName
	 */
	public void setUserLastName(final String value)
	{
		setUserLastName( getSession().getSessionContext(), value );
	}
	
}
