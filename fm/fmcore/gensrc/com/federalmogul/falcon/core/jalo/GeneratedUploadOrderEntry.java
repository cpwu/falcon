/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import com.federalmogul.falcon.core.jalo.UploadOrder;
import com.federalmogul.falcon.core.jalo.UploadOrderHistory;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.UploadOrderEntry UploadOrderEntry}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUploadOrderEntry extends GenericItem
{
	/** Qualifier of the <code>UploadOrderEntry.rawPartNumber</code> attribute **/
	public static final String RAWPARTNUMBER = "rawPartNumber";
	/** Qualifier of the <code>UploadOrderEntry.Accountcode</code> attribute **/
	public static final String ACCOUNTCODE = "Accountcode";
	/** Qualifier of the <code>UploadOrderEntry.basePrice</code> attribute **/
	public static final String BASEPRICE = "basePrice";
	/** Qualifier of the <code>UploadOrderEntry.partDescription</code> attribute **/
	public static final String PARTDESCRIPTION = "partDescription";
	/** Qualifier of the <code>UploadOrderEntry.updatedTime</code> attribute **/
	public static final String UPDATEDTIME = "updatedTime";
	/** Qualifier of the <code>UploadOrderEntry.order</code> attribute **/
	public static final String ORDER = "order";
	/** Qualifier of the <code>UploadOrderEntry.partNumber</code> attribute **/
	public static final String PARTNUMBER = "partNumber";
	/** Qualifier of the <code>UploadOrderEntry.createdTime</code> attribute **/
	public static final String CREATEDTIME = "createdTime";
	/** Qualifier of the <code>UploadOrderEntry.externalSystem</code> attribute **/
	public static final String EXTERNALSYSTEM = "externalSystem";
	/** Qualifier of the <code>UploadOrderEntry.orderentryNumber</code> attribute **/
	public static final String ORDERENTRYNUMBER = "orderentryNumber";
	/** Qualifier of the <code>UploadOrderEntry.partResolutionMsg</code> attribute **/
	public static final String PARTRESOLUTIONMSG = "partResolutionMsg";
	/** Qualifier of the <code>UploadOrderEntry.partbrandState</code> attribute **/
	public static final String PARTBRANDSTATE = "partbrandState";
	/** Qualifier of the <code>UploadOrderEntry.partFlag</code> attribute **/
	public static final String PARTFLAG = "partFlag";
	/** Qualifier of the <code>UploadOrderEntry.history</code> attribute **/
	public static final String HISTORY = "history";
	/** Qualifier of the <code>UploadOrderEntry.quantity</code> attribute **/
	public static final String QUANTITY = "quantity";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n ORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedUploadOrderEntry> ORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedUploadOrderEntry>(
	FmCoreConstants.TC.UPLOADORDERENTRY,
	false,
	"order",
	"orderentryNumber",
	false,
	true,
	CollectionType.LIST
	);
	/**
	* {@link OneToManyHandler} for handling 1:n HISTORY's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<UploadOrderHistory> HISTORYHANDLER = new OneToManyHandler<UploadOrderHistory>(
	FmCoreConstants.TC.UPLOADORDERHISTORY,
	true,
	"entry",
	"updatedTime",
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(RAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(ACCOUNTCODE, AttributeMode.INITIAL);
		tmp.put(BASEPRICE, AttributeMode.INITIAL);
		tmp.put(PARTDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(UPDATEDTIME, AttributeMode.INITIAL);
		tmp.put(ORDER, AttributeMode.INITIAL);
		tmp.put(PARTNUMBER, AttributeMode.INITIAL);
		tmp.put(CREATEDTIME, AttributeMode.INITIAL);
		tmp.put(EXTERNALSYSTEM, AttributeMode.INITIAL);
		tmp.put(ORDERENTRYNUMBER, AttributeMode.INITIAL);
		tmp.put(PARTRESOLUTIONMSG, AttributeMode.INITIAL);
		tmp.put(PARTBRANDSTATE, AttributeMode.INITIAL);
		tmp.put(PARTFLAG, AttributeMode.INITIAL);
		tmp.put(QUANTITY, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.Accountcode</code> attribute.
	 * @return the Accountcode - Ship To account code
	 */
	public FMCustomerAccount getAccountcode(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, ACCOUNTCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.Accountcode</code> attribute.
	 * @return the Accountcode - Ship To account code
	 */
	public FMCustomerAccount getAccountcode()
	{
		return getAccountcode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.Accountcode</code> attribute. 
	 * @param value the Accountcode - Ship To account code
	 */
	public void setAccountcode(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, ACCOUNTCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.Accountcode</code> attribute. 
	 * @param value the Accountcode - Ship To account code
	 */
	public void setAccountcode(final FMCustomerAccount value)
	{
		setAccountcode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.basePrice</code> attribute.
	 * @return the basePrice
	 */
	public Double getBasePrice(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, BASEPRICE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.basePrice</code> attribute.
	 * @return the basePrice
	 */
	public Double getBasePrice()
	{
		return getBasePrice( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @return the basePrice
	 */
	public double getBasePriceAsPrimitive(final SessionContext ctx)
	{
		Double value = getBasePrice( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @return the basePrice
	 */
	public double getBasePriceAsPrimitive()
	{
		return getBasePriceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, BASEPRICE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final Double value)
	{
		setBasePrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final SessionContext ctx, final double value)
	{
		setBasePrice( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final double value)
	{
		setBasePrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.createdTime</code> attribute.
	 * @return the createdTime - The date for which to part creted
	 */
	public Date getCreatedTime(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, CREATEDTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.createdTime</code> attribute.
	 * @return the createdTime - The date for which to part creted
	 */
	public Date getCreatedTime()
	{
		return getCreatedTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.createdTime</code> attribute. 
	 * @param value the createdTime - The date for which to part creted
	 */
	public void setCreatedTime(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, CREATEDTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.createdTime</code> attribute. 
	 * @param value the createdTime - The date for which to part creted
	 */
	public void setCreatedTime(final Date value)
	{
		setCreatedTime( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		ORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.externalSystem</code> attribute.
	 * @return the externalSystem
	 */
	public String getExternalSystem(final SessionContext ctx)
	{
		return (String)getProperty( ctx, EXTERNALSYSTEM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.externalSystem</code> attribute.
	 * @return the externalSystem
	 */
	public String getExternalSystem()
	{
		return getExternalSystem( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.externalSystem</code> attribute. 
	 * @param value the externalSystem
	 */
	public void setExternalSystem(final SessionContext ctx, final String value)
	{
		setProperty(ctx, EXTERNALSYSTEM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.externalSystem</code> attribute. 
	 * @param value the externalSystem
	 */
	public void setExternalSystem(final String value)
	{
		setExternalSystem( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.history</code> attribute.
	 * @return the history
	 */
	public List<UploadOrderHistory> getHistory(final SessionContext ctx)
	{
		return (List<UploadOrderHistory>)HISTORYHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.history</code> attribute.
	 * @return the history
	 */
	public List<UploadOrderHistory> getHistory()
	{
		return getHistory( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.history</code> attribute. 
	 * @param value the history
	 */
	public void setHistory(final SessionContext ctx, final List<UploadOrderHistory> value)
	{
		HISTORYHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.history</code> attribute. 
	 * @param value the history
	 */
	public void setHistory(final List<UploadOrderHistory> value)
	{
		setHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to history. 
	 * @param value the item to add to history
	 */
	public void addToHistory(final SessionContext ctx, final UploadOrderHistory value)
	{
		HISTORYHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to history. 
	 * @param value the item to add to history
	 */
	public void addToHistory(final UploadOrderHistory value)
	{
		addToHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from history. 
	 * @param value the item to remove from history
	 */
	public void removeFromHistory(final SessionContext ctx, final UploadOrderHistory value)
	{
		HISTORYHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from history. 
	 * @param value the item to remove from history
	 */
	public void removeFromHistory(final UploadOrderHistory value)
	{
		removeFromHistory( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.order</code> attribute.
	 * @return the order
	 */
	public UploadOrder getOrder(final SessionContext ctx)
	{
		return (UploadOrder)getProperty( ctx, ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.order</code> attribute.
	 * @return the order
	 */
	public UploadOrder getOrder()
	{
		return getOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final SessionContext ctx, final UploadOrder value)
	{
		ORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final UploadOrder value)
	{
		setOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.orderentryNumber</code> attribute.
	 * @return the orderentryNumber
	 */
	public String getOrderentryNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERENTRYNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.orderentryNumber</code> attribute.
	 * @return the orderentryNumber
	 */
	public String getOrderentryNumber()
	{
		return getOrderentryNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.orderentryNumber</code> attribute. 
	 * @param value the orderentryNumber
	 */
	public void setOrderentryNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERENTRYNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.orderentryNumber</code> attribute. 
	 * @param value the orderentryNumber
	 */
	public void setOrderentryNumber(final String value)
	{
		setOrderentryNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partbrandState</code> attribute.
	 * @return the partbrandState
	 */
	public String getPartbrandState(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTBRANDSTATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partbrandState</code> attribute.
	 * @return the partbrandState
	 */
	public String getPartbrandState()
	{
		return getPartbrandState( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partbrandState</code> attribute. 
	 * @param value the partbrandState
	 */
	public void setPartbrandState(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTBRANDSTATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partbrandState</code> attribute. 
	 * @param value the partbrandState
	 */
	public void setPartbrandState(final String value)
	{
		setPartbrandState( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partDescription</code> attribute.
	 * @return the partDescription - Part Description
	 */
	public String getPartDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partDescription</code> attribute.
	 * @return the partDescription - Part Description
	 */
	public String getPartDescription()
	{
		return getPartDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partDescription</code> attribute. 
	 * @param value the partDescription - Part Description
	 */
	public void setPartDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partDescription</code> attribute. 
	 * @param value the partDescription - Part Description
	 */
	public void setPartDescription(final String value)
	{
		setPartDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partFlag</code> attribute.
	 * @return the partFlag - Part Number
	 */
	public String getPartFlag(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partFlag</code> attribute.
	 * @return the partFlag - Part Number
	 */
	public String getPartFlag()
	{
		return getPartFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partFlag</code> attribute. 
	 * @param value the partFlag - Part Number
	 */
	public void setPartFlag(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partFlag</code> attribute. 
	 * @param value the partFlag - Part Number
	 */
	public void setPartFlag(final String value)
	{
		setPartFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber()
	{
		return getPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final String value)
	{
		setPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partResolutionMsg</code> attribute.
	 * @return the partResolutionMsg
	 */
	public String getPartResolutionMsg(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTRESOLUTIONMSG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.partResolutionMsg</code> attribute.
	 * @return the partResolutionMsg
	 */
	public String getPartResolutionMsg()
	{
		return getPartResolutionMsg( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partResolutionMsg</code> attribute. 
	 * @param value the partResolutionMsg
	 */
	public void setPartResolutionMsg(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTRESOLUTIONMSG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.partResolutionMsg</code> attribute. 
	 * @param value the partResolutionMsg
	 */
	public void setPartResolutionMsg(final String value)
	{
		setPartResolutionMsg( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.quantity</code> attribute.
	 * @return the quantity
	 */
	public Integer getQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.quantity</code> attribute.
	 * @return the quantity
	 */
	public Integer getQuantity()
	{
		return getQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @return the quantity
	 */
	public int getQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @return the quantity
	 */
	public int getQuantityAsPrimitive()
	{
		return getQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final Integer value)
	{
		setQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final SessionContext ctx, final int value)
	{
		setQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final int value)
	{
		setQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber()
	{
		return getRawPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final String value)
	{
		setRawPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to part updated
	 */
	public Date getUpdatedTime(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, UPDATEDTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderEntry.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to part updated
	 */
	public Date getUpdatedTime()
	{
		return getUpdatedTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to part updated
	 */
	public void setUpdatedTime(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, UPDATEDTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderEntry.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to part updated
	 */
	public void setUpdatedTime(final Date value)
	{
		setUpdatedTime( getSession().getSessionContext(), value );
	}
	
}
