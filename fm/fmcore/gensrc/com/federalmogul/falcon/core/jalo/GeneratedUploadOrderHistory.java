/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.falcon.core.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomer;
import com.federalmogul.falcon.core.jalo.UploadOrder;
import com.federalmogul.falcon.core.jalo.UploadOrderEntry;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.federalmogul.falcon.core.jalo.UploadOrderHistory UploadOrderHistory}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedUploadOrderHistory extends GenericItem
{
	/** Qualifier of the <code>UploadOrderHistory.modifiedattribute</code> attribute **/
	public static final String MODIFIEDATTRIBUTE = "modifiedattribute";
	/** Qualifier of the <code>UploadOrderHistory.user</code> attribute **/
	public static final String USER = "user";
	/** Qualifier of the <code>UploadOrderHistory.updatedTime</code> attribute **/
	public static final String UPDATEDTIME = "updatedTime";
	/** Qualifier of the <code>UploadOrderHistory.entry</code> attribute **/
	public static final String ENTRY = "entry";
	/** Qualifier of the <code>UploadOrderHistory.modifiedValueTo</code> attribute **/
	public static final String MODIFIEDVALUETO = "modifiedValueTo";
	/** Qualifier of the <code>UploadOrderHistory.modifiedValueFrom</code> attribute **/
	public static final String MODIFIEDVALUEFROM = "modifiedValueFrom";
	/** Qualifier of the <code>UploadOrderHistory.order</code> attribute **/
	public static final String ORDER = "order";
	/** Qualifier of the <code>UploadOrderHistory.status</code> attribute **/
	public static final String STATUS = "status";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n ENTRY's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedUploadOrderHistory> ENTRYHANDLER = new BidirectionalOneToManyHandler<GeneratedUploadOrderHistory>(
	FmCoreConstants.TC.UPLOADORDERHISTORY,
	false,
	"entry",
	"updatedTime",
	false,
	true,
	CollectionType.LIST
	);
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n ORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedUploadOrderHistory> ORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedUploadOrderHistory>(
	FmCoreConstants.TC.UPLOADORDERHISTORY,
	false,
	"order",
	"updatedTime",
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(MODIFIEDATTRIBUTE, AttributeMode.INITIAL);
		tmp.put(USER, AttributeMode.INITIAL);
		tmp.put(UPDATEDTIME, AttributeMode.INITIAL);
		tmp.put(ENTRY, AttributeMode.INITIAL);
		tmp.put(MODIFIEDVALUETO, AttributeMode.INITIAL);
		tmp.put(MODIFIEDVALUEFROM, AttributeMode.INITIAL);
		tmp.put(ORDER, AttributeMode.INITIAL);
		tmp.put(STATUS, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		ENTRYHANDLER.newInstance(ctx, allAttributes);
		ORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.entry</code> attribute.
	 * @return the entry
	 */
	public UploadOrderEntry getEntry(final SessionContext ctx)
	{
		return (UploadOrderEntry)getProperty( ctx, ENTRY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.entry</code> attribute.
	 * @return the entry
	 */
	public UploadOrderEntry getEntry()
	{
		return getEntry( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.entry</code> attribute. 
	 * @param value the entry
	 */
	public void setEntry(final SessionContext ctx, final UploadOrderEntry value)
	{
		ENTRYHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.entry</code> attribute. 
	 * @param value the entry
	 */
	public void setEntry(final UploadOrderEntry value)
	{
		setEntry( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedattribute</code> attribute.
	 * @return the modifiedattribute
	 */
	public String getModifiedattribute(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODIFIEDATTRIBUTE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedattribute</code> attribute.
	 * @return the modifiedattribute
	 */
	public String getModifiedattribute()
	{
		return getModifiedattribute( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedattribute</code> attribute. 
	 * @param value the modifiedattribute
	 */
	public void setModifiedattribute(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODIFIEDATTRIBUTE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedattribute</code> attribute. 
	 * @param value the modifiedattribute
	 */
	public void setModifiedattribute(final String value)
	{
		setModifiedattribute( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedValueFrom</code> attribute.
	 * @return the modifiedValueFrom
	 */
	public String getModifiedValueFrom(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODIFIEDVALUEFROM);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedValueFrom</code> attribute.
	 * @return the modifiedValueFrom
	 */
	public String getModifiedValueFrom()
	{
		return getModifiedValueFrom( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedValueFrom</code> attribute. 
	 * @param value the modifiedValueFrom
	 */
	public void setModifiedValueFrom(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODIFIEDVALUEFROM,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedValueFrom</code> attribute. 
	 * @param value the modifiedValueFrom
	 */
	public void setModifiedValueFrom(final String value)
	{
		setModifiedValueFrom( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedValueTo</code> attribute.
	 * @return the modifiedValueTo
	 */
	public String getModifiedValueTo(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MODIFIEDVALUETO);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.modifiedValueTo</code> attribute.
	 * @return the modifiedValueTo
	 */
	public String getModifiedValueTo()
	{
		return getModifiedValueTo( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedValueTo</code> attribute. 
	 * @param value the modifiedValueTo
	 */
	public void setModifiedValueTo(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MODIFIEDVALUETO,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.modifiedValueTo</code> attribute. 
	 * @param value the modifiedValueTo
	 */
	public void setModifiedValueTo(final String value)
	{
		setModifiedValueTo( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.order</code> attribute.
	 * @return the order
	 */
	public UploadOrder getOrder(final SessionContext ctx)
	{
		return (UploadOrder)getProperty( ctx, ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.order</code> attribute.
	 * @return the order
	 */
	public UploadOrder getOrder()
	{
		return getOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final SessionContext ctx, final UploadOrder value)
	{
		ORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final UploadOrder value)
	{
		setOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.status</code> attribute.
	 * @return the status
	 */
	public String getStatus(final SessionContext ctx)
	{
		return (String)getProperty( ctx, STATUS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.status</code> attribute.
	 * @return the status
	 */
	public String getStatus()
	{
		return getStatus( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.status</code> attribute. 
	 * @param value the status
	 */
	public void setStatus(final SessionContext ctx, final String value)
	{
		setProperty(ctx, STATUS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.status</code> attribute. 
	 * @param value the status
	 */
	public void setStatus(final String value)
	{
		setStatus( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to part updated
	 */
	public Date getUpdatedTime(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, UPDATEDTIME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.updatedTime</code> attribute.
	 * @return the updatedTime - The date for which to part updated
	 */
	public Date getUpdatedTime()
	{
		return getUpdatedTime( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to part updated
	 */
	public void setUpdatedTime(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, UPDATEDTIME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.updatedTime</code> attribute. 
	 * @param value the updatedTime - The date for which to part updated
	 */
	public void setUpdatedTime(final Date value)
	{
		setUpdatedTime( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.user</code> attribute.
	 * @return the user
	 */
	public FMCustomer getUser(final SessionContext ctx)
	{
		return (FMCustomer)getProperty( ctx, USER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>UploadOrderHistory.user</code> attribute.
	 * @return the user
	 */
	public FMCustomer getUser()
	{
		return getUser( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.user</code> attribute. 
	 * @param value the user
	 */
	public void setUser(final SessionContext ctx, final FMCustomer value)
	{
		setProperty(ctx, USER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>UploadOrderHistory.user</code> attribute. 
	 * @param value the user
	 */
	public void setUser(final FMCustomer value)
	{
		setUser( getSession().getSessionContext(), value );
	}
	
}
