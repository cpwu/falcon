/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.core.jalo.FMCustomerAccount;
import com.fmo.wom.jalo.FMIPOOrderEntry;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.util.OneToManyHandler;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link com.fmo.wom.jalo.FMIPOOrder FMIPOOrder}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMIPOOrder extends GenericItem
{
	/** Qualifier of the <code>FMIPOOrder.billtoparty</code> attribute **/
	public static final String BILLTOPARTY = "billtoparty";
	/** Qualifier of the <code>FMIPOOrder.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMIPOOrder.ordercomments</code> attribute **/
	public static final String ORDERCOMMENTS = "ordercomments";
	/** Qualifier of the <code>FMIPOOrder.userFirstName</code> attribute **/
	public static final String USERFIRSTNAME = "userFirstName";
	/** Qualifier of the <code>FMIPOOrder.freightterms</code> attribute **/
	public static final String FREIGHTTERMS = "freightterms";
	/** Qualifier of the <code>FMIPOOrder.PONumber</code> attribute **/
	public static final String PONUMBER = "PONumber";
	/** Qualifier of the <code>FMIPOOrder.entries</code> attribute **/
	public static final String ENTRIES = "entries";
	/** Qualifier of the <code>FMIPOOrder.partyid</code> attribute **/
	public static final String PARTYID = "partyid";
	/** Qualifier of the <code>FMIPOOrder.shiptoparty</code> attribute **/
	public static final String SHIPTOPARTY = "shiptoparty";
	/**
	* {@link OneToManyHandler} for handling 1:n ENTRIES's relation attributes from 'many' side.
	**/
	protected static final OneToManyHandler<FMIPOOrderEntry> ENTRIESHANDLER = new OneToManyHandler<FMIPOOrderEntry>(
	FmCoreConstants.TC.FMIPOORDERENTRY,
	true,
	"order",
	"orderentryNumber",
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(BILLTOPARTY, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(ORDERCOMMENTS, AttributeMode.INITIAL);
		tmp.put(USERFIRSTNAME, AttributeMode.INITIAL);
		tmp.put(FREIGHTTERMS, AttributeMode.INITIAL);
		tmp.put(PONUMBER, AttributeMode.INITIAL);
		tmp.put(PARTYID, AttributeMode.INITIAL);
		tmp.put(SHIPTOPARTY, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.billtoparty</code> attribute.
	 * @return the billtoparty - Sold To account code
	 */
	public FMCustomerAccount getBilltoparty(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, BILLTOPARTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.billtoparty</code> attribute.
	 * @return the billtoparty - Sold To account code
	 */
	public FMCustomerAccount getBilltoparty()
	{
		return getBilltoparty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.billtoparty</code> attribute. 
	 * @param value the billtoparty - Sold To account code
	 */
	public void setBilltoparty(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, BILLTOPARTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.billtoparty</code> attribute. 
	 * @param value the billtoparty - Sold To account code
	 */
	public void setBilltoparty(final FMCustomerAccount value)
	{
		setBilltoparty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.code</code> attribute.
	 * @return the code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.code</code> attribute.
	 * @return the code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.code</code> attribute. 
	 * @param value the code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.entries</code> attribute.
	 * @return the entries
	 */
	public List<FMIPOOrderEntry> getEntries(final SessionContext ctx)
	{
		return (List<FMIPOOrderEntry>)ENTRIESHANDLER.getValues( ctx, this );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.entries</code> attribute.
	 * @return the entries
	 */
	public List<FMIPOOrderEntry> getEntries()
	{
		return getEntries( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.entries</code> attribute. 
	 * @param value the entries
	 */
	public void setEntries(final SessionContext ctx, final List<FMIPOOrderEntry> value)
	{
		ENTRIESHANDLER.setValues( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.entries</code> attribute. 
	 * @param value the entries
	 */
	public void setEntries(final List<FMIPOOrderEntry> value)
	{
		setEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to entries. 
	 * @param value the item to add to entries
	 */
	public void addToEntries(final SessionContext ctx, final FMIPOOrderEntry value)
	{
		ENTRIESHANDLER.addValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to entries. 
	 * @param value the item to add to entries
	 */
	public void addToEntries(final FMIPOOrderEntry value)
	{
		addToEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from entries. 
	 * @param value the item to remove from entries
	 */
	public void removeFromEntries(final SessionContext ctx, final FMIPOOrderEntry value)
	{
		ENTRIESHANDLER.removeValue( ctx, this, value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from entries. 
	 * @param value the item to remove from entries
	 */
	public void removeFromEntries(final FMIPOOrderEntry value)
	{
		removeFromEntries( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.freightterms</code> attribute.
	 * @return the freightterms - SAP Purchase Order Number
	 */
	public String getFreightterms(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FREIGHTTERMS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.freightterms</code> attribute.
	 * @return the freightterms - SAP Purchase Order Number
	 */
	public String getFreightterms()
	{
		return getFreightterms( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.freightterms</code> attribute. 
	 * @param value the freightterms - SAP Purchase Order Number
	 */
	public void setFreightterms(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FREIGHTTERMS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.freightterms</code> attribute. 
	 * @param value the freightterms - SAP Purchase Order Number
	 */
	public void setFreightterms(final String value)
	{
		setFreightterms( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERCOMMENTS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.ordercomments</code> attribute.
	 * @return the ordercomments - SAP Order commments
	 */
	public String getOrdercomments()
	{
		return getOrdercomments( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERCOMMENTS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.ordercomments</code> attribute. 
	 * @param value the ordercomments - SAP Order commments
	 */
	public void setOrdercomments(final String value)
	{
		setOrdercomments( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.partyid</code> attribute.
	 * @return the partyid - SAP Purchase Order Number
	 */
	public String getPartyid(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTYID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.partyid</code> attribute.
	 * @return the partyid - SAP Purchase Order Number
	 */
	public String getPartyid()
	{
		return getPartyid( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.partyid</code> attribute. 
	 * @param value the partyid - SAP Purchase Order Number
	 */
	public void setPartyid(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTYID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.partyid</code> attribute. 
	 * @param value the partyid - SAP Purchase Order Number
	 */
	public void setPartyid(final String value)
	{
		setPartyid( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.PONumber</code> attribute.
	 * @return the PONumber - SAP Purchase Order Number
	 */
	public String getPONumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PONUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.PONumber</code> attribute.
	 * @return the PONumber - SAP Purchase Order Number
	 */
	public String getPONumber()
	{
		return getPONumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.PONumber</code> attribute. 
	 * @param value the PONumber - SAP Purchase Order Number
	 */
	public void setPONumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PONUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.PONumber</code> attribute. 
	 * @param value the PONumber - SAP Purchase Order Number
	 */
	public void setPONumber(final String value)
	{
		setPONumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.shiptoparty</code> attribute.
	 * @return the shiptoparty - Ship To account code
	 */
	public FMCustomerAccount getShiptoparty(final SessionContext ctx)
	{
		return (FMCustomerAccount)getProperty( ctx, SHIPTOPARTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.shiptoparty</code> attribute.
	 * @return the shiptoparty - Ship To account code
	 */
	public FMCustomerAccount getShiptoparty()
	{
		return getShiptoparty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.shiptoparty</code> attribute. 
	 * @param value the shiptoparty - Ship To account code
	 */
	public void setShiptoparty(final SessionContext ctx, final FMCustomerAccount value)
	{
		setProperty(ctx, SHIPTOPARTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.shiptoparty</code> attribute. 
	 * @param value the shiptoparty - Ship To account code
	 */
	public void setShiptoparty(final FMCustomerAccount value)
	{
		setShiptoparty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.userFirstName</code> attribute.
	 * @return the userFirstName
	 */
	public String getUserFirstName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, USERFIRSTNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrder.userFirstName</code> attribute.
	 * @return the userFirstName
	 */
	public String getUserFirstName()
	{
		return getUserFirstName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.userFirstName</code> attribute. 
	 * @param value the userFirstName
	 */
	public void setUserFirstName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, USERFIRSTNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrder.userFirstName</code> attribute. 
	 * @param value the userFirstName
	 */
	public void setUserFirstName(final String value)
	{
		setUserFirstName( getSession().getSessionContext(), value );
	}
	
}
