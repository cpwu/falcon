/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.falcon.core.jalo.FMDCCenter;
import com.fmo.wom.jalo.FMIPOOrder;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.type.CollectionType;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.util.BidirectionalOneToManyHandler;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.fmo.wom.jalo.FMIPOOrderEntry FMIPOOrderEntry}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMIPOOrderEntry extends GenericItem
{
	/** Qualifier of the <code>FMIPOOrderEntry.CustomerItemId</code> attribute **/
	public static final String CUSTOMERITEMID = "CustomerItemId";
	/** Qualifier of the <code>FMIPOOrderEntry.rawPartNumber</code> attribute **/
	public static final String RAWPARTNUMBER = "rawPartNumber";
	/** Qualifier of the <code>FMIPOOrderEntry.TransportationMethod</code> attribute **/
	public static final String TRANSPORTATIONMETHOD = "TransportationMethod";
	/** Qualifier of the <code>FMIPOOrderEntry.basePrice</code> attribute **/
	public static final String BASEPRICE = "basePrice";
	/** Qualifier of the <code>FMIPOOrderEntry.partDescription</code> attribute **/
	public static final String PARTDESCRIPTION = "partDescription";
	/** Qualifier of the <code>FMIPOOrderEntry.shipfromparty</code> attribute **/
	public static final String SHIPFROMPARTY = "shipfromparty";
	/** Qualifier of the <code>FMIPOOrderEntry.scaccode</code> attribute **/
	public static final String SCACCODE = "scaccode";
	/** Qualifier of the <code>FMIPOOrderEntry.order</code> attribute **/
	public static final String ORDER = "order";
	/** Qualifier of the <code>FMIPOOrderEntry.partNumber</code> attribute **/
	public static final String PARTNUMBER = "partNumber";
	/** Qualifier of the <code>FMIPOOrderEntry.PromisedShipDate</code> attribute **/
	public static final String PROMISEDSHIPDATE = "PromisedShipDate";
	/** Qualifier of the <code>FMIPOOrderEntry.ShippingMethod</code> attribute **/
	public static final String SHIPPINGMETHOD = "ShippingMethod";
	/** Qualifier of the <code>FMIPOOrderEntry.CommonCarrier</code> attribute **/
	public static final String COMMONCARRIER = "CommonCarrier";
	/** Qualifier of the <code>FMIPOOrderEntry.suppliercode</code> attribute **/
	public static final String SUPPLIERCODE = "suppliercode";
	/** Qualifier of the <code>FMIPOOrderEntry.orderentryNumber</code> attribute **/
	public static final String ORDERENTRYNUMBER = "orderentryNumber";
	/** Qualifier of the <code>FMIPOOrderEntry.SupplierItemId</code> attribute **/
	public static final String SUPPLIERITEMID = "SupplierItemId";
	/** Qualifier of the <code>FMIPOOrderEntry.partFlag</code> attribute **/
	public static final String PARTFLAG = "partFlag";
	/** Qualifier of the <code>FMIPOOrderEntry.quantity</code> attribute **/
	public static final String QUANTITY = "quantity";
	/** Qualifier of the <code>FMIPOOrderEntry.manufacturercode</code> attribute **/
	public static final String MANUFACTURERCODE = "manufacturercode";
	/** Qualifier of the <code>FMIPOOrderEntry.brandAAIAId</code> attribute **/
	public static final String BRANDAAIAID = "brandAAIAId";
	/**
	* {@link BidirectionalOneToManyHandler} for handling 1:n ORDER's relation attributes from 'one' side.
	**/
	protected static final BidirectionalOneToManyHandler<GeneratedFMIPOOrderEntry> ORDERHANDLER = new BidirectionalOneToManyHandler<GeneratedFMIPOOrderEntry>(
	FmCoreConstants.TC.FMIPOORDERENTRY,
	false,
	"order",
	"orderentryNumber",
	false,
	true,
	CollectionType.LIST
	);
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CUSTOMERITEMID, AttributeMode.INITIAL);
		tmp.put(RAWPARTNUMBER, AttributeMode.INITIAL);
		tmp.put(TRANSPORTATIONMETHOD, AttributeMode.INITIAL);
		tmp.put(BASEPRICE, AttributeMode.INITIAL);
		tmp.put(PARTDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(SHIPFROMPARTY, AttributeMode.INITIAL);
		tmp.put(SCACCODE, AttributeMode.INITIAL);
		tmp.put(ORDER, AttributeMode.INITIAL);
		tmp.put(PARTNUMBER, AttributeMode.INITIAL);
		tmp.put(PROMISEDSHIPDATE, AttributeMode.INITIAL);
		tmp.put(SHIPPINGMETHOD, AttributeMode.INITIAL);
		tmp.put(COMMONCARRIER, AttributeMode.INITIAL);
		tmp.put(SUPPLIERCODE, AttributeMode.INITIAL);
		tmp.put(ORDERENTRYNUMBER, AttributeMode.INITIAL);
		tmp.put(SUPPLIERITEMID, AttributeMode.INITIAL);
		tmp.put(PARTFLAG, AttributeMode.INITIAL);
		tmp.put(QUANTITY, AttributeMode.INITIAL);
		tmp.put(MANUFACTURERCODE, AttributeMode.INITIAL);
		tmp.put(BRANDAAIAID, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.basePrice</code> attribute.
	 * @return the basePrice
	 */
	public Double getBasePrice(final SessionContext ctx)
	{
		return (Double)getProperty( ctx, BASEPRICE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.basePrice</code> attribute.
	 * @return the basePrice
	 */
	public Double getBasePrice()
	{
		return getBasePrice( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @return the basePrice
	 */
	public double getBasePriceAsPrimitive(final SessionContext ctx)
	{
		Double value = getBasePrice( ctx );
		return value != null ? value.doubleValue() : 0.0d;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @return the basePrice
	 */
	public double getBasePriceAsPrimitive()
	{
		return getBasePriceAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final SessionContext ctx, final Double value)
	{
		setProperty(ctx, BASEPRICE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final Double value)
	{
		setBasePrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final SessionContext ctx, final double value)
	{
		setBasePrice( ctx,Double.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.basePrice</code> attribute. 
	 * @param value the basePrice
	 */
	public void setBasePrice(final double value)
	{
		setBasePrice( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.brandAAIAId</code> attribute.
	 * @return the brandAAIAId
	 */
	public String getBrandAAIAId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BRANDAAIAID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.brandAAIAId</code> attribute.
	 * @return the brandAAIAId
	 */
	public String getBrandAAIAId()
	{
		return getBrandAAIAId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.brandAAIAId</code> attribute. 
	 * @param value the brandAAIAId
	 */
	public void setBrandAAIAId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BRANDAAIAID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.brandAAIAId</code> attribute. 
	 * @param value the brandAAIAId
	 */
	public void setBrandAAIAId(final String value)
	{
		setBrandAAIAId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.CommonCarrier</code> attribute.
	 * @return the CommonCarrier
	 */
	public String getCommonCarrier(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COMMONCARRIER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.CommonCarrier</code> attribute.
	 * @return the CommonCarrier
	 */
	public String getCommonCarrier()
	{
		return getCommonCarrier( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.CommonCarrier</code> attribute. 
	 * @param value the CommonCarrier
	 */
	public void setCommonCarrier(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COMMONCARRIER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.CommonCarrier</code> attribute. 
	 * @param value the CommonCarrier
	 */
	public void setCommonCarrier(final String value)
	{
		setCommonCarrier( getSession().getSessionContext(), value );
	}
	
	@Override
	protected Item createItem(final SessionContext ctx, final ComposedType type, final ItemAttributeMap allAttributes) throws JaloBusinessException
	{
		ORDERHANDLER.newInstance(ctx, allAttributes);
		return super.createItem( ctx, type, allAttributes );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.CustomerItemId</code> attribute.
	 * @return the CustomerItemId
	 */
	public String getCustomerItemId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMERITEMID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.CustomerItemId</code> attribute.
	 * @return the CustomerItemId
	 */
	public String getCustomerItemId()
	{
		return getCustomerItemId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.CustomerItemId</code> attribute. 
	 * @param value the CustomerItemId
	 */
	public void setCustomerItemId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMERITEMID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.CustomerItemId</code> attribute. 
	 * @param value the CustomerItemId
	 */
	public void setCustomerItemId(final String value)
	{
		setCustomerItemId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.manufacturercode</code> attribute.
	 * @return the manufacturercode
	 */
	public String getManufacturercode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MANUFACTURERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.manufacturercode</code> attribute.
	 * @return the manufacturercode
	 */
	public String getManufacturercode()
	{
		return getManufacturercode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.manufacturercode</code> attribute. 
	 * @param value the manufacturercode
	 */
	public void setManufacturercode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MANUFACTURERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.manufacturercode</code> attribute. 
	 * @param value the manufacturercode
	 */
	public void setManufacturercode(final String value)
	{
		setManufacturercode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.order</code> attribute.
	 * @return the order
	 */
	public FMIPOOrder getOrder(final SessionContext ctx)
	{
		return (FMIPOOrder)getProperty( ctx, ORDER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.order</code> attribute.
	 * @return the order
	 */
	public FMIPOOrder getOrder()
	{
		return getOrder( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final SessionContext ctx, final FMIPOOrder value)
	{
		ORDERHANDLER.addValue( ctx, value, this  );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.order</code> attribute. 
	 * @param value the order
	 */
	public void setOrder(final FMIPOOrder value)
	{
		setOrder( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.orderentryNumber</code> attribute.
	 * @return the orderentryNumber
	 */
	public String getOrderentryNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ORDERENTRYNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.orderentryNumber</code> attribute.
	 * @return the orderentryNumber
	 */
	public String getOrderentryNumber()
	{
		return getOrderentryNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.orderentryNumber</code> attribute. 
	 * @param value the orderentryNumber
	 */
	public void setOrderentryNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ORDERENTRYNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.orderentryNumber</code> attribute. 
	 * @param value the orderentryNumber
	 */
	public void setOrderentryNumber(final String value)
	{
		setOrderentryNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partDescription</code> attribute.
	 * @return the partDescription - Part Description
	 */
	public String getPartDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partDescription</code> attribute.
	 * @return the partDescription - Part Description
	 */
	public String getPartDescription()
	{
		return getPartDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partDescription</code> attribute. 
	 * @param value the partDescription - Part Description
	 */
	public void setPartDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partDescription</code> attribute. 
	 * @param value the partDescription - Part Description
	 */
	public void setPartDescription(final String value)
	{
		setPartDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partFlag</code> attribute.
	 * @return the partFlag - Part Number
	 */
	public String getPartFlag(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTFLAG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partFlag</code> attribute.
	 * @return the partFlag - Part Number
	 */
	public String getPartFlag()
	{
		return getPartFlag( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partFlag</code> attribute. 
	 * @param value the partFlag - Part Number
	 */
	public void setPartFlag(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTFLAG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partFlag</code> attribute. 
	 * @param value the partFlag - Part Number
	 */
	public void setPartFlag(final String value)
	{
		setPartFlag( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, PARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.partNumber</code> attribute.
	 * @return the partNumber - Part Number
	 */
	public String getPartNumber()
	{
		return getPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, PARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.partNumber</code> attribute. 
	 * @param value the partNumber - Part Number
	 */
	public void setPartNumber(final String value)
	{
		setPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.PromisedShipDate</code> attribute.
	 * @return the PromisedShipDate - The date for which to gather order total information
	 */
	public Date getPromisedShipDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, PROMISEDSHIPDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.PromisedShipDate</code> attribute.
	 * @return the PromisedShipDate - The date for which to gather order total information
	 */
	public Date getPromisedShipDate()
	{
		return getPromisedShipDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.PromisedShipDate</code> attribute. 
	 * @param value the PromisedShipDate - The date for which to gather order total information
	 */
	public void setPromisedShipDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, PROMISEDSHIPDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.PromisedShipDate</code> attribute. 
	 * @param value the PromisedShipDate - The date for which to gather order total information
	 */
	public void setPromisedShipDate(final Date value)
	{
		setPromisedShipDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.quantity</code> attribute.
	 * @return the quantity
	 */
	public Integer getQuantity(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, QUANTITY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.quantity</code> attribute.
	 * @return the quantity
	 */
	public Integer getQuantity()
	{
		return getQuantity( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @return the quantity
	 */
	public int getQuantityAsPrimitive(final SessionContext ctx)
	{
		Integer value = getQuantity( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @return the quantity
	 */
	public int getQuantityAsPrimitive()
	{
		return getQuantityAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, QUANTITY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final Integer value)
	{
		setQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final SessionContext ctx, final int value)
	{
		setQuantity( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.quantity</code> attribute. 
	 * @param value the quantity
	 */
	public void setQuantity(final int value)
	{
		setQuantity( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RAWPARTNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.rawPartNumber</code> attribute.
	 * @return the rawPartNumber - Raw Part Number
	 */
	public String getRawPartNumber()
	{
		return getRawPartNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RAWPARTNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.rawPartNumber</code> attribute. 
	 * @param value the rawPartNumber - Raw Part Number
	 */
	public void setRawPartNumber(final String value)
	{
		setRawPartNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.scaccode</code> attribute.
	 * @return the scaccode
	 */
	public String getScaccode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SCACCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.scaccode</code> attribute.
	 * @return the scaccode
	 */
	public String getScaccode()
	{
		return getScaccode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.scaccode</code> attribute. 
	 * @param value the scaccode
	 */
	public void setScaccode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SCACCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.scaccode</code> attribute. 
	 * @param value the scaccode
	 */
	public void setScaccode(final String value)
	{
		setScaccode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.shipfromparty</code> attribute.
	 * @return the shipfromparty - Ship From party. Basically DC info
	 */
	public FMDCCenter getShipfromparty(final SessionContext ctx)
	{
		return (FMDCCenter)getProperty( ctx, SHIPFROMPARTY);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.shipfromparty</code> attribute.
	 * @return the shipfromparty - Ship From party. Basically DC info
	 */
	public FMDCCenter getShipfromparty()
	{
		return getShipfromparty( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.shipfromparty</code> attribute. 
	 * @param value the shipfromparty - Ship From party. Basically DC info
	 */
	public void setShipfromparty(final SessionContext ctx, final FMDCCenter value)
	{
		setProperty(ctx, SHIPFROMPARTY,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.shipfromparty</code> attribute. 
	 * @param value the shipfromparty - Ship From party. Basically DC info
	 */
	public void setShipfromparty(final FMDCCenter value)
	{
		setShipfromparty( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.ShippingMethod</code> attribute.
	 * @return the ShippingMethod
	 */
	public String getShippingMethod(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIPPINGMETHOD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.ShippingMethod</code> attribute.
	 * @return the ShippingMethod
	 */
	public String getShippingMethod()
	{
		return getShippingMethod( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.ShippingMethod</code> attribute. 
	 * @param value the ShippingMethod
	 */
	public void setShippingMethod(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIPPINGMETHOD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.ShippingMethod</code> attribute. 
	 * @param value the ShippingMethod
	 */
	public void setShippingMethod(final String value)
	{
		setShippingMethod( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.suppliercode</code> attribute.
	 * @return the suppliercode
	 */
	public String getSuppliercode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUPPLIERCODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.suppliercode</code> attribute.
	 * @return the suppliercode
	 */
	public String getSuppliercode()
	{
		return getSuppliercode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.suppliercode</code> attribute. 
	 * @param value the suppliercode
	 */
	public void setSuppliercode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUPPLIERCODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.suppliercode</code> attribute. 
	 * @param value the suppliercode
	 */
	public void setSuppliercode(final String value)
	{
		setSuppliercode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.SupplierItemId</code> attribute.
	 * @return the SupplierItemId
	 */
	public String getSupplierItemId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SUPPLIERITEMID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.SupplierItemId</code> attribute.
	 * @return the SupplierItemId
	 */
	public String getSupplierItemId()
	{
		return getSupplierItemId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.SupplierItemId</code> attribute. 
	 * @param value the SupplierItemId
	 */
	public void setSupplierItemId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SUPPLIERITEMID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.SupplierItemId</code> attribute. 
	 * @param value the SupplierItemId
	 */
	public void setSupplierItemId(final String value)
	{
		setSupplierItemId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.TransportationMethod</code> attribute.
	 * @return the TransportationMethod
	 */
	public String getTransportationMethod(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRANSPORTATIONMETHOD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOOrderEntry.TransportationMethod</code> attribute.
	 * @return the TransportationMethod
	 */
	public String getTransportationMethod()
	{
		return getTransportationMethod( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.TransportationMethod</code> attribute. 
	 * @param value the TransportationMethod
	 */
	public void setTransportationMethod(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRANSPORTATIONMETHOD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOOrderEntry.TransportationMethod</code> attribute. 
	 * @param value the TransportationMethod
	 */
	public void setTransportationMethod(final String value)
	{
		setTransportationMethod( getSession().getSessionContext(), value );
	}
	
}
