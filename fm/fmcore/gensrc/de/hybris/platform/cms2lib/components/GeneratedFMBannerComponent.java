/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2lib.components.BannerComponent;
import de.hybris.platform.cms2lib.components.FMHomeCarouselComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.util.Utilities;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMBannerComponent FMBannerComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMBannerComponent extends BannerComponent
{
	/** Qualifier of the <code>FMBannerComponent.color</code> attribute **/
	public static final String COLOR = "color";
	/** Qualifier of the <code>FMBannerComponent.carouselcontainer</code> attribute **/
	public static final String CAROUSELCONTAINER = "carouselcontainer";
	/** Qualifier of the <code>FMBannerComponent.sourceLink</code> attribute **/
	public static final String SOURCELINK = "sourceLink";
	/** Qualifier of the <code>FMBannerComponent.sequenceId</code> attribute **/
	public static final String SEQUENCEID = "sequenceId";
	/** Qualifier of the <code>FMBannerComponent.fmrotatingComponent</code> attribute **/
	public static final String FMROTATINGCOMPONENT = "fmrotatingComponent";
	/** Relation ordering override parameter constants for BannersForFMHomeCarouselComponent from ((fmcore))*/
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED = "relation.BannersForFMHomeCarouselComponent.source.ordered";
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_TGT_ORDERED = "relation.BannersForFMHomeCarouselComponent.target.ordered";
	/** Relation disable markmodifed parameter constants for BannersForFMHomeCarouselComponent from ((fmcore))*/
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED = "relation.BannersForFMHomeCarouselComponent.markmodified";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(BannerComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(COLOR, AttributeMode.INITIAL);
		tmp.put(CAROUSELCONTAINER, AttributeMode.INITIAL);
		tmp.put(SOURCELINK, AttributeMode.INITIAL);
		tmp.put(SEQUENCEID, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.carouselcontainer</code> attribute.
	 * @return the carouselcontainer
	 */
	public String getCarouselcontainer(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMBannerComponent.getCarouselcontainer requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, CAROUSELCONTAINER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.carouselcontainer</code> attribute.
	 * @return the carouselcontainer
	 */
	public String getCarouselcontainer()
	{
		return getCarouselcontainer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @return the localized carouselcontainer
	 */
	public Map<Language,String> getAllCarouselcontainer(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,CAROUSELCONTAINER,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @return the localized carouselcontainer
	 */
	public Map<Language,String> getAllCarouselcontainer()
	{
		return getAllCarouselcontainer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @param value the carouselcontainer
	 */
	public void setCarouselcontainer(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMBannerComponent.setCarouselcontainer requires a session language", 0 );
		}
		setLocalizedProperty(ctx, CAROUSELCONTAINER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @param value the carouselcontainer
	 */
	public void setCarouselcontainer(final String value)
	{
		setCarouselcontainer( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @param value the carouselcontainer
	 */
	public void setAllCarouselcontainer(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,CAROUSELCONTAINER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.carouselcontainer</code> attribute. 
	 * @param value the carouselcontainer
	 */
	public void setAllCarouselcontainer(final Map<Language,String> value)
	{
		setAllCarouselcontainer( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.color</code> attribute.
	 * @return the color
	 */
	public String getColor(final SessionContext ctx)
	{
		return (String)getProperty( ctx, COLOR);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.color</code> attribute.
	 * @return the color
	 */
	public String getColor()
	{
		return getColor( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.color</code> attribute. 
	 * @param value the color
	 */
	public void setColor(final SessionContext ctx, final String value)
	{
		setProperty(ctx, COLOR,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.color</code> attribute. 
	 * @param value the color
	 */
	public void setColor(final String value)
	{
		setColor( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.fmrotatingComponent</code> attribute.
	 * @return the fmrotatingComponent
	 */
	public Collection<FMHomeCarouselComponent> getFmrotatingComponent(final SessionContext ctx)
	{
		final List<FMHomeCarouselComponent> items = getLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.fmrotatingComponent</code> attribute.
	 * @return the fmrotatingComponent
	 */
	public Collection<FMHomeCarouselComponent> getFmrotatingComponent()
	{
		return getFmrotatingComponent( getSession().getSessionContext() );
	}
	
	public long getFmrotatingComponentCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			false,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null
		);
	}
	
	public long getFmrotatingComponentCount()
	{
		return getFmrotatingComponentCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.fmrotatingComponent</code> attribute. 
	 * @param value the fmrotatingComponent
	 */
	public void setFmrotatingComponent(final SessionContext ctx, final Collection<FMHomeCarouselComponent> value)
	{
		setLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.fmrotatingComponent</code> attribute. 
	 * @param value the fmrotatingComponent
	 */
	public void setFmrotatingComponent(final Collection<FMHomeCarouselComponent> value)
	{
		setFmrotatingComponent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmrotatingComponent. 
	 * @param value the item to add to fmrotatingComponent
	 */
	public void addToFmrotatingComponent(final SessionContext ctx, final FMHomeCarouselComponent value)
	{
		addLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmrotatingComponent. 
	 * @param value the item to add to fmrotatingComponent
	 */
	public void addToFmrotatingComponent(final FMHomeCarouselComponent value)
	{
		addToFmrotatingComponent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmrotatingComponent. 
	 * @param value the item to remove from fmrotatingComponent
	 */
	public void removeFromFmrotatingComponent(final SessionContext ctx, final FMHomeCarouselComponent value)
	{
		removeLinkedItems( 
			ctx,
			false,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmrotatingComponent. 
	 * @param value the item to remove from fmrotatingComponent
	 */
	public void removeFromFmrotatingComponent(final FMHomeCarouselComponent value)
	{
		removeFromFmrotatingComponent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sequenceId</code> attribute.
	 * @return the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public Integer getSequenceId(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, SEQUENCEID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sequenceId</code> attribute.
	 * @return the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public Integer getSequenceId()
	{
		return getSequenceId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @return the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public int getSequenceIdAsPrimitive(final SessionContext ctx)
	{
		Integer value = getSequenceId( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @return the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public int getSequenceIdAsPrimitive()
	{
		return getSequenceIdAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @param value the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public void setSequenceId(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, SEQUENCEID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @param value the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public void setSequenceId(final Integer value)
	{
		setSequenceId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @param value the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public void setSequenceId(final SessionContext ctx, final int value)
	{
		setSequenceId( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sequenceId</code> attribute. 
	 * @param value the sequenceId - Attribute is used in display the images in sequence manner.
	 */
	public void setSequenceId(final int value)
	{
		setSequenceId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sourceLink</code> attribute.
	 * @return the sourceLink - Attribute is used in case of videos
	 */
	public String getSourceLink(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SOURCELINK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMBannerComponent.sourceLink</code> attribute.
	 * @return the sourceLink - Attribute is used in case of videos
	 */
	public String getSourceLink()
	{
		return getSourceLink( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sourceLink</code> attribute. 
	 * @param value the sourceLink - Attribute is used in case of videos
	 */
	public void setSourceLink(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SOURCELINK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMBannerComponent.sourceLink</code> attribute. 
	 * @param value the sourceLink - Attribute is used in case of videos
	 */
	public void setSourceLink(final String value)
	{
		setSourceLink( getSession().getSessionContext(), value );
	}
	
}
