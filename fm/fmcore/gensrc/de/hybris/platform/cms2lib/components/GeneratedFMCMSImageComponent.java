/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.CMSImageComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMCMSImageComponent FMCMSImageComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMCMSImageComponent extends CMSImageComponent
{
	/** Qualifier of the <code>FMCMSImageComponent.imageText</code> attribute **/
	public static final String IMAGETEXT = "imageText";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(CMSImageComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(IMAGETEXT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCMSImageComponent.imageText</code> attribute.
	 * @return the imageText
	 */
	public String getImageText(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMCMSImageComponent.getImageText requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, IMAGETEXT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCMSImageComponent.imageText</code> attribute.
	 * @return the imageText
	 */
	public String getImageText()
	{
		return getImageText( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @return the localized imageText
	 */
	public Map<Language,String> getAllImageText(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,IMAGETEXT,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @return the localized imageText
	 */
	public Map<Language,String> getAllImageText()
	{
		return getAllImageText( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @param value the imageText
	 */
	public void setImageText(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMCMSImageComponent.setImageText requires a session language", 0 );
		}
		setLocalizedProperty(ctx, IMAGETEXT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @param value the imageText
	 */
	public void setImageText(final String value)
	{
		setImageText( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @param value the imageText
	 */
	public void setAllImageText(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,IMAGETEXT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMCMSImageComponent.imageText</code> attribute. 
	 * @param value the imageText
	 */
	public void setAllImageText(final Map<Language,String> value)
	{
		setAllImageText( getSession().getSessionContext(), value );
	}
	
}
