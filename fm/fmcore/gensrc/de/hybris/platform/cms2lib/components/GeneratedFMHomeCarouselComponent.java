/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.SimpleCMSComponent;
import de.hybris.platform.cms2lib.components.FMBannerComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMHomeCarouselComponent FMHomeCarouselComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMHomeCarouselComponent extends SimpleCMSComponent
{
	/** Qualifier of the <code>FMHomeCarouselComponent.timeout</code> attribute **/
	public static final String TIMEOUT = "timeout";
	/** Qualifier of the <code>FMHomeCarouselComponent.fmbanners</code> attribute **/
	public static final String FMBANNERS = "fmbanners";
	/** Relation ordering override parameter constants for BannersForFMHomeCarouselComponent from ((fmcore))*/
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED = "relation.BannersForFMHomeCarouselComponent.source.ordered";
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_TGT_ORDERED = "relation.BannersForFMHomeCarouselComponent.target.ordered";
	/** Relation disable markmodifed parameter constants for BannersForFMHomeCarouselComponent from ((fmcore))*/
	protected static String BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED = "relation.BannersForFMHomeCarouselComponent.markmodified";
	/** Qualifier of the <code>FMHomeCarouselComponent.description</code> attribute **/
	public static final String DESCRIPTION = "description";
	/** Qualifier of the <code>FMHomeCarouselComponent.learingdescription</code> attribute **/
	public static final String LEARINGDESCRIPTION = "learingdescription";
	/** Qualifier of the <code>FMHomeCarouselComponent.heading</code> attribute **/
	public static final String HEADING = "heading";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SimpleCMSComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(TIMEOUT, AttributeMode.INITIAL);
		tmp.put(DESCRIPTION, AttributeMode.INITIAL);
		tmp.put(LEARINGDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(HEADING, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.description</code> attribute.
	 * @return the description
	 */
	public String getDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, DESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.description</code> attribute.
	 * @return the description
	 */
	public String getDescription()
	{
		return getDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.description</code> attribute. 
	 * @param value the description
	 */
	public void setDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, DESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.description</code> attribute. 
	 * @param value the description
	 */
	public void setDescription(final String value)
	{
		setDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.fmbanners</code> attribute.
	 * @return the fmbanners
	 */
	public List<FMBannerComponent> getFmbanners(final SessionContext ctx)
	{
		final List<FMBannerComponent> items = getLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.fmbanners</code> attribute.
	 * @return the fmbanners
	 */
	public List<FMBannerComponent> getFmbanners()
	{
		return getFmbanners( getSession().getSessionContext() );
	}
	
	public long getFmbannersCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			true,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null
		);
	}
	
	public long getFmbannersCount()
	{
		return getFmbannersCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.fmbanners</code> attribute. 
	 * @param value the fmbanners
	 */
	public void setFmbanners(final SessionContext ctx, final List<FMBannerComponent> value)
	{
		setLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.fmbanners</code> attribute. 
	 * @param value the fmbanners
	 */
	public void setFmbanners(final List<FMBannerComponent> value)
	{
		setFmbanners( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmbanners. 
	 * @param value the item to add to fmbanners
	 */
	public void addToFmbanners(final SessionContext ctx, final FMBannerComponent value)
	{
		addLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmbanners. 
	 * @param value the item to add to fmbanners
	 */
	public void addToFmbanners(final FMBannerComponent value)
	{
		addToFmbanners( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmbanners. 
	 * @param value the item to remove from fmbanners
	 */
	public void removeFromFmbanners(final SessionContext ctx, final FMBannerComponent value)
	{
		removeLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.BANNERSFORFMHOMECAROUSELCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(BANNERSFORFMHOMECAROUSELCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmbanners. 
	 * @param value the item to remove from fmbanners
	 */
	public void removeFromFmbanners(final FMBannerComponent value)
	{
		removeFromFmbanners( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.heading</code> attribute.
	 * @return the heading
	 */
	public String getHeading(final SessionContext ctx)
	{
		return (String)getProperty( ctx, HEADING);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.heading</code> attribute.
	 * @return the heading
	 */
	public String getHeading()
	{
		return getHeading( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.heading</code> attribute. 
	 * @param value the heading
	 */
	public void setHeading(final SessionContext ctx, final String value)
	{
		setProperty(ctx, HEADING,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.heading</code> attribute. 
	 * @param value the heading
	 */
	public void setHeading(final String value)
	{
		setHeading( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.learingdescription</code> attribute.
	 * @return the learingdescription
	 */
	public String getLearingdescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, LEARINGDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.learingdescription</code> attribute.
	 * @return the learingdescription
	 */
	public String getLearingdescription()
	{
		return getLearingdescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.learingdescription</code> attribute. 
	 * @param value the learingdescription
	 */
	public void setLearingdescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, LEARINGDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.learingdescription</code> attribute. 
	 * @param value the learingdescription
	 */
	public void setLearingdescription(final String value)
	{
		setLearingdescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.timeout</code> attribute.
	 * @return the timeout
	 */
	public Integer getTimeout(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, TIMEOUT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.timeout</code> attribute.
	 * @return the timeout
	 */
	public Integer getTimeout()
	{
		return getTimeout( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @return the timeout
	 */
	public int getTimeoutAsPrimitive(final SessionContext ctx)
	{
		Integer value = getTimeout( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @return the timeout
	 */
	public int getTimeoutAsPrimitive()
	{
		return getTimeoutAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @param value the timeout
	 */
	public void setTimeout(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, TIMEOUT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @param value the timeout
	 */
	public void setTimeout(final Integer value)
	{
		setTimeout( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @param value the timeout
	 */
	public void setTimeout(final SessionContext ctx, final int value)
	{
		setTimeout( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMHomeCarouselComponent.timeout</code> attribute. 
	 * @param value the timeout
	 */
	public void setTimeout(final int value)
	{
		setTimeout( getSession().getSessionContext(), value );
	}
	
}
