/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.CMSParagraphComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMParagraphComponent FMParagraphComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMParagraphComponent extends CMSParagraphComponent
{
	/** Qualifier of the <code>FMParagraphComponent.subContent</code> attribute **/
	public static final String SUBCONTENT = "subContent";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(CMSParagraphComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(SUBCONTENT, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMParagraphComponent.subContent</code> attribute.
	 * @return the subContent
	 */
	public String getSubContent(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMParagraphComponent.getSubContent requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, SUBCONTENT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMParagraphComponent.subContent</code> attribute.
	 * @return the subContent
	 */
	public String getSubContent()
	{
		return getSubContent( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @return the localized subContent
	 */
	public Map<Language,String> getAllSubContent(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,SUBCONTENT,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @return the localized subContent
	 */
	public Map<Language,String> getAllSubContent()
	{
		return getAllSubContent( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @param value the subContent
	 */
	public void setSubContent(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMParagraphComponent.setSubContent requires a session language", 0 );
		}
		setLocalizedProperty(ctx, SUBCONTENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @param value the subContent
	 */
	public void setSubContent(final String value)
	{
		setSubContent( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @param value the subContent
	 */
	public void setAllSubContent(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,SUBCONTENT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMParagraphComponent.subContent</code> attribute. 
	 * @param value the subContent
	 */
	public void setAllSubContent(final Map<Language,String> value)
	{
		setAllSubContent( getSession().getSessionContext(), value );
	}
	
}
