/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.CMSParagraphComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloInvalidParameterException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.c2l.C2LManager;
import de.hybris.platform.jalo.c2l.Language;
import de.hybris.platform.util.Utilities;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMSupportAboutusComponent FMSupportAboutusComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMSupportAboutusComponent extends CMSParagraphComponent
{
	/** Qualifier of the <code>FMSupportAboutusComponent.fmParagraphComp</code> attribute **/
	public static final String FMPARAGRAPHCOMP = "fmParagraphComp";
	/** Relation ordering override parameter constants for HomeFMSupportAboutusComponent from ((fmcore))*/
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED = "relation.HomeFMSupportAboutusComponent.source.ordered";
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_TGT_ORDERED = "relation.HomeFMSupportAboutusComponent.target.ordered";
	/** Relation disable markmodifed parameter constants for HomeFMSupportAboutusComponent from ((fmcore))*/
	protected static String HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED = "relation.HomeFMSupportAboutusComponent.markmodified";
	/** Qualifier of the <code>FMSupportAboutusComponent.linkText</code> attribute **/
	public static final String LINKTEXT = "linkText";
	/** Qualifier of the <code>FMSupportAboutusComponent.headLine</code> attribute **/
	public static final String HEADLINE = "headLine";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(CMSParagraphComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(LINKTEXT, AttributeMode.INITIAL);
		tmp.put(HEADLINE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.fmParagraphComp</code> attribute.
	 * @return the fmParagraphComp
	 */
	public List<CMSParagraphComponent> getFmParagraphComp(final SessionContext ctx)
	{
		final List<CMSParagraphComponent> items = getLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false
		);
		return items;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.fmParagraphComp</code> attribute.
	 * @return the fmParagraphComp
	 */
	public List<CMSParagraphComponent> getFmParagraphComp()
	{
		return getFmParagraphComp( getSession().getSessionContext() );
	}
	
	public long getFmParagraphCompCount(final SessionContext ctx)
	{
		return getLinkedItemsCount(
			ctx,
			true,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null
		);
	}
	
	public long getFmParagraphCompCount()
	{
		return getFmParagraphCompCount( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.fmParagraphComp</code> attribute. 
	 * @param value the fmParagraphComp
	 */
	public void setFmParagraphComp(final SessionContext ctx, final List<CMSParagraphComponent> value)
	{
		setLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			value,
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.fmParagraphComp</code> attribute. 
	 * @param value the fmParagraphComp
	 */
	public void setFmParagraphComp(final List<CMSParagraphComponent> value)
	{
		setFmParagraphComp( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmParagraphComp. 
	 * @param value the item to add to fmParagraphComp
	 */
	public void addToFmParagraphComp(final SessionContext ctx, final CMSParagraphComponent value)
	{
		addLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Adds <code>value</code> to fmParagraphComp. 
	 * @param value the item to add to fmParagraphComp
	 */
	public void addToFmParagraphComp(final CMSParagraphComponent value)
	{
		addToFmParagraphComp( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmParagraphComp. 
	 * @param value the item to remove from fmParagraphComp
	 */
	public void removeFromFmParagraphComp(final SessionContext ctx, final CMSParagraphComponent value)
	{
		removeLinkedItems( 
			ctx,
			true,
			FmCoreConstants.Relations.HOMEFMSUPPORTABOUTUSCOMPONENT,
			null,
			Collections.singletonList(value),
			Utilities.getRelationOrderingOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_SRC_ORDERED, true),
			false,
			Utilities.getMarkModifiedOverride(HOMEFMSUPPORTABOUTUSCOMPONENT_MARKMODIFIED)
		);
	}
	
	/**
	 * <i>Generated method</i> - Removes <code>value</code> from fmParagraphComp. 
	 * @param value the item to remove from fmParagraphComp
	 */
	public void removeFromFmParagraphComp(final CMSParagraphComponent value)
	{
		removeFromFmParagraphComp( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.headLine</code> attribute.
	 * @return the headLine
	 */
	public String getHeadLine(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMSupportAboutusComponent.getHeadLine requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, HEADLINE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.headLine</code> attribute.
	 * @return the headLine
	 */
	public String getHeadLine()
	{
		return getHeadLine( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @return the localized headLine
	 */
	public Map<Language,String> getAllHeadLine(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,HEADLINE,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @return the localized headLine
	 */
	public Map<Language,String> getAllHeadLine()
	{
		return getAllHeadLine( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @param value the headLine
	 */
	public void setHeadLine(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMSupportAboutusComponent.setHeadLine requires a session language", 0 );
		}
		setLocalizedProperty(ctx, HEADLINE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @param value the headLine
	 */
	public void setHeadLine(final String value)
	{
		setHeadLine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @param value the headLine
	 */
	public void setAllHeadLine(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,HEADLINE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.headLine</code> attribute. 
	 * @param value the headLine
	 */
	public void setAllHeadLine(final Map<Language,String> value)
	{
		setAllHeadLine( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.linkText</code> attribute.
	 * @return the linkText
	 */
	public String getLinkText(final SessionContext ctx)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMSupportAboutusComponent.getLinkText requires a session language", 0 );
		}
		return (String)getLocalizedProperty( ctx, LINKTEXT);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.linkText</code> attribute.
	 * @return the linkText
	 */
	public String getLinkText()
	{
		return getLinkText( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @return the localized linkText
	 */
	public Map<Language,String> getAllLinkText(final SessionContext ctx)
	{
		return (Map<Language,String>)getAllLocalizedProperties(ctx,LINKTEXT,C2LManager.getInstance().getAllLanguages());
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @return the localized linkText
	 */
	public Map<Language,String> getAllLinkText()
	{
		return getAllLinkText( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @param value the linkText
	 */
	public void setLinkText(final SessionContext ctx, final String value)
	{
		if( ctx == null || ctx.getLanguage() == null )
		{
			throw new JaloInvalidParameterException("GeneratedFMSupportAboutusComponent.setLinkText requires a session language", 0 );
		}
		setLocalizedProperty(ctx, LINKTEXT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @param value the linkText
	 */
	public void setLinkText(final String value)
	{
		setLinkText( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @param value the linkText
	 */
	public void setAllLinkText(final SessionContext ctx, final Map<Language,String> value)
	{
		setAllLocalizedProperties(ctx,LINKTEXT,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMSupportAboutusComponent.linkText</code> attribute. 
	 * @param value the linkText
	 */
	public void setAllLinkText(final Map<Language,String> value)
	{
		setAllLinkText( getSession().getSessionContext(), value );
	}
	
}
