/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.cms2lib.components;

import com.federalmogul.core.constants.FmCoreConstants;
import de.hybris.platform.cms2.jalo.contents.components.SimpleCMSComponent;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.media.Media;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.cms2lib.components.FMVideoCarouselComponent FMVideoCarouselComponent}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMVideoCarouselComponent extends SimpleCMSComponent
{
	/** Qualifier of the <code>FMVideoCarouselComponent.bannerName</code> attribute **/
	public static final String BANNERNAME = "bannerName";
	/** Qualifier of the <code>FMVideoCarouselComponent.buttonName</code> attribute **/
	public static final String BUTTONNAME = "buttonName";
	/** Qualifier of the <code>FMVideoCarouselComponent.bannerLink</code> attribute **/
	public static final String BANNERLINK = "bannerLink";
	/** Qualifier of the <code>FMVideoCarouselComponent.videos</code> attribute **/
	public static final String VIDEOS = "videos";
	/** Qualifier of the <code>FMVideoCarouselComponent.bannerImage</code> attribute **/
	public static final String BANNERIMAGE = "bannerImage";
	/** Qualifier of the <code>FMVideoCarouselComponent.display2Videos</code> attribute **/
	public static final String DISPLAY2VIDEOS = "display2Videos";
	/** Qualifier of the <code>FMVideoCarouselComponent.bannerDescription</code> attribute **/
	public static final String BANNERDESCRIPTION = "bannerDescription";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(SimpleCMSComponent.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(BANNERNAME, AttributeMode.INITIAL);
		tmp.put(BUTTONNAME, AttributeMode.INITIAL);
		tmp.put(BANNERLINK, AttributeMode.INITIAL);
		tmp.put(VIDEOS, AttributeMode.INITIAL);
		tmp.put(BANNERIMAGE, AttributeMode.INITIAL);
		tmp.put(DISPLAY2VIDEOS, AttributeMode.INITIAL);
		tmp.put(BANNERDESCRIPTION, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerDescription</code> attribute.
	 * @return the bannerDescription
	 */
	public String getBannerDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BANNERDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerDescription</code> attribute.
	 * @return the bannerDescription
	 */
	public String getBannerDescription()
	{
		return getBannerDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerDescription</code> attribute. 
	 * @param value the bannerDescription
	 */
	public void setBannerDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BANNERDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerDescription</code> attribute. 
	 * @param value the bannerDescription
	 */
	public void setBannerDescription(final String value)
	{
		setBannerDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerImage</code> attribute.
	 * @return the bannerImage
	 */
	public String getBannerImage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BANNERIMAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerImage</code> attribute.
	 * @return the bannerImage
	 */
	public String getBannerImage()
	{
		return getBannerImage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerImage</code> attribute. 
	 * @param value the bannerImage
	 */
	public void setBannerImage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BANNERIMAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerImage</code> attribute. 
	 * @param value the bannerImage
	 */
	public void setBannerImage(final String value)
	{
		setBannerImage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerLink</code> attribute.
	 * @return the bannerLink
	 */
	public String getBannerLink(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BANNERLINK);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerLink</code> attribute.
	 * @return the bannerLink
	 */
	public String getBannerLink()
	{
		return getBannerLink( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerLink</code> attribute. 
	 * @param value the bannerLink
	 */
	public void setBannerLink(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BANNERLINK,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerLink</code> attribute. 
	 * @param value the bannerLink
	 */
	public void setBannerLink(final String value)
	{
		setBannerLink( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerName</code> attribute.
	 * @return the bannerName
	 */
	public String getBannerName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BANNERNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.bannerName</code> attribute.
	 * @return the bannerName
	 */
	public String getBannerName()
	{
		return getBannerName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerName</code> attribute. 
	 * @param value the bannerName
	 */
	public void setBannerName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BANNERNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.bannerName</code> attribute. 
	 * @param value the bannerName
	 */
	public void setBannerName(final String value)
	{
		setBannerName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.buttonName</code> attribute.
	 * @return the buttonName
	 */
	public String getButtonName(final SessionContext ctx)
	{
		return (String)getProperty( ctx, BUTTONNAME);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.buttonName</code> attribute.
	 * @return the buttonName
	 */
	public String getButtonName()
	{
		return getButtonName( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.buttonName</code> attribute. 
	 * @param value the buttonName
	 */
	public void setButtonName(final SessionContext ctx, final String value)
	{
		setProperty(ctx, BUTTONNAME,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.buttonName</code> attribute. 
	 * @param value the buttonName
	 */
	public void setButtonName(final String value)
	{
		setButtonName( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute.
	 * @return the display2Videos
	 */
	public Boolean isDisplay2Videos(final SessionContext ctx)
	{
		return (Boolean)getProperty( ctx, DISPLAY2VIDEOS);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute.
	 * @return the display2Videos
	 */
	public Boolean isDisplay2Videos()
	{
		return isDisplay2Videos( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @return the display2Videos
	 */
	public boolean isDisplay2VideosAsPrimitive(final SessionContext ctx)
	{
		Boolean value = isDisplay2Videos( ctx );
		return value != null ? value.booleanValue() : false;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @return the display2Videos
	 */
	public boolean isDisplay2VideosAsPrimitive()
	{
		return isDisplay2VideosAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @param value the display2Videos
	 */
	public void setDisplay2Videos(final SessionContext ctx, final Boolean value)
	{
		setProperty(ctx, DISPLAY2VIDEOS,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @param value the display2Videos
	 */
	public void setDisplay2Videos(final Boolean value)
	{
		setDisplay2Videos( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @param value the display2Videos
	 */
	public void setDisplay2Videos(final SessionContext ctx, final boolean value)
	{
		setDisplay2Videos( ctx,Boolean.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.display2Videos</code> attribute. 
	 * @param value the display2Videos
	 */
	public void setDisplay2Videos(final boolean value)
	{
		setDisplay2Videos( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.videos</code> attribute.
	 * @return the videos
	 */
	public List<Media> getVideos(final SessionContext ctx)
	{
		List<Media> coll = (List<Media>)getProperty( ctx, VIDEOS);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMVideoCarouselComponent.videos</code> attribute.
	 * @return the videos
	 */
	public List<Media> getVideos()
	{
		return getVideos( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.videos</code> attribute. 
	 * @param value the videos
	 */
	public void setVideos(final SessionContext ctx, final List<Media> value)
	{
		setProperty(ctx, VIDEOS,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMVideoCarouselComponent.videos</code> attribute. 
	 * @param value the videos
	 */
	public void setVideos(final List<Media> value)
	{
		setVideos( getSession().getSessionContext(), value );
	}
	
}
