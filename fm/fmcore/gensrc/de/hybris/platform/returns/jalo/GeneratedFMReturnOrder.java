/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *  
 */
package de.hybris.platform.returns.jalo;

import com.federalmogul.core.constants.FmCoreConstants;
import com.federalmogul.falcon.core.jalo.FMReturnItems;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.enumeration.EnumerationValue;
import de.hybris.platform.jalo.order.Order;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Generated class for type {@link de.hybris.platform.returns.jalo.FMReturnOrder FMReturnOrder}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMReturnOrder extends Order
{
	/** Qualifier of the <code>FMReturnOrder.confirmationNumber</code> attribute **/
	public static final String CONFIRMATIONNUMBER = "confirmationNumber";
	/** Qualifier of the <code>FMReturnOrder.salesOrderNumber</code> attribute **/
	public static final String SALESORDERNUMBER = "salesOrderNumber";
	/** Qualifier of the <code>FMReturnOrder.returnDescription</code> attribute **/
	public static final String RETURNDESCRIPTION = "returnDescription";
	/** Qualifier of the <code>FMReturnOrder.invoiceNumber</code> attribute **/
	public static final String INVOICENUMBER = "invoiceNumber";
	/** Qualifier of the <code>FMReturnOrder.returnId</code> attribute **/
	public static final String RETURNID = "returnId";
	/** Qualifier of the <code>FMReturnOrder.returnItems</code> attribute **/
	public static final String RETURNITEMS = "returnItems";
	/** Qualifier of the <code>FMReturnOrder.reasonOfReturn</code> attribute **/
	public static final String REASONOFRETURN = "reasonOfReturn";
	/** Qualifier of the <code>FMReturnOrder.returnMessage</code> attribute **/
	public static final String RETURNMESSAGE = "returnMessage";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Order.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(CONFIRMATIONNUMBER, AttributeMode.INITIAL);
		tmp.put(SALESORDERNUMBER, AttributeMode.INITIAL);
		tmp.put(RETURNDESCRIPTION, AttributeMode.INITIAL);
		tmp.put(INVOICENUMBER, AttributeMode.INITIAL);
		tmp.put(RETURNID, AttributeMode.INITIAL);
		tmp.put(RETURNITEMS, AttributeMode.INITIAL);
		tmp.put(REASONOFRETURN, AttributeMode.INITIAL);
		tmp.put(RETURNMESSAGE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.confirmationNumber</code> attribute.
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CONFIRMATIONNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.confirmationNumber</code> attribute.
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber()
	{
		return getConfirmationNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.confirmationNumber</code> attribute. 
	 * @param value the confirmationNumber
	 */
	public void setConfirmationNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CONFIRMATIONNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.confirmationNumber</code> attribute. 
	 * @param value the confirmationNumber
	 */
	public void setConfirmationNumber(final String value)
	{
		setConfirmationNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.invoiceNumber</code> attribute.
	 * @return the invoiceNumber
	 */
	public Integer getInvoiceNumber(final SessionContext ctx)
	{
		return (Integer)getProperty( ctx, INVOICENUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.invoiceNumber</code> attribute.
	 * @return the invoiceNumber
	 */
	public Integer getInvoiceNumber()
	{
		return getInvoiceNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @return the invoiceNumber
	 */
	public int getInvoiceNumberAsPrimitive(final SessionContext ctx)
	{
		Integer value = getInvoiceNumber( ctx );
		return value != null ? value.intValue() : 0;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @return the invoiceNumber
	 */
	public int getInvoiceNumberAsPrimitive()
	{
		return getInvoiceNumberAsPrimitive( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @param value the invoiceNumber
	 */
	public void setInvoiceNumber(final SessionContext ctx, final Integer value)
	{
		setProperty(ctx, INVOICENUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @param value the invoiceNumber
	 */
	public void setInvoiceNumber(final Integer value)
	{
		setInvoiceNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @param value the invoiceNumber
	 */
	public void setInvoiceNumber(final SessionContext ctx, final int value)
	{
		setInvoiceNumber( ctx,Integer.valueOf( value ) );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.invoiceNumber</code> attribute. 
	 * @param value the invoiceNumber
	 */
	public void setInvoiceNumber(final int value)
	{
		setInvoiceNumber( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.reasonOfReturn</code> attribute.
	 * @return the reasonOfReturn
	 */
	public EnumerationValue getReasonOfReturn(final SessionContext ctx)
	{
		return (EnumerationValue)getProperty( ctx, REASONOFRETURN);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.reasonOfReturn</code> attribute.
	 * @return the reasonOfReturn
	 */
	public EnumerationValue getReasonOfReturn()
	{
		return getReasonOfReturn( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.reasonOfReturn</code> attribute. 
	 * @param value the reasonOfReturn
	 */
	public void setReasonOfReturn(final SessionContext ctx, final EnumerationValue value)
	{
		setProperty(ctx, REASONOFRETURN,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.reasonOfReturn</code> attribute. 
	 * @param value the reasonOfReturn
	 */
	public void setReasonOfReturn(final EnumerationValue value)
	{
		setReasonOfReturn( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnDescription</code> attribute.
	 * @return the returnDescription
	 */
	public String getReturnDescription(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RETURNDESCRIPTION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnDescription</code> attribute.
	 * @return the returnDescription
	 */
	public String getReturnDescription()
	{
		return getReturnDescription( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnDescription</code> attribute. 
	 * @param value the returnDescription
	 */
	public void setReturnDescription(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RETURNDESCRIPTION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnDescription</code> attribute. 
	 * @param value the returnDescription
	 */
	public void setReturnDescription(final String value)
	{
		setReturnDescription( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnId</code> attribute.
	 * @return the returnId
	 */
	public String getReturnId(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RETURNID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnId</code> attribute.
	 * @return the returnId
	 */
	public String getReturnId()
	{
		return getReturnId( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnId</code> attribute. 
	 * @param value the returnId
	 */
	public void setReturnId(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RETURNID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnId</code> attribute. 
	 * @param value the returnId
	 */
	public void setReturnId(final String value)
	{
		setReturnId( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnItems</code> attribute.
	 * @return the returnItems
	 */
	public List<FMReturnItems> getReturnItems(final SessionContext ctx)
	{
		List<FMReturnItems> coll = (List<FMReturnItems>)getProperty( ctx, RETURNITEMS);
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnItems</code> attribute.
	 * @return the returnItems
	 */
	public List<FMReturnItems> getReturnItems()
	{
		return getReturnItems( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnItems</code> attribute. 
	 * @param value the returnItems
	 */
	public void setReturnItems(final SessionContext ctx, final List<FMReturnItems> value)
	{
		setProperty(ctx, RETURNITEMS,value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnItems</code> attribute. 
	 * @param value the returnItems
	 */
	public void setReturnItems(final List<FMReturnItems> value)
	{
		setReturnItems( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnMessage</code> attribute.
	 * @return the returnMessage
	 */
	public String getReturnMessage(final SessionContext ctx)
	{
		return (String)getProperty( ctx, RETURNMESSAGE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.returnMessage</code> attribute.
	 * @return the returnMessage
	 */
	public String getReturnMessage()
	{
		return getReturnMessage( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnMessage</code> attribute. 
	 * @param value the returnMessage
	 */
	public void setReturnMessage(final SessionContext ctx, final String value)
	{
		setProperty(ctx, RETURNMESSAGE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.returnMessage</code> attribute. 
	 * @param value the returnMessage
	 */
	public void setReturnMessage(final String value)
	{
		setReturnMessage( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.salesOrderNumber</code> attribute.
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SALESORDERNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMReturnOrder.salesOrderNumber</code> attribute.
	 * @return the salesOrderNumber
	 */
	public String getSalesOrderNumber()
	{
		return getSalesOrderNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.salesOrderNumber</code> attribute. 
	 * @param value the salesOrderNumber
	 */
	public void setSalesOrderNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SALESORDERNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMReturnOrder.salesOrderNumber</code> attribute. 
	 * @param value the salesOrderNumber
	 */
	public void setSalesOrderNumber(final String value)
	{
		setSalesOrderNumber( getSession().getSessionContext(), value );
	}
	
}
