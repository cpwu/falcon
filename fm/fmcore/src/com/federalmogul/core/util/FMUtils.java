package com.federalmogul.core.util;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;

import com.federalmogul.core.constants.FmCoreConstants;

import de.hybris.platform.util.Config;

public class FMUtils {
	
	    private static final Logger LOG = Logger.getLogger(FMUtils.class);
	    // Extract the middle part of a string from "open" to "close"
		public static String extractMiddle(final String str, final String open, final String close)
		{
			final int begin = str.indexOf(open) + open.length();
			final int end = str.indexOf(close, begin);
			return str.substring(begin, end);
		}

		// Extract the middle part of a string from "open" to the end
		public static String extractMiddle(final String str, final String open)
		{
			final int begin = str.indexOf(open) + open.length();
			return str.substring(begin);
		}
		
		public static String readURLContent(final String urlString) throws IOException
		{
			final URL url = new URL(urlString);
			String content = new String();
			try
			{
				url.openConnection();
				final Scanner scan = new Scanner(url.openStream());
				while (scan.hasNext())
				{
					content += scan.nextLine();
				}
				scan.close();
			}
			catch (final Exception e)
			{
				LOG.error("Exception...." + e.getMessage());
			}
			return content;
		}
		
		public static String makeLocation(final String addr) throws IOException
		{
			//move to local.properties
			String url = Config.getParameter(FmCoreConstants.GEOCODE_URL);
			url += URLEncoder.encode(addr, "UTF-8");
			// calling readURL method
			final String content = FMUtils.readURLContent(url);
			//final String address = extractMiddle(content, "\"formatted_address\" : \"", "\",");
			final String latlng = FMUtils.extractMiddle(content, "\"location\" : {", "},");
			final double latitude = Double.parseDouble(FMUtils.extractMiddle(latlng, "\"lat\" :", ","));
			final double longitude = Double.parseDouble(FMUtils.extractMiddle(latlng, "\"lng\" :"));
			final String latlang = latitude + "_" + longitude;
			return latlang;
		}
		
		public static String getLocationByLocationQuery(final String locationQuery) throws IOException
		{
			String url = Config.getParameter(FmCoreConstants.GEOCODE_URL);
			url += URLEncoder.encode(locationQuery, "UTF-8");
			// calling readURL method
			final String content = FMUtils.readURLContent(url);
			final String latlng = FMUtils.extractMiddle(content, "\"location\" : {", "},");
			return latlng;
		}
		
		public static List<String> formShopTypeFilterString(String shopType){
			List<String> shopTypeList = new ArrayList<String>();
			if(FmCoreConstants.JOBBER.equals(shopType)){
				shopTypeList.add(FmCoreConstants.JOBBER);	
				shopTypeList.add(FmCoreConstants.BOTH);
			}else if(FmCoreConstants.INSTALLER.equals(shopType)){
				shopTypeList.add(FmCoreConstants.INSTALLER);	
				shopTypeList.add(FmCoreConstants.BOTH);
		    }else if(FmCoreConstants.ALL.equals(shopType)||FmCoreConstants.BOTH.equals(shopType)){
		    	shopTypeList.add(FmCoreConstants.JOBBER);
		    	shopTypeList.add(FmCoreConstants.INSTALLER);
		    	shopTypeList.add(FmCoreConstants.BOTH);
		    }else{
		    	shopTypeList.add(FmCoreConstants.INVALIDSHOPTYPE);
		    }
			return shopTypeList;
		}	

}
