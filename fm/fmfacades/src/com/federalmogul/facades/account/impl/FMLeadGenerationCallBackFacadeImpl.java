/**
 * 
 */
package com.federalmogul.facades.account.impl;

import de.hybris.platform.commercefacades.customer.impl.DefaultCustomerFacade;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.soap.SOAPException;

import org.apache.log4j.Logger;

import com.federalmogul.core.model.FMLeadGenerationCallBackModel;
import com.federalmogul.facades.account.FMLeadGenerationCallBackFacade;
import com.federalmogul.facades.customer.data.FMLeadGenerationCallBackData;
import com.fm.falcon.webservices.dto.LeadGenerationCallBackRequestDTO;
import com.fm.falcon.webservices.soap.helper.LeadGenerationCallBackHelper;


/**
 * @author Balaji
 * 
 */
public class FMLeadGenerationCallBackFacadeImpl extends DefaultCustomerFacade implements FMLeadGenerationCallBackFacade
{

	/*
	 * @Resource private UserService userService;
	 * 
	 * @Resource(name = "customerData") CustomerData customerData = new CustomerData();
	 * 
	 * @Autowired private UserService userservice;
	 */

	private static final Logger LOG = Logger.getLogger(FMLeadGenerationCallBackFacadeImpl.class);
	final static HashMap QUERYTYPES = new HashMap();

	@Override
	public void createCallBack(final FMLeadGenerationCallBackData fMLeadGenerationCallBackData)
			throws UnsupportedOperationException, ClassNotFoundException, SOAPException, IOException
	{
		//validateParameterNotNullStandardMessage("fmReturnOrderData", fmReturnOrderData);
		//Assert.hasText(fmReturnOrderData.getFirstName(), "The field [FirstName] cannot be empty");
		//Assert.hasText(fmReturnOrderData.getLastName(), "The field [LastName] cannot be empty");

		createCrmCallBack(fMLeadGenerationCallBackData);

	}


	/**
	 * User defined method for saving the response Return ID in model which returns the FMReturnItemsModel
	 * 
	 * @return FMCustomerModel
	 * @throws IOException
	 * @throws SOAPException
	 * @throws ClassNotFoundException
	 * @throws UnsupportedOperationException
	 */

	@Override
	public FMLeadGenerationCallBackModel createCrmCallBack(final FMLeadGenerationCallBackData fMLeadGenerationCallBackData)
			throws UnsupportedOperationException, ClassNotFoundException, SOAPException, IOException
	{
		LOG.info("Emntered createCrmReturnOrder In FacadeImpl");

		final FMLeadGenerationCallBackModel fMLeadGenerationCallBackModel = getModelService().create(
				FMLeadGenerationCallBackModel.class);
		final LeadGenerationCallBackHelper leadGenerationCallBackHelper = new LeadGenerationCallBackHelper();


		//validateParameterNotNullStandardMessage("fmReturnItemsData", fmReturnItemsData);
		//Assert.hasText(fmReturnItemsData.getSapAccountCodeData(), "The field [SapAccountCode] cannot be empty");
		//Assert.hasText(fmReturnItemsData.getPurchaseOrderNumber(), "The field [PurchaseOrderNumber] cannot be empty");
		//Assert.hasText(fmReturnItemsData.getAccountNumberData(), "The field [AccountNumber] cannot be empty");
		//Assert.hasText(fmReturnItemsData.getOriginalOrderNumber(), "The field [OriginalOrderNumber] cannot be empty");
		//Assert.hasText(fmReturnItemsData.getReturnReason(), "The field [ReturnReason] cannot be empty");

		leadGenerationCallBackHelper.sendSOAPMessage(convertDataToDTO(fMLeadGenerationCallBackData));

		//	if (!leadGenerationCallBackResponseDTO.getResponseCode().equals("000"))
		//	{
		//		throw new IOException(leadGenerationCallBackResponseDTO.getResponseCode());
		//	}

		//fMLeadGenerationCallBackModel.set

		//fmReturnItemsModel.setReturnID(regResponse.getReturnId());
		//fmReturnItemsModel.setReturnStatus(regResponse.getReturnStatus());

		return fMLeadGenerationCallBackModel;
	}




	static
	{
		//QUERYTYPES.put(LeadGenerationSubjects.PRODUCTENQUIRY, "ZLAD");
		//QUERYTYPES.put(LeadGenerationSubjects.CALLBACK, "ZACT");
		//QUERYTYPES.put(LeadGenerationSubjects.GENERALENQUIRY, "ZACT");
		//QUERYTYPES.put(LeadGenerationSubjects.ORDERENQUIRY, "ZACT");

		//QUERYTYPES.put(LeadGenerationSubjects.PROMOTIONOFFERENQUIRY, "ZLAD");
		QUERYTYPES.put("Sales Inquiry", "ZLAD");
		QUERYTYPES.put("Order Inquiry", "ZACT");
		QUERYTYPES.put("Product Inquiry", "ZLAD");
		QUERYTYPES.put("Promotional Inquiry", "ZLAD");
		QUERYTYPES.put("Other", "ZACT");
		//QUERYTYPES.put("Callback", "ZACT");
		//QUERYTYPES.put("General Inquiry", "ZACT");

		//QUERYTYPES.put(LeadGenerationSubjects.DCTCLOCATIONENQUERY, "ZLAD");


	}

	private LeadGenerationCallBackRequestDTO convertDataToDTO(final FMLeadGenerationCallBackData fMLeadGenerationCallBackData)
	{

		//LOG.info("inside convertDataToDTO Methd to set the Values in Helper class ");

		final LeadGenerationCallBackRequestDTO reqDTO = new LeadGenerationCallBackRequestDTO();

		reqDTO.setServiceName("MT_ECOMMERCECALLBACK");

		LOG.info("subject::" + fMLeadGenerationCallBackData.getLeadSubjects());

		//reqDTO.setQueryType(fMLeadGenerationCallBackData.getSubjects().toString());
		//reqDTO.setQueryType("" + QUERYTYPES.get(fMLeadGenerationCallBackData.getSubjects()));
		reqDTO.setQueryType("" + QUERYTYPES.get(fMLeadGenerationCallBackData.getLeadSubjects()));
		LOG.info("QueryType::" + reqDTO.getQueryType());
		reqDTO.setCompany(fMLeadGenerationCallBackData.getDefaultShippingAddress().getCompanyName());
		LOG.info("COMPANY::" + reqDTO.getCompany());
		reqDTO.setDepartment(fMLeadGenerationCallBackData.getDepartment());
		LOG.info("Department::" + reqDTO.getDepartment());
		reqDTO.setFreeText(fMLeadGenerationCallBackData.getCallBackDescription());
		LOG.info("FreeText::" + reqDTO.getFreeText());
		reqDTO.setJobTitle(fMLeadGenerationCallBackData.getJobtitle());
		LOG.info("JobTitle::" + reqDTO.getJobTitle());
		reqDTO.setFirstName(fMLeadGenerationCallBackData.getFirstName());
		LOG.info("FirstName::" + reqDTO.getFirstName());

		reqDTO.setLastName(fMLeadGenerationCallBackData.getLastName());
		LOG.info("LastName::" + reqDTO.getLastName());

		reqDTO.setEmail(fMLeadGenerationCallBackData.getEmail());
		LOG.info("Email::" + reqDTO.getEmail());


		reqDTO.setCountry(fMLeadGenerationCallBackData.getDefaultShippingAddress().getCountry().getIsocode());
		LOG.info("Country::" + reqDTO.getCountry());

		reqDTO.setStreetAddress1(fMLeadGenerationCallBackData.getDefaultShippingAddress().getLine1());
		LOG.info("StreetAddress1::" + reqDTO.getStreetAddress1());
		reqDTO.setStreetAddress2(fMLeadGenerationCallBackData.getDefaultShippingAddress().getLine2());
		LOG.info("StreetAddress2::" + reqDTO.getStreetAddress2());

		reqDTO.setCity(fMLeadGenerationCallBackData.getDefaultShippingAddress().getTown());
		LOG.info("City::" + reqDTO.getCity());

		final String stateSize = (fMLeadGenerationCallBackData.getDefaultShippingAddress().getRegion().getIsocode());

		if (!stateSize.isEmpty())
		{

			reqDTO.setStateCode(stateSize.substring(3, stateSize.length()));
			LOG.info("StateCode::" + reqDTO.getStateCode());
		}

		reqDTO.setZipcode(fMLeadGenerationCallBackData.getDefaultShippingAddress().getPostalCode());
		LOG.info("Zipcode::" + reqDTO.getZipcode());

		reqDTO.setTelephone(fMLeadGenerationCallBackData.getDefaultShippingAddress().getPhone());
		LOG.info("Telephone::" + reqDTO.getTelephone());

		//reqDTO.setSapaccountcode(fMLeadGenerationCallBackData.getUnit().toString());
		//LOG.info("Sapaccountcode::" + reqDTO.getSapaccountcode());


		return reqDTO;
	}


}
