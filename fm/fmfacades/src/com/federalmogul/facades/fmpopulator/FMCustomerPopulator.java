/**
 * 
 */
package com.federalmogul.facades.fmpopulator;

import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.TitleModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.federalmogul.core.model.FMCustomerAccountModel;
import com.federalmogul.core.model.FMCustomerModel;
import com.federalmogul.core.model.FMTaxDocumentModel;
import com.federalmogul.facades.address.data.FMB2bAddressData;
import com.federalmogul.facades.user.data.FMCustomerAccountData;
import com.federalmogul.facades.user.data.FMCustomerData;
import com.federalmogul.facades.user.data.FMTaxDocumentData;


/**
 * @author SA297584
 * 
 */
public class FMCustomerPopulator implements Populator<FMCustomerModel, FMCustomerData>
{
	//private Converter<AddressModel, FMB2bAddressData> fmB2BaddressConverter;

	private Populator<AddressModel, FMB2bAddressData> fmb2bAddressPopulator;
	private Populator<AddressModel, AddressData> addressPopulator;

	private Converter<FMCustomerModel, FMCustomerData> fmCustomerConverter;
	private Converter<UserModel, FMCustomerData> fmCurrentCustomerConverter;
	private static final Logger LOG = Logger.getLogger(FMCustomerPopulator.class);

	/**
	 * @return the fmCurrentCustomerConverter
	 */
	public Converter<UserModel, FMCustomerData> getFmCurrentCustomerConverter()
	{
		return fmCurrentCustomerConverter;
	}

	/**
	 * @param fmCurrentCustomerConverter
	 *           the fmCurrentCustomerConverter to set
	 */
	public void setFmCurrentCustomerConverter(final Converter<UserModel, FMCustomerData> fmCurrentCustomerConverter)
	{
		this.fmCurrentCustomerConverter = fmCurrentCustomerConverter;
	}

	/**
	 * @return the fmCustomerConverter
	 */
	public Converter<FMCustomerModel, FMCustomerData> getFmCustomerConverter()
	{
		return fmCustomerConverter;
	}

	/**
	 * @param fmCustomerConverter
	 *           the fmCustomerConverter to set
	 */
	@Required
	public void setFmCustomerConverter(final Converter<FMCustomerModel, FMCustomerData> fmCustomerConverter)
	{
		this.fmCustomerConverter = fmCustomerConverter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hybris.platform.converters.Populator#populate(java.lang.Object, java.lang.Object)
	 */
	@Override
	public void populate(final FMCustomerModel source, final FMCustomerData target) throws ConversionException
	{

		LOG.info("HELOOOO : INSIDE POPULATOR");


		Assert.notNull(source, "Parameter source cannot be null.");
		Assert.notNull(target, "Parameter target cannot be null.");


		final TitleModel title = source.getTitle();
		if (title != null)
		{
			target.setTitleCode(title.getCode());
		}



		//	target.setTitleCode(source.getTitle().getCode());
		target.setUid(source.getUid());
		target.setEmail(source.getEmail());
		target.setLastName(source.getLastName());
		target.setName(source.getName());
		target.setFirstName(source.getName().substring(0, (source.getName().length() - source.getLastName().length()) - 1));
		if (source.getNewsLetterSubscription() != null)
		{
			target.setNewsLetterSubscription(source.getNewsLetterSubscription());
		}
		if (source.getEncodedPassword() != null)
		{
			target.setPassword(source.getEncodedPassword());
		}

		target.setActive(source.getActive());
		target.setIsLoginDisabled(Boolean.valueOf(source.isLoginDisabled()));
		target.setWorkContactNo(source.getWorkContactNo());

		if (target.getDefaultShippingAddress() == null)
		{
			LOG.info("target defaults shipping addresss is nulll*******");
			final AddressData addressData = new AddressData();
			target.setDefaultShippingAddress(addressData);

		}

		if (source.getDefaultShipmentAddress() != null)
		{
			if (source.getDefaultShipmentAddress().getPhone1() != null)
			{
				LOG.info("**************address" + source.getDefaultShipmentAddress());
				LOG.info("********phone" + source.getDefaultShipmentAddress().getPhone1());
				LOG.info("shiping addresss for target*******" + target.getDefaultShippingAddress());
				target.getDefaultShippingAddress().setPhone(source.getDefaultShipmentAddress().getPhone1());

			}
			final AddressData addressData = new AddressData();
			getAddressPopulator().populate(source.getDefaultShipmentAddress(), addressData);
			LOG.info("Default shipping address in populator :: " + addressData.getCompanyName() + addressData.getLine1()
					+ addressData.getLine2());
			target.setDefaultShippingAddress(addressData);
		}

		final List<String> roleString = new ArrayList<String>();

		final Set<PrincipalGroupModel> userGroups = source.getGroups();

		for (final PrincipalGroupModel userGroupsIterator : userGroups)
		{
			roleString.add(userGroupsIterator.getUid());
		}

		target.setRoles(roleString);




		//to set channel code
		target.setUserTypeDescription(source.getChannelCode());

		final FMCustomerAccountModel sourceunit = (FMCustomerAccountModel) source.getDefaultB2BUnit();
		final FMCustomerAccountData targetunit = new FMCustomerAccountData();
		final List<FMB2bAddressData> unitAddressData = new ArrayList<FMB2bAddressData>();

		//changed here
		final Set<PrincipalGroupModel> groupss = source.getGroups();
		final Iterator groupsiterator = groupss.iterator();
		while (groupsiterator.hasNext())
		{
			final PrincipalGroupModel groupmodel = (PrincipalGroupModel) groupsiterator.next();

			/*
			 * if (source.getGroups().contains("FMJobberGroup") || source.getGroups().contains("FMInstallerGroup")) {
			 */
			if ("FMJobberGroup".equals(groupmodel.getUid()) && !(sourceunit == null)
					|| "FMInstallerGroup".equals(groupmodel.getUid()) && !(sourceunit == null))
			{

				targetunit.setTaxID(sourceunit.getTaxID());

				LOG.info("TAX ID FROM MODEL" + targetunit.getTaxID());
				final List<FMTaxDocumentData> taxdocsdata = new ArrayList<FMTaxDocumentData>();
				final List<FMTaxDocumentModel> newtaxdocsmodel = sourceunit.getFmtaxDocument();
				final Iterator taxdocsmodeliterator = newtaxdocsmodel.iterator();
				while (taxdocsmodeliterator.hasNext())
				{
					final FMTaxDocumentModel newtaxdocmodel = (FMTaxDocumentModel) taxdocsmodeliterator.next();


					final FMTaxDocumentData fmtaxdoc = new FMTaxDocumentData();
					//fmtaxdoc.setTaxdoc(newtaxdocmodel.getDoc());
					fmtaxdoc.setDocname(newtaxdocmodel.getDocname());
					LOG.info("FILE NAME IN POULATOR" + fmtaxdoc.getDocname());

					if (newtaxdocmodel.getValidate() != null)
					{

						fmtaxdoc.setValidate(newtaxdocmodel.getValidate().toString());
					}

					final RegionData statedata = new RegionData();
					if (newtaxdocmodel.getState().getIsocode() != null)
					{
						statedata.setIsocode(newtaxdocmodel.getState().getIsocode());

						fmtaxdoc.setState(statedata);
						//	LOG.info("state inside populator" + fmtaxdoc.getState().getName() + fmtaxdoc.getState().getIsocode());
					}
					//need to change here --siva instead of text add the enum value
					/*
					 * if (newtaxdocmodel.getValidate() != null) { fmtaxdoc.setValidate(newtaxdocmodel.getValidate()); } else
					 * { fmtaxdoc.setValidate("Not Reviewed"); }
					 */

					taxdocsdata.add(fmtaxdoc);
				}
				targetunit.setTaxDocumentList(taxdocsdata);

				targetunit.setProspectId(sourceunit.getProspectuid());
				//targetunit.setUid(sourceunit.getUid());
				//targetunit.setName(sourceunit.getLocName());
				//target.setFmunit(targetunit);

				//target.setUnit(targetunit);


			}


		}
		if (sourceunit != null)
		{
			final List<AddressModel> unitaddresses = (List<AddressModel>) sourceunit.getAddresses();
			LOG.info("SIZE FROM MODELLLLLLLLLLLLLLLLLLLLLLL" + sourceunit.getAddresses().size());
			LOG.info("UID OF UNIT" + sourceunit.getUid());

			final Iterator unitaddressesIterator = unitaddresses.iterator();
			while (unitaddressesIterator.hasNext())
			{
				final FMB2bAddressData fmB2bAddressData = new FMB2bAddressData();
				final AddressModel unitAddress = (AddressModel) unitaddressesIterator.next();
				LOG.info("ADDRESSLINE" + unitAddress.getLine1());
				getFmb2bAddressPopulator().populate(unitAddress, fmB2bAddressData);
				fmB2bAddressData.setCompanyName(sourceunit.getLocName());
				unitAddressData.add(fmB2bAddressData);
			}


			targetunit.setName(sourceunit.getLocName());
			targetunit.setUnitAddress(unitAddressData);
			targetunit.setUid(sourceunit.getUid());

			targetunit.setNabsAccountCode(sourceunit.getNabsAccountCode());

			target.setFmunit(targetunit);

			target.setUnit(targetunit);
		}
		target.setIsGarageRewardMember(source.getIsGarageRewardMember());
		target.setIsLoginDisabled(source.isLoginDisabled());
		target.setLoyaltySignup(source.getLoyaltySubscription());
		target.setLmsSigninId(source.getLmsSigninID());
		target.setCrmContactId(source.getCrmCustomerID());
		target.setB2cLoyaltyMembershipId(source.getB2cLoyaltyMembershipId());
		target.setSurveyStatus(source.getSurveyStatus());
	}

	/**
	 * @return the fmb2bAddressPopulator
	 */
	public Populator<AddressModel, FMB2bAddressData> getFmb2bAddressPopulator()
	{
		return fmb2bAddressPopulator;
	}

	/**
	 * @param fmb2bAddressPopulator
	 *           the fmb2bAddressPopulator to set
	 */
	public void setFmb2bAddressPopulator(final Populator<AddressModel, FMB2bAddressData> fmb2bAddressPopulator)
	{
		this.fmb2bAddressPopulator = fmb2bAddressPopulator;
	}

	/**
	 * @return the addressPopulator
	 */
	public Populator<AddressModel, AddressData> getAddressPopulator()
	{
		return addressPopulator;
	}

	/**
	 * @param addressPopulator
	 *           the addressPopulator to set
	 */
	public void setAddressPopulator(final Populator<AddressModel, AddressData> addressPopulator)
	{
		this.addressPopulator = addressPopulator;
	}
}