/**
 * 
 */
package com.federalmogul.facades.order.converters.populator;

import de.hybris.platform.commercefacades.order.converters.populator.CartPopulator;
import de.hybris.platform.commercefacades.order.data.AbstractOrderData;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;


/**
 * @author mamud

 * 
 */
/**
 * Converter implementation for {@link de.hybris.platform.core.model.order.CartModel} as source and
 * {@link de.hybris.platform.commercefacades.order.data.CartData} as target type.
 */
public class FMCartPopulator<T extends CartData> extends CartPopulator<T>
{
	@Override
	public void populate(final CartModel source, final T target)
	{
		super.populate(source, target);
		populateSAPCustDetails(source, target);

	}

	void populateSAPCustDetails(final AbstractOrderModel source, final AbstractOrderData target)
	{
		target.setFmordertype(source.getFmordertype());
		target.setCustponumber(source.getCustponumber());
		target.setSapordernumber(source.getSapordernumber());
		target.setOrdercomments(source.getOrdercomments());
		target.setPocustid(source.getPocustid());
	}

}
