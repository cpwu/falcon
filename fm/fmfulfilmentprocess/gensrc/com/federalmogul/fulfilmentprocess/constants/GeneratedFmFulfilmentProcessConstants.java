/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.fulfilmentprocess.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmFulfilmentProcessConstants
{
	public static final String EXTENSIONNAME = "fmfulfilmentprocess";
	public static class Attributes
	{
		public static class ConsignmentProcess
		{
			public static final String DONE = "done".intern();
			public static final String WAITINGFORCONSIGNMENT = "waitingForConsignment".intern();
			public static final String WAREHOUSECONSIGNMENTSTATE = "warehouseConsignmentState".intern();
		}
	}
	
	protected GeneratedFmFulfilmentProcessConstants()
	{
		// private constructor
	}
	
	
}
