/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.federalmogul.initialdata.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmInitialDataConstants
{
	public static final String EXTENSIONNAME = "fminitialdata";
	
	protected GeneratedFmInitialDataConstants()
	{
		// private constructor
	}
	
	
}
