/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fm.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmintegrationConstants
{
	public static final String EXTENSIONNAME = "fmintegration";
	
	protected GeneratedFmintegrationConstants()
	{
		// private constructor
	}
	
	
}
