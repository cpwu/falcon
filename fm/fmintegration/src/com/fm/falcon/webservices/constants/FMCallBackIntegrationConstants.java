/**
 * 
 */
package com.fm.falcon.webservices.constants;

/**
 * @author Balaji
 *
 */


/**
 * Interface for integration request/response XML element names to be referred as constants
 * 
 */

public interface FMCallBackIntegrationConstants
{
	String URN = "ecom";
	String NAME_SPACE = "urn://Falcon//Interface//eCommerceCallBack";

	String QUERYTYPE = "queryType";
	String SAPACCOUNTCODE = "sapaccountcode";
	String JOBTITLE = "JobTitle";
	String DEPARTMENT = "Department";
	String COMPANY = "Company";

	String FIRSTNAME = "FirstName";
	String LASTNAME = "LastName";
	String COUNTRY = "Country";
	String EMAIL = "Email";

	String TELEPHONE = "Telephone";
	String STREETADDRESS1 = "StreetAddress1";
	String STREETADDRESS2 = "StreetAddress2";
	String CITY = "City";

	String ZIPCODE = "Zipcode";
	String STATECODE = "StateCode";
	String FREETEXT = "freeText";

}
