/**
 * 
 */
package com.fm.falcon.webservices.soap.helper;

import de.hybris.platform.util.Config;

import java.io.IOException;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.apache.log4j.Logger;

import com.fm.falcon.webservices.constants.FMCallBackIntegrationConstants;
import com.fm.falcon.webservices.dto.CRMRequestDTO;
import com.fm.falcon.webservices.dto.LeadGenerationCallBackRequestDTO;
import com.fm.falcon.webservices.dto.LeadGenerationCallBackResponseDTO;
import com.fm.falcon.webservices.soap.AbstractSoapMessage;
import com.fm.falcon.webservices.soap.common.SoapMessageCreator;


/**
 * @author Balaji Class implements sendSOAPMessage method of {@link AbstractSoapMessage} to send return request.
 * 
 */
public class LeadGenerationCallBackHelper extends SoapMessageCreator
{
	/**
	 * Logger class to log messages
	 */
	private static final Logger LOG = Logger.getLogger(LeadGenerationCallBackHelper.class);

	/**
	 * CallBack request realization method for posting the message to external system and gets the parsed response object
	 * 
	 * @param crmRequestDTO
	 *           Request transfer object
	 * @return CRM response transfer object
	 * @throws SOAPException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@Override
	public LeadGenerationCallBackResponseDTO sendSOAPMessage(final CRMRequestDTO crmRequestDTO)
			throws UnsupportedOperationException, SOAPException, IOException, ClassNotFoundException
	{

		LOG.info("Inside sendSOAPMessage");

		final SOAPMessage callBackMsg = createSoapMessage();
		this.authenticategCallBackRequest();
		final SOAPBodyElement callbackrequest = createSoapBodyForReturnCallBack(callBackMsg, crmRequestDTO.getServiceName());

		createChildElements(callbackrequest, (LeadGenerationCallBackRequestDTO) crmRequestDTO);
		callBackMsg.saveChanges();

		LOG.info(" callBack WSDL URL:::::" + Config.getParameter("leadGenerationEndPointURL"));

		final SOAPBody response = sendSoapRequest(callBackMsg, Config.getParameter("leadGenerationEndPointURL"));
		//sendSoapRequestWithoutResponse(callBackMsg, Config.getParameter("leadGenerationEndPointURL"));

		LOG.info("\n  in sendSOAPMessage After sending request to PI:" + response);

		//	final LeadGenerationCallBackResponseDTO leadResponse = new LeadGenerationCallBackResponseDTO();
		//	leadResponse.setText("Your request has been submitted Successfully..");

		//	leadResponse.setResponseCode("000");

		//	LOG.info("Inside response::::::::::::" + leadResponse.getText());

		//	return leadResponse;

		return parseResponse(response);
	}

	/**
	 * Method parses the response and retrieves the response element values.
	 * 
	 * @param response
	 *           Response SOAPBody
	 * @return UserRegistrationResponseDTO
	 */

	@SuppressWarnings("unused")
	private LeadGenerationCallBackResponseDTO parseResponse(final SOAPBody response)
	{

		LOG.info("Inside parseResponse::" + response);

		final LeadGenerationCallBackResponseDTO responseDTO = new LeadGenerationCallBackResponseDTO();

		return responseDTO;
	}


	//	private void callBackTestData(final SOAPBodyElement bodyElement) throws SOAPException
	//	{
	//
	//		LOG.info(" Start Call Back Test data ==>");
	//
	//		final SOAPElement leadTask = bodyElement.addChildElement(createQname("LEAD_TASK"));
	//
	//		LOG.info(" leadTask " + leadTask);
	//
	//		final SOAPElement queryType = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.QUERYTYPE)).addTextNode(
	//				"ZACT");
	//		LOG.info(" queryType " + queryType);
	//		final SOAPElement sapaccountcode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.SAPACCOUNTCODE))
	//				.addTextNode("1234567891");
	//		LOG.info(" sapaccountcode " + sapaccountcode);
	//		final SOAPElement jobTitle = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.JOBTITLE)).addTextNode(
	//				"Manager");
	//		LOG.info(" JobTitle " + jobTitle);
	//		final SOAPElement department = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.DEPARTMENT))
	//				.addTextNode("Purchasing");
	//		LOG.info(" Department " + department);
	//		final SOAPElement company = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.COMPANY)).addTextNode(
	//				"Global Mfg Company ");
	//		LOG.info(" Company " + company);
	//
	//		final SOAPElement firstName = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.FIRSTNAME)).addTextNode(
	//				"Smith");
	//		LOG.info(" FirstName " + firstName);
	//		final SOAPElement lastName = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.LASTNAME)).addTextNode(
	//				"John");
	//		LOG.info(" LastName " + lastName);
	//		final SOAPElement country = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.COUNTRY))
	//				.addTextNode("US ");
	//		LOG.info(" Country " + country);
	//
	//		final SOAPElement email = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.EMAIL)).addTextNode(
	//				"leadtest@yahoo.com");
	//		LOG.info(" Email " + email);
	//		final SOAPElement telephone = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.TELEPHONE)).addTextNode(
	//				"2345677654");
	//		LOG.info(" Telephone " + telephone);
	//		final SOAPElement streetAddress1 = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STREETADDRESS1))
	//				.addTextNode("Sarjapur road");
	//		LOG.info(" StreetAddress1 " + streetAddress1);
	//		final SOAPElement streetAddress2 = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STREETADDRESS2))
	//				.addTextNode("Kaikodrahali ");
	//		LOG.info(" StreetAddress2 " + streetAddress2);
	//
	//		final SOAPElement city = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.CITY)).addTextNode("Michigan");
	//		LOG.info(" City " + city);
	//		final SOAPElement zipcode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.ZIPCODE)).addTextNode(
	//				"41138");
	//		LOG.info(" Zipcode " + zipcode);
	//		final SOAPElement stateCode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STATECODE)).addTextNode(
	//				"CA");
	//		LOG.info(" StateCode " + stateCode);
	//		final SOAPElement freeText = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.FREETEXT)).addTextNode(
	//				"I would like to know info on FM products ");
	//		LOG.info(" freeText " + freeText);
	//
	//		LOG.info(" End Call Back Test data ==>");
	//
	//
	//	}

	private void callBackData(final SOAPBodyElement bodyElement,
			final LeadGenerationCallBackRequestDTO leadGenerationCallBackRequestDTO) throws SOAPException
	{

		LOG.info(" Start Call Back data ==>");

		final SOAPElement leadTask = bodyElement.addChildElement(createQname("LEAD_TASK"));

		LOG.info(" leadTask " + leadTask);

		final SOAPElement queryType = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.QUERYTYPE)).addTextNode(
				leadGenerationCallBackRequestDTO.getQueryType());
		LOG.info(" queryType " + queryType);
		//final SOAPElement sapaccountcode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.SAPACCOUNTCODE))
		//	.addTextNode(leadGenerationCallBackRequestDTO.getSapaccountcode());
		//LOG.info(" sapaccountcode " + sapaccountcode);

		if (leadGenerationCallBackRequestDTO.getJobTitle() != null)
		{
			final SOAPElement jobTitle = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.JOBTITLE)).addTextNode(
					leadGenerationCallBackRequestDTO.getJobTitle());
			LOG.info(" JobTitle " + jobTitle);
		}
		/*
		 * final SOAPElement department = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.DEPARTMENT))
		 * .addTextNode(leadGenerationCallBackRequestDTO.getDepartment()); LOG.info(" Department " + department);
		 */
		if (leadGenerationCallBackRequestDTO.getCompany() != null)
		{
			final SOAPElement company = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.COMPANY)).addTextNode(
					leadGenerationCallBackRequestDTO.getCompany());
			LOG.info(" Company " + company);
		}

		final SOAPElement firstName = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.FIRSTNAME)).addTextNode(
				leadGenerationCallBackRequestDTO.getFirstName());
		LOG.info(" FirstName " + firstName);
		final SOAPElement lastName = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.LASTNAME)).addTextNode(
				leadGenerationCallBackRequestDTO.getLastName());
		LOG.info(" LastName " + lastName);
		if (leadGenerationCallBackRequestDTO.getCountry() != null)
		{
			final SOAPElement country = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.COUNTRY)).addTextNode(
					leadGenerationCallBackRequestDTO.getCountry());
			LOG.info(" Country " + country);
		}

		final SOAPElement email = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.EMAIL)).addTextNode(
				leadGenerationCallBackRequestDTO.getEmail());
		LOG.info(" Email " + email);
		final SOAPElement telephone = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.TELEPHONE)).addTextNode(
				leadGenerationCallBackRequestDTO.getTelephone());
		LOG.info(" Telephone " + telephone);
		if (leadGenerationCallBackRequestDTO.getStreetAddress1() != null)
		{
			final SOAPElement streetAddress1 = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STREETADDRESS1))
					.addTextNode(leadGenerationCallBackRequestDTO.getStreetAddress1());
			LOG.info(" StreetAddress1 " + streetAddress1);
		}
		if (leadGenerationCallBackRequestDTO.getStreetAddress2() != null)
		{
			final SOAPElement streetAddress2 = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STREETADDRESS2))
					.addTextNode(leadGenerationCallBackRequestDTO.getStreetAddress2());
			LOG.info(" StreetAddress2 " + streetAddress2);
		}

		if (leadGenerationCallBackRequestDTO.getCity() != null)
		{
			final SOAPElement city = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.CITY)).addTextNode(
					leadGenerationCallBackRequestDTO.getCity());
			LOG.info(" City " + city);
		}
		if (leadGenerationCallBackRequestDTO.getZipcode() != null)
		{
			final SOAPElement zipcode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.ZIPCODE)).addTextNode(
					leadGenerationCallBackRequestDTO.getZipcode());
			LOG.info(" Zipcode " + zipcode);
		}

		if (leadGenerationCallBackRequestDTO.getStateCode() != null)
		{
			final SOAPElement stateCode = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.STATECODE))
					.addTextNode(leadGenerationCallBackRequestDTO.getStateCode());
			LOG.info(" StateCode " + stateCode);
		}

		final SOAPElement freeText = leadTask.addChildElement(createQname(FMCallBackIntegrationConstants.FREETEXT)).addTextNode(
				leadGenerationCallBackRequestDTO.getFreeText());
		LOG.info(" freeText " + freeText);


		LOG.info(" End Call Back data ==>");


	}


	private void createChildElements(final SOAPBodyElement bodyElement,
			final LeadGenerationCallBackRequestDTO leadGenerationCallBackRequestDTO) throws SOAPException
	{

		LOG.info(" createChildElements ");

		//QueryType for lead in SAP CRM :- ZLAD
		//QueryType for activity in SAP CRM:-  ZACT

		//callBackTestData(bodyElement);

		callBackData(bodyElement, leadGenerationCallBackRequestDTO);


	}

	public static void main(final String[] args)
	{
		LOG.info("inside main method");

		//		final LeadGenerationCallBackRequestDTO dto = new LeadGenerationCallBackRequestDTO();
		//		dto.setServiceName("Request");
		//
		//		dto.setSapAccountCode("sapAcc001");
		//		dto.setPurchaseOrderNumber("PO420");
		//		dto.setOriginalOrderNumber("OAN9211");
		//		dto.setAccountNumber("AccNo555");

		//		final LeadGenerationCallBackHelper helperclas = new LeadGenerationCallBackHelper();
		//		try
		//		{
		//			helperclas.sendSOAPMessage(dto);
		//		}
		//		catch (final Exception e)
		//		{
		//			LOG.error(e.getMessage());
		//		}
	}


}
