/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedFmsapintegrationConstants
{
	public static final String EXTENSIONNAME = "fmsapintegration";
	public static class TC
	{
		public static final String FMIPOTPACCOUNT = "FMIPOTPACCOUNT".intern();
		public static final String FMIPOTRACKING = "FMIPOTRACKING".intern();
		public static final String FMIPOTRADINGPARTNER = "FMIPOTRADINGPARTNER".intern();
		public static final String FMTPCARRIERSHIPMETHODS = "FMtpcarriershipmethods".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedFmsapintegrationConstants()
	{
		// private constructor
	}
	
	
}
