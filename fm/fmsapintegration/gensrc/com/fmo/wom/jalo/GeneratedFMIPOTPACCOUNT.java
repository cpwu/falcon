/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.fmo.wom.constants.FmsapintegrationConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.fmo.wom.jalo.FMIPOTPACCOUNT FMIPOTPACCOUNT}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMIPOTPACCOUNT extends GenericItem
{
	/** Qualifier of the <code>FMIPOTPACCOUNT.UPDATE_TIMESTAMP</code> attribute **/
	public static final String UPDATE_TIMESTAMP = "UPDATE_TIMESTAMP";
	/** Qualifier of the <code>FMIPOTPACCOUNT.CREATE_USERID</code> attribute **/
	public static final String CREATE_USERID = "CREATE_USERID";
	/** Qualifier of the <code>FMIPOTPACCOUNT.TP_ID</code> attribute **/
	public static final String TP_ID = "TP_ID";
	/** Qualifier of the <code>FMIPOTPACCOUNT.FM_BILLTO_ACCT_CD</code> attribute **/
	public static final String FM_BILLTO_ACCT_CD = "FM_BILLTO_ACCT_CD";
	/** Qualifier of the <code>FMIPOTPACCOUNT.UPDATE_USERID</code> attribute **/
	public static final String UPDATE_USERID = "UPDATE_USERID";
	/** Qualifier of the <code>FMIPOTPACCOUNT.CREATE_TIMESTAMP</code> attribute **/
	public static final String CREATE_TIMESTAMP = "CREATE_TIMESTAMP";
	/** Qualifier of the <code>FMIPOTPACCOUNT.INACTIVE_FROM_DATE</code> attribute **/
	public static final String INACTIVE_FROM_DATE = "INACTIVE_FROM_DATE";
	/** Qualifier of the <code>FMIPOTPACCOUNT.ACTIVE_FLG</code> attribute **/
	public static final String ACTIVE_FLG = "ACTIVE_FLG";
	/** Qualifier of the <code>FMIPOTPACCOUNT.TP_ACCT_ID</code> attribute **/
	public static final String TP_ACCT_ID = "TP_ACCT_ID";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(UPDATE_TIMESTAMP, AttributeMode.INITIAL);
		tmp.put(CREATE_USERID, AttributeMode.INITIAL);
		tmp.put(TP_ID, AttributeMode.INITIAL);
		tmp.put(FM_BILLTO_ACCT_CD, AttributeMode.INITIAL);
		tmp.put(UPDATE_USERID, AttributeMode.INITIAL);
		tmp.put(CREATE_TIMESTAMP, AttributeMode.INITIAL);
		tmp.put(INACTIVE_FROM_DATE, AttributeMode.INITIAL);
		tmp.put(ACTIVE_FLG, AttributeMode.INITIAL);
		tmp.put(TP_ACCT_ID, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.ACTIVE_FLG</code> attribute.
	 * @return the ACTIVE_FLG - ACTIV_FLG
	 */
	public String getACTIVE_FLG(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ACTIVE_FLG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.ACTIVE_FLG</code> attribute.
	 * @return the ACTIVE_FLG - ACTIV_FLG
	 */
	public String getACTIVE_FLG()
	{
		return getACTIVE_FLG( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.ACTIVE_FLG</code> attribute. 
	 * @param value the ACTIVE_FLG - ACTIV_FLG
	 */
	public void setACTIVE_FLG(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ACTIVE_FLG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.ACTIVE_FLG</code> attribute. 
	 * @param value the ACTIVE_FLG - ACTIV_FLG
	 */
	public void setACTIVE_FLG(final String value)
	{
		setACTIVE_FLG( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.CREATE_TIMESTAMP</code> attribute.
	 * @return the CREATE_TIMESTAMP - CREATE_TIMESTAMP
	 */
	public Date getCREATE_TIMESTAMP(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, CREATE_TIMESTAMP);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.CREATE_TIMESTAMP</code> attribute.
	 * @return the CREATE_TIMESTAMP - CREATE_TIMESTAMP
	 */
	public Date getCREATE_TIMESTAMP()
	{
		return getCREATE_TIMESTAMP( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.CREATE_TIMESTAMP</code> attribute. 
	 * @param value the CREATE_TIMESTAMP - CREATE_TIMESTAMP
	 */
	public void setCREATE_TIMESTAMP(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, CREATE_TIMESTAMP,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.CREATE_TIMESTAMP</code> attribute. 
	 * @param value the CREATE_TIMESTAMP - CREATE_TIMESTAMP
	 */
	public void setCREATE_TIMESTAMP(final Date value)
	{
		setCREATE_TIMESTAMP( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.CREATE_USERID</code> attribute.
	 * @return the CREATE_USERID - CREATE_USERID
	 */
	public String getCREATE_USERID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CREATE_USERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.CREATE_USERID</code> attribute.
	 * @return the CREATE_USERID - CREATE_USERID
	 */
	public String getCREATE_USERID()
	{
		return getCREATE_USERID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.CREATE_USERID</code> attribute. 
	 * @param value the CREATE_USERID - CREATE_USERID
	 */
	public void setCREATE_USERID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CREATE_USERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.CREATE_USERID</code> attribute. 
	 * @param value the CREATE_USERID - CREATE_USERID
	 */
	public void setCREATE_USERID(final String value)
	{
		setCREATE_USERID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.FM_BILLTO_ACCT_CD</code> attribute.
	 * @return the FM_BILLTO_ACCT_CD - FM_BILLTO_ACCT_CD
	 */
	public String getFM_BILLTO_ACCT_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, FM_BILLTO_ACCT_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.FM_BILLTO_ACCT_CD</code> attribute.
	 * @return the FM_BILLTO_ACCT_CD - FM_BILLTO_ACCT_CD
	 */
	public String getFM_BILLTO_ACCT_CD()
	{
		return getFM_BILLTO_ACCT_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.FM_BILLTO_ACCT_CD</code> attribute. 
	 * @param value the FM_BILLTO_ACCT_CD - FM_BILLTO_ACCT_CD
	 */
	public void setFM_BILLTO_ACCT_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, FM_BILLTO_ACCT_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.FM_BILLTO_ACCT_CD</code> attribute. 
	 * @param value the FM_BILLTO_ACCT_CD - FM_BILLTO_ACCT_CD
	 */
	public void setFM_BILLTO_ACCT_CD(final String value)
	{
		setFM_BILLTO_ACCT_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.INACTIVE_FROM_DATE</code> attribute.
	 * @return the INACTIVE_FROM_DATE - INACTIV_FROM_DATE
	 */
	public Date getINACTIVE_FROM_DATE(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, INACTIVE_FROM_DATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.INACTIVE_FROM_DATE</code> attribute.
	 * @return the INACTIVE_FROM_DATE - INACTIV_FROM_DATE
	 */
	public Date getINACTIVE_FROM_DATE()
	{
		return getINACTIVE_FROM_DATE( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.INACTIVE_FROM_DATE</code> attribute. 
	 * @param value the INACTIVE_FROM_DATE - INACTIV_FROM_DATE
	 */
	public void setINACTIVE_FROM_DATE(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, INACTIVE_FROM_DATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.INACTIVE_FROM_DATE</code> attribute. 
	 * @param value the INACTIVE_FROM_DATE - INACTIV_FROM_DATE
	 */
	public void setINACTIVE_FROM_DATE(final Date value)
	{
		setINACTIVE_FROM_DATE( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.TP_ACCT_ID</code> attribute.
	 * @return the TP_ACCT_ID - code for TP_ACCOUNT
	 */
	public String getTP_ACCT_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TP_ACCT_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.TP_ACCT_ID</code> attribute.
	 * @return the TP_ACCT_ID - code for TP_ACCOUNT
	 */
	public String getTP_ACCT_ID()
	{
		return getTP_ACCT_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.TP_ACCT_ID</code> attribute. 
	 * @param value the TP_ACCT_ID - code for TP_ACCOUNT
	 */
	public void setTP_ACCT_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TP_ACCT_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.TP_ACCT_ID</code> attribute. 
	 * @param value the TP_ACCT_ID - code for TP_ACCOUNT
	 */
	public void setTP_ACCT_ID(final String value)
	{
		setTP_ACCT_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.TP_ID</code> attribute.
	 * @return the TP_ID - Unique id for TP_ACCOUNT
	 */
	public String getTP_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TP_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.TP_ID</code> attribute.
	 * @return the TP_ID - Unique id for TP_ACCOUNT
	 */
	public String getTP_ID()
	{
		return getTP_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.TP_ID</code> attribute. 
	 * @param value the TP_ID - Unique id for TP_ACCOUNT
	 */
	public void setTP_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TP_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.TP_ID</code> attribute. 
	 * @param value the TP_ID - Unique id for TP_ACCOUNT
	 */
	public void setTP_ID(final String value)
	{
		setTP_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.UPDATE_TIMESTAMP</code> attribute.
	 * @return the UPDATE_TIMESTAMP - UPDATE_TIMESTAMP
	 */
	public Date getUPDATE_TIMESTAMP(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, UPDATE_TIMESTAMP);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.UPDATE_TIMESTAMP</code> attribute.
	 * @return the UPDATE_TIMESTAMP - UPDATE_TIMESTAMP
	 */
	public Date getUPDATE_TIMESTAMP()
	{
		return getUPDATE_TIMESTAMP( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.UPDATE_TIMESTAMP</code> attribute. 
	 * @param value the UPDATE_TIMESTAMP - UPDATE_TIMESTAMP
	 */
	public void setUPDATE_TIMESTAMP(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, UPDATE_TIMESTAMP,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.UPDATE_TIMESTAMP</code> attribute. 
	 * @param value the UPDATE_TIMESTAMP - UPDATE_TIMESTAMP
	 */
	public void setUPDATE_TIMESTAMP(final Date value)
	{
		setUPDATE_TIMESTAMP( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.UPDATE_USERID</code> attribute.
	 * @return the UPDATE_USERID - UPDATE_USERID
	 */
	public String getUPDATE_USERID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, UPDATE_USERID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTPACCOUNT.UPDATE_USERID</code> attribute.
	 * @return the UPDATE_USERID - UPDATE_USERID
	 */
	public String getUPDATE_USERID()
	{
		return getUPDATE_USERID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.UPDATE_USERID</code> attribute. 
	 * @param value the UPDATE_USERID - UPDATE_USERID
	 */
	public void setUPDATE_USERID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, UPDATE_USERID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTPACCOUNT.UPDATE_USERID</code> attribute. 
	 * @param value the UPDATE_USERID - UPDATE_USERID
	 */
	public void setUPDATE_USERID(final String value)
	{
		setUPDATE_USERID( getSession().getSessionContext(), value );
	}
	
}
