/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.fmo.wom.constants.FmsapintegrationConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.fmo.wom.jalo.FMIPOTRACKING FMIPOTRACKING}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMIPOTRACKING extends GenericItem
{
	/** Qualifier of the <code>FMIPOTRACKING.ipoVersion</code> attribute **/
	public static final String IPOVERSION = "ipoVersion";
	/** Qualifier of the <code>FMIPOTRACKING.code</code> attribute **/
	public static final String CODE = "code";
	/** Qualifier of the <code>FMIPOTRACKING.masterOrderNumber</code> attribute **/
	public static final String MASTERORDERNUMBER = "masterOrderNumber";
	/** Qualifier of the <code>FMIPOTRACKING.IPORequest</code> attribute **/
	public static final String IPOREQUEST = "IPORequest";
	/** Qualifier of the <code>FMIPOTRACKING.createDate</code> attribute **/
	public static final String CREATEDATE = "createDate";
	/** Qualifier of the <code>FMIPOTRACKING.customer</code> attribute **/
	public static final String CUSTOMER = "customer";
	/** Qualifier of the <code>FMIPOTRACKING.IPOResponse</code> attribute **/
	public static final String IPORESPONSE = "IPOResponse";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(IPOVERSION, AttributeMode.INITIAL);
		tmp.put(CODE, AttributeMode.INITIAL);
		tmp.put(MASTERORDERNUMBER, AttributeMode.INITIAL);
		tmp.put(IPOREQUEST, AttributeMode.INITIAL);
		tmp.put(CREATEDATE, AttributeMode.INITIAL);
		tmp.put(CUSTOMER, AttributeMode.INITIAL);
		tmp.put(IPORESPONSE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.code</code> attribute.
	 * @return the code - code
	 */
	public String getCode(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CODE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.code</code> attribute.
	 * @return the code - code
	 */
	public String getCode()
	{
		return getCode( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.code</code> attribute. 
	 * @param value the code - code
	 */
	public void setCode(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CODE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.code</code> attribute. 
	 * @param value the code - code
	 */
	public void setCode(final String value)
	{
		setCode( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.createDate</code> attribute.
	 * @return the createDate - CREATE_TIMESTAMP
	 */
	public Date getCreateDate(final SessionContext ctx)
	{
		return (Date)getProperty( ctx, CREATEDATE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.createDate</code> attribute.
	 * @return the createDate - CREATE_TIMESTAMP
	 */
	public Date getCreateDate()
	{
		return getCreateDate( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.createDate</code> attribute. 
	 * @param value the createDate - CREATE_TIMESTAMP
	 */
	public void setCreateDate(final SessionContext ctx, final Date value)
	{
		setProperty(ctx, CREATEDATE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.createDate</code> attribute. 
	 * @param value the createDate - CREATE_TIMESTAMP
	 */
	public void setCreateDate(final Date value)
	{
		setCreateDate( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.customer</code> attribute.
	 * @return the customer - Unique id for
	 */
	public String getCustomer(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CUSTOMER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.customer</code> attribute.
	 * @return the customer - Unique id for
	 */
	public String getCustomer()
	{
		return getCustomer( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.customer</code> attribute. 
	 * @param value the customer - Unique id for
	 */
	public void setCustomer(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CUSTOMER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.customer</code> attribute. 
	 * @param value the customer - Unique id for
	 */
	public void setCustomer(final String value)
	{
		setCustomer( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.IPORequest</code> attribute.
	 * @return the IPORequest
	 */
	public String getIPORequest(final SessionContext ctx)
	{
		return (String)getProperty( ctx, IPOREQUEST);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.IPORequest</code> attribute.
	 * @return the IPORequest
	 */
	public String getIPORequest()
	{
		return getIPORequest( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.IPORequest</code> attribute. 
	 * @param value the IPORequest
	 */
	public void setIPORequest(final SessionContext ctx, final String value)
	{
		setProperty(ctx, IPOREQUEST,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.IPORequest</code> attribute. 
	 * @param value the IPORequest
	 */
	public void setIPORequest(final String value)
	{
		setIPORequest( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.IPOResponse</code> attribute.
	 * @return the IPOResponse
	 */
	public String getIPOResponse(final SessionContext ctx)
	{
		return (String)getProperty( ctx, IPORESPONSE);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.IPOResponse</code> attribute.
	 * @return the IPOResponse
	 */
	public String getIPOResponse()
	{
		return getIPOResponse( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.IPOResponse</code> attribute. 
	 * @param value the IPOResponse
	 */
	public void setIPOResponse(final SessionContext ctx, final String value)
	{
		setProperty(ctx, IPORESPONSE,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.IPOResponse</code> attribute. 
	 * @param value the IPOResponse
	 */
	public void setIPOResponse(final String value)
	{
		setIPOResponse( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.ipoVersion</code> attribute.
	 * @return the ipoVersion - IPO Version
	 */
	public String getIpoVersion(final SessionContext ctx)
	{
		return (String)getProperty( ctx, IPOVERSION);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.ipoVersion</code> attribute.
	 * @return the ipoVersion - IPO Version
	 */
	public String getIpoVersion()
	{
		return getIpoVersion( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.ipoVersion</code> attribute. 
	 * @param value the ipoVersion - IPO Version
	 */
	public void setIpoVersion(final SessionContext ctx, final String value)
	{
		setProperty(ctx, IPOVERSION,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.ipoVersion</code> attribute. 
	 * @param value the ipoVersion - IPO Version
	 */
	public void setIpoVersion(final String value)
	{
		setIpoVersion( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.masterOrderNumber</code> attribute.
	 * @return the masterOrderNumber - IPO Version
	 */
	public String getMasterOrderNumber(final SessionContext ctx)
	{
		return (String)getProperty( ctx, MASTERORDERNUMBER);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMIPOTRACKING.masterOrderNumber</code> attribute.
	 * @return the masterOrderNumber - IPO Version
	 */
	public String getMasterOrderNumber()
	{
		return getMasterOrderNumber( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.masterOrderNumber</code> attribute. 
	 * @param value the masterOrderNumber - IPO Version
	 */
	public void setMasterOrderNumber(final SessionContext ctx, final String value)
	{
		setProperty(ctx, MASTERORDERNUMBER,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMIPOTRACKING.masterOrderNumber</code> attribute. 
	 * @param value the masterOrderNumber - IPO Version
	 */
	public void setMasterOrderNumber(final String value)
	{
		setMasterOrderNumber( getSession().getSessionContext(), value );
	}
	
}
