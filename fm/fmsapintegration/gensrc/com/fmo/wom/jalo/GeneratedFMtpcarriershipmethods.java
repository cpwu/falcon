/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.fmo.wom.constants.FmsapintegrationConstants;
import de.hybris.platform.jalo.GenericItem;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type {@link com.fmo.wom.jalo.FMtpcarriershipmethods FMtpcarriershipmethods}.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFMtpcarriershipmethods extends GenericItem
{
	/** Qualifier of the <code>FMtpcarriershipmethods.CARRIER_CD</code> attribute **/
	public static final String CARRIER_CD = "CARRIER_CD";
	/** Qualifier of the <code>FMtpcarriershipmethods.TP_CARRIER_SHIPMTHD_ID</code> attribute **/
	public static final String TP_CARRIER_SHIPMTHD_ID = "TP_CARRIER_SHIPMTHD_ID";
	/** Qualifier of the <code>FMtpcarriershipmethods.SHIP_MTHD_CD</code> attribute **/
	public static final String SHIP_MTHD_CD = "SHIP_MTHD_CD";
	/** Qualifier of the <code>FMtpcarriershipmethods.TP_ID</code> attribute **/
	public static final String TP_ID = "TP_ID";
	/** Qualifier of the <code>FMtpcarriershipmethods.SCAC_CD</code> attribute **/
	public static final String SCAC_CD = "SCAC_CD";
	/** Qualifier of the <code>FMtpcarriershipmethods.TRNSPRT_MTHD_CD</code> attribute **/
	public static final String TRNSPRT_MTHD_CD = "TRNSPRT_MTHD_CD";
	/** Qualifier of the <code>FMtpcarriershipmethods.CARRIER_SHIPMTHD_ID</code> attribute **/
	public static final String CARRIER_SHIPMTHD_ID = "CARRIER_SHIPMTHD_ID";
	/** Qualifier of the <code>FMtpcarriershipmethods.ACTIVE_FLG</code> attribute **/
	public static final String ACTIVE_FLG = "ACTIVE_FLG";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>();
		tmp.put(CARRIER_CD, AttributeMode.INITIAL);
		tmp.put(TP_CARRIER_SHIPMTHD_ID, AttributeMode.INITIAL);
		tmp.put(SHIP_MTHD_CD, AttributeMode.INITIAL);
		tmp.put(TP_ID, AttributeMode.INITIAL);
		tmp.put(SCAC_CD, AttributeMode.INITIAL);
		tmp.put(TRNSPRT_MTHD_CD, AttributeMode.INITIAL);
		tmp.put(CARRIER_SHIPMTHD_ID, AttributeMode.INITIAL);
		tmp.put(ACTIVE_FLG, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.ACTIVE_FLG</code> attribute.
	 * @return the ACTIVE_FLG - ACTIV_FLG
	 */
	public String getACTIVE_FLG(final SessionContext ctx)
	{
		return (String)getProperty( ctx, ACTIVE_FLG);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.ACTIVE_FLG</code> attribute.
	 * @return the ACTIVE_FLG - ACTIV_FLG
	 */
	public String getACTIVE_FLG()
	{
		return getACTIVE_FLG( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.ACTIVE_FLG</code> attribute. 
	 * @param value the ACTIVE_FLG - ACTIV_FLG
	 */
	public void setACTIVE_FLG(final SessionContext ctx, final String value)
	{
		setProperty(ctx, ACTIVE_FLG,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.ACTIVE_FLG</code> attribute. 
	 * @param value the ACTIVE_FLG - ACTIV_FLG
	 */
	public void setACTIVE_FLG(final String value)
	{
		setACTIVE_FLG( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.CARRIER_CD</code> attribute.
	 * @return the CARRIER_CD - CARRIER_CD
	 */
	public String getCARRIER_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.CARRIER_CD</code> attribute.
	 * @return the CARRIER_CD - CARRIER_CD
	 */
	public String getCARRIER_CD()
	{
		return getCARRIER_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.CARRIER_CD</code> attribute. 
	 * @param value the CARRIER_CD - CARRIER_CD
	 */
	public void setCARRIER_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.CARRIER_CD</code> attribute. 
	 * @param value the CARRIER_CD - CARRIER_CD
	 */
	public void setCARRIER_CD(final String value)
	{
		setCARRIER_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the CARRIER_SHIPMTHD_ID - CARRIER_SHIPMTHD_ID
	 */
	public String getCARRIER_SHIPMTHD_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, CARRIER_SHIPMTHD_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the CARRIER_SHIPMTHD_ID - CARRIER_SHIPMTHD_ID
	 */
	public String getCARRIER_SHIPMTHD_ID()
	{
		return getCARRIER_SHIPMTHD_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the CARRIER_SHIPMTHD_ID - CARRIER_SHIPMTHD_ID
	 */
	public void setCARRIER_SHIPMTHD_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, CARRIER_SHIPMTHD_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the CARRIER_SHIPMTHD_ID - CARRIER_SHIPMTHD_ID
	 */
	public void setCARRIER_SHIPMTHD_ID(final String value)
	{
		setCARRIER_SHIPMTHD_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.SCAC_CD</code> attribute.
	 * @return the SCAC_CD - SCAC_CD
	 */
	public String getSCAC_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SCAC_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.SCAC_CD</code> attribute.
	 * @return the SCAC_CD - SCAC_CD
	 */
	public String getSCAC_CD()
	{
		return getSCAC_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.SCAC_CD</code> attribute. 
	 * @param value the SCAC_CD - SCAC_CD
	 */
	public void setSCAC_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SCAC_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.SCAC_CD</code> attribute. 
	 * @param value the SCAC_CD - SCAC_CD
	 */
	public void setSCAC_CD(final String value)
	{
		setSCAC_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.SHIP_MTHD_CD</code> attribute.
	 * @return the SHIP_MTHD_CD - SHIP_MTHD_CD
	 */
	public String getSHIP_MTHD_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, SHIP_MTHD_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.SHIP_MTHD_CD</code> attribute.
	 * @return the SHIP_MTHD_CD - SHIP_MTHD_CD
	 */
	public String getSHIP_MTHD_CD()
	{
		return getSHIP_MTHD_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.SHIP_MTHD_CD</code> attribute. 
	 * @param value the SHIP_MTHD_CD - SHIP_MTHD_CD
	 */
	public void setSHIP_MTHD_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, SHIP_MTHD_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.SHIP_MTHD_CD</code> attribute. 
	 * @param value the SHIP_MTHD_CD - SHIP_MTHD_CD
	 */
	public void setSHIP_MTHD_CD(final String value)
	{
		setSHIP_MTHD_CD( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TP_CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the TP_CARRIER_SHIPMTHD_ID - TP_CARRIER_SHIPMTHD_ID
	 */
	public String getTP_CARRIER_SHIPMTHD_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TP_CARRIER_SHIPMTHD_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TP_CARRIER_SHIPMTHD_ID</code> attribute.
	 * @return the TP_CARRIER_SHIPMTHD_ID - TP_CARRIER_SHIPMTHD_ID
	 */
	public String getTP_CARRIER_SHIPMTHD_ID()
	{
		return getTP_CARRIER_SHIPMTHD_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TP_CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the TP_CARRIER_SHIPMTHD_ID - TP_CARRIER_SHIPMTHD_ID
	 */
	public void setTP_CARRIER_SHIPMTHD_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TP_CARRIER_SHIPMTHD_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TP_CARRIER_SHIPMTHD_ID</code> attribute. 
	 * @param value the TP_CARRIER_SHIPMTHD_ID - TP_CARRIER_SHIPMTHD_ID
	 */
	public void setTP_CARRIER_SHIPMTHD_ID(final String value)
	{
		setTP_CARRIER_SHIPMTHD_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TP_ID</code> attribute.
	 * @return the TP_ID - TP_ID
	 */
	public String getTP_ID(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TP_ID);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TP_ID</code> attribute.
	 * @return the TP_ID - TP_ID
	 */
	public String getTP_ID()
	{
		return getTP_ID( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TP_ID</code> attribute. 
	 * @param value the TP_ID - TP_ID
	 */
	public void setTP_ID(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TP_ID,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TP_ID</code> attribute. 
	 * @param value the TP_ID - TP_ID
	 */
	public void setTP_ID(final String value)
	{
		setTP_ID( getSession().getSessionContext(), value );
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TRNSPRT_MTHD_CD</code> attribute.
	 * @return the TRNSPRT_MTHD_CD - TRNSPRT_MTHD_CD
	 */
	public String getTRNSPRT_MTHD_CD(final SessionContext ctx)
	{
		return (String)getProperty( ctx, TRNSPRT_MTHD_CD);
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>FMtpcarriershipmethods.TRNSPRT_MTHD_CD</code> attribute.
	 * @return the TRNSPRT_MTHD_CD - TRNSPRT_MTHD_CD
	 */
	public String getTRNSPRT_MTHD_CD()
	{
		return getTRNSPRT_MTHD_CD( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TRNSPRT_MTHD_CD</code> attribute. 
	 * @param value the TRNSPRT_MTHD_CD - TRNSPRT_MTHD_CD
	 */
	public void setTRNSPRT_MTHD_CD(final SessionContext ctx, final String value)
	{
		setProperty(ctx, TRNSPRT_MTHD_CD,value);
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>FMtpcarriershipmethods.TRNSPRT_MTHD_CD</code> attribute. 
	 * @param value the TRNSPRT_MTHD_CD - TRNSPRT_MTHD_CD
	 */
	public void setTRNSPRT_MTHD_CD(final String value)
	{
		setTRNSPRT_MTHD_CD( getSession().getSessionContext(), value );
	}
	
}
