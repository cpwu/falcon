/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.fmo.wom.jalo;

import com.fmo.wom.constants.FmsapintegrationConstants;
import com.fmo.wom.jalo.FMIPOTPACCOUNT;
import com.fmo.wom.jalo.FMIPOTRACKING;
import com.fmo.wom.jalo.FMIPOTRADINGPARTNER;
import com.fmo.wom.jalo.FMtpcarriershipmethods;
import de.hybris.platform.jalo.Item;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.JaloBusinessException;
import de.hybris.platform.jalo.JaloSystemException;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.extension.Extension;
import de.hybris.platform.jalo.type.ComposedType;
import de.hybris.platform.jalo.type.JaloGenericCreationException;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type <code>FmsapintegrationManager</code>.
 */
@SuppressWarnings({"deprecation","unused","cast","PMD"})
public abstract class GeneratedFmsapintegrationManager extends Extension
{
	protected static final Map<String, Map<String, AttributeMode>> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, Map<String, AttributeMode>> ttmp = new HashMap();
		DEFAULT_INITIAL_ATTRIBUTES = ttmp;
	}
	@Override
	public Map<String, AttributeMode> getDefaultAttributeModes(final Class<? extends Item> itemClass)
	{
		Map<String, AttributeMode> ret = new HashMap<>();
		final Map<String, AttributeMode> attr = DEFAULT_INITIAL_ATTRIBUTES.get(itemClass.getName());
		if (attr != null)
		{
			ret.putAll(attr);
		}
		return ret;
	}
	
	public FMIPOTPACCOUNT createFMIPOTPACCOUNT(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmsapintegrationConstants.TC.FMIPOTPACCOUNT );
			return (FMIPOTPACCOUNT)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMIPOTPACCOUNT : "+e.getMessage(), 0 );
		}
	}
	
	public FMIPOTPACCOUNT createFMIPOTPACCOUNT(final Map attributeValues)
	{
		return createFMIPOTPACCOUNT( getSession().getSessionContext(), attributeValues );
	}
	
	public FMIPOTRACKING createFMIPOTRACKING(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmsapintegrationConstants.TC.FMIPOTRACKING );
			return (FMIPOTRACKING)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMIPOTRACKING : "+e.getMessage(), 0 );
		}
	}
	
	public FMIPOTRACKING createFMIPOTRACKING(final Map attributeValues)
	{
		return createFMIPOTRACKING( getSession().getSessionContext(), attributeValues );
	}
	
	public FMIPOTRADINGPARTNER createFMIPOTRADINGPARTNER(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmsapintegrationConstants.TC.FMIPOTRADINGPARTNER );
			return (FMIPOTRADINGPARTNER)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMIPOTRADINGPARTNER : "+e.getMessage(), 0 );
		}
	}
	
	public FMIPOTRADINGPARTNER createFMIPOTRADINGPARTNER(final Map attributeValues)
	{
		return createFMIPOTRADINGPARTNER( getSession().getSessionContext(), attributeValues );
	}
	
	public FMtpcarriershipmethods createFMtpcarriershipmethods(final SessionContext ctx, final Map attributeValues)
	{
		try
		{
			ComposedType type = getTenant().getJaloConnection().getTypeManager().getComposedType( FmsapintegrationConstants.TC.FMTPCARRIERSHIPMETHODS );
			return (FMtpcarriershipmethods)type.newInstance( ctx, attributeValues );
		}
		catch( JaloGenericCreationException e)
		{
			final Throwable cause = e.getCause();
			throw (cause instanceof RuntimeException ?
			(RuntimeException)cause
			:
			new JaloSystemException( cause, cause.getMessage(), e.getErrorCode() ) );
		}
		catch( JaloBusinessException e )
		{
			throw new JaloSystemException( e ,"error creating FMtpcarriershipmethods : "+e.getMessage(), 0 );
		}
	}
	
	public FMtpcarriershipmethods createFMtpcarriershipmethods(final Map attributeValues)
	{
		return createFMtpcarriershipmethods( getSession().getSessionContext(), attributeValues );
	}
	
	@Override
	public String getName()
	{
		return FmsapintegrationConstants.EXTENSIONNAME;
	}
	
}
