package com.fmo.wom.business.as;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fmo.wom.business.as.config.USCanAS;
import com.fmo.wom.common.exception.WOMBusinessDataException;
import com.fmo.wom.common.exception.WOMExternalSystemException;
import com.fmo.wom.common.exception.WOMTransactionException;
import com.fmo.wom.common.exception.WOMValidationException;
import com.fmo.wom.common.util.MessageResourceConstants;
import com.fmo.wom.domain.AccountBO;
import com.fmo.wom.domain.CustomerSalesOrgBO;
import com.fmo.wom.domain.InventoryBO;
import com.fmo.wom.domain.ItemBO;
import com.fmo.wom.domain.ProblemBO;
import com.fmo.wom.domain.SapAcctBO;
import com.fmo.wom.domain.enums.ExternalSystem;
import com.fmo.wom.domain.enums.Market;
import com.fmo.wom.domain.enums.OrderSource;
import com.fmo.wom.domain.enums.OrderType;
import com.fmo.wom.integration.NabsService;
import com.fmo.wom.integration.SAPService;


public class InventoryASUSCanImpl extends InventoryASBase implements InventoryAS, USCanAS
{


	private final NabsService nabsService;

	private final MarketAS marketASUSCan;

	@Override
	protected MarketAS getMarketAS()
	{
		return marketASUSCan;
	}

	public InventoryASUSCanImpl()
	{
		super();
		marketASUSCan = new MarketASUSCanImpl();
		nabsService = WOMServices.getNabsService();
	}

	@Override
	public Market getMarket()
	{
		return Market.US_CANADA;
	}

	@Override
	public List<ItemBO> checkInventory(final String masterOrderNumber, final List<ItemBO> itemList, final AccountBO billToAcct,
			final AccountBO shipToAcct) throws WOMBusinessDataException, WOMExternalSystemException, WOMTransactionException,
			WOMValidationException
	{
		return null;
	}

	/*
	 * public List<ItemBO> checkInventory(String masterOrderNumber, List<ItemBO> itemList, AccountBO billToAcct,
	 * AccountBO shipToAcct, OrderSource orderSource, String payementMethod) throws WOMBusinessDataException,
	 * WOMExternalSystemException, WOMTransactionException, WOMValidationException {
	 * 
	 * SapAcctBO sapAcct = billToAcct.getSapAccount(); CustomerSalesOrgBO sapSaleOrg =
	 * sapAcct.getCustomerSalesOrganization(); return checkInventory(masterOrderNumber, itemList, billToAcct,
	 * shipToAcct,sapSaleOrg, OrderType.EMERGENCY, orderSource, payementMethod); }
	 */


	public List<ItemBO> checkInventory(final String masterOrderNumber, final List<ItemBO> itemList, final AccountBO billToAcct,
			final AccountBO shipToAcct, final CustomerSalesOrgBO salesOrg, final OrderType orderType, final OrderSource orderSource/*
																																										   * ,
																																										   * String
																																										   * paymentMethod
																																										   */)
			throws WOMBusinessDataException, WOMExternalSystemException, WOMTransactionException, WOMValidationException
	{

		final List<ItemBO> nabsList = new ArrayList<ItemBO>();
		final List<ItemBO> sapList = new ArrayList<ItemBO>();

		// for the given input list, put the ones that do not have problems into
		// the appropriate
		// external system list
		for (final ItemBO lineItem : itemList)
		{
			// no need to check it if we can't process it
			if (lineItem.canProcessItem())
			{
				if (lineItem.getExternalSystem() == ExternalSystem.NABS)
				{
					nabsList.add(lineItem);
				}
				else if (lineItem.getExternalSystem() == ExternalSystem.EVRST)
				{
					sapList.add(lineItem);
				}
			}
		}

		final String billToAcctCode = billToAcct != null ? billToAcct.getAccountCode() : null;
		String shipToAcctCode = shipToAcct != null ? shipToAcct.getAccountCode() : null;
		// call NABS check inventory
		if (nabsList.size() > 0)
		{
			nabsService.checkInventory(masterOrderNumber, nabsList, billToAcctCode, shipToAcctCode, orderType);
		}
		final SapAcctBO sapAcct = billToAcct != null ? billToAcct.getSapAccount() : null;
		final String sapAccoutCode = sapAcct != null ? sapAcct.getSapAccountCode() : null;

		//For ShipToAcctCode
		final SapAcctBO sapShipToAcct = shipToAcct != null ? shipToAcct.getSapAccount() : null;
		final String sapShipToAccoutCode = sapShipToAcct != null ? sapShipToAcct.getSapAccountCode() : null;
		shipToAcctCode = sapShipToAccoutCode != null ? sapShipToAccoutCode : shipToAcctCode;

		// Call SAP check Inventory
		SAPService.checkInventory(sapList, sapAccoutCode, shipToAcctCode, salesOrg, orderType.getEconCode(),
				orderSource.getOrderSource()/* ,paymentMethod */);

		// Go through Item list, find out no inventory, no sufficient inventory
		// problem
		setInventoryProblem(itemList, orderSource);
		return itemList;
	}

	public List<ItemBO> tscPickUpCheckInventory(final List<ItemBO> itemsList, final AccountBO billToAcct,
			final AccountBO shipToAcct, final CustomerSalesOrgBO salesOrg, final OrderType orderType, final OrderSource orderSource)
			throws WOMBusinessDataException, WOMExternalSystemException, WOMTransactionException, WOMValidationException
	{
		final List<ItemBO> eccItemsList = new ArrayList<ItemBO>();

		for (final ItemBO lineItem : itemsList)
		{
			// no need to check it if we can't process it
			if (lineItem.canProcessItem())
			{
				if (lineItem.getExternalSystem() == ExternalSystem.EVRST)
				{
					eccItemsList.add(lineItem);
				}
			}
		}

		final SapAcctBO sapBillToAcct = billToAcct != null ? billToAcct.getSapAccount() : null;
		final String sapBillToAccoutCode = sapBillToAcct != null ? sapBillToAcct.getSapAccountCode() : null;

		final SapAcctBO sapShipToAcct = shipToAcct != null ? shipToAcct.getSapAccount() : null;
		final String sapshipToAccoutCode = sapShipToAcct != null ? sapShipToAcct.getSapAccountCode() : null;

		SAPService.tscPickUpcheckInventory(eccItemsList, sapBillToAccoutCode, sapshipToAccoutCode, salesOrg,
				orderType.getEconCode(), orderSource.getOrderSource());

		setInventoryProblem(eccItemsList, orderSource);
		return eccItemsList;
	}

	protected List<ItemBO> getSelectedInventoryForAllItems(final List<ItemBO> itemList)
	{
		// reduce inventory list to selected ones for each item
		// unless an item is found in multiple locations
		for (final ItemBO anItem : itemList)
		{
			if (!isPartFoundInMultipleLocations(anItem))
			{
				anItem.setInventory(shortListRelevantInventory(anItem));
			}
		}
		return itemList;
	}

	// Utility method to check the passed-in part and return true 
	// if it contains a PART_FOUND_IN_MULTIPLE_LOCATIONS problem.
	public boolean isPartFoundInMultipleLocations(final ItemBO anItem)
	{

		if (anItem.getProblemItemList() == null)
		{
			return false;
		}

		for (final ProblemBO aProblem : anItem.getProblemItemList())
		{
			if (MessageResourceConstants.PART_FOUND_IN_MULTIPLE_LOCATIONS.equals(aProblem.getMessageKey()))
			{
				// found one
				return true;
			}
		}

		// made it through
		return false;
	}

	/**
	 * Selects inventory that is relevant or selected or that can fulfill the request. The MainDC is the inventory that
	 * is selected by default as it is returned by NABS and is needed to be supplied back to NABS. The selected inventory
	 * is the next best that can fulfill the request.
	 * 
	 * @param inventory
	 * @return List of reduced selected inventories.
	 */
	private List<InventoryBO> shortListRelevantInventory(final ItemBO anItem)
	{
		final InventoryBO selected = anItem.getSelectedInventory();
		final InventoryBO main = anItem.getMainDCInventory();

		final Set<InventoryBO> temp = new HashSet<InventoryBO>();
		if (selected != null)
		{
			temp.add(selected);
		}
		if (main != null)
		{
			temp.add(main);
		}

		return new ArrayList<InventoryBO>(temp);

	}

}
