/**
 * 
 */
package com.federalmogul.storefront.controllers.pages;

import de.hybris.platform.b2bacceleratorfacades.company.B2BCommerceUserFacade;
import de.hybris.platform.b2bacceleratorfacades.company.CompanyB2BCommerceFacade;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.i18n.I18NFacade;
import de.hybris.platform.commercefacades.order.CheckoutFacade;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commercefacades.user.data.CountryData;
import de.hybris.platform.commercefacades.user.data.RegionData;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.user.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.federalmogul.facades.account.FMCustomerFacade;
//import com.federalmogul.facades.account.FMCustomerFacade;
import com.federalmogul.facades.account.FMLeadGenerationCallBackFacade;
import com.federalmogul.facades.account.FMUserFacade;
import com.federalmogul.facades.customer.data.FMLeadGenerationCallBackData;
import com.federalmogul.facades.user.data.FMCustomerData;
import com.federalmogul.storefront.breadcrumb.impl.SupportBreadcrumbBuilder;
import com.federalmogul.storefront.controllers.ControllerConstants;
import com.federalmogul.storefront.controllers.util.GlobalMessages;
import com.federalmogul.storefront.forms.LeadGenerationCallBackRequestForm;


/**
 * @author Balaji Controller for home page.
 * 
 * 
 */

@Controller
@Scope("tenant")
@RequestMapping("/leadGeneration")
//@RequestMapping("/support")
public class FMLeadGenerationCallBackController extends SearchPageController
{

	//<!-- Balaji---Start Order Lead Generation Call back --> 
	private static final String FMLEADGENERATION_CALLBACK_CMS_PAGE = "LeadGenerationCallBackPage";
	protected static final String LANDING_SUPPORT_PAGE = "fmContactUsPage";
	//<!-- Balaji---End Order Lead Generation Call back -->

	private static final Logger LOG = Logger.getLogger(FMLeadGenerationCallBackController.class);

	/*
	 * @Resource(name = "accountBreadcrumbBuilder") private ResourceBreadcrumbBuilder accountBreadcrumbBuilder;
	 */
	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Resource(name = "checkoutFacade")
	private CheckoutFacade checkoutFacade;

	//@Resource(name = "b2bCustomerFacade")
	//protected CustomerFacade customerFacade;

	//@Resource
	//protected FMCustomerFacade fmCustomerFacade;

	@Resource
	protected FMUserFacade fmUserfacade;
	@Resource
	protected B2BCommerceUserFacade b2bCommerceUserFacade;

	@Resource(name = "i18NFacade")
	private I18NFacade i18NFacade;

	@Resource(name = "leadGenerationCallBackFacade")
	protected FMLeadGenerationCallBackFacade fmLeadGenerationCallBackFacade;

	@Resource(name = "b2bCommerceFacade")
	protected CompanyB2BCommerceFacade companyB2BCommerceFacade;

	@Resource(name = "SupportBreadcrumbBuilder")
	protected SupportBreadcrumbBuilder supportBreadcrumbBuilder;

	@Resource(name = "fmCustomerFacade")
	protected FMCustomerFacade fmCustomerFacade;

	@Autowired
	@Resource
	private UserService userService;

	//<!-- Balaji Start FM Lead Generation Call Back  -->
	@RequestMapping(value = "/contact-us", method = RequestMethod.GET)
	public ModelAndView leadGenerationCallBack(final Model model) throws CMSItemNotFoundException
	{
		final UserModel user = userService.getCurrentUser();
		@SuppressWarnings("deprecation")
		final boolean isUserAnonymous = user == null || userService.isAnonymousUser(user);
		final LeadGenerationCallBackRequestForm leadForm = new LeadGenerationCallBackRequestForm();
		final List leadGenerationSubjects = getQueryList();
		LOG.info("leadGeneration-callBack ==>" + leadGenerationSubjects.size());
		model.addAttribute("leadGenerationSubjects", leadGenerationSubjects);
		if (!isUserAnonymous)
		{
			final FMCustomerData currentuserData = fmCustomerFacade.getCurrentFMCustomer();
			LOG.info("leadGeneration-callBack ==>");

			//model.addAttribute("leadGenerationSubjects", LeadGenerationSubjects.values());


			leadForm.setFirstName(currentuserData.getFirstName());
			leadForm.setLastName(currentuserData.getLastName());
			leadForm.setEmail(currentuserData.getEmail());

			if (currentuserData.getFmunit() != null)
			{
				leadForm.setCompanyName(currentuserData.getFmunit().getName());
			}
			if (currentuserData.getDefaultShippingAddress() != null)
			{
				leadForm.setPhoneno(currentuserData.getDefaultShippingAddress().getPhone());
				leadForm.setAddressline1(currentuserData.getDefaultShippingAddress().getLine1());
				leadForm.setAddressline2(currentuserData.getDefaultShippingAddress().getLine2());
				leadForm.setCity(currentuserData.getDefaultShippingAddress().getTown());
				if (currentuserData.getDefaultShippingAddress().getRegion() != null)
				{
					leadForm.setState(currentuserData.getDefaultShippingAddress().getRegion().getName());
				}
				leadForm.setZipCode(currentuserData.getDefaultShippingAddress().getPostalCode());
				if (currentuserData.getDefaultShippingAddress().getCountry() != null)
				{
					leadForm.setCountry(currentuserData.getDefaultShippingAddress().getCountry().getIsocode());
				}
			}
		}
		model.addAttribute("callBackFormData", leadForm);
		final List<CountryData> countries = new ArrayList<CountryData>();
		countries.add(checkoutFacade.getCountryForIsocode("US"));
		countries.add(checkoutFacade.getCountryForIsocode("CA"));
		model.addAttribute("countryData", countries);
		//model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("regionsdatas", getI18NFacade().getRegionsForAllCountries().values());
		storeCmsPageInModel(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
		//model.addAttribute("breadcrumbs", "Contact Us");
		model.addAttribute("breadcrumbs", supportBreadcrumbBuilder.getEmailContactUsBreadcrumbs("Contact-Us"));
		model.addAttribute("metaRobots", "no-index,no-follow");
		model.addAttribute("selectedLink", "LeadGenerationCallBack");
		return new ModelAndView(ControllerConstants.Views.Pages.Account.FMLeadGenerationCallBack);
	}

	/**
	 * controller mapping for saving the values after clicking Submit button
	 * 
	 * @param callBackRequestForm
	 * @param result
	 * @return String
	 * @throws CMSItemNotFoundException
	 */
	@RequestMapping(value = "/contact-us-submit", method = RequestMethod.POST)
	public String leadGenerationCallBack(
			@ModelAttribute("callBackFormData") final LeadGenerationCallBackRequestForm callBackRequestForm, final Model model)
			throws CMSItemNotFoundException
	{

		LOG.info("leadGeneration-callBack request ==>");

		final List leadGenerationSubjects = getQueryList();
		LOG.info("leadGeneration-callBack ==>" + leadGenerationSubjects.size());


		if (!validateform(callBackRequestForm, model))
		{
			LOG.info("leadGeneration-false request ==>");
			storeCmsPageInModel(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
			//storeCmsPageInModel(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
			//setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
			//model.addAttribute("leadGenerationSubjects", LeadGenerationSubjects.values());
			model.addAttribute("leadGenerationSubjects", leadGenerationSubjects);
			//model.addAttribute("callBackData", new LeadGenerationCallBackRequestForm());
			/*
			 * final UserModel user = userService.getCurrentUser();
			 * 
			 * @SuppressWarnings("deprecation") final boolean isUserAnonymous = user == null ||
			 * userService.isAnonymousUser(user); final LeadGenerationCallBackRequestForm leadForm = new
			 * LeadGenerationCallBackRequestForm(); if (!isUserAnonymous) {
			 * 
			 * final FMCustomerData currentuserData = fmCustomerFacade.getCurrentFMCustomer();
			 * 
			 * 
			 * 
			 * leadForm.setFirstName(currentuserData.getFirstName()); leadForm.setLastName(currentuserData.getLastName());
			 * leadForm.setEmail(currentuserData.getEmail());
			 * 
			 * if (currentuserData.getFmunit() != null) { leadForm.setCompanyName(currentuserData.getFmunit().getName()); }
			 * if (currentuserData.getDefaultShippingAddress() != null) {
			 * leadForm.setPhoneno(currentuserData.getDefaultShippingAddress().getPhone());
			 * leadForm.setAddressline1(currentuserData.getDefaultShippingAddress().getLine1());
			 * leadForm.setAddressline2(currentuserData.getDefaultShippingAddress().getLine2());
			 * leadForm.setCity(currentuserData.getDefaultShippingAddress().getTown()); if
			 * (currentuserData.getDefaultShippingAddress().getRegion() != null) {
			 * leadForm.setState(currentuserData.getDefaultShippingAddress().getRegion().getName()); }
			 * leadForm.setZipCode(currentuserData.getDefaultShippingAddress().getPostalCode()); if
			 * (currentuserData.getDefaultShippingAddress().getCountry() != null) {
			 * leadForm.setCountry(currentuserData.getDefaultShippingAddress().getCountry().getIsocode()); } }
			 * 
			 * }
			 */

			model.addAttribute("callBackFormData", callBackRequestForm);
			final List<CountryData> countries = new ArrayList<CountryData>();
			countries.add(checkoutFacade.getCountryForIsocode("US"));
			countries.add(checkoutFacade.getCountryForIsocode("CA"));
			LOG.info("countries ==>" + countries.size());
			LOG.info("countries 1 ==>" + countries.get(0));
			model.addAttribute("countryData", countries);
			model.addAttribute("componentUid", "supportgeneralInquiry");
			//model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
			model.addAttribute("regionsdatas", getI18NFacade().getRegionsForAllCountries().values());
			model.addAttribute("breadcrumbs", supportBreadcrumbBuilder.getEmailContactUsBreadcrumbs("Contact-Us"));
			//return ControllerConstants.Views.Pages.Account.FMLeadGenerationCallBack;
			return ControllerConstants.Views.Pages.Support.ContactUsPage;
			//return FORWARD_PREFIX + "/leadGeneration/leadGeneration-callBack";
		}
		else
		{
			try
			{
				fmLeadGenerationCallBackFacade.createCallBack(populateLeadGenerationCallBackData(callBackRequestForm));
				GlobalMessages.addInfoMessage(model, "Your message has been submitted.");
			}
			catch (final Exception e)
			{
				GlobalMessages.addInfoMessage(model, "Your message has not been submitted... Please try again");
				LOG.error("Exception ...." + e.getMessage());

			}

			//storeCmsPageInModel(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
			//setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
			storeCmsPageInModel(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
			model.addAttribute("leadGenerationSubjects", leadGenerationSubjects);
			model.addAttribute("callBackData", new LeadGenerationCallBackRequestForm());
			model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
			model.addAttribute("regionsdatas", getI18NFacade().getRegionsForAllCountries().values());
			//model.addAttribute("breadcrumbs", "Contact Us");
			model.addAttribute("breadcrumbs", supportBreadcrumbBuilder.getEmailContactUsBreadcrumbs("Contact-Us"));
			model.addAttribute("metaRobots", "no-index,no-follow");
			model.addAttribute("selectedLink", "LeadGenerationCallBackRequest");
			model.addAttribute("componentUid", "supportgeneralInquiry");
			//return ControllerConstants.Views.Pages.Account.FMLeadGenerationCallBack;
			return ControllerConstants.Views.Pages.Support.ContactUsPage;

		}
	}

	@RequestMapping(value = "/contact-us-reset", method = RequestMethod.GET)
	public ModelAndView leadGenerationCallBackReset(final Model model) throws CMSItemNotFoundException
	{

		LOG.info("leadGeneration-callBack ==>");
		final List leadGenerationSubjects = getQueryList();
		LOG.info("leadGeneration-callBack ==>" + leadGenerationSubjects.size());
		//model.addAttribute("leadGenerationSubjects", LeadGenerationSubjects.values());
		model.addAttribute("leadGenerationSubjects", leadGenerationSubjects);


		model.addAttribute("callBackFormData", new LeadGenerationCallBackRequestForm());
		final List<CountryData> countries = new ArrayList<CountryData>();
		countries.add(checkoutFacade.getCountryForIsocode("US"));
		countries.add(checkoutFacade.getCountryForIsocode("CA"));
		model.addAttribute("countryData", countries);
		//model.addAttribute("countryData", checkoutFacade.getDeliveryCountries());
		model.addAttribute("regionsdatas", getI18NFacade().getRegionsForAllCountries().values());
		//storeCmsPageInModel(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
		//setUpMetaDataForContentPage(model, getContentPageForLabelOrId(FMLEADGENERATION_CALLBACK_CMS_PAGE));
		storeCmsPageInModel(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(LANDING_SUPPORT_PAGE));
		//model.addAttribute("breadcrumbs", "Contact Us");
		model.addAttribute("breadcrumbs", supportBreadcrumbBuilder.getEmailContactUsBreadcrumbs("Contact-Us"));
		model.addAttribute("metaRobots", "no-index,no-follow");
		model.addAttribute("selectedLink", "LeadGenerationCallBack");
		model.addAttribute("componentUid", "supportgeneralInquiry");
		return new ModelAndView(ControllerConstants.Views.Pages.Support.ContactUsPage);
	}


	private boolean validateform(final LeadGenerationCallBackRequestForm leadGenerationCallBackRequestForm, final Model model)
	{
		//LOG.info("Inside vaildate form method of LeadGenerationCallBackRequestForm "+ leadGenerationCallBackRequestForm.getSubjects().name());

		LOG.info("Inside vaildate form method of LeadGenerationCallBackRequestForm "
				+ leadGenerationCallBackRequestForm.getSubjects());

		if (null == leadGenerationCallBackRequestForm.getPhoneno() || leadGenerationCallBackRequestForm.getPhoneno().isEmpty())
		{
			//GlobalMessages.addErrorMessage(model, "Please enter your Phone Number");
			LOG.info("Inside vaildate form :: Please enter your last Phone Number");

		}
		else
		{
			if (!(leadGenerationCallBackRequestForm.getPhoneno().trim().matches("\\d{3}-\\d{3}-\\d{4}")
					|| leadGenerationCallBackRequestForm.getPhoneno().trim().matches("\\(\\d{3}\\) \\d{3}-\\d{4}")
					|| leadGenerationCallBackRequestForm.getPhoneno().trim().matches("\\d{10}")
					|| leadGenerationCallBackRequestForm.getPhoneno().trim().matches("\\d{3}.\\d{3}.\\d{4}")
					|| leadGenerationCallBackRequestForm.getPhoneno().trim().matches("[(]\\d{3}[)]\\d{3}-\\d{4}") || leadGenerationCallBackRequestForm
					.getPhoneno().trim().matches("\\d{2}-\\d{1}.\\d{1}-\\d{1} \\d{3} \\d{2}")))

			{
				GlobalMessages.addErrorMessage(model, "Please verify that you've entered a 10-digit phone number.");
				LOG.info("Inside vaildate form :: invalid  phone number");
			}
		}
		//		if (null == leadGenerationCallBackRequestForm.getCallBackDescription()
		//				|| leadGenerationCallBackRequestForm.getCallBackDescription().isEmpty())
		//		{
		//			GlobalMessages.addErrorMessage(model, "Please enter call back description");
		//			LOG.info("Inside vaildate form :: Please enter call back description");
		//
		//		}
		//		if (null == leadGenerationCallBackRequestForm.getFirstName() || leadGenerationCallBackRequestForm.getFirstName().isEmpty())
		//		{
		//			GlobalMessages.addErrorMessage(model, "Please enter your first Name");
		//			LOG.info("Inside vaildate form :: Please enter your first Name");
		//
		//		}
		//
		//		if (null == leadGenerationCallBackRequestForm.getLastName() || leadGenerationCallBackRequestForm.getLastName().isEmpty())
		//		{
		//			GlobalMessages.addErrorMessage(model, "Please enter your last Name");
		//			LOG.info("Inside vaildate form :: Please enter your last Name");
		//
		//		}
		//
		//		if (null == leadGenerationCallBackRequestForm.getEmail() || leadGenerationCallBackRequestForm.getEmail().isEmpty())
		//		{
		//			GlobalMessages.addErrorMessage(model, "Please enter your EMail Id ");
		//			LOG.info("Inside vaildate form :: Please enter your last EMail Id");
		//
		//		}
		//
		//		if (null == leadGenerationCallBackRequestForm.getPhoneno() || leadGenerationCallBackRequestForm.getPhoneno().isEmpty())
		//		{
		//			GlobalMessages.addErrorMessage(model, "Please enter your Phone Number");
		//			LOG.info("Inside vaildate form :: Please enter your last Phone Number");
		//
		//		}
		//
		if (GlobalMessages.hasErrorMessage(model))
		{
			LOG.info("Inside vaildate form :: returning failure");
			return false;
		}
		else
		{
			LOG.info("Inside vaildate form :: returning success");
			return true;
		}


	}

	private FMLeadGenerationCallBackData populateLeadGenerationCallBackData(
			final LeadGenerationCallBackRequestForm leadGenerationCallBackRequestForm)
	{

		LOG.info("FORM leadGenerationCallBackRequestForm FORM ::" + leadGenerationCallBackRequestForm.getAddressline1() + " ::"
				+ leadGenerationCallBackRequestForm.getAddressline2() + ":: " + leadGenerationCallBackRequestForm.getCity() + ":: "
				+ leadGenerationCallBackRequestForm.getPhoneno());

		final FMLeadGenerationCallBackData fMLeadGenerationCallBackData = new FMLeadGenerationCallBackData();


		LOG.info("Inside populate Call Back CreationData:");

		//fMLeadGenerationCallBackData.setSubjects(leadGenerationCallBackRequestForm.getSubjects());
		fMLeadGenerationCallBackData.setLeadSubjects(leadGenerationCallBackRequestForm.getSubjects());
		LOG.info("Subjects::" + leadGenerationCallBackRequestForm.getSubjects());
		fMLeadGenerationCallBackData.setCallBackDescription(leadGenerationCallBackRequestForm.getCallBackDescription());
		LOG.info("CallBackDescription::" + leadGenerationCallBackRequestForm.getCallBackDescription());
		fMLeadGenerationCallBackData.setFirstName(leadGenerationCallBackRequestForm.getFirstName());
		LOG.info("FirstName" + leadGenerationCallBackRequestForm.getFirstName());
		fMLeadGenerationCallBackData.setLastName(leadGenerationCallBackRequestForm.getLastName());
		LOG.info("LastName::" + leadGenerationCallBackRequestForm.getLastName());
		fMLeadGenerationCallBackData.setJobtitle(leadGenerationCallBackRequestForm.getJobtitle());
		LOG.info("Jobtitle::" + leadGenerationCallBackRequestForm.getJobtitle());
		fMLeadGenerationCallBackData.setDepartment(leadGenerationCallBackRequestForm.getDepartment());
		LOG.info("Department::" + leadGenerationCallBackRequestForm.getDepartment());

		//fMLeadGenerationCallBackData.setContactNumber(leadGenerationCallBackRequestForm.getPhoneno());

		fMLeadGenerationCallBackData.setEmail(leadGenerationCallBackRequestForm.getEmail());



		if (leadGenerationCallBackRequestForm.getAccCode() != null)
		{
			fMLeadGenerationCallBackData.setUnit(companyB2BCommerceFacade.getUnitForUid(leadGenerationCallBackRequestForm
					.getAccCode()));
			LOG.info("SapActCode::" + leadGenerationCallBackRequestForm.getAccCode());
		}

		final AddressData customeraddress = new AddressData();
		customeraddress.setLine1(leadGenerationCallBackRequestForm.getAddressline1());
		customeraddress.setLine2(leadGenerationCallBackRequestForm.getAddressline2());
		customeraddress.setTown(leadGenerationCallBackRequestForm.getCity());
		LOG.info("FORM FORM CITY" + leadGenerationCallBackRequestForm.getCity());

		final RegionData state = new RegionData();
		state.setIsocodeShort(leadGenerationCallBackRequestForm.getState());
		state.setIsocode(leadGenerationCallBackRequestForm.getState());
		LOG.info("iso" + state.getIsocode());
		customeraddress.setRegion(state);
		customeraddress.setPostalCode(leadGenerationCallBackRequestForm.getZipCode());

		final CountryData country = new CountryData();
		country.setIsocode(leadGenerationCallBackRequestForm.getCountry());
		LOG.info("FROM FORM country" + leadGenerationCallBackRequestForm.getCountry());
		LOG.info("FROM DATA country" + country.getIsocode());

		customeraddress.setCountry(country);
		customeraddress.setPhone(leadGenerationCallBackRequestForm.getPhoneno());
		customeraddress.setCompanyName(leadGenerationCallBackRequestForm.getCompanyName());
		customeraddress.setVisibleInAddressBook(true);

		fMLeadGenerationCallBackData.setDefaultShippingAddress(customeraddress);

		return fMLeadGenerationCallBackData;
	}

	public List getQueryList()
	{
		final List leadGenerationSubjects = new ArrayList();

		leadGenerationSubjects.add("Sales Inquiry");
		leadGenerationSubjects.add("Order Inquiry");
		leadGenerationSubjects.add("Product Inquiry");
		leadGenerationSubjects.add("Promotional Inquiry");
		leadGenerationSubjects.add("Other");

		return leadGenerationSubjects;




	}

	// Balaji---End Call Back  - 
	/**
	 * @return the i18NFacade
	 */
	public I18NFacade getI18NFacade()
	{
		return i18NFacade;
	}

	/**
	 * @param i18nFacade
	 *           the i18NFacade to set
	 */
	public void setI18NFacade(final I18NFacade i18nFacade)
	{
		i18NFacade = i18nFacade;
	}


}
