/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.federalmogul.storefront.controllers.pages.checkout;

import de.hybris.platform.acceleratorservices.config.SiteConfigService;
import de.hybris.platform.acceleratorservices.controllers.page.PageType;
import de.hybris.platform.b2b.services.B2BOrderService;
import de.hybris.platform.b2bacceleratorfacades.company.B2BCommerceUserFacade;
import de.hybris.platform.b2bacceleratorfacades.company.CompanyB2BCommerceFacade;
import de.hybris.platform.b2bacceleratorfacades.order.data.B2BUnitData;
import de.hybris.platform.b2bacceleratorservices.company.CompanyB2BCommerceService;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.CartModificationData;
import de.hybris.platform.commercefacades.order.data.OrderData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductFacade;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.UserFacade;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.core.GenericSearchConstants.LOG;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.security.PrincipalGroupModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.order.InvalidCartException;
import de.hybris.platform.orderprocessing.model.OrderProcessModel;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.federalmogul.core.model.FMCustomerAccountModel;
import com.federalmogul.core.model.FMCustomerModel;
import com.federalmogul.core.model.TSCLocationModel;
import com.federalmogul.core.payment.utils.FMDigestUtils;
import com.federalmogul.core.product.PartService;
import com.federalmogul.facades.account.FMCustomerFacade;
import com.federalmogul.facades.order.FMDistrubtionCenterFacade;
import com.federalmogul.facades.order.data.DistrubtionCenterData;
import com.federalmogul.facades.order.data.FMTempPdfViewData;
import com.federalmogul.facades.user.data.FMCustomerData;
import com.federalmogul.falcon.core.model.FMCorporateModel;
import com.federalmogul.falcon.core.model.FMDCCenterModel;
import com.federalmogul.falcon.core.model.FMDCShippingModel;
import com.federalmogul.falcon.core.model.FMDistrubtionCenterModel;
import com.federalmogul.falcon.core.model.FMPartModel;
import com.federalmogul.hybris.facades.order.FMCheckoutFacade;
import com.federalmogul.storefront.annotations.RequireHardLogIn;
import com.federalmogul.storefront.constants.WebConstants;
import com.federalmogul.storefront.controllers.ControllerConstants;
import com.federalmogul.storefront.controllers.util.GlobalMessages;
import com.federalmogul.storefront.forms.AddressForm;
import com.federalmogul.storefront.forms.HopPaymentDetailsForm;
import com.federalmogul.storefront.security.B2BUserGroupProvider;
import com.fmo.wom.business.as.OrderASUSCanImpl;
import com.fmo.wom.common.dao.AuditInterceptor;
import com.fmo.wom.common.exception.WOMExternalSystemException;
import com.fmo.wom.domain.AddressBO;
import com.fmo.wom.domain.BillToAcctBO;
import com.fmo.wom.domain.CountryBO;
import com.fmo.wom.domain.CreditCheckBO;
import com.fmo.wom.domain.CustomerSalesOrgBO;
import com.fmo.wom.domain.DistributionCenterBO;
import com.fmo.wom.domain.InventoryBO;
import com.fmo.wom.domain.ItemBO;
import com.fmo.wom.domain.ManualShipToAddressBO;
import com.fmo.wom.domain.NabsShippingCodeBO;
import com.fmo.wom.domain.OrderBO;
import com.fmo.wom.domain.PartBO;
import com.fmo.wom.domain.PriceBO;
import com.fmo.wom.domain.QuantityBO;
import com.fmo.wom.domain.SalesOrganizationBO;
import com.fmo.wom.domain.SapAcctBO;
import com.fmo.wom.domain.SapShippingCodeBO;
import com.fmo.wom.domain.ShipToAcctBO;
import com.fmo.wom.domain.ShippingCodeBO;
import com.fmo.wom.domain.UserAccountBO;
import com.fmo.wom.domain.enums.BackOrderPolicy;
import com.fmo.wom.domain.enums.ExternalSystem;
import com.fmo.wom.domain.enums.OrderSource;
import com.fmo.wom.domain.enums.OrderType;
import com.fmo.wom.domain.enums.UsageType;
import com.fmo.wom.integration.SAPService;
import com.fmo.wom.integration.nabs.action.GetMasterOrderNumberAction;


/**
 * MultiStepCheckoutController
 */
@Controller
@Scope("tenant")
@RequestMapping(value = "/checkout/multi")
@SessionAttributes("dc")
public class MultiStepCheckoutController extends AbstractCheckoutController
{
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(MultiStepCheckoutController.class);
	int linenumber = 1;
	private static final String MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL = "multiStepCheckout";
	private static final String REDIRECT_URL_CART = FORWARD_PREFIX + "/cart";
	private static final String REDIRECT_URL_CHECKOUT_CONFIRMATION = REDIRECT_PREFIX + "/checkout/multi/reviewPlaceOrder";
	private static final String CARRIER_CODE_UPS = "UPS";
	private static final String CARRIER_ACCOUNTCODE_DUMMY = "dummy";
	private static final String CARRIER_ACCOUNTCODE_NA = "NA";
	private static final String CARRIER_NAME_COLLECT = "Collect";
	private static final String CARRIER_NAME_FEDXCOLLECT = "FedEx Collect";
	private static final String CARRIER_CODE_FED = "FED";



	FMTempPdfViewData globalTempDataForSavePdf = new FMTempPdfViewData();
	@Autowired
	private FMDistrubtionCenterFacade fmDistrubtionCenterFacade;

	@Resource(name = "b2bUserGroupProvider")
	private B2BUserGroupProvider b2bUserGroupProvider;

	@Resource(name = "b2bProductFacade")
	private ProductFacade productFacade;

	/*
	 * @Autowired private CustomerFacade customerFacade;
	 */

	@Autowired
	private CommonI18NService commonI18NService;

	@Autowired
	private UserService userService;

	@Autowired
	private ModelService modelService;

	@Autowired
	private EnumerationService enumerationService;

	@Resource(name = "userFacade")
	protected UserFacade userFacade;

	@Autowired
	private CartService cartService;

	/*
	 * @Autowired private OrderService orderService;
	 */
	@Resource
	protected FMCustomerFacade fmCustomerFacade;

	@Autowired
	private FMCheckoutFacade checkoutFacade;

	@Autowired
	protected B2BCommerceUserFacade b2bCommerceUserFacade;

	@Deprecated
	@Resource(name = "cartFacade")
	private CartFacade cartFacade;

	@Resource(name = "cartFacade")
	private de.hybris.platform.b2bacceleratorfacades.api.cart.CartFacade b2bCartFacade;

	@Autowired
	private SessionService service;

	/**
	 * @return the service
	 */
	public SessionService getService()
	{
		return service;
	}

	/**
	 * @param service
	 *           the service to set
	 */
	public void setService(final SessionService service)
	{
		this.service = service;
	}

	@Resource(name = "partService")
	private PartService partService;

	@Resource(name = "b2bCommerceFacade")
	protected CompanyB2BCommerceFacade companyB2BCommerceFacade;
	@Resource
	private CustomerAccountService customerAccountService;

	@Resource
	protected CompanyB2BCommerceService companyB2BCommerceService;
	@Resource(name = "siteConfigService")
	private SiteConfigService siteConfigService;
	/*
	 * @Autowired private BaseStoreService baseStoreService;
	 */

	/*
	 * @Autowired private OrderFacade orderFacade;
	 */

	@Resource
	private B2BOrderService b2bOrderService;

	/**
	 * @return the customerAccountService
	 */
	public CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}

	/**
	 * @param customerAccountService
	 *           the customerAccountService to set
	 */
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}



	/**
	 * This is the entry point (first page) for the the multi-step checkout process. The page returned by this call acts
	 * as a template landing page and an example for actual implementation.
	 *
	 * @param model
	 *           - the model for the view.
	 * @return - the multi-step checkout page if the basket contains any items or the cart page otherwise.
	 * @throws CMSItemNotFoundException
	 *            - when a CMS page is not found
	 */



	@RequestMapping(method = RequestMethod.GET)
	public String nextPageBasedOnUser()
	{
		final UserModel user = userService.getCurrentUser();

		final boolean isUserAnonymous = user == null || userService.isAnonymousUser(user);

		final Set<PrincipalGroupModel> groupss = user.getGroups();
		final Iterator groupsiterator = groupss.iterator();
		while (groupsiterator.hasNext())
		{
			final PrincipalGroupModel groupmodel = (PrincipalGroupModel) groupsiterator.next();

			LOG.info("Groups:" + groupmodel.getUid());

			if (!isUserAnonymous)
			{

				if ("FMB2BB".equals(groupmodel.getUid()))
				{
					LOG.info("Hitting B2B");
					return REDIRECT_PREFIX + "/checkout/multi/deliverymethod";
				}
				else if ("FMB2SB".equals(groupmodel.getUid()))
				{
					LOG.info("Hitting B2b");
					return REDIRECT_PREFIX + "/checkout/multi/deliverymethod";
				}
				else if ("FMB2C".equals(groupmodel.getUid()))
				{
					LOG.info("Hitting B2C");
					return REDIRECT_PREFIX + "/checkout/multi/shipto";
				}
				else if ("FMCSR".equals(groupmodel.getUid()))
				{
					LOG.info("Hitting B2B");
					return REDIRECT_PREFIX + "/checkout/multi/deliverymethod";
				}
			}
		}
		return REDIRECT_PREFIX + "/checkout/multi/deliverymethod";
	}

	@RequestMapping(value = "/shipto", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@RequireHardLogIn
	public String gotoShipingAddress(final Model model) throws CMSItemNotFoundException
	{
		if (!b2bUserGroupProvider.isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.invalid.accountType");
			return FORWARD_PREFIX + "/cart";
		}


		//will through error as we need to take address from b2c cutomer
		B2BUnitData shipToUnit = null;
		AddressData shipToAddress = new AddressData();
		final B2BUnitData parentUnit = null;
		AddressData soldToAddress = new AddressData();
		B2BUnitData soldToUnit = null;
		shipToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
				WebConstants.SHIPTO_ACCOUNT_ID));

		for (final AddressData billToAddresss : shipToUnit.getAddresses())
		{
			shipToAddress = billToAddresss;
		}



		if (hasItemsInCart())
		{
			final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

			soldToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
					WebConstants.SOLDTO_ACCOUNT_ID));
			if (soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
			{
				//companyB2BCommerceFacade.getb
				//soldToUnit = getB2BUnitConverter().convert(soldToUnitModel);
				if (soldToUnit.getAddresses() != null)
				{
					for (final AddressData soldToAddresss : soldToUnit.getAddresses())
					{
						soldToAddress = soldToAddresss;
					}
				}
			}
			else
			{
				soldToAddress = shipToAddress;
			}

			if (soldToAddress != null && soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
			{
				model.addAttribute("soldToAddress", soldToAddress);
				model.addAttribute("soldToUnit", soldToUnit);
			}
			else
			{
				model.addAttribute("soldToAddress", shipToAddress);
				model.addAttribute("soldToUnit", shipToUnit);
			}
			model.addAttribute("billToAddress", shipToAddress);
			model.addAttribute("soldToUnit", parentUnit);
			model.addAttribute("shipToAddress", shipToAddress);
			model.addAttribute("shipToUnit", shipToUnit);
			model.addAttribute("allItems", cartData.getEntries());
			model.addAttribute("metaRobots", "no-index,no-follow");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			return ControllerConstants.Views.Pages.MultiStepCheckout.checkoutShiptoAddress;
		}

		LOG.info("Missing or empty cart");
		return FORWARD_PREFIX + "/cart";
	}

	@RequestMapping(value = "/deliverymethod", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@RequireHardLogIn
	public String gotoDeliverymethod(@ModelAttribute("addressForm") final AddressForm addressForm, final Model model,
			final HttpServletRequest request) throws CMSItemNotFoundException
	{

		LOG.info("");

		final String pickUpStore = getSessionService().getAttribute("pickUpStore");
		final AddressData storeAddress = (AddressData) getSessionService().getAttribute("tscaddress");
		if (!b2bUserGroupProvider.isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.invalid.accountType");
			return FORWARD_PREFIX + "/cart";
		}
		if (hasItemsInCart())
		{

			if (LOG.isDebugEnabled())
			{
				LOG.debug("gotoDeliverymethod hasItemsInCart is true");
			}

			LOG.info("addressForm.getFirstName()" + addressForm.getFirstName());

			//final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

			CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

			//Retriving address,
			final UserModel user = userService.getCurrentUser();
			final boolean isUserAnonymous = user == null || userService.isAnonymousUser(user);
			AddressData shipToAddress = new AddressData();
			AddressData soldToAddress = new AddressData();
			B2BUnitData shipToUnit = null;
			B2BUnitData soldToUnit = null;
			String carrierAccCode = null;
			//final B2BUnitModel shipToUnitModel = null;
			if (!isUserAnonymous)
			{
				shipToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SHIPTO_ACCOUNT_ID));

				for (final AddressData shipToAddresss : shipToUnit.getAddresses())
				{
					shipToAddress = shipToAddresss;
				}

				final AddressData manualShipToAddress = (AddressData) getSessionService().getAttribute("shiptToAddress");
				if (manualShipToAddress != null)
				{
					shipToAddress = manualShipToAddress;
					model.addAttribute("manualShipTo", true);
					//shipToUnit.setName(manualShipToAddress.getFirstName() + " " + manualShipToAddress.getLastName());
					shipToUnit.setName(manualShipToAddress.getFirstName());
				}

				if (shipToAddress != null && null == pickUpStore)
				{
					LOG.info("Inside Delivery address");
					//cartData = getCheckoutFlowFacade().getCheckoutCart();
					final AddressData deliveryAddres = new AddressData();
					deliveryAddres.setFirstName(shipToUnit.getName());
					deliveryAddres.setPostalCode(shipToAddress.getPostalCode());
					deliveryAddres.setTown(shipToAddress.getTown());
					deliveryAddres.setLine1(shipToAddress.getLine1());
					deliveryAddres.setLine2(shipToAddress.getLine2());
					deliveryAddres.setCountry(shipToAddress.getCountry());
					deliveryAddres.setRegion(shipToAddress.getRegion());
					deliveryAddres.setShippingAddress(true);
					deliveryAddres.setBillingAddress(false);
					deliveryAddres.setDefaultAddress(Boolean.TRUE.equals(shipToAddress.isDefaultAddress()));
					userFacade.addAddress(deliveryAddres);
					getCheckoutFlowFacade().setDeliveryAddress(deliveryAddres);

				}

				if (null != pickUpStore && "true".equalsIgnoreCase(pickUpStore) && null != storeAddress)
				{
					model.addAttribute("storeInvItemBO", getSessionService().getAttribute("tscInventoryListItemBO"));
					storeAddress.setShippingAddress(true);
					storeAddress.setBillingAddress(false);
					storeAddress.setDefaultAddress(Boolean.TRUE.equals(shipToAddress.isDefaultAddress()));
					userFacade.addAddress(storeAddress);
					getCheckoutFlowFacade().setDeliveryAddress(storeAddress);

				}

				//rahul carrier Code
				soldToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SOLDTO_ACCOUNT_ID));
				final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
						.getUnitForUid(soldToUnit.getUid());
				carrierAccCode = fmCustomerAccModel.getCarrierAccountCode();
				getSessionService().setAttribute("carrierAccCode", carrierAccCode);
				LOG.info("carrierAccCode" + carrierAccCode);
				//mahaveer DC List...
				getDistrubtionCenter(cartData, model);
				cartData = getCheckoutFlowFacade().getCheckoutCart();
				if (!"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
				{
					//companyB2BCommerceFacade.getb
					//soldToUnit = getB2BUnitConverter().convert(soldToUnitModel);
					if (soldToUnit.getAddresses() != null)
					{
						for (final AddressData soldToAddresss : soldToUnit.getAddresses())
						{
							soldToAddress = soldToAddresss;
						}
					}
				}
				else
				{
					soldToAddress = shipToAddress;
				}
			}
			if (soldToAddress != null && soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
			{
				model.addAttribute("soldToAddress", soldToAddress);
				model.addAttribute("soldToUnit", soldToUnit);
			}
			else
			{
				model.addAttribute("soldToAddress", shipToAddress);
				model.addAttribute("soldToUnit", shipToUnit);
			}
			model.addAttribute("cartData", cartData);
			model.addAttribute("carrierAccCode", carrierAccCode);
			model.addAttribute("allItems", cartData.getEntries());
			model.addAttribute("metaRobots", "no-index,no-follow");
			model.addAttribute("storePickupAddress", storeAddress);
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));

			// Build up the HOP form data and render page containing form
			final HopPaymentDetailsForm hopPaymentDetailsForm = new HopPaymentDetailsForm();

			try
			{
				setupHostOrderPostPage(hopPaymentDetailsForm, model, shipToAddress, soldToAddress, shipToUnit, request, cartData);
			}
			catch (final Exception e)
			{
				LOG.error("Failed to build beginCreateSubscription request", e);
				GlobalMessages.addErrorMessage(model, "checkout.multi.paymentMethod.addPaymentDetails.generalError");
				model.addAttribute("hopPaymentDetailsForm", hopPaymentDetailsForm);
			}
			/*
			 * List<? extends DeliveryModeData> deliveryModes = getCheckoutFlowFacade().getSupportedDeliveryModes(); if
			 * (deliveryModes == null) { deliveryModes = Collections.<ZoneDeliveryModeData> emptyList(); }
			 *
			 * model.addAttribute("deliveryModes", deliveryModes);
			 */
			model.addAttribute("pickUpStore", pickUpStore);
			final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
			model.addAttribute("itemBO", itemBOSession);
			if (getSessionService().getAttribute("logedUserType").equals("ShipTo"))
			{
				model.addAttribute("logedUserType", "ShipTo");
			}


			return ControllerConstants.Views.Pages.MultiStepCheckout.checkoutMultiDeliveryMethod;
		}
		LOG.info("Missing or empty cart");
		return FORWARD_PREFIX + "/cart";
	}

	@RequestMapping(value = "/paymentDetails", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@RequireHardLogIn
	public String gotoPaymentDetails(final Model model) throws CMSItemNotFoundException
	{
		if (!b2bUserGroupProvider.isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.invalid.accountType");
			return FORWARD_PREFIX + "/cart";
		}
		final String pickUpStore = getSessionService().getAttribute("pickUpStore");
		final AddressData storeAddress = (AddressData) getSessionService().getAttribute("tscaddress");
		model.addAttribute("pickUpStore", pickUpStore);
		model.addAttribute("storePickupAddress", storeAddress);

		if (hasItemsInCart())
		{
			final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

			model.addAttribute("cartData", cartData);
			model.addAttribute("allItems", cartData.getEntries());
			model.addAttribute("metaRobots", "no-index,no-follow");
			final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
			model.addAttribute("itemBO", itemBOSession);
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));

			return ControllerConstants.Views.Pages.MultiStepCheckout.checkoutMultiPaymentDetails;

		}
		LOG.info("Missing or empty cart");
		return FORWARD_PREFIX + "/cart";
	}

	@RequestMapping(value = "/reviewPlaceOrder", method =
	{ RequestMethod.POST, RequestMethod.GET })
	@RequireHardLogIn
	public String gotoReviewPlaceOrder(final Model model, @RequestParam("poNumber") final String poNumber,
			@RequestParam("orderInstruction") final String orderInstruction) throws CMSItemNotFoundException
	{

		LOG.info("gotoReviewPlaceOrder ordercode :: ");
		//final OrderModel orderModel = getCustomerAccountService().getOrderForCode((CustomerModel) userService.getCurrentUser(),
		//ordercode, baseStoreService.getCurrentBaseStore());
		if (!b2bUserGroupProvider.isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.invalid.accountType");
			return FORWARD_PREFIX + "/cart";
		}

		//final CartModel cartModel = cartService.getSessionCart();

		final String pickUpStore = getSessionService().getAttribute("pickUpStore");
		final AddressData storeAddress = (AddressData) getSessionService().getAttribute("tscaddress");
		model.addAttribute("pickUpStore", pickUpStore);
		model.addAttribute("storePickupAddress", storeAddress);
		final List<ItemBO> ineventoryList = (List<ItemBO>) getSessionService().getAttribute("tscInventoryListItemBO");
		if (null != pickUpStore && "true".equalsIgnoreCase(pickUpStore) && null != storeAddress && null != ineventoryList
				&& ineventoryList.size() > 0)
		{
			partModification(ineventoryList);
		}

		final UserModel user = userService.getCurrentUser();
		final boolean isUserAnonymous = user == null || userService.isAnonymousUser(user);
		AddressData shipToAddress = new AddressData();
		AddressData soldToAddress = new AddressData();
		B2BUnitData shipToUnit = null;
		B2BUnitData soldToUnit = null;
		if (hasItemsInCart())
		{
			CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
			if (!isUserAnonymous)
			{
				shipToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SHIPTO_ACCOUNT_ID));

				for (final AddressData shipToAddresss : shipToUnit.getAddresses())
				{
					shipToAddress = shipToAddresss;
				}

				soldToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SOLDTO_ACCOUNT_ID));
				if (soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
				{
					if (soldToUnit.getAddresses() != null)
					{
						for (final AddressData soldToAddresss : soldToUnit.getAddresses())
						{
							soldToAddress = soldToAddresss;
						}
					}
				}
				else
				{
					soldToAddress = shipToAddress;
					soldToUnit = shipToUnit;
				}
			}
			if (soldToAddress != null && soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
			{
				model.addAttribute("soldToAddress", soldToAddress);
				model.addAttribute("soldToUnit", soldToUnit);
				model.addAttribute("billToAddress", soldToAddress);
			}
			else
			{
				model.addAttribute("soldToAddress", shipToAddress);
				model.addAttribute("soldToUnit", shipToUnit);
				model.addAttribute("billToAddress", shipToAddress);
			}
			model.addAttribute("carrierAccCode", getSessionService().getAttribute("carrierAccCode"));
			//manual Shipping address
			final AddressData manualShipToAddress = (AddressData) getSessionService().getAttribute("shiptToAddress");
			if (manualShipToAddress != null)
			{
				shipToAddress = manualShipToAddress;
				model.addAttribute("manualShipTo", true);
				shipToUnit.setName(manualShipToAddress.getFirstName());
				shipToUnit.setUid(null);
			}
			model.addAttribute("shipToAddress", shipToAddress);
			model.addAttribute("shipToUnit", shipToUnit);

			if (cartData != null && cartData.getEntries() != null && !cartData.getEntries().isEmpty())
			{
				model.addAttribute("allItems", cartData.getEntries());
			}

			//CREDIT check

			final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
					.getUnitForUid((String) getSessionService().getAttribute(WebConstants.SOLDTO_ACCOUNT_ID));
			final SalesOrganizationBO salesOrg = new SalesOrganizationBO();
			salesOrg.setCode(fmCustomerAccModel.getSalesorg());
			try
			{
				final CreditCheckBO creditCheckBO = SAPService.doCreditCheck(fmCustomerAccModel.getUid(), salesOrg);
				LOG.info(" Check out Page creditCheckBO .. getCreditBlock :: " + creditCheckBO.getCreditBlock());
				LOG.info("Checkout Page :  Order Blocked ... :: " + creditCheckBO.getOrderBlock());
				model.addAttribute(WebConstants.FM_CREDIT_BLOCK, creditCheckBO.getCreditBlock());
				model.addAttribute(WebConstants.FM_ORDER_BLOCK, creditCheckBO.getOrderBlock());
			}
			catch (final WOMExternalSystemException e)
			{
				model.addAttribute(WebConstants.FM_CREDIT_BLOCK, "false");
				model.addAttribute(WebConstants.FM_ORDER_BLOCK, "false");
			}
			catch (final Exception e)
			{
				model.addAttribute(WebConstants.FM_CREDIT_BLOCK, "false");
				model.addAttribute(WebConstants.FM_ORDER_BLOCK, "false");
			}


			setOrderPODetails(cartData.getCode(), poNumber, orderInstruction);
			cartData = getCheckoutFlowFacade().getCheckoutCart();
			model.addAttribute("cartData", cartData);
			model.addAttribute("pickUpStore", pickUpStore);
			final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
			model.addAttribute("itemBO", itemBOSession);
			model.addAttribute("metaRobots", "no-index,no-follow");

			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));

			if (getSessionService().getAttribute("logedUserType").equals("ShipTo"))
			{
				model.addAttribute("logedUserType", "ShipTo");
			}

			return ControllerConstants.Views.Pages.MultiStepCheckout.checkoutMultiReviewPlaceOrder;

		}
		LOG.info("Missing or empty cart");
		return REDIRECT_PREFIX + "/cart";
	}

	@RequestMapping(value = "/orderConfirmation", method = RequestMethod.GET)
	@RequireHardLogIn
	public String gotoOrderConfirmation(final Model model) throws CMSItemNotFoundException
	{
		if (!b2bUserGroupProvider.isCurrentUserAuthorizedToCheckOut())
		{
			GlobalMessages.addErrorMessage(model, "checkout.error.invalid.accountType");
			return FORWARD_PREFIX + "/cart";
		}
		OrderData orderData = null;
		final UserModel user = userService.getCurrentUser();
		final boolean isUserAnonymous = user == null || userService.isAnonymousUser(user);
		AddressData shipToAddress = new AddressData();
		AddressData soldToAddress = new AddressData();
		B2BUnitData shipToUnit = null;
		B2BUnitData soldToUnit = null;
		if (hasItemsInCart())
		{
			final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
			if (!isUserAnonymous)
			{
				final String pickUpStore = getSessionService().getAttribute("pickUpStore");
				final AddressData storeAddress = (AddressData) getSessionService().getAttribute("tscaddress");
				model.addAttribute("pickUpStore", pickUpStore);
				model.addAttribute("storePickupAddress", storeAddress);

				shipToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SHIPTO_ACCOUNT_ID));

				for (final AddressData shipToAddresss : shipToUnit.getAddresses())
				{
					shipToAddress = shipToAddresss;
				}
				//if (shipToAddress != null && getCheckoutFlowFacade().setDeliveryAddress(shipToAddress))
				//{
				//	cartData = getCheckoutFlowFacade().getCheckoutCart();

				//}



				soldToUnit = companyB2BCommerceFacade.getUnitForUid((String) getSessionService().getAttribute(
						WebConstants.SOLDTO_ACCOUNT_ID));
				if (soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
				{
					if (soldToUnit.getAddresses() != null)
					{
						for (final AddressData soldToAddresss : soldToUnit.getAddresses())
						{
							soldToAddress = soldToAddresss;
						}
					}
				}
				else
				{
					soldToAddress = shipToAddress;
					soldToUnit = shipToUnit;
				}
			}
			if (soldToAddress != null && soldToUnit != null && !"16427113580".equalsIgnoreCase(soldToUnit.getUid()))
			{
				model.addAttribute("soldToAddress", soldToAddress);
				getSessionService().setAttribute("soldToAddress", soldToAddress);
				model.addAttribute("soldToUnit", soldToUnit);
				getSessionService().setAttribute("soldToUnit", soldToUnit);
				model.addAttribute("billToAddress", soldToAddress);
				getSessionService().setAttribute("billToAddress", soldToAddress);
			}
			else
			{
				model.addAttribute("soldToAddress", shipToAddress);
				getSessionService().setAttribute("soldToAddress", shipToAddress);
				model.addAttribute("soldToUnit", shipToUnit);
				getSessionService().setAttribute("soldToUnit", shipToUnit);
				model.addAttribute("billToAddress", shipToAddress);
				getSessionService().setAttribute("billToAddress", shipToAddress);
			}
			//manual Shipping address

			final AddressData manualShipToAddress = (AddressData) getSessionService().getAttribute("shiptToAddress");
			if (manualShipToAddress != null)
			{
				shipToAddress = manualShipToAddress;
				model.addAttribute("manualShipTo", true);
				shipToUnit.setName(manualShipToAddress.getFirstName());
				shipToUnit.setUid(null);
			}
			model.addAttribute("shipToAddress", shipToAddress);
			getSessionService().setAttribute("shipToAddress", shipToAddress);
			model.addAttribute("shipToUnit", shipToUnit);
			getSessionService().setAttribute("shipToUnit", shipToUnit);

			try
			{
				womProcessOrder(model, soldToUnit, shipToUnit, soldToAddress, shipToAddress);
			}
			catch (final Exception e)
			{
				final CartModel cartModel = cartService.getSessionCart();
				LOG.error(e.getMessage());
				final String[] parms =
				{ e.getMessage() };
				GlobalMessages.addErrorMessage(model,
						getMessageSource().getMessage("fm.checkout.externalsystem.error", parms, getI18nService().getCurrentLocale()));
				return FORWARD_PREFIX + "/checkout/multi/reviewPlaceOrder?poNumber=" + cartModel.getCustponumber()
						+ "&orderInstruction=" + cartModel.getOrdercomments();

			}

			model.addAttribute("carrierAccCode", getSessionService().getAttribute("carrierAccCode"));


			model.addAttribute("shipToAddress", shipToAddress);
			getSessionService().setAttribute("shipToAddress", shipToAddress);
			model.addAttribute("shipToUnit", shipToUnit);
			getSessionService().setAttribute("shipToUnit", shipToUnit);
			model.addAttribute("cartData", cartData);
			model.addAttribute("allItems", cartData.getEntries());
			final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
			model.addAttribute("itemBO", itemBOSession);
			model.addAttribute("metaRobots", "no-index,no-follow");
			storeCmsPageInModel(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			setUpMetaDataForContentPage(model, getContentPageForLabelOrId(MULTI_STEP_CHECKOUT_CMS_PAGE_LABEL));
			try
			{
				orderData = getCheckoutFlowFacade().placeOrder();
				final List<String> groupUID = new ArrayList<String>();
				final Set<PrincipalGroupModel> groupss = user.getGroups();
				for (final PrincipalGroupModel groupModel : groupss)
				{
					final String groupId = groupModel.getUid();
					groupUID.add(groupId);
				}
				final OrderModel orderModel = b2bOrderService.getOrderByCode(orderData.getCode());
				if (groupUID.contains("FMCSR")){
					final FMCustomerAccountModel CustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
					.getUnitForUid(soldToUnit.getUid());
					orderModel.setUnit(CustomerAccModel);
					modelService.save(orderModel);

				}
				getSessionService().setAttribute("orderData", orderData);

				LOG.info("******ORDER EMAIL START****************");

				final OrderProcessModel orderProcessModel = (OrderProcessModel) getBusinessProcessService().createProcess(
						"b2bOrderConfirmationEmailProcess" + System.currentTimeMillis(), "b2bOrderConfirmationEmailProcess");

				orderProcessModel.setOrder(orderModel);
				LOG.info("************orderProcessModel:" + orderProcessModel);
				modelService.save(orderProcessModel);
				getBusinessProcessService().startProcess(orderProcessModel);

				LOG.info("******ORDER EMAIL END*****");
			}
			catch (final InvalidCartException e)
			{
				LOG.error("in InvalidCartException::" + e.getMessage());
			}
			if (getSessionService().getAttribute("logedUserType").equals("ShipTo"))
			{
				model.addAttribute("logedUserType", "ShipTo");
				globalTempDataForSavePdf.setShipToLogin("yes");
			}

			model.addAttribute("orderData", orderData);
			model.addAttribute("pageType", PageType.ORDERCONFIRMATION.name());

			globalTempDataForSavePdf.setOrderData(orderData);

			globalTempDataForSavePdf.setShipToAddress((AddressData) getSessionService().getAttribute("shipToAddress"));
			globalTempDataForSavePdf.setSoldToAddress((AddressData) getSessionService().getAttribute("soldToAddress"));
			globalTempDataForSavePdf.setShipToUnit((B2BUnitData) getSessionService().getAttribute("shipToUnit"));
			globalTempDataForSavePdf.setSoldToUnit((B2BUnitData) getSessionService().getAttribute("soldToUnit"));
			globalTempDataForSavePdf.setCartData(setPriceType(cartData));
			return ControllerConstants.Views.Pages.MultiStepCheckout.checkoutMultiOrderConfirmation;

		}
		LOG.info("Missing or empty cart");
		return FORWARD_PREFIX + "/cart";
	}

	protected void setupHostOrderPostPage(final HopPaymentDetailsForm hopPaymentDetailsForm, final Model model,
			final AddressData shipToAddress, final AddressData soldToAddress, final B2BUnitData shipToUnit,
			final HttpServletRequest request, final CartData cartData)
	{
		try
		{

			//final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();

			hopPaymentDetailsForm.setParameters(new HashMap<String, String>());
			hopPaymentDetailsForm.setUnSignedParams(new HashMap<String, String>());

			hopPaymentDetailsForm.getParameters().put("access_key", checkoutFacade.getAccessKey());
			hopPaymentDetailsForm.getParameters().put("reference_number", checkoutFacade.getReferenceNumber());
			hopPaymentDetailsForm.getParameters().put("profile_id", checkoutFacade.getProfileId());

			hopPaymentDetailsForm.getParameters().put("signed_date_time", String.valueOf(checkoutFacade.getUTCDateTime()));

			hopPaymentDetailsForm.getParameters().put("currency", commonI18NService.getCurrentCurrency().getIsocode());
			hopPaymentDetailsForm.getParameters().put("transaction_uuid", UUID.randomUUID().toString());
			hopPaymentDetailsForm.getParameters().put("locale", "en");

			hopPaymentDetailsForm.getParameters().put("signed_field_names", checkoutFacade.getSignedFieldNames());
			hopPaymentDetailsForm.getParameters().put("unsigned_field_names", "");

			hopPaymentDetailsForm.getParameters().put("amount",
					cartData.getTotalPrice().getValue().add(checkoutFacade.getExtraAuthAmount(cartData)).toString());


			hopPaymentDetailsForm.getParameters().put("transaction_type", checkoutFacade.getTransactionType());


			hopPaymentDetailsForm.getParameters().put("bill_to_forename", shipToUnit.getName());
			hopPaymentDetailsForm.getParameters().put("bill_to_surname", shipToUnit.getName());
			hopPaymentDetailsForm.getParameters().put("bill_to_address_city", soldToAddress.getTown());
			hopPaymentDetailsForm.getParameters().put("bill_to_address_country", soldToAddress.getCountry().getIsocode());
			hopPaymentDetailsForm.getParameters().put("bill_to_address_state", soldToAddress.getRegion().getIsocodeShort());
			hopPaymentDetailsForm.getParameters().put("bill_to_address_line1", soldToAddress.getLine1());
			hopPaymentDetailsForm.getParameters().put("bill_to_address_postal_code", soldToAddress.getPostalCode());
			hopPaymentDetailsForm.getParameters().put("bill_to_email", "TESOM@fm.com");

			//Shipping Address
			hopPaymentDetailsForm.getParameters().put("ship_to_forename", shipToUnit.getName());
			hopPaymentDetailsForm.getParameters().put("ship_to_surname", shipToUnit.getName());
			hopPaymentDetailsForm.getParameters().put("ship_to_address_city", soldToAddress.getTown());
			hopPaymentDetailsForm.getParameters().put("ship_to_address_country", soldToAddress.getCountry().getIsocode());
			hopPaymentDetailsForm.getParameters().put("ship_to_address_state", soldToAddress.getRegion().getIsocodeShort());
			hopPaymentDetailsForm.getParameters().put("ship_to_address_line1", soldToAddress.getLine1());
			hopPaymentDetailsForm.getParameters().put("ship_to_address_postal_code", soldToAddress.getPostalCode());
			hopPaymentDetailsForm.getParameters().put("ship_to_email", "TESOM@fm.com");

			try
			{
				hopPaymentDetailsForm.getParameters().put(
						"signature",
						FMDigestUtils.getPublicDigest(checkoutFacade.buildDataToSign(hopPaymentDetailsForm.getParameters()),
								checkoutFacade.getSharedSecret()));
			}
			catch (final InvalidKeyException e)
			{
				LOG.error("Invalid Cybersource Key:");
				LOG.error(e.getMessage());
			}
			catch (final NoSuchAlgorithmException e)
			{
				LOG.error("No Such Algorithm");
				LOG.error(e.getMessage());
			}

			model.addAttribute("paymentFormUrl", checkoutFacade.getPostUrl());
		}
		catch (final IllegalArgumentException e)
		{
			LOG.warn("Failed to set up Host order post page " + e.getMessage());
			GlobalMessages.addErrorMessage(model, "HOP Error");
		}

		model.addAttribute("hostOrderPostForm", new AddressForm());
		model.addAttribute("hopPaymentDetailsForm", hopPaymentDetailsForm);
		model.addAttribute("paymentInfos", getUserFacade().getCCPaymentInfos(true));

	}

	@RequestMapping(value = "/hop-response", method = RequestMethod.POST)
	public String doHandleSopResponse(final HttpServletRequest request, @Valid final HopPaymentDetailsForm hopPaymentDetailsForm,
			final BindingResult bindingResult, final Model model) throws CMSItemNotFoundException, InvalidCartException
	{
		getSessionService().getAttribute("customertype");
		final CartData cartData = getCheckoutFlowFacade().getCheckoutCart();
		final AbstractOrderModel abstractOrderModel = b2bOrderService.getAbstractOrderForCode(cartData.getCode());
		final Map<String, String> resultMap = checkoutFacade.getRequestParameterMap(request);
		LOG.info("Entered hop-respnse handler" + resultMap);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Hop-Response Result map:" + resultMap);
		}

		final PaymentTransactionModel transaction = (PaymentTransactionModel) this.modelService
				.create(PaymentTransactionModel.class);
		transaction.setCode(resultMap.get("req_reference_number"));
		transaction.setRequestId(resultMap.get("transaction_id"));
		transaction.setRequestToken(resultMap.get("payment_token"));
		transaction.setPaymentProvider("cybersource");
		transaction.setOrder(abstractOrderModel);
		if ("ERROR".equalsIgnoreCase(resultMap.get("decision")) || "DECLINE".equalsIgnoreCase(resultMap.get("decision")))
		{
			if ("DECLINE".equalsIgnoreCase(resultMap.get("decision")) && "200".equals(resultMap.get("reason_code")))
			{
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.payment.failed.AVS_FAILED");
			}
			else
			{
				GlobalMessages.addErrorMessage(model, "checkout.placeOrder.payment.failed." + resultMap.get("decision"));

			}
			return FORWARD_PREFIX + "/cart";
		}
		else if ("ACCEPT".equalsIgnoreCase(resultMap.get("decision")) || "REVIEW".equalsIgnoreCase(resultMap.get("decision")))
		{

			//final OrderData orderData = checkoutFacade.placeOrder(resultMap, transaction);

			final CreditCardPaymentInfoModel cardPaymentInfoModel = modelService.create(CreditCardPaymentInfoModel.class);

			cardPaymentInfoModel.setCode(UUID.randomUUID().toString());
			cardPaymentInfoModel.setNumber(resultMap.get("req_card_number"));
			final UserModel userModel = abstractOrderModel.getUser();
			cardPaymentInfoModel.setUser(userModel);
			cardPaymentInfoModel.setCcOwner(userModel.getName());
			final String ccmmyear = resultMap.get("req_card_expiry_date");
			if (ccmmyear != null)
			{
				final String[] cc = ccmmyear.split("-");
				cardPaymentInfoModel.setValidToMonth(cc[0]);
				cardPaymentInfoModel.setValidToYear(cc[1]);
			}
			final String ccType = resultMap.get("req_card_type");
			String ccTypeName = null;

			if ("001".equalsIgnoreCase(ccType))
			{
				ccTypeName = "VISA";
			}
			else if ("002".equalsIgnoreCase(ccType))
			{
				ccTypeName = "MASTER";
			}
			else if ("003".equalsIgnoreCase(ccType))
			{
				ccTypeName = "AMEX";
			}
			else if ("004".equalsIgnoreCase(ccType))
			{
				ccTypeName = "DISCOVER";
			}
			else if ("005".equalsIgnoreCase(ccType))
			{
				ccTypeName = "DINERS CLUB";
			}
			else if ("006".equalsIgnoreCase(ccType))
			{
				ccTypeName = "VISA";
			}
			else if ("007".equalsIgnoreCase(ccType))
			{
				ccTypeName = "VISA";
			}
			else
			{
				ccTypeName = "VISA";
			}
			final CreditCardType cardType = (CreditCardType) enumerationService.getEnumerationValue(
					CreditCardType.class.getSimpleName(), ccTypeName);
			cardPaymentInfoModel.setType(cardType);
			modelService.save(cardPaymentInfoModel);
			abstractOrderModel.setPaymentInfo(cardPaymentInfoModel);
			transaction.setInfo(cardPaymentInfoModel);

			//Create Payment transaction model and set transaction status
			final PaymentTransactionEntryModel transactionEntry = checkoutFacade.setTransactionStatus(resultMap, transaction);
			modelService.save(transaction);
			modelService.save(transactionEntry);
			final List<PaymentTransactionModel> paymentTransactions = abstractOrderModel.getPaymentTransactions();
			final List<PaymentTransactionModel> paymentTransactionsList = new ArrayList<PaymentTransactionModel>();
			final Iterator<PaymentTransactionModel> paymentIterator = paymentTransactions.iterator();
			while (paymentIterator.hasNext())
			{
				paymentTransactionsList.add(paymentIterator.next());
			}
			paymentTransactionsList.add(transaction);
			abstractOrderModel.setPaymentTransactions(paymentTransactionsList);
			modelService.save(abstractOrderModel);
			return REDIRECT_URL_CHECKOUT_CONFIRMATION + "?poNumber=123456789012345&orderInstruction=test";
		}
		return FORWARD_PREFIX + "/cart";
	}


	public void womProcessOrder(final Model model, final B2BUnitData soldToUnit, final B2BUnitData shipToUnit,
			final AddressData soldToAddress, final AddressData shipToAddress) throws Exception
	{
		final CartModel cartModel = cartService.getSessionCart();
		globalTempDataForSavePdf.setCartModel(cartModel);
		Date deliveryDate = null;
		linenumber = 1;
		final String shipToCountry = (null != shipToAddress && null != shipToAddress.getCountry().getIsocode()) ? shipToAddress
				.getCountry().getIsocode() : "US";
		if (cartModel.getEntries() != null && !cartModel.getEntries().isEmpty())
		{
			final List<ItemBO> itemsList = new ArrayList<ItemBO>();
			final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
			for (final ItemBO items : itemBOSession)
			{
				for (final AbstractOrderEntryModel entry : cartModel.getEntries())
				{

					String code = entry.getProduct().getCode();
					boolean catalogProduct = true;
					final FMPartModel productModel = partService.getPartForCode(entry.getProduct().getCode());
					for (final FMCorporateModel corp : productModel.getCorporate())
					{
						if ("dummy".equalsIgnoreCase(corp.getCorpcode()) || "FELPF".equalsIgnoreCase(corp.getCorpcode())
								|| "SPDPF".equalsIgnoreCase(corp.getCorpcode()) || "SLDPF".equalsIgnoreCase(corp.getCorpcode()))
						{
							catalogProduct = false;
						}
					}
					if (!catalogProduct && null != productModel.getFlagcode())
					{
						code = productModel.getFlagcode().toUpperCase() + productModel.getRawPartNumber();
					}

					deliveryDate = entry.getNamedDeliveryDate();
					String displayPartNumber = items.getDisplayPartNumber();
					if ((items.getExternalSystem() != null && items.getExternalSystem().equals(ExternalSystem.NABS))
							|| (null != items.getProductFlag()))
					{
						displayPartNumber = items.getProductFlag() + items.getDisplayPartNumber();
					}
					if (items.getPartNumber().equalsIgnoreCase(entry.getProduct().getCode())
							|| items.getDisplayPartNumber().equalsIgnoreCase(entry.getProduct().getCode())
							|| displayPartNumber.equalsIgnoreCase(entry.getProduct().getCode())
							|| (code.equalsIgnoreCase(displayPartNumber)))
					{
						//if (!items.hasProblemItem() && items.getExternalSystem().equals(ExternalSystem.EVRST)
						if (items.getExternalSystem().equals(ExternalSystem.EVRST)
								&& "ecc".equalsIgnoreCase(siteConfigService.getString("wom.ecc", "ecc")))
						{
							itemsList.addAll(getECCItemsList(entry, items, shipToCountry));
						}
						//if (!items.hasProblemItem() && items.getExternalSystem().equals(ExternalSystem.NABS)
						if (items.getExternalSystem().equals(ExternalSystem.NABS)
								&& "nabs".equalsIgnoreCase(siteConfigService.getString("wom.nabs", "nabs")))
						{
							itemsList.addAll(getNABSItemsList(entry, items, shipToCountry));
						}
					}
				}
			}

			final OrderBO order = new OrderBO();
			final OrderASUSCanImpl processOrderService = new OrderASUSCanImpl();

			if (cartModel.getFmordertype() == null)
			{
				cartModel.setFmordertype("STOCK");
			}
			if ("EMERGENCY".equalsIgnoreCase(cartModel.getFmordertype()))
			{
				order.setOrderType(OrderType.EMERGENCY);
			}
			if ("REGULAR".equalsIgnoreCase(cartModel.getFmordertype()))
			{
				order.setOrderType(OrderType.REGULAR);
			}
			if ("STOCK".equalsIgnoreCase(cartModel.getFmordertype()))
			{
				order.setOrderType(OrderType.STOCK);
			}
			if ("PICKUP".equalsIgnoreCase(cartModel.getFmordertype()))
			{
				order.setOrderType(OrderType.PICKUP);
			}
			if (cartModel.getCustponumber() != null)
			{
				order.setCustPoNbr(cartModel.getCustponumber());
			}
			else
			{
				order.setCustPoNbr(getPONumber());
			}
			order.setOrderSource(OrderSource.HYBRIS);
			order.setUsageType(UsageType.PURCHASE);


			final String soldToAcc = (String) getSessionService().getAttribute(WebConstants.SOLDTO_ACCOUNT_ID);
			final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
					.getUnitForUid(soldToAcc);
			double freefrieghtamt = 0.0;
			if (fmCustomerAccModel != null && fmCustomerAccModel.getFreeFreightAmt() != null
					&& fmCustomerAccModel.getFreeFreightAmt() > 0)
			{
				double sessionfrieghamount = 0.0;
				if (getSessionService().getAttribute("freightPrice") != null)
				{
					sessionfrieghamount = Double.valueOf(getSessionService().getAttribute("freightPrice").toString());
				 LOG.info("sessionfrieghamount ::" + sessionfrieghamount);

				}
				freefrieghtamt = (fmCustomerAccModel.getFreeFreightAmt().doubleValue() - sessionfrieghamount);
				LOG.info("freefrieghtamt ::" + freefrieghtamt + "O/P" + (freefrieghtamt <= 0 ? true : false));
			}
			order.setReceivesFreeFreight(freefrieghtamt <= 0 ? true : false);
			order.setMarketCode(processOrderService.getMarket());
			if (deliveryDate != null && "STOCK".equalsIgnoreCase(cartModel.getFmordertype()))
			{
				order.setFutureDate(deliveryDate);
				LOG.info("STOCK deliveryDate" + deliveryDate);
			}
			else
			{
				order.setFutureDate(null);
			}
			final String moNumber = getmasterOrderNumber();
			order.setMstrOrdNbr(moNumber != null ? moNumber : cartModel.getCode());
			final FMCustomerModel user = (FMCustomerModel) cartModel.getUser();
			/*
			 * String userName = user.getUid().split("@")[0].replace(".", ","); if (userName != null && userName.length()
			 * >= 20) { userName = userName.split(",")[0]; }
			 */
			LOG.info("userName {}" + user.getLastName());
			order.setOrderedBy(user.getLastName());
			order.setUserAccount(getUserAccount(user));
			order.setBillToAcct(getBillTOAccount(soldToUnit, soldToAddress));
			order.setShipToAcct(getShiptoAccount(shipToAddress, shipToUnit));

			//Manual Address
			final AddressData manualShipToAddress = (AddressData) getSessionService().getAttribute("shiptToAddress");
			if (manualShipToAddress != null)
			{
				order.setManualShipTo(true);
				order.setManualShipToAddress(getManualShipToAddress(manualShipToAddress));
			}


			LOG.info("Print :: ItemsList " + itemsList);
			if (null == itemsList || itemsList.isEmpty())
			{
				throw new Exception("Item List is Empty");
			}
			order.setItemList(itemsList);
			LOG.info(" PLace Order :: cartModel.getOrdercomments() {}" + cartModel.getOrdercomments());
			if (cartModel.getOrdercomments() != null && !cartModel.getOrdercomments().isEmpty())
			{
				order.setComment1(cartModel.getOrdercomments());
			}
			else
			{
				order.setComment1("No Comments");
			}
			final OrderBO result = processOrderService.processOrder(order);
			LOG.info("Order Result BO :: " + result);
			LOG.info("Order Result: " + result.getSapConfirmationNumber() + "Master Order Number : " + result.getMstrOrdNbr());
			setOrderDetails(cartModel.getCode(), result.getSapConfirmationNumber(), moNumber);
			model.addAttribute("sapOrderCode", result.getSapConfirmationNumber());
			getSessionService().setAttribute("sapOrderCode", result.getSapConfirmationNumber());
			globalTempDataForSavePdf.setSapOrderCode(result.getMstrOrdNbr());
		}
	}

	private UserAccountBO getUserAccount(final UserModel user)
	{
		final String soldToAcc = (String) getSessionService().getAttribute(WebConstants.SOLDTO_ACCOUNT_ID);
		FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService.getUnitForUid(soldToAcc);
		final String shipToAcc = (String) getSessionService().getAttribute(WebConstants.SHIPTO_ACCOUNT_ID);
		final FMCustomerAccountModel sfmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
				.getUnitForUid(shipToAcc);

		final UserAccountBO userAcct = new UserAccountBO();
		final String loggedUserType = (String) getSessionService().getAttribute("logedUserType");
		LOG.info("loggedUserType ==>" + loggedUserType);
		if (loggedUserType != null && "ShipTo".equalsIgnoreCase(loggedUserType))
		{
			fmCustomerAccModel = sfmCustomerAccModel;
			userAcct.setLoggedInAsBillto(false);
		}
		else
		{
			userAcct.setLoggedInAsBillto(true);
		}
		LOG.info("user.getName() -->" + user.getName());
		userAcct.setUserID(user.getName());
		userAcct.setAccountCode(fmCustomerAccModel.getNabsAccountCode());
		AuditInterceptor.setUserAccount(userAcct);
		return userAcct;
	}

	private BillToAcctBO getBillTOAccount(final B2BUnitData soldToUnit, final AddressData soldToAddress)
	{
		final BillToAcctBO billToAcct = new BillToAcctBO();

		final String soldToAcc = (String) getSessionService().getAttribute(WebConstants.SOLDTO_ACCOUNT_ID);
		final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
				.getUnitForUid(soldToAcc);

		billToAcct.setAccountCode(fmCustomerAccModel.getNabsAccountCode());//soldToUnit.getNabsAccountCode());
		final SapAcctBO sapAccount = new SapAcctBO();
		sapAccount.setSapAccountCode(fmCustomerAccModel.getUid());

		final CustomerSalesOrgBO custSalesOrg = new CustomerSalesOrgBO();
		custSalesOrg.setDistributionChannel(fmCustomerAccModel.getDistributionChannel());
		custSalesOrg.setDivision(fmCustomerAccModel.getDivision());

		final SalesOrganizationBO salesOrg = new SalesOrganizationBO();
		salesOrg.setCode(fmCustomerAccModel.getSalesorg());
		custSalesOrg.setSalesOrganization(salesOrg);
		sapAccount.setCustomerSalesOrganization(custSalesOrg);
		billToAcct.setSapAccount(sapAccount);
		return billToAcct;
	}

	private ShipToAcctBO getShiptoAccount(final AddressData shipToAddress, final B2BUnitData shipToUnit)
	{

		final String shipToAcc = (String) getSessionService().getAttribute(WebConstants.SHIPTO_ACCOUNT_ID);
		final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
				.getUnitForUid(shipToAcc);

		final ShipToAcctBO aShipTo = new ShipToAcctBO();
		aShipTo.setAccountName(shipToUnit.getName());
		aShipTo.setAccountCode(fmCustomerAccModel.getNabsAccountCode());//shipToUnit.getUid());

		final SapAcctBO sapAccount = new SapAcctBO();
		sapAccount.setSapAccountCode(fmCustomerAccModel.getUid());
		aShipTo.setSapAccount(sapAccount);


		AddressModel addres = fmCustomerAccModel.getBillingAddress();
		for (final AddressModel address : fmCustomerAccModel.getAddresses())
		{
			addres = address;
		}

		final String pickUpStore = getSessionService().getAttribute("pickUpStore");
		final AddressData storeAddress = (AddressData) getSessionService().getAttribute("tscaddress");

		LOG.info("pickUpStore Ship to :: Flag" + pickUpStore);
		if (pickUpStore != null && "true".equalsIgnoreCase(pickUpStore) && storeAddress != null)
		{
			LOG.info("Store Pickup Started Ship to");
			final AddressBO tscAddress = new AddressBO();
			tscAddress.setCity(storeAddress.getTown());
			tscAddress.setAddrLine1(storeAddress.getLine1());
			tscAddress.setAddrLine2(storeAddress.getLine2());
			tscAddress.setZipOrPostalCode(storeAddress.getPostalCode());
			tscAddress.setStateOrProv(storeAddress.getRegion().getName());
			final CountryBO country = new CountryBO();
			country.setIsoCountryCode(storeAddress.getCountry().getIsocode());
			tscAddress.setCountry(country);
			aShipTo.setAddress(tscAddress);
		}
		else
		{
			/*
			 * //manual Shipping address final AddressData manualShipToAddress = (AddressData)
			 * getSessionService().getAttribute("shiptToAddress"); if (manualShipToAddress != null) {
			 *
			 * LOG.info("Store Pickup Started Ship to"); final AddressBO manualShipTo = new AddressBO();
			 * manualShipTo.setCity(manualShipToAddress.getTown());
			 * manualShipTo.setAddrLine1(manualShipToAddress.getLine1());
			 * manualShipTo.setAddrLine2(manualShipToAddress.getLine2());
			 * manualShipTo.setZipOrPostalCode(manualShipToAddress.getPostalCode());
			 * manualShipTo.setStateOrProv(manualShipToAddress.getRegion().getName()); final CountryBO country = new
			 * CountryBO(); country.setIsoCountryCode(manualShipToAddress.getCountry().getIsocode());
			 * manualShipTo.setCountry(country); aShipTo.setAddress(manualShipTo);
			 *
			 *
			 * } else {
			 */

			final AddressBO anAddress = new AddressBO();
			anAddress.setCity(addres.getTown());
			anAddress.setAddrLine1(addres.getLine1());
			anAddress.setAddrLine2(addres.getLine2());
			anAddress.setZipOrPostalCode(addres.getPostalcode());
			anAddress.setStateOrProv(addres.getRegion().getName());
			final CountryBO country = new CountryBO();
			country.setIsoCountryCode(addres.getCountry().getIsocode());
			anAddress.setCountry(country);
			aShipTo.setAddress(anAddress);
			//}
		}

		return aShipTo;
	}

	private ManualShipToAddressBO getManualShipToAddress(final AddressData manualShipToAddress)
	{
		final ManualShipToAddressBO manualAddress = new ManualShipToAddressBO();
		manualAddress.setName(manualShipToAddress.getFirstName());
		final AddressBO manualShipTo = new AddressBO();
		manualShipTo.setCity(manualShipToAddress.getTown());
		manualShipTo.setAddrLine1(manualShipToAddress.getLine1());
		manualShipTo.setAddrLine2(manualShipToAddress.getLine2());
		manualShipTo.setZipOrPostalCode(manualShipToAddress.getPostalCode());
		manualShipTo.setStateOrProv(manualShipToAddress.getRegion().getIsocode().split("-")[1]);
		final CountryBO country = new CountryBO();
		country.setIsoCountryCode(manualShipToAddress.getRegion().getIsocode().split("-")[0]);
		manualShipTo.setCountry(country);
		manualAddress.setAddress(manualShipTo);
		return manualAddress;
	}


	private QuantityBO getDefaultQuanity(final int quantity)
	{
		final QuantityBO qty = new QuantityBO();
		qty.setRequestedQuantity(quantity);
		return qty;
	}

	private List<ItemBO> getECCItemsList(final AbstractOrderEntryModel entry, final ItemBO item, final String shipToCountry)
	{

		final List<ItemBO> eccItemsList = new ArrayList<ItemBO>();
		if (entry.getDistrubtionCenter() != null && entry.getDistrubtionCenter().size() > 0)
		{
			final ItemBO anHeader = new PartBO();
			anHeader.setDisplayPartNumber(item.getDisplayPartNumber());
			anHeader.setPartNumber(item.getPartNumber());
			anHeader.setComment("This is The Header Record :: " + item.getPartNumber());
			((PartBO) anHeader).setHeader(true);
			((PartBO) anHeader).setProcessOrderLineNumber(entry.getEntryNumber());
			anHeader.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
			anHeader.setExternalSystem(ExternalSystem.EVRST);
			anHeader.setLineNumber(linenumber++);
			eccItemsList.add(anHeader);
			for (final FMDistrubtionCenterModel dcs : entry.getDistrubtionCenter())
			{
				final ItemBO anItem = new PartBO();
				anItem.setDisplayPartNumber(item.getDisplayPartNumber());
				anItem.setPartNumber(item.getPartNumber());
				anItem.setAaiaBrand(item.getAaiaBrand());
				anItem.setLineNumber(linenumber);
				anItem.setScacCode("UPS-REG");
				anItem.setExternalSystem(ExternalSystem.EVRST);
				((PartBO) anItem).setProcessOrderLineNumber(entry.getEntryNumber());
				if (dcs.getBackorderFlag() != null && "nothing".equalsIgnoreCase(dcs.getBackorderFlag()))
				{
					anItem.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
					anItem.setBackorderPolicy(BackOrderPolicy.BACKORDER_ALL);
				}
				else if (dcs.getBackorderFlag() != null && "partial".equalsIgnoreCase(dcs.getBackorderFlag()))
				{
					anItem.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
					anItem.setBackorderPolicy(BackOrderPolicy.SHIP_AND_BACKORDER);
				}
				else
				{
					anItem.setItemQty(getDefaultQuanity(Integer.parseInt(dcs.getAvailableQTY())));
				}


				final InventoryBO inventory = new InventoryBO();
				final DistributionCenterBO dc = new DistributionCenterBO();
				dc.setCode(dcs.getDistrubtionCenterDataCode());
				dc.setAvailabilityDate(dcs.getAvailableDate());
				dc.setEmergencyCutoffTime(getCutoffTime(dcs.getDistrubtionCenterDataCode()).getTime());

				inventory.setDistributionCenter(dc);
				inventory.setAvailableQty(Integer.parseInt(dcs.getAvailableQTY()));
				//inventory.setAssignedQty(Integer.parseInt(dcs.getAvailableQTY()));
				inventory.setAssignedQty(0);
				inventory.setSelectedLocation(true);

				final FMDCShippingModel shipMethod = fmDistrubtionCenterFacade.getShippingMethod(dcs.getDistrubtionCenterDataCode(),
						dcs.getCarrierName(), dcs.getShippingMethod(), shipToCountry.toUpperCase());

				final ShippingCodeBO shippingCode = new ShippingCodeBO();
				final SapShippingCodeBO sapShippingCode = new SapShippingCodeBO();
				LOG.info("else Shipmethod Carrier Code :: " + shipMethod.getCARRIER_CD());
				LOG.info("else Shipmethod Carrier name :: " + shipMethod.getCARRIER_NAME());
				sapShippingCode.setCarrierCode(shipMethod.getSAP_CARRIER_CD());
				sapShippingCode.setIncoTerms(shipMethod.getINCO_TERMS());
				sapShippingCode.setRoute(shipMethod.getROUTE());
				shippingCode.setSapShippingCode(sapShippingCode);
				inventory.setShippingCode(shippingCode);

				anItem.addInventory(inventory);
				eccItemsList.add(anItem);
				linenumber++;
			}
		}
		else
		{
			final String pickUpStore = getSessionService().getAttribute("pickUpStore");
			if (pickUpStore != null && "true".equalsIgnoreCase(pickUpStore))
			{
				final TSCLocationModel store = (TSCLocationModel) getSessionService().getAttribute("storeDetail");

				LOG.info("pickUpStore :: Flag" + pickUpStore);
				//if (pickUpStore != null && pickUpStore.equalsIgnoreCase("true"))
				//{
				LOG.info("Store Pickup Started");
				final String tscCode = store.getCode();//storeAddress.getFirstName().split("-")[0].trim();
				final ItemBO anHeader = new PartBO();
				anHeader.setDisplayPartNumber(item.getDisplayPartNumber());
				anHeader.setPartNumber(item.getPartNumber());
				((PartBO) anHeader).setHeader(true);
				anHeader.setComment("This is The Header Record :: " + item.getPartNumber());
				((PartBO) anHeader).setProcessOrderLineNumber(entry.getEntryNumber());
				anHeader.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
				anHeader.setExternalSystem(ExternalSystem.EVRST);
				anHeader.setLineNumber(linenumber++);

				if (tscCode != null && (!tscCode.isEmpty()))
				{
					final InventoryBO anInventory = new InventoryBO();
					anInventory.setAvailableQty(Long.valueOf(entry.getQuantity()).intValue());
					anInventory.setAssignedQty(Long.valueOf(entry.getQuantity()).intValue());
					anInventory.setSelectedLocation(true);

					final DistributionCenterBO distCenter = new DistributionCenterBO();
					distCenter.setCode(tscCode);

					anInventory.setDistributionCenter(distCenter);
					//anInventory.setShippingCode(getTSCPickUpShippingCode());
					anHeader.addInventory(anInventory);
				}

				eccItemsList.add(anHeader);

				final ItemBO anItem = new PartBO();
				anItem.setDisplayPartNumber(item.getDisplayPartNumber());
				anItem.setPartNumber(item.getPartNumber());
				anItem.setAaiaBrand(item.getAaiaBrand());
				anItem.setLineNumber(linenumber);
				anItem.setScacCode("UPS-REG");
				((PartBO) anItem).setProcessOrderLineNumber(entry.getEntryNumber());
				anItem.setItemQty(getDefaultQuanity(Long.valueOf(entry.getQuantity()).intValue()));
				anItem.setExternalSystem(ExternalSystem.EVRST);

				final InventoryBO inventory = new InventoryBO();
				inventory.setAvailableQty(Long.valueOf(entry.getQuantity()).intValue());
				inventory.setAssignedQty(Long.valueOf(entry.getQuantity()).intValue());

				//final String pickUpStore = getSessionService().getAttribute("pickUpStore");

				LOG.info("TSC Code :: " + tscCode);
				final DistributionCenterBO tscCenter = new DistributionCenterBO();
				tscCenter.setCode(tscCode);
				tscCenter.setAvailabilityDate(GregorianCalendar.getInstance().getTime());
				tscCenter.setEmergencyCutoffTime(GregorianCalendar.getInstance().getTime());

				inventory.setDistributionCenter(tscCenter);

				final FMDCShippingModel tscShipMethod = fmDistrubtionCenterFacade.getShippingMethod(tscCode, "OTH", "PKUP",
						shipToCountry.toUpperCase());

				final ShippingCodeBO tscShippingCode = new ShippingCodeBO();
				final SapShippingCodeBO tscSAPShippingCode = new SapShippingCodeBO();
				LOG.info("TSC Shipmethod Carrier Code :: " + tscShipMethod.getSAP_CARRIER_CD());
				tscSAPShippingCode.setCarrierCode(tscShipMethod.getSAP_CARRIER_CD());

				tscSAPShippingCode.setIncoTerms(tscShipMethod.getINCO_TERMS());
				tscSAPShippingCode.setRoute(tscShipMethod.getROUTE());
				tscShippingCode.setSapShippingCode(tscSAPShippingCode);
				inventory.setShippingCode(tscShippingCode);
				inventory.setSelectedLocation(true);
				anItem.addInventory(inventory);
				eccItemsList.add(anItem);
				linenumber++;
			}
			/*
			 * else { final DistributionCenterBO distCenter = new DistributionCenterBO(); distCenter.setCode("2156");
			 * distCenter.setAvailabilityDate(GregorianCalendar.getInstance().getTime());
			 * inventory.setDistributionCenter(distCenter);
			 * distCenter.setEmergencyCutoffTime(getCutoffTime("2156").getTime()); final FMDCShippingModel shipMethod =
			 * fmDistrubtionCenterFacade.getShippingMethod("2156", "FED", "GRD");
			 *
			 * final ShippingCodeBO shippingCode = new ShippingCodeBO(); final SapShippingCodeBO sapShippingCode = new
			 * SapShippingCodeBO();
			 *
			 * sapShippingCode.setCarrierCode(shipMethod.getCARRIER_CD());
			 *
			 * sapShippingCode.setIncoTerms(shipMethod.getINCO_TERMS()); sapShippingCode.setRoute(shipMethod.getROUTE());
			 * shippingCode.setSapShippingCode(sapShippingCode); inventory.setShippingCode(shippingCode);
			 * inventory.setSelectedLocation(true); }
			 */


		}
		return eccItemsList;
	}

	private List<ItemBO> getNABSItemsList(final AbstractOrderEntryModel entry, final ItemBO item, final String shipToCountry)
	{
		final List<ItemBO> nabsItemsList = new ArrayList<ItemBO>();
		if (entry.getDistrubtionCenter() != null && entry.getDistrubtionCenter().size() > 0)
		{
			for (final FMDistrubtionCenterModel dcs : entry.getDistrubtionCenter())
			{
				final ItemBO anItem = new PartBO();
				anItem.setDisplayPartNumber(item.getDisplayPartNumber());
				anItem.setPartNumber(item.getPartNumber());
				anItem.setLineNumber(linenumber);
				anItem.setExternalSystem(ExternalSystem.NABS);
				anItem.setProductFlag(item.getProductFlag());
				anItem.setBrandState(item.getBrandState());

				if (dcs.getBackorderFlag() != null && "nothing".equalsIgnoreCase(dcs.getBackorderFlag()))
				{
					anItem.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
					anItem.setBackorderPolicy(BackOrderPolicy.BACKORDER_ALL);
				}
				else if (dcs.getBackorderFlag() != null && "partial".equalsIgnoreCase(dcs.getBackorderFlag()))
				{
					anItem.setItemQty(getDefaultQuanity(entry.getQuantity().intValue()));
					anItem.setBackorderPolicy(BackOrderPolicy.SHIP_AND_BACKORDER);
				}
				else
				{
					anItem.setItemQty(getDefaultQuanity(Integer.parseInt(dcs.getAvailableQTY())));
				}

				final FMDCShippingModel shipMethod = fmDistrubtionCenterFacade.getShippingMethod(dcs.getDistrubtionCenterDataCode(),
						dcs.getCarrierName(), dcs.getShippingMethod(), shipToCountry.toUpperCase());

				final DistributionCenterBO distrCenter = new DistributionCenterBO();
				distrCenter.setDistCenterId(Long.parseLong(shipMethod.getDIST_CNTR_ID()));
				distrCenter.setCode(dcs.getDistrubtionCenterDataCode());
				distrCenter.setName(shipMethod.getDIST_CNTR_NAME());
				distrCenter.setAvailabilityDate(dcs.getAvailableDate());
				distrCenter.setEmergencyCutoffTime(getCutoffTime(dcs.getDistrubtionCenterDataCode()).getTime());

				final InventoryBO inventory = new InventoryBO();
				inventory.setDistributionCenter(distrCenter);
				inventory.setSelectedLocation(true);
				inventory.setDistCntrId(distrCenter.getDistCenterId());
				inventory.setAvailableQty(Integer.parseInt(dcs.getAvailableQTY()));
				inventory.setAssignedQty(Integer.parseInt(dcs.getAvailableQTY()));
				inventory.setSelectedLocation(true);

				final ShippingCodeBO shippingCode = new ShippingCodeBO();
				final NabsShippingCodeBO nabsShippingCode = new NabsShippingCodeBO();
				nabsShippingCode.setStockShippingCode(shipMethod.getSTOCK_SHIP_CD());
				nabsShippingCode.setEmergencyShippingCode(shipMethod.getEMERG_SHIP_CD());

				shippingCode.setNabsShippingCode(nabsShippingCode);
				inventory.setShippingCode(shippingCode);

				anItem.addInventory(inventory);
				nabsItemsList.add(anItem);
				linenumber++;
			}
		}
		/*
		 * else { final ItemBO anItem = new PartBO(); anItem.setDisplayPartNumber(item.getDisplayPartNumber());
		 * anItem.setPartNumber(item.getPartNumber()); anItem.setLineNumber(linenumber);
		 * anItem.setItemQty(getDefaultQuanity(Long.valueOf(entry.getQuantity()).intValue()));
		 * anItem.setExternalSystem(ExternalSystem.NABS); anItem.setProductFlag(item.getProductFlag());
		 * anItem.setBrandState(item.getBrandState());
		 *
		 *
		 * final DistributionCenterBO distrCenter = new DistributionCenterBO(); distrCenter.setDistCenterId(22);
		 * distrCenter.setCode("019"); distrCenter.setName("Maysville");
		 * distrCenter.setEmergencyCutoffTime(getCutoffTime("019").getTime());
		 *
		 *
		 *
		 * final InventoryBO inventory = new InventoryBO();
		 * inventory.setAvailableQty(Long.valueOf(entry.getQuantity()).intValue());
		 * inventory.setAssignedQty(Long.valueOf(entry.getQuantity()).intValue()); inventory.setSelectedLocation(true);
		 * inventory.setDistCntrId(distrCenter.getDistCenterId()); inventory.setDistributionCenter(distrCenter);
		 * inventory.setSelectedLocation(true); final ShippingCodeBO shippingCode = new ShippingCodeBO(); final
		 * NabsShippingCodeBO nabsShippingCode = new NabsShippingCodeBO(); nabsShippingCode.setStockShippingCode("FXIPR");
		 * nabsShippingCode.setEmergencyShippingCode("FXIPR"); shippingCode.setNabsShippingCode(nabsShippingCode);
		 * inventory.setShippingCode(shippingCode);
		 *
		 * anItem.addInventory(inventory); nabsItemsList.add(anItem); linenumber++; }
		 */
		return nabsItemsList;
	}

	private void setOrderPODetails(final String orderCode, final String poNumber, final String orderInstruction)
	{
		//final AbstractOrderModel abstractOrderModel = getAbstractOrderForCode(orderCode);
		final AbstractOrderModel abstractOrderModel = b2bOrderService.getAbstractOrderForCode(orderCode);
		//validateParameterNotNull(abstractOrderModel, String.format("Order %s does not exist", orderCode));
		abstractOrderModel.setCustponumber(poNumber);
		abstractOrderModel.setOrdercomments(orderInstruction);
		modelService.save(abstractOrderModel);
	}

	private void setOrderDetails(final String orderCode, final String sapOrder, final String nabsOrder)
	{
		final AbstractOrderModel abstractOrderModel = b2bOrderService.getAbstractOrderForCode(orderCode);
		abstractOrderModel.setSapordernumber(sapOrder);
		abstractOrderModel.setPurchaseOrderNumber(nabsOrder);
		modelService.save(abstractOrderModel);
	}

	private void getDistrubtionCenter(final CartData cartData, final Model model)
	{

		final List<String> distrubtionCenterData = new ArrayList<String>();
		if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
		{
			for (final OrderEntryData entry : cartData.getEntries())
			{
				if (entry.getDistrubtionCenter() != null && !entry.getDistrubtionCenter().isEmpty())
				{
					for (final DistrubtionCenterData dcentry : entry.getDistrubtionCenter())
					{
						distrubtionCenterData.add(dcentry.getDistrubtionCenterDataCode() + "-"
						//+ getDCDateformat(dcentry.getAvailableDate() != null ? dcentry.getAvailableDate() : new Date()) + "_"
								+ getDCDateformat(new Date()) + "_" + dcentry.getDistrubtionCenterDataName());
					}
				}
				/*
				 * else { final CartModificationData cartModification =
				 * b2bCartFacade.updateOrderEntry(getOrderEntryData(Long.valueOf(0) .longValue(),
				 * entry.getProduct().getCode(), entry.getEntryNumber())); LOG.info("cartModification ==>" +
				 * cartModification); }
				 */
			}
		}
		final ArrayList<String> distrubtionCenterList = removeDuplicates(distrubtionCenterData);
		model.addAttribute("dc", distrubtionCenterList);
	}

	public ArrayList<String> removeDuplicates(final List<String> list)
	{

		final ArrayList<String> result = new ArrayList<>();

		final HashSet<String> set = new HashSet<>();

		for (final String item : list)
		{
			if (!set.contains(item))
			{
				result.add(item);
				set.add(item);
			}
		}
		return result;
	}

	public String getDCDateformat(final Date date)
	{
		final SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		String rdate = null;
		try
		{
			rdate = sdf.format(date);
		}
		catch (final Exception e)
		{
			rdate = sdf.format(new Date());
		}
		return rdate;
	}



	private String getPONumber()
	{
		final GetMasterOrderNumberAction masterOrderNumAction = new GetMasterOrderNumberAction();
		masterOrderNumAction.setCallingSystem(null);
		final String poNumber = masterOrderNumAction.executeAction();
		return poNumber;
	}

	private String getmasterOrderNumber()
	{
		final OrderASUSCanImpl processOrderService = new OrderASUSCanImpl();
		return processOrderService.getMasterOrderNumber();
	}

	public BusinessProcessService getBusinessProcessService()
	{
		return (BusinessProcessService) Registry.getApplicationContext().getBean("businessProcessService");
	}


	@RequestMapping(method = RequestMethod.GET, value = "/carrierAccCode/{carrierAccCode}")
	public void setcarrierAccountCode(final Model model, @PathVariable("carrierAccCode") final String carrierAccCode)
	{
		LOG.info("Inside setcarrierAccountCode Method");
		final FMCustomerData fmCustomerData = fmCustomerFacade.getCurrentFMCustomer();
		LOG.info("UNIT ID" + fmCustomerData.getFmunit().getUid());
		final FMCustomerAccountModel fmCustomerAccModel = (FMCustomerAccountModel) companyB2BCommerceService
				.getUnitForUid(fmCustomerData.getFmunit().getUid());
		fmCustomerAccModel.setCarrierAccountCode(carrierAccCode);
		final String newCarrierAccCode = fmCustomerAccModel.getCarrierAccountCode();
		LOG.info("new carrierAccCode" + newCarrierAccCode);
		getSessionService().setAttribute("carrierAccCode", newCarrierAccCode);
		modelService.save(fmCustomerAccModel);


	}

	@RequestMapping(method = RequestMethod.POST, value = "/carrier/{dc}")
	public String getCarrierName(final Model model, @PathVariable("dc") final String dccode)
	{
		LOG.info("Inside Add getCarrierName Method");
		final List<String> carrier = fmDistrubtionCenterFacade.getCarrierName(dccode);
		model.addAttribute("carrierSize", carrier.size());
		model.addAttribute("carrier", carrier);
		model.addAttribute("defaultCarrierCollect", siteConfigService.getString("fm.default.carrier.collect", "UPS"));
		model.addAttribute("defaultCACarrierCollect", siteConfigService.getString("fm.default.ca.carrier.collect", "UPS"));
		return "pages/fm/ajax/orderDetails";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/shipmethod/{dc}/{carriercode}/{shipToCountry}")
	public String getShipMethod(final Model model, @PathVariable("carriercode") final String carriercode,
			@PathVariable("dc") final String dccode, @PathVariable("shipToCountry") final String shipToCountry)
	{
		LOG.info("Inside Add getShipMethod Method");
		final List<String> shipMethod = fmDistrubtionCenterFacade.getShippingMethodName(carriercode, dccode, shipToCountry);
		model.addAttribute("shipMethodSize", shipMethod.size());
		model.addAttribute("shipmethod", shipMethod);
		return "pages/fm/ajax/orderDetails";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/shippingmethod/{dc}/{carriercode}/{shipcode}/{carrieracccode}/{carriername}/{shipname}")
	public String saveShipMethod(final Model model, @PathVariable("dc") final String dccode,
			@PathVariable("carriercode") final String carriercode, @PathVariable("shipcode") final String shipcode,
			@PathVariable("carrieracccode") final String carrieracccode, @PathVariable("carriername") final String carriername,
			@PathVariable("shipname") final String shipname)
	{
		LOG.info("Inside Add saveShipMethod Method");
		final CartModel cartModel = cartService.getSessionCart();
		if (cartModel.getEntries() != null && !cartModel.getEntries().isEmpty())
		{
			for (final AbstractOrderEntryModel entry : cartModel.getEntries())
			{
				if (entry.getDistrubtionCenter() != null && !entry.getDistrubtionCenter().isEmpty())
				{
					for (final FMDistrubtionCenterModel dcentry : entry.getDistrubtionCenter())
					{
						if (dccode.equalsIgnoreCase(dcentry.getDistrubtionCenterDataCode()))
						{
							LOG.info("INside the DC MOdel For Save SHipping Method");
							dcentry.setCarrierName(carriercode);
							dcentry.setShippingMethod(shipcode);
							dcentry.setCarrierAccountCode(carrieracccode);
							dcentry.setCarrierDispName(carriername);
							dcentry.setShippingMethodName(shipname);
							modelService.save(dcentry);
						}
					}
				}
			}
		}

		model.addAttribute("saveShippingMethod", "Saved");
		return "pages/fm/ajax/orderDetails";
	}

	private Calendar getCutoffTime(final String code)
	{
		final Calendar calculationTime = GregorianCalendar.getInstance();
		final FMDCCenterModel dcdetails = fmDistrubtionCenterFacade.getCutOffTimeForDC(code);
		final Date dcCutoffTime = dcdetails.getEmergencyCutOffTime();
		final Calendar dcCutoffTimeCalendar = GregorianCalendar.getInstance();
		dcCutoffTimeCalendar.setTime(dcCutoffTime);
		final int cutoffHourOfDay = dcCutoffTimeCalendar.get(Calendar.HOUR_OF_DAY);
		final int cutoffMinute = dcCutoffTimeCalendar.get(Calendar.MINUTE);

		LOG.info("cutoffHourOfDay :: " + cutoffHourOfDay);
		LOG.info("cutoffMinute :: " + cutoffMinute);
		final Calendar cutoffTimeCal = new GregorianCalendar(calculationTime.get(GregorianCalendar.YEAR),
				calculationTime.get(GregorianCalendar.MONTH), calculationTime.get(GregorianCalendar.DAY_OF_MONTH), cutoffHourOfDay,
				cutoffMinute);
		LOG.info("DC CutoffTimeCal :: " + cutoffTimeCal.getTime());
		return cutoffTimeCal;
	}

	protected OrderEntryData getOrderEntryData(final long quantity, final String productCode, final Integer entryNumber)
	{

		final OrderEntryData orderEntry = new OrderEntryData();
		orderEntry.setQuantity(quantity);
		orderEntry.setProduct(new ProductData());
		orderEntry.getProduct().setCode(productCode);
		orderEntry.setEntryNumber(entryNumber);

		return orderEntry;
	}

	private CartData setPriceType(final CartData cartData)
	{
		final List<ItemBO> itemBOSession = getSessionService().getAttribute("itemBO");
		final List<OrderEntryData> entryies = new ArrayList<OrderEntryData>();
		boolean nabsPart = true;
		String nabsCode = null;
		String displayPartNumber = null;
		for (final OrderEntryData entry : cartData.getEntries())
		{
			final FMPartModel productModel = partService.getPartForCode(entry.getProduct().getCode());
			for (final FMCorporateModel corp : productModel.getCorporate())
			{
				if ("dummy".equalsIgnoreCase(corp.getCorpcode()) || "FELPF".equalsIgnoreCase(corp.getCorpcode())
						|| "SPDPF".equalsIgnoreCase(corp.getCorpcode()) || "SLDPF".equalsIgnoreCase(corp.getCorpcode()))
				{
					nabsPart = false;
				}
			}
			if (!nabsPart && null != productModel.getFlagcode())
			{
				nabsCode = productModel.getFlagcode().toUpperCase() + productModel.getRawPartNumber();
			}

			for (final ItemBO resolvedItem : itemBOSession)
			{
				 displayPartNumber = resolvedItem.getDisplayPartNumber();
				if ((resolvedItem.getExternalSystem() != null && resolvedItem.getExternalSystem().equals(ExternalSystem.NABS))
						|| (null != resolvedItem.getProductFlag()))
				{
					displayPartNumber = resolvedItem.getProductFlag() + resolvedItem.getDisplayPartNumber();
				}
				if (resolvedItem.getPartNumber().equalsIgnoreCase(entry.getProduct().getCode())
						|| resolvedItem.getDisplayPartNumber().equalsIgnoreCase(entry.getProduct().getCode())
						|| (displayPartNumber.equalsIgnoreCase(nabsCode))||displayPartNumber.equalsIgnoreCase(entry.getProduct().getCode()))
				{
					if (resolvedItem.getItemPrice() != null)
					{
						final PriceBO priceBO = resolvedItem.getItemPrice();
						if (priceBO != null && priceBO.getPriceType() != null)
						{
							if ("WD1".equalsIgnoreCase(priceBO.getPriceType().toString()))
							{
								entry.setPriceType("D");
							}
							else if ("JBR".equalsIgnoreCase(priceBO.getPriceType().toString()))
							{
								entry.setPriceType("J");
							}
							else
							{
								entry.setPriceType(priceBO.getPriceType().toString());
							}
						}
					}

				}
			}
			entryies.add(entry);
		}

		cartData.setEntries(entryies);
		return cartData;
	}

	@RequireHardLogIn
	@RequestMapping(value = "/orderConfirmationExport", method = RequestMethod.GET)
	public ModelAndView downloadPdf(final Model model) throws CMSItemNotFoundException
	{
		LOG.info("PURCHASE ORDER NUMBER" + globalTempDataForSavePdf.getOrderData().getPurchaseOrderNumber());
		LOG.info("PURCHASE ORDER NUMBER" + globalTempDataForSavePdf.getOrderData().getFmordertype());
		//	List a = new ArrayList();
		LOG.info("FROM GLOBAL ordercode" + globalTempDataForSavePdf.getOrderData().getCode());
		LOG.info("FROM GLOBAL soldtounit" + globalTempDataForSavePdf.getSoldToUnit().getUid());
		LOG.info("FROM GLOBAL shiptounit" + globalTempDataForSavePdf.getShipToUnit().getUid());
		LOG.info("FROM GLOBAL shiptoaddress" + globalTempDataForSavePdf.getShipToAddress().getLine1());
		LOG.info("FROM GLOBAL shiptoaddress" + globalTempDataForSavePdf.getShipToAddress().getCompanyName());
		LOG.info("FROM GLOBAL soldtoaddress" + globalTempDataForSavePdf.getSoldToAddress().getLine1());
		LOG.info("FROM GLOBAL soldtoaddress" + globalTempDataForSavePdf.getSoldToAddress().getCompanyName());
		//return new ModelAndView("orderConfirmPDFView", "shippedOrderDetailsBO", globalOrderData);
		return new ModelAndView("orderConfirmPDFView", "globalData", globalTempDataForSavePdf);
	}

	private boolean partModification(final List<ItemBO> ineventoryList)
	{
		boolean isPartModified = false;
		try
		{
			for (final ItemBO resolvedItem : ineventoryList)
			{
				CartData cartData = cartFacade.getSessionCart();
				if (cartData.getEntries() != null && !cartData.getEntries().isEmpty())
				{
					for (final OrderEntryData entry : cartData.getEntries())
					{
						isPartModified = false;
						String code = entry.getProduct().getCode();
						boolean catalogProduct = true;
						final FMPartModel productModel = partService.getPartForCode(entry.getProduct().getCode());
						String dispPart = productModel.getPartNumber();
						for (final FMCorporateModel corp : productModel.getCorporate())
						{
							//dummy(NABS) Product is not allowed for TSC orders so we are deleting from the cart if its contain product flag is null.
							if (("FELPF".equalsIgnoreCase(corp.getCorpcode()) || "SPDPF".equalsIgnoreCase(corp.getCorpcode()) || "SLDPF"
									.equalsIgnoreCase(corp.getCorpcode()))
									|| ("dummy".equalsIgnoreCase(corp.getCorpcode()) && null != productModel.getFlagcode()))
							{
								catalogProduct = false;
								entry.setQuantity((long) (0));
								final CartModificationData cartModification = b2bCartFacade.updateOrderEntry(entry);
								LOG.info("Cart Modification message" + cartModification.getStatusMessage());
								isPartModified = true;

							}

						}

						if (!catalogProduct && productModel.getFlagcode() != null)
						{
							code = productModel.getFlagcode().toUpperCase() + productModel.getRawPartNumber();
							dispPart = productModel.getFlagcode().toUpperCase() + productModel.getPartNumber();
						}


						String displayPartNumber = resolvedItem.getDisplayPartNumber();
						if ((resolvedItem.getExternalSystem() != null && resolvedItem.getExternalSystem().equals(ExternalSystem.NABS))
								|| (null != resolvedItem.getProductFlag()))
						{
							displayPartNumber = resolvedItem.getProductFlag() + resolvedItem.getDisplayPartNumber();
						}
						if (resolvedItem.getPartNumber().equalsIgnoreCase(entry.getProduct().getCode())
								|| displayPartNumber.equalsIgnoreCase(entry.getProduct().getCode())
								|| (code.equalsIgnoreCase(displayPartNumber)) || (dispPart.equalsIgnoreCase(displayPartNumber)))
						{
							if (resolvedItem.getInventory() != null && resolvedItem.getInventory().size() > 0 && !isPartModified)
							{
								final List<InventoryBO> inventorys = resolvedItem.getInventory();
								for (final InventoryBO inventory : inventorys)
								{
									if (inventory.getAvailableQty() < resolvedItem.getItemQty().getRequestedQuantity())
									{
										entry.setQuantity((long) (inventory.getAvailableQty()));
										final CartModificationData cartModification = b2bCartFacade.updateOrderEntry(entry);
										LOG.info("Cart Modification message" + cartModification.getStatusMessage());
										isPartModified = true;
									}
								}
							}
						}
					}
				}

			}
		}
		catch (final Exception e)
		{
			LOG.error("Exception::" + e.getMessage());
			e.printStackTrace();
		}
		LOG.info("removePart :: isTSCPartModified ==>" + isPartModified);
		return isPartModified;
	}
}
