/**
 * 
 */
package com.federalmogul.storefront.forms;

import com.federalmogul.facades.user.data.FMCustomerData;


/**
 * @author Balaji
 * 
 */
public class LeadGenerationCallBackRequestForm extends FMCustomerData
{

	//private LeadGenerationSubjects subjects;
	private String subjects;

	/**
	 * @return the subjects
	 */
	public String getSubjects()
	{
		return subjects;
	}

	/**
	 * @param subjects
	 *           the subjects to set
	 */
	public void setSubjects(final String subjects)
	{
		this.subjects = subjects;
	}

	private String callBackDescription;
	private String jobtitle;
	private String department;

	private String email;
	private String firstName;
	private String lastName;
	private String addressline1;
	private String addressline2;
	private String city;
	private String state;
	private String country;
	private String zipCode;
	private String phoneno;
	private String CompanyName;
	private String accCode;




	/**
	 * @return the email
	 */
	@Override
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *           the email to set
	 */
	@Override
	public void setEmail(final String email)
	{
		this.email = email;
	}

	/**
	 * @return the firstName
	 */
	@Override
	public String getFirstName()
	{
		return firstName;
	}

	/**
	 * @param firstName
	 *           the firstName to set
	 */
	@Override
	public void setFirstName(final String firstName)
	{
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Override
	public String getLastName()
	{
		return lastName;
	}

	/**
	 * @param lastName
	 *           the lastName to set
	 */
	@Override
	public void setLastName(final String lastName)
	{
		this.lastName = lastName;
	}

	/**
	 * @return the addressline1
	 */
	public String getAddressline1()
	{
		return addressline1;
	}

	/**
	 * @param addressline1
	 *           the addressline1 to set
	 */
	public void setAddressline1(final String addressline1)
	{
		this.addressline1 = addressline1;
	}

	/**
	 * @return the addressline2
	 */
	public String getAddressline2()
	{
		return addressline2;
	}

	/**
	 * @param addressline2
	 *           the addressline2 to set
	 */
	public void setAddressline2(final String addressline2)
	{
		this.addressline2 = addressline2;
	}

	/**
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * @param city
	 *           the city to set
	 */
	public void setCity(final String city)
	{
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState()
	{
		return state;
	}

	/**
	 * @param state
	 *           the state to set
	 */
	public void setState(final String state)
	{
		this.state = state;
	}

	/**
	 * @return the country
	 */
	public String getCountry()
	{
		return country;
	}

	/**
	 * @param country
	 *           the country to set
	 */
	public void setCountry(final String country)
	{
		this.country = country;
	}

	/**
	 * @return the zipCode
	 */
	public String getZipCode()
	{
		return zipCode;
	}

	/**
	 * @param zipCode
	 *           the zipCode to set
	 */
	public void setZipCode(final String zipCode)
	{
		this.zipCode = zipCode;
	}

	/**
	 * @return the phoneno
	 */
	public String getPhoneno()
	{
		return phoneno;
	}

	/**
	 * @param phoneno
	 *           the phoneno to set
	 */
	public void setPhoneno(final String phoneno)
	{
		this.phoneno = phoneno;
	}

	/**
	 * @return the companyName
	 */
	public String getCompanyName()
	{
		return CompanyName;
	}

	/**
	 * @param companyName
	 *           the companyName to set
	 */
	public void setCompanyName(final String companyName)
	{
		CompanyName = companyName;
	}

	/**
	 * @return the accCode
	 */
	public String getAccCode()
	{
		return accCode;
	}

	/**
	 * @param accCode
	 *           the accCode to set
	 */
	public void setAccCode(final String accCode)
	{
		this.accCode = accCode;
	}


	//	/**
	//	 * @return the subjects
	//	 */
	//	public LeadGenerationSubjects getSubjects()
	//	{
	//		return subjects;
	//	}
	//
	//	/**
	//	 * @param subjects
	//	 *           the subjects to set
	//	 */
	//	public void setSubjects(final LeadGenerationSubjects subjects)
	//	{
	//		this.subjects = subjects;
	//	}

	/**
	 * @return the callBackDescription
	 */
	public String getCallBackDescription()
	{
		return callBackDescription;
	}

	/**
	 * @param callBackDescription
	 *           the callBackDescription to set
	 */
	public void setCallBackDescription(final String callBackDescription)
	{
		this.callBackDescription = callBackDescription;
	}

	/**
	 * @return the jobtitle
	 */
	public String getJobtitle()
	{
		return jobtitle;
	}

	/**
	 * @param jobtitle
	 *           the jobtitle to set
	 */
	public void setJobtitle(final String jobtitle)
	{
		this.jobtitle = jobtitle;
	}

	/**
	 * @return the department
	 */
	public String getDepartment()
	{
		return department;
	}

	/**
	 * @param department
	 *           the department to set
	 */
	public void setDepartment(final String department)
	{
		this.department = department;
	}


}
