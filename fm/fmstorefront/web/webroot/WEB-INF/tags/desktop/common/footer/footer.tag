<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>


<%-- <cms:pageSlot position="Footer" var="feature" element="div" class="footer">
	<cms:component component="${feature}"/>
</cms:pageSlot> --%>

<%-- working 
<div class="row footerRow">
	 <footer>
	 	
		 <cms:pageSlot position="Footer" var="feature" element="footer">
			<cms:component component="${feature}"/>
		</cms:pageSlot> 
	</footer> 
	

</div>
 --%>

<footer class="footerRow">
  <div class="container">
	 	<div class="col-lg-10 col-lg-offset-1" style="margin-left: 0.333%; width: 106.333%; !important">
			<div class="row">
      			<div class="footerLinks clearfix" >
				 <cms:pageSlot position="Footer" var="feature" >
					<cms:component component="${feature}"/>
				</cms:pageSlot>
 <div class="col-xs-12 col-lg-2 link-container visible-lg visible-md visible-sm">
        <h5>Connect with Us</h5>
            <div class="row">
                  <ul class="unstyled footer-icons mobslider col-xs-8" >          
                     <li class="col-xs-4"><a href="https://www.facebook.com/FMmotorparts?fref=ts" class="fbSprite" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="col-xs-4"><a href="https://twitter.com/FMmotorparts" class="twitterSprite" target="_blank"><i class="fa fa-twitter"></i></a></li>
                   <li class="col-xs-4"><a href="https://www.youtube.com/channel/UCHG0iMdw5inMgvnXiOQS0YQ" class="youtubeSprite" target="_blank"><i class="fa fa-youtube"></i></a></li>       
                  <li class="col-xs-4"><a href="https://instagram.com/fmmotorparts" class="instagramSprite" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li class="col-xs-4"><a href="https://www.linkedin.com/company/federal-mogul-motorparts" class="linkedinSprite" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
	<%-- 
	</br></br>
       <h5>eStore</h5>
            <div class="">
		<div class="" >
                  <ul class="unstyled mobslider" >          
                     <li class=""><a href="#" class="">Gear</a></li>
                </ul>
            	</div>
	</div> --%>

        </div>
			</div>
			<div class="copyRightRow">
				<c:set var="fmComponentName" value="extendedfooterLinks"
					scope="session" />
				<p class="text-muted">
					<cms:pageSlot position="Footer" var="feature">
						<cms:component component="${feature}"/>
					</cms:pageSlot>
				</p>
			</div>
		</div>	 
	</div>
	</div>
</footer>	