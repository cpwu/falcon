<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="hideHeaderLinks" required="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="header" tagdir="/WEB-INF/tags/desktop/common/header" %>



<%-- Test if the UiExperience is currently overriden and we should show the UiExperience prompt --%>
<c:if
	test="${uiExperienceOverride and not sessionScope.hideUiExperienceLevelOverridePrompt}">
	<c:url value="/_s/ui-experience?level="
		var="clearUiExperienceLevelOverrideUrl" />
	<c:url value="/_s/ui-experience-level-prompt?hide=true"
		var="stayOnDesktopStoreUrl" />
	<div class="backToMobileStore">
		<a href="${clearUiExperienceLevelOverrideUrl}"><span
			class="greyDot">&lt;</span> <spring:theme
				code="text.swithToMobileStore" /></a> <span class="greyDot closeDot"><a
			href="${stayOnDesktopStoreUrl}">x</a></span>
	</div>
</c:if>




<%-- Test if the UiExperience is currently overriden and we should show the UiExperience prompt --%>

<c:if
	test="${uiExperienceOverride and not sessionScope.hideUiExperienceLevelOverridePrompt}">
	<c:url value="/_s/ui-experience?level="
		var="clearUiExperienceLevelOverrideUrl" />
	<c:url value="/_s/ui-experience-level-prompt?hide=true"
		var="stayOnDesktopStoreUrl" />
	<div class="backToMobileStore">
		<a href="${clearUiExperienceLevelOverrideUrl}"><span
			class="greyDot">&lt;</span> <spring:theme
				code="text.swithToMobileStore" /></a> <span class="greyDot closeDot"><a
			href="${stayOnDesktopStoreUrl}">x</a></span>
	</div>
</c:if>

<!-- Anonymous Page -->
<c:if
	test="${customerType ne 'b2bCustomer' and customerType ne 'b2BCustomer' and customerType ne 'b2cCustomer' and customerType ne 'CSRCustomer' and customerType ne 'FMAdmin'}">
	<div class="clearfix">
		<header class="headerRow">
			<div class="container">
				<div class="row">
					<header:language/>
					<div class="col-lg-8  clearfix visible-lg visible-md visible-sm">
						<div class="clearfix ">
							<div class="extreemTopRightContent">
								<ul class="quicklinks pull-right ">
								    <li><a href="http://www.partsmatter.com" target="_blank"><spring:theme code="header.parts.matter"/></a></li>
									<sec:authorize ifNotGranted="ROLE_FMB2BB">								   
									  	<li><span class="fa  fa-map-marker"></span><a href="<c:url value="/where-to-buy"/>"> Where to
											Buy?</a></li> 
									</sec:authorize>
									<!--  <li><a href="#" class="">Register</a></li>  -->

									<li><span class="fa  fa-user"></span>  <a href="<c:url value='/sign-in'/>" class="">Sign In</a></li>

								</ul>
							</div>
						</div>
						<div class="clearfix ">
							<div class="extreemTopRightContent ">
								<div class="contactOrPromotionMsg  pull-right">
									<%-- <a href="<c:url value="/leadGeneration/contact-us"/>"><span class="glyphicon glyphicon-envelope"></span> --%>
									<a href="<c:url value="/support/contact-us/supportgeneralInquiry?complnkname=Contact Form"/>"><span class="glyphicon glyphicon-envelope"></span>
										Email</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	</div>
</c:if>

<%-- <cms:pageSlot position="SearchBox" var="component">
			
<cms:component component="${component}"/>
		
</cms:pageSlot> --%>

<!-- B2B Page  -->

<c:if
	test="${customerType eq 'b2bCustomer' or customerType eq 'b2BCustomer'}">

	<header class="headerRow">
		<div class="container">
			<div class="row">
				<header:language/>
				<div class="col-lg-8  col-md-9 col-xs-2 clearfix ">
					<div class="clearfix ">
						<div class="extreemTopRightContent">
							<!-- show quick links on other device -->
							<ul class="quicklinks pull-right visible-lg visible-md">

								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="DINWebBold"><ycommerce:testId
											code="header_LoggedUser">
											<spring:theme code="header.welcome"
												arguments="${user.firstName} ${user.lastName}"
												htmlEscape="true" />
										</ycommerce:testId></li>
								</sec:authorize>

								<c:if test="${!fn:contains(siteName,'Loyalty')  }" >
								<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/my-fmaccount/profile"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>
								<c:if test="${fn:contains(siteName,'Loyalty')  }" >
									<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/loyalty/history?clear=true&site=federalmogul"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>	 
								<!-- <c:url value="/where-to-buy"/>  -->
								<li><a href="http://www.partsmatter.com" target="_blank"><spring:theme code="header.parts.matter"/></a></li>
								<sec:authorize ifNotGranted="ROLE_FMB2BB">
									  <li><span class="fa  fa-map-marker"></span><a href="${contextPath}/federalmogul/en/USD/where-to-buy"> Where to
										Buy?</a></li>
								</sec:authorize>
								<!-- <li><a href="#" class="text-capitalize"><span
										class="fa fa-user"></span> My account</a></li> -->

								<%--  <li><ycommerce:testId code="header_myAccount">
										<a href="<c:url value='/my-account'/>"><spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li> --%>


								<%-- <sec:authorize ifAnyGranted="ROLE_B2BADMINGROUP">
									<spring:url
										value="/my-company/organization-management/manage-users"
										var="encodedUrl" />
									<li><a href="${encodedUrl}" class="selected"><spring:theme
												code="header.link.account" /></a></li>
								</sec:authorize> --%>


								<%-- 	<sec:authorize ifAnyGranted="ROLE_B2BADMINGROUP">
									<li><ycommerce:testId code="header_myCompany">
											<a href="<c:url value='/my-network'/>">My Network</a>
										</ycommerce:testId></li>
								</sec:authorize>  --%>

								<%-- <sec:authorize ifAnyGranted="ROLE_B2BADMINGROUP">
									<li><ycommerce:testId code="header_myCompany">
											<a href="<c:url value='/my-company'/>">My Network</a>
										</ycommerce:testId></li>
								</sec:authorize> --%>

								<!--  <li><a href="#" class="">Register</a></li>  -->
								<!-- <li><a href="#" class="text-capitalize">Sign out</a></li> -->

								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="text-capitalize"><ycommerce:testId
											code="header_signOut">
											<a href="${contextPath}/federalmogul/${currentLanguage.isocode}/${currentCurrency.isocode}/logout"><spring:theme
													code="header.link.logout" /></a>
										</ycommerce:testId></li>
								</sec:authorize>

								<!-- Fix as part of FAL-20 remove pipe symbol after signout if no shopping cart  -->
								 <c:if test="${!fn:contains(siteName,'Loyalty')  }">
									<sec:authorize ifNotGranted="ROLE_FMB2T">
										<cms:pageSlot position="MiniCart" var="cart" limit="7">
											<cms:component component="${cart}" element="li"
												class="miniCart" />
										</cms:pageSlot>
									</sec:authorize>
								</c:if>
								<c:if test="${fn:contains(siteName,'Loyalty')  }">
										<cms:pageSlot position="MiniCart" var="cart" limit="7">
											<cms:component component="${cart}" element="li"
												class="miniCart" />
										</cms:pageSlot>
								</c:if> 

								<sec:authorize ifAnyGranted="ROLE_FMCSR,ROLE_FMBUVOR">
									<c:if test="${csrAccountEmulation eq 'true'}">
										<li class="text-capitalize"><ycommerce:testId
												code="header_signOut">
												<a href="<c:url value='/csr-emulation/end-emulate'/>">End
													Emulate (${accountId}${accountNm}) </a>
											</ycommerce:testId></li>
									</c:if>

								</sec:authorize>

								<c:if test="${fn:contains(siteName,'Loyalty')  }">
									<li><a
										href="${contextPath}/loyalty/${currentLanguage.isocode}/${currentCurrency.isocode}/loyaltycart"
										class="text-capitalize"><span class="fa fa-shopping-cart"></span>&nbsp;Shopping
											Cart<span class="cartDigit">${totalItems}</span></a>
									</li>
								</c:if>
								<!-- <a href="#"  class="miniCart" onClick="$('.miniCartPopup').show();"><span
										class="fa fa-shopping-cart"></span> Shopping Cart&nbsp;&nbsp;<span
										class="cartDigit">${totalItems}</span></a></li> -->
							</ul>
						</div>
					</div>
					<div class="clearfix visible-lg visible-md">
						<div class="extreemTopRightContent ">
							<div class="contactOrPromotionMsg  pull-right">
								<c:url value="/online-tools/fmHelpCenter" var="helpcenterUrl" />
								<a href="/fmstorefront/support/technical-line"><span class="fa fa-phone"></span> Call</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a
									href="<c:url value="/support/contact-us/supportgeneralInquiry?complnkname=Contact Form"/>"><span class="fa fa-envelope"></span> Email</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a
									target="_blank" href="https://webchat.fmmotorparts.com/system/templates/chat/sunburst/chat.html?entryPointId=1000&templateName=sunburst&languageCode=en&countryCode=US&ver=v11" onclick="window.open(this.href, 'mywin',
'location=1,left=20,top=20,width=425,height=600,toolbar=1,resizable=0'); return false;"><span class="fa fa-comment"></span> Chat</a>&nbsp;&nbsp;&nbsp; |
									&nbsp;&nbsp;&nbsp;<span class="fa fa-question-circle"></span><a href="${helpcenterUrl}"> Help Center</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</c:if>

<!--B2C Home page  -->
<c:if test="${customerType eq 'b2cCustomer' || customerType eq 'FMAdmin'}">

	<header class="headerRow" itemscope
		itemtype="http://schema.org/Organization">
		<div class="container">
			<div class="row">
				<header:language/>
				<div class="col-lg-8  col-md-9 col-xs-2 clearfix ">
					<div class="clearfix ">
						<div class="extreemTopRightContent">
							<!-- show quick links on other device -->
							<ul class="quicklinks pull-right visible-lg visible-md">

								<!-- 
							<li><strong class="DINWebBold">Welcome,</strong> Teresa of
								Company XYZ</li> -->

								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="DINWebBold"><ycommerce:testId
											code="header_LoggedUser">
											<spring:theme code="header.welcome"
												arguments="${user.firstName} ${user.lastName}"
												htmlEscape="true" />
										</ycommerce:testId></li>
								</sec:authorize>


								<!-- <li><a href="#" class="text-capitalize"><span
										class="fa fa-user"></span> My account</a></li> -->

								<c:if test="${!fn:contains(siteName,'Loyalty')  }" >
								<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/my-fmaccount/profile"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>
								
								<c:if test="${fn:contains(siteName,'Loyalty')  }" >
									<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/loyalty/history?clear=true&site=federalmogul"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>	
								
								<sec:authorize ifNotGranted="ROLE_FMB2BB">
									<li><span class="fa  fa-map-marker"></span><a href="<c:url value="/where-to-buy"/>"> Where to
										Buy?</a></li>
								</sec:authorize>
								<!--  <li><a href="#" class="">Register</a></li>  -->

								<!-- <li><a href="#" class="text-capitalize">Sign out</a></li> -->

								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="text-capitalize"><ycommerce:testId
											code="header_signOut">
											<a href="<c:url value='/logout'/>"><spring:theme
													code="header.link.logout" /></a>
										</ycommerce:testId></li>
								</sec:authorize>

								<li><a href="<c:url value='/cart'/>"
									class="text-capitalize"><span class="fa fa-shopping-cart
"></span>Shopping Cart<span class="cartDigit">${totalItems}</span></a></li>
							</ul>
							<!-- show quick links on mobile -->
							<ul class="quicklinks pull-right visible-xs ">
								<li><a href="#" class=""><span class="fa fa-map-marker"></span></a></li>
								<!--  <li><a href="#" class="">Register</a></li>  -->
								<li><a href="#" class=""><span class="fa fa-user"></span></a></li>
								<li><a href="#" class=""><span
										class="fa fa-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="clearfix visible-lg visible-md">
						<div class="extreemTopRightContent ">
							<div class="contactOrPromotionMsg  pull-right">
								<c:url value="/online-tools/fmHelpCenter" var="helpcenterUrl" />
								<a href="/fmstorefront/support/technical-line"><span class="fa fa-phone"></span> Call</a>&nbsp;&nbsp;&nbsp;<a
									href="<c:url value="/support/contact-us/supportgeneralInquiry?complnkname=Contact Form"/>"><span class="fa fa-envelope"></span> Email</a>&nbsp;&nbsp;
								<a href="https://webchat.fmmotorparts.com/system/templates/chat/sunburst/chat.html?entryPointId=1000&templateName=sunburst&languageCode=en&countryCode=US&ver=v11" onclick="window.open(this.href, 'mywin',
'location=1,left=20,top=20,width=425,height=600,toolbar=1,resizable=0'); return false;"><span class="fa fa-comment"></span> Chat</a>&nbsp;&nbsp;&nbsp; |
									&nbsp;&nbsp;&nbsp;<span class="fa fa-question-circle"></span><a href="${helpcenterUrl}"> Help Center</a>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</c:if>

<c:if test="${customerType eq 'CSRCustomer'}">

	<header class="headerRow" itemscope
		itemtype="http://schema.org/Organization">
		<div class="container">
			<div class="row">
				<header:language/>
				<div class="col-lg-8  col-md-9 col-xs-2 clearfix ">
					<div class="clearfix ">
						<div class="extreemTopRightContent">
							<!-- show quick links on other device -->
							<ul class="quicklinks pull-right visible-lg visible-md">

								<!-- 
							<li><strong class="DINWebBold">Welcome,</strong> Teresa of
								Company XYZ</li> -->

								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="DINWebBold"><ycommerce:testId
											code="header_LoggedUser">
											<spring:theme code="header.welcome"
												arguments="${user.firstName},${user.lastName}"
												htmlEscape="true" />
										</ycommerce:testId></li>
								</sec:authorize>

								<c:if test="${!fn:contains(siteName,'Loyalty')  }" >
								<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/my-fmaccount/profile"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>
								
								<c:if test="${fn:contains(siteName,'Loyalty')  }" >
									<li><ycommerce:testId code="header_myAccount">
										<a href="${contextPath}/federalmogul/en/USD/loyalty/history?clear=true&site=federalmogul"><span class="fa fa-user"></span> <spring:theme
												code="header.link.account" /></a>
									</ycommerce:testId></li>
								</c:if>	
								<li><a href="http://www.partsmatter.com" target="_blank"><spring:theme code="header.parts.matter"/></a></li>
								<sec:authorize ifNotGranted="ROLE_FMB2BB">
									<li><span class="fa  fa-map-marker"></span><a href="<c:url value="/where-to-buy"/>"> Where to
										Buy?</a></li>
								</sec:authorize>
								<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">
									<li class="text-capitalize"><ycommerce:testId
											code="header_signOut">
											<a href="<c:url value='/logout'/>"><spring:theme
													code="header.link.logout" /></a>
										</ycommerce:testId></li>
								</sec:authorize>
								<c:if test="${csrAccountEmulation eq 'true'}">
									<sec:authorize ifNotGranted="ROLE_FMBUVOR">
										<cms:pageSlot position="MiniCart" var="cart" limit="7">
									<cms:component component="${cart}" element="li"
										class="miniCart" />
								</cms:pageSlot>
									</sec:authorize>


									<sec:authorize ifAnyGranted="ROLE_CUSTOMERGROUP">

										<li class="text-capitalize"><ycommerce:testId
												code="header_signOut">
												<a href="<c:url value='/csr-emulation/end-emulate'/>">End
													Emulate (${accountId}${accountNm})</a>
											</ycommerce:testId></li>

									</sec:authorize>
								</c:if>

							</ul>
							<!-- show quick links on mobile -->
							<ul class="quicklinks pull-right visible-xs ">
								<li><a href="#" class=""><span class="fa fa-map-marker"></span></a></li>
								<!--  <li><a href="#" class="">Register</a></li>  -->
								<li><a href="#" class=""><span class="fa fa-user"></span></a></li>
								<li><a href="#" class=""><span
										class="fa fa-shopping-cart"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="clearfix visible-lg visible-md">
						<div class="extreemTopRightContent ">
							<c:url value="/online-tools/fmHelpCenter" var="helpcenterUrl" />
							<div class="contactOrPromotionMsg  pull-right">
								<a href="/fmstorefront/support/technical-line"><span class="fa fa-phone"></span> Call</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
									href="<c:url value="/support/contact-us/supportgeneralInquiry?complnkname=Contact Form"/>"><span class="fa fa-envelope"></span> Email</a>&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="https://webchat.fmmotorparts.com/system/templates/chat/sunburst/chat.html?entryPointId=1000&templateName=sunburst&languageCode=en&countryCode=US&ver=v11" onclick="window.open(this.href, 'mywin','location=1,left=20,top=20,width=425,height=600,toolbar=1,resizable=0'); return false;"><span class="fa fa-comment"></span> Chat</a>
								&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<span class="fa fa-question-circle"></span><a href="${helpcenterUrl}"> Help Center</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
</c:if>

