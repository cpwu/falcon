<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<div class="col-lg-4 col-md-3 col-xs-4">
	<div class="dropdown visible-lg visible-md">
		<button class="btn lang-logo btn-default dropdown-toggle languageDrpDwn"
			type="button" id="dropdownMenu1" data-toggle="dropdown">
			<img class="" alt="United	States"
				src="${contextPath}/_ui/desktop/common/images/us.png">United
			States <span class="caret"></span>
		</button>
		<ul class="dropdown-menu lang-logo" role="menu" aria-labelledby="dropdownMenu1">
			<li role="presentation"><a role="menuitem" tabindex="-1"
				href="/"><img class="" alt="Brazil"
					src="${contextPath}/_ui/desktop/common/images/us.png">United
					States</a></li>
			<li><a target="_Blank" href="http://www.federalmogulmp.com/pt-BR"><img
					class="" alt="Brazil"
					src="${contextPath}/_ui/desktop/common/images/brasil.png">Brasil</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/cs-CZ"><img
					class="" alt="Czech Republic"
					src="${contextPath}/_ui/desktop/common/images/czechrepublic.png">Czech
					Republic</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/fr-FR"><img
					class="" alt="France"
					src="${contextPath}/_ui/desktop/common/images/france.png">France</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/de-DE"><img
					class="" alt="Germany"
					src="${contextPath}/_ui/desktop/common/images/germany.png">Deutschland</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/it-IT"><img
					class="" alt="Italy"
					src="${contextPath}/_ui/desktop/common/images/italy.png">Italy</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/es-ES"><img
					class="" alt="Spain"
					src="${contextPath}/_ui/desktop/common/images/spain.png">Spain</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/en-GB"><img
					class="" alt="United Kingdom"
					src="${contextPath}/_ui/desktop/common/images/uk.png">United
					Kingdom</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/zh-CN"><img
					class="" alt="China"
					src="${contextPath}/_ui/desktop/common/images/china.png">China</a></li>

			<li><a target="_Blank" href="http://www.federalmogulmp.com/ru-RU"><img
					class="" alt="Russia"
					src="${contextPath}/_ui/desktop/common/images/russia.png">Russia</a></li>
		</ul>
	</div>
	<cms:pageSlot position="SiteLogo" var="logo" limit="1">
		<cms:component component="${logo}" class="siteLogo" element="div" />
	</cms:pageSlot>
</div>


