<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ attribute name="selected" required="false" type="java.lang.String"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>

<!-- Starts: Manage Account Right hand side panel -->

<div class="row">
	<div class="col-md-12">
		<h1 class="text-uppercase">Contact Form</h1>
	</div>
</div>
          <div id="globalMessages">
		<common:globalMessages />
	</div>
<section class="">
	<p class="reqField">
		<span class="fm_fntRed">*</span>Required Fields
	</p>
	<c:url value="/leadGeneration/contact-us-submit" var="encodedUrl" />
	<form:form action="${encodedUrl}" method="post"
		commandName="callBackFormData" class="manageUserForm" id="callBackFormDataForm">
		<div class="leadGen">
		<div class="mngUserborder clearfix">
			<div class="">
				<label class="text-capitalize" for="subjects">subject<span
					class="required fm_fntRed">*</span>
				</label>
				<form:select name="subjects" path="subjects" id="subjects" Required="true"
					class="form-control width270">
					<option value="" selected="selected">Select Query Type</option>
					<c:forEach items="${leadGenerationSubjects}" var="subjects">
						<option value="${subjects}">${subjects}</option>
					</c:forEach>
				</form:select>
				   <div class="errorMsg fm_fntRed" style="display:none;">Please Select Query Type </div>
			</div>
			<div class="">

				<label class="text-capitalize" for="callBackDescription">Message<span
					class="required fm_fntRed">*</span></label>
				<form:textarea class="form-control width448"
					name="callBackDescription" path="callBackDescription"
					id="callBackDescription" maxlength="500" type="text"
					Required="true"></form:textarea>
					<div class="errorMsg fm_fntRed" style="display:none;">Please enter Message</div>
				<span class="char-count"></span>

			</div>
			<div class="pageSubHeading text-capitalize topMargn_20">your
				information</div>
			<div class="">
				<label class="text-capitalize" for="firstName">First name<span
					class="required fm_fntRed">*</span></label>
				<form:input type="text" name="firstName" id="firstName"
					path="firstName" class="form-control width270" Required="true"
					value="${callBackFormData.firstName}" />
					<div class="errorMsg fm_fntRed" style="display:none;">Please enter your First Name </div>
			</div>
			<div class="">
				<label class="text-capitalize" for="lastName">Last name<span
					class="required fm_fntRed">*</span></label>
				<form:input type="text" name="lastName" id="lastName"
					path="lastName" class="form-control width270" Required="true"
					value="${callBackFormData.lastName}" />
					<div class="errorMsg fm_fntRed" style="display:none;">Please enter your Last Name </div>
			</div>
			<div class="">
				<label class="text-capitalize" for="email">Email Address<span
					class="required fm_fntRed">*</span></label>
				<form:input type="email" name="email" id="email" path="email"
					class="form-control width270" Required="true"
					value="${callBackFormData.email}" />
					<div class="errorMsg fm_fntRed" style="display:none;">Please enter your Email Address</div>
				<div class="">
					<label class="text-capitalize" for="phoneno">Telephone<span
						class="required fm_fntRed">*</span></label>
					<form:input type="tel" name="phoneno" id="phoneno" path="phoneno" maxlength="15"
						class="form-control width270" Required="required"
						value="${callBackFormData.phoneno}" />
						<div class="errorMsg fm_fntRed" style="display:none;">Please enter Phone Number</div>
				</div>
			</div>

			<%--<div class="">
				<label class="text-capitalize" for="department">Department</label>
				<form:input type="text" name="department" id="department"
					path="department" class="form-control width270" />
			</div>--%>
			<div class="">
				<label class="text-capitalize" for="CompanyName">company</label>
				<form:input type="text" name="CompanyName" id="CompanyName"
					path="CompanyName" class="form-control width270"
					value="${callBackFormData.companyName}" />
			</div>
			<div class="">
				<label class="text-capitalize" for="jobtitle">job title</label>
				<form:input type="text" name="jobtitle" id="jobTitle"
					path="jobtitle" class="form-control width270" />
			</div>
			<div class="">
				<label class="" for="address">Address</label>
				<form:input id="addressline1" type="text" name="addressline1"
					path="addressline1" class="form-control width270"
					value="${callBackFormData.addressline1}" />
				<form:input id="addressline2" type="text" name="addressline2"
					path="addressline2" class="form-control width270 topMargn_10"
					value="${callBackFormData.addressline2}" />
			</div>
			<div class="">
				<label class="" for="city">City</label>
				<form:input id="city" type="text" name="city" path="city"
					class="form-control width270" value="${callBackFormData.city}" />
			</div>
			<div class="">
				<label class="" for="state">State / Province</label>
				<form:select id="state" name="state" path="state"
					class="form-control width270" onBlur="getCountryLeadGenCallBack()">
					<c:choose>
						<c:when test="${callBackFormData.state eq null}">
							<!-- 	<option value="Default">Select</option> -->
							<option value="" selected="selected">Select</option>
						</c:when>
						<c:otherwise>
							<option value="${callBackFormData.state}">${callBackFormData.state}</option>
						</c:otherwise>
					</c:choose>
					<c:forEach items="${regionsdatas}" var="reg">
						<c:forEach items="${reg}" var="val">
							<c:if test="${fn:contains(val.isocode,'US-')}">
								<option value="${val.isocode}">${val.name}</option>
							</c:if>
							<c:if test="${fn:contains(val.isocode,'CA-')}">
								<option value="${val.isocode}">${val.name}</option>
							</c:if>
						</c:forEach>
					</c:forEach>
				</form:select>
			</div>
			<div class="">
				<label class="" for="zipCode"><span class="text-uppercase">Zip</span>
					/ Postal Code</label>
				<form:input type="text" id="zipCode" name="zipCode" path="zipCode"
					class="form-control width270" value="${callBackFormData.zipCode}" />
			</div>
			<div class="">
				<label class="" for="country">Country</label>
				<form:select id="country" name="country" path="country"
					class="form-control width270">

					<c:choose>
						<c:when test="${callBackFormData.country eq null}">
							<!-- <option value="Default">Select</option> -->
							<option value="" selected="selected">Select</option>
						</c:when>
						<c:otherwise>
							<option value="${callBackFormData.country}">${callBackFormData.country }</option>
						</c:otherwise>
					</c:choose>
					<c:forEach items="${countryData}" var="iso">

						<option value="${iso.isocode}">${iso.name}</option>

					</c:forEach>
				</form:select>
			</div>
		</div>
		</div>

		<div class="profileBtn topMargn_25 btmMrgn_30">
			<!-- <a href="profile_password.html"></a> -->
			<button id="submit" class="btn btn-fmDefault" type="submit">submit</button>
			<a class="btn btn-link btnReset text-capitalize topMargn"
				type="reset"
				href="/fmstorefront/federalmogul/en/USD/leadGeneration/contact-us-reset"><button
					class="btn btn-sm btn-fmDefault btn-fm-Grey text-uppercase pull-right"
					data-dismiss="modal">Reset</button></a>
		</div>
		
	</form:form>
</section>


