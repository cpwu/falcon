<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>

<c:url value="/search/" var="searchUrl"/>
<c:url value="/search/autocomplete/${component.uid}" var="autocompleteUrl"/>

<%-- <div class="row">
	<nav class="navbar navbar-inverse fm-navbar clearfix" role="navigation">
		<div class="col-lg-10 col-lg-offset-1">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<!--<a class="navbar-brand navbarEstore" href="#">eStore</a>-->
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<cms:pageSlot position="NavigationBar" var="component">
						<cms:component component="${component}" />
					</cms:pageSlot>
				</ul>
			</div>
		</div>
	</nav>
</div> --%>


<nav role="navigation" class="navbar navbar-inverse fm-navbar ">
	<div class="container">
		<div class="row">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav col-sm-10 col-md-10 ">
					<cms:pageSlot position="NavigationBar" var="component">
						<cms:component component="${component}" />
					</cms:pageSlot>

				</ul>
				<div
					class="col-sm-2 col-md-2 navbar-right searchBar smallB2b-SearhBar">
					<!-- <form class="navbar-form smallB2b-Navform" role="search"
						name="search_form" method="get" action="${searchUrl}">
						<div class="input-group">
							<div class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<input type="text" class="form-control" placeholder="Site Search"
								name="q">
						</div>
					</form> -->
			<c:if test="${currentsiteUID ne 'loyalty'}" >
					<form enctype="multipart/form-data" method="POST" action="/fmstorefront/federalmogul/en/USD/catalog/part-Number-search" class="ymmForm navbar-form smallB2b-Navform" id="partInterchangeForm">
                		<div class="input-group">
	                		<div class="input-group-btn">
								<button class="btn btn-default" type="submit">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<input type="text" name="partNumber" placeholder="Part # Search" class="form-control" required="required" id="externalPart">
						</div>
                		<!-- <div class="form-group regFormFieldGroup"> 
                  			<input type="text" name="partNumber" placeholder="Part#" class="form-control" required="required" id="externalPart">
                		</div>               
                		 <div class="form-group topMargn_25"> <button class="btn  btn-sm btn-fmDefault" type="submit">Look it Up</button> </div> -->
                            
             	 		</form>
			   </c:if>
				</div>

			</div>
		</div>
	</div>
</nav>

