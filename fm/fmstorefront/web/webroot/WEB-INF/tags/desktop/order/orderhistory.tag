<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<c:if test="${customerType eq 'b2BCustomer'}">
	<c:set var="B2BSubtotal" value="0"/>
	 <c:forEach items="${cartData.entries}" var="entry">
	 	<c:if test="${entry.basePrice.value > 0}">
		 	<c:forEach items="${entry.distrubtionCenter}" var="dcentry">
		 			<c:choose>
		  				<c:when test="${dcentry.backorderFlag =='nothing'}">
		  					<c:set var="qty" value="${dcentry.backorderQTYAll}"/>
		  				</c:when>
		  				<c:when test="${dcentry.backorderFlag == 'partial'}">
		  					<c:set var="qty" value="${(dcentry.availableQTY + dcentry.backorderQTY)}"/>
		  				</c:when>
		  				<c:otherwise>
		  					<c:set var="qty" value="${dcentry.availableQTY}"/>
		  				</c:otherwise>
		  			</c:choose>
		  			<c:set var="B2BSubtotal" value="${B2BSubtotal +(entry.basePrice.value * qty )}" />
		 	</c:forEach>
	 	</c:if>
	 </c:forEach>
</c:if>

              <table  class="orderSummaryTable">
                <tbody>
                  <tr>
                    <td class="text-left text-capitalize">subtotal</td>
                    <format:price priceData="${order.totalPriceWithTax}"/>
                    <c:if test="${customerType eq 'b2BCustomer'}">
                    	<td class="text-right"> 
			<c:if test="${empty pickUpStore }">
				<format:fmprice price="${B2BSubtotal}"/>
			</c:if>

			<c:if test="${not empty pickUpStore }">
		 		<format:price priceData="${cartData.totalPrice}"/>
		 	</c:if>
			</td>
                    </c:if>
                    <c:if test="${customerType ne 'b2BCustomer'}">
                    	<td class="text-right"> <format:price priceData="${cartData.subTotal}"/></td>
                    </c:if>
                  </tr>
                  <c:if test="${customerType eq 'b2bCustomer' or customerType eq 'b2cCustomer'}">
	                  <tr class="fm_fntRed">
	                    <td class="text-left text-capitalize">savings</td>
	                    <td class="text-right"><format:price priceData="${cartData.totalDiscounts}"/></td>
	                  </tr>
                 
	                  <tr>
	                    <td class="text-left text-capitalize">shipping</td>
	                    <td class="text-right"><format:price priceData="${cartData.deliveryCost}" displayFreeForZero="true"/></td>
	                  </tr>
		 			</c:if>
                  <c:if test="${customerType eq 'b2cCustomer'}">
	                  <tr>
	                    <td class="text-left text-capitalize">taxes</td>
	                    <td class="text-right"><format:price priceData="${cartData.totalTax}"/></td>
	                  </tr>
	              </c:if>
                  <tr class="estTotal">
                    <td class="text-left text-capitalize">estimated total</td>
                    <c:if test="${customerType eq 'b2BCustomer'}">
                    	<td class="text-right"> 
			<c:if test="${empty pickUpStore }">
				<format:fmprice price="${B2BSubtotal}"/>
			</c:if>

			<c:if test="${not empty pickUpStore }">
		 		<format:price priceData="${cartData.totalPrice}"/>
		 	</c:if>
			</td>

                    </c:if>
                    <c:if test="${customerType ne 'b2BCustomer'}">
                    	<td class="text-right"><format:price priceData="${cartData.totalPrice}"/></td>
                    </c:if>
                    
                  </tr>
                </tbody>
              </table>
           