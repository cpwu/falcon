<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
 
 
 
<c:if test="${fmComponentName == 'extendedfooterLinks'}">
       <div class="">
              <p class="text-muted">
              &copy; <script>document.write(new Date().getFullYear())</script>    Federal-Mogul Motorparts
             
                     <c:forEach items="${navigationNodes}" var="node">
                           <c:if test="${node.visible}">
                          
                          
                                 
                                  <c:forEach items="${node.links}" step="${component.wrapAfter}"
                                         varStatus="i">
                                         <c:forEach items="${node.links}" var="childlink"
                                                begin="${i.index}" end="${i.index + component.wrapAfter - 1}">
                                               
                                                | <a href="${childlink.url}">${childlink.linkName}</a>
 
                                         </c:forEach>
                                  </c:forEach>
 
                           </c:if>
                     </c:forEach>
              </p>
       </div>
</c:if>
 


