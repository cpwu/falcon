<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product"%>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format"%>

<spring:theme code="text.addToCart" var="addToCartText"/>
<spring:theme code="text.popupCartTitle" var="popupCartTitleText"/>
<c:url value="/cart" var="cartUrl"/>
<c:url value="/cart/checkout" var="checkoutUrl"/>


<button type="button" class="close" onclick="inventoryClose()"><span aria-hidden="true" class="fa fa-close"></span><span class="sr-only">Close</span></button>
    
<h2 class="panel-title text-uppercase quickOrderTitle"><span class="fa fa-exchange"></span> <span class="text-uppercase">Inventory check</span></h2>

<br>
<c:if test="${empty inventory && not empty ErrorMessage}">
	<div class="cart_modal_popup empty-popup-cart">
		<span style="color:red">${ErrorMessage} </span>
	</div>
</c:if>
<c:if test="${not empty inventory}">
<div class="table-responsive col-lg-12 userTable">
<table id="myTable" class="table tablesorter" >
	<thead>
	  <tr>
	    <th class="text-capitalize">Part Number</th>
	    <th class="text-capitalize">Location</th>
	    <th class="text-capitalize">Available</th>
	  </tr>
	</thead>
	<tbody>
		<c:forEach items="${inventory}" var="item">
			<c:forEach items="${item.inventory}" var="inventory">
				<tr>    
		            <td class="text-uppercase">${item.displayPartNumber}</td>
		            <td class="text-capitalize">${inventory.distributionCenter.name}</td>
		            <c:choose>
		            <c:when test="${ inventory.availableQty > item.itemQty.requestedQuantity}">
		            	<td class="text-capitalize">${item.itemQty.requestedQuantity}+ </td>
		            </c:when>
		            <c:otherwise>
		            	<td class="text-capitalize">${inventory.availableQty }  </td>
		            </c:otherwise>
		            </c:choose>
		            
				</tr>
			</c:forEach>
		</c:forEach>
	</tbody>
</table>
</div>

</c:if>

 
