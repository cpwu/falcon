<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="federalmogul" tagdir="/WEB-INF/tags/desktop/federalmogul"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!-- jsp for getting the fm registration details -->

<template:page pageTitle="${pageTitle}">
	<%-- <c:out value="<%=request.getContextPath()%>"/> --%>

<section class="breadcrumbPanel visible-lg visible-md visible-sm" itemscope itemtype="http://schema.org/Product">
  <div class="container">
    <div class="row">
      <ul class="breadcrumb">
        <breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}"/>
      </ul>
    </div>
  </div>
</section>
	<section class="productDetailPage pageContet">
		<section class="registrationContent">
			<div class="container">
				<div class="clearfix bgwhite">
					<div class=" regFrm">
						<h3 class="text-uppercase  registrationHeading">Registration</h3>
						<p class="reqField">
							<span class="fm_fntRed">*</span> Required Fields
						</p>
						<div id="globalMessages">
							<common:globalMessages />
						</div>
						<c:url value="/registration/createcustomer" var="submitAction" />

						<form:form class="registrationB2b" method="POST"
							commandName="fmdata" id="registrationB2b" name="registrationB2b"
							action="${submitAction}" enctype="multipart/form-data">

							<input type="hidden" id="technicianType" name="technicianType"/>
							<div class="panel  panel-primary panel-frm ">
								<div class="panel-heading clickable ">
									<h3 class="panel-title text-uppercase">Account Type</h3>
									<span class="pull-right "><i class="fa fa-minus"></i></span>
								</div>
								<div class="panel-body abc">
									<div class="form-group  regFormFieldGroup">
										<label class="" for="whatdescribes">What Best
											Describes You?<span class="fm_fntRed">*</span>
										</label> <input id="sessionusertype" value="${sessionusertype}"
											type="hidden" name="sessionusertype">

										<c:if test="${sessionusertype != null}">

											<body onload="retainRegistrationValues()" />

										</c:if>

										<c:if test="${sessionusertype != null}">
											<form:select path="usertypedesc" id="whatdescribes"
												name="cars" class="form-control width375" Required="true">
												<option value="" selected="selected">Select</option>
												<c:forEach items="${fmusertype}" var="usertype">
													<c:choose>
														<c:when test="${usertype eq sessionusertype}">
															<c:if
																test="${usertype.code eq 'WarehouseDistributorLightVehicle'}">
																<option value="${usertype}" selected="selected">Warehouse
																	Distributor Light Vehicle</option>
															</c:if>
															<c:if
																test="${usertype.code eq 'WarehouseDistributorCommercialVehicle'}">
																<option value="${usertype}" selected="selected">Warehouse
																	Distributor Commercial Vehicle</option>
															</c:if>
															<c:if test="${usertype.code eq 'Retailer'}">
																<option value="${usertype}" selected="selected">Retailer</option>
															</c:if>
															<c:if test="${usertype.code eq 'JobberPartStore'}">
																<option value="RETAILER" selected="selected">Jobber
																</option>
															</c:if>
															<%-- <c:if test="${usertype.code eq 'JobberPartStore'}">
																<option value="${usertype}" selected="selected">Part Store</option>
															</c:if> --%>   
															<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}" selected="selected">Repair Shop Owner </option>
															</c:if>
															<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}" selected="selected">Technician</option>
															</c:if>
															<c:if test="${usertype.code eq 'SalesRep'}">
																<option value="${usertype}">F-M Employee</option>
															</c:if>
															
													<%--		<c:if test="${usertype.code eq 'ConsumerDIY'}">
																<option value="${usertype}" selected="selected">Consumer DIY</option>
															</c:if>
															<c:if test="${usertype.code eq 'ConsumerDIFM'}">
																<option value="${usertype}" selected="selected">Consumer DIFM</option>
															</c:if> --%>  
														</c:when>
														<c:otherwise>
															<c:if
																test="${usertype.code eq 'WarehouseDistributorLightVehicle'}">
																<option value="${usertype}">Warehouse
																	Distributor Light Vehicle</option>
															</c:if>
															<c:if
																test="${usertype.code eq 'WarehouseDistributorCommercialVehicle'}">
																<option value="${usertype}">Warehouse
																	Distributor Commercial Vehicle</option>
															</c:if>
															<c:if test="${usertype.code eq 'Retailer'}">
																<option value="${usertype}">Retailer</option>
															</c:if>
															<c:if test="${usertype.code eq 'JobberPartStore'}">
																<option value="RETAILER">Jobber</option>
															</c:if>
													<%--		<c:if test="${usertype.code eq 'JobberPartStore'}">
																<option value="${usertype}">Part Store</option>
															</c:if>--%> 
															<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}">Repair Shop Owner</option>
															</c:if>
															<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}">Technician</option>
															</c:if>
															<c:if test="${usertype.code eq 'SalesRep'}">
																<option value="${usertype}">F-M Employee</option>
															</c:if>
															
													<%--		<c:if test="${usertype.code eq 'ConsumerDIY'}">
																<option value="${usertype}">Consumer DIY</option>
															</c:if>
															<c:if test="${usertype.code eq 'ConsumerDIFM'}">
																<option value="${usertype}">Consumer DIFM</option>
															</c:if>--%>
														</c:otherwise>
													</c:choose>
												</c:forEach>

											</form:select>
										</c:if>


										<c:if test="${sessionusertype == null}">
											<form:select path="usertypedesc" id="whatdescribes"
												name="cars" class="form-control width375">
												<!-- 	<option value="Default">Select</option> -->
												<option value="" selected="selected">Select</option>
												<c:forEach items="${fmusertype}" var="usertype">
													<c:if
														test="${usertype.code eq 'WarehouseDistributorLightVehicle'}">
														<option value="${usertype}">Warehouse Distributor
															Light Vehicle</option>
													</c:if>
													<c:if
														test="${usertype.code eq 'WarehouseDistributorCommercialVehicle'}">
														<option value="${usertype}">Warehouse Distributor
															Commercial Vehicle</option>
													</c:if>
													<c:if test="${usertype.code eq 'Retailer'}">
														<option value="${usertype}">Retailer</option>
													</c:if>
													<c:if test="${usertype.code eq 'JobberPartStore'}">
														<option value="RETAILER">Jobber</option>
													</c:if> 
												<!--	 <c:if test="${usertype.code eq 'JobberPartStore'}">
																<option value="${usertype}">Part Store</option>
															</c:if>-->
													<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}">Repair Shop Owner</option>
													</c:if>
													<c:if test="${usertype.code eq 'ShopOwnerTechnician'}">
																<option value="${usertype}">Technician</option>
															</c:if>
															<c:if test="${usertype.code eq 'SalesRep'}">
																<option value="${usertype}">F-M Employee</option>
															</c:if>
															
											<!--		<c:if test="${usertype.code eq 'ConsumerDIY'}">
														<option value="${usertype}">Consumer DIY</option>
													</c:if>
													<c:if test="${usertype.code eq 'ConsumerDIFM'}">
														<option value="${usertype}">Consumer DIFM</option>
													</c:if>  -->
												</c:forEach>

											</form:select>
										</c:if>
									</div>
								</div>
							</div>
							<div class="panel  panel-primary panel-frm panel-frm-filled "
								style="display: none">
								<div class="panel-heading">
									<h3 class="panel-title text-uppercase">Account Details</h3>
									<span class="pull-right "></span>
								</div>

								<div class="panel-body">

									<div class="form-group  regFormFieldGroup form-B2Bbody">
										<div class="form-accCode">
											<label class="" for="accCode">Account Code<span
											class="required fm_fntRed">*</span><span id="accCodeToolTip"
												class="tip"
												data-original-title="Do not have an Account Code? You may still register for new account"
												data-toggle="tooltip" data-placement="right" title="">
													<span class="fa fa-question-circle"></span>
											</span>
											</label>
											<div class="clearfix">
												<div class="col-lg-2 lftPad_0">
													<form:input id="accNo" type="text" name="accCode"
														class="form-control width175 pull-left" path="accCode"
														 onblur="getUnits();validateAccount();" maxlength="11" />
													<div class="errorMsg fm_fntRed width195"
														style="display: none;">Please enter Account Code</div>
													
														<!-- For account code error-->
														<div class="errorAccCode fm_fntRed width215" id="errorAccCode"></div>
												</div>
												<div class="col-lg-10 ">
													<div class="lftPad_20 pull-left">

														<p>Don't know or cant remember your Account Code?</p>
														<p>Call customer service at 1-800-334-3210</p>

													</div>
														
												</div>
											</div>
										</div>
									<!--<div class="form-salesrepaccCode">
											<label class="" for="accCode">Account Code <span
												class="tip"
												data-original-title="Accountcode Must start with SUS/SCA"
												data-toggle="tooltip" data-placement="right" title="">
													<span class="fa fa-question-circle"></span>
											</span>
											</label>
											<div class="clearfix">
												<div class="col-lg-2 lftPad_0">
													<form:input id="accNo" type="text" name="accCode"
														class="form-control width175 pull-left" path="accCode"
														 onblur="getUnits()" maxlength="11" />
													<div class="errorMsg fm_fntRed width195"
														style="display: none;">Please enter Account Code</div>
												</div>
												<div class="col-lg-10 ">
													<div class="lftPad_20 pull-left">

														<p>Don't know or cant remember your Account Code?</p>
														<p>Call customer service at 1-800-334-3210</p>

													</div>

												</div>
											</div>
										</div>-->

										<div class="form-group taxid  regFormFieldGroup form-taxid">
											<label class="topMargn_20" for="taxid">Tax ID /
												Canada Business Number<!-- <span class="required fm_fntRed">*</span>  -->
											</label>
											<form:input id="taxid" type="text" path="taxID" name="tax"
												class="form-control width165" />
											<!-- <div class="errorMsg fm_fntRed" style="display: none;">Please
												enter Tax ID</div>  -->
										</div>
										
<div class="form-group  regFormFieldGroup">
								<div class="form-accCode form-group regFormFieldGroup form-b2bSub garagegurus-body">
									<div class="regFormFieldGroup form-reg-b2t" >
										<br><label for="addressSame">Did someone refer you?</label>
										<br/>
										<form:radiobutton path="refered" id="referredBy" name="referredBy" label="Yes" value="Yes" class="lftMrgn_5"  onclick="$('.forReferBy').show();"/> &nbsp;&nbsp;&nbsp;&nbsp;
										<form:radiobutton path="refered" id="referredBy" name="referredBy" label="No" value="No"  class="lftMrgn_5"  onclick="$('.forReferBy').hide();"/>
									</div>
								</div>
								
									<div class="form-group regFormFieldGroup forReferBy garagegurus-body">
									<label class="" for="referById">Please enter the email address of the person who referred you</label> </br>
									<form:input class="form-control width375" id="referById" path="referredBy" type="text"/>
								</div>
										<label class="" for="email">Email Address<span
											class="required fm_fntRed">*</span> <span class="tip"
											data-original-title="This will become your User ID"
											data-toggle="tooltip" data-placement="right" title="">
												<span class="fa fa-question-circle"></span>
										</span></label>
										<form:input id="email" type="email" name="email"
												class="form-control width165" path="email" Required="true"
												onchange="getUnits();validateEmail();"/>
											<div class="fm_fntRed" id="errorEmail"></div>
											<div class="errorMsg fm_fntRed" style="display: none;">Please
												enter Email Address</div>
										</div>
										<div class="form-group  regFormFieldGroup  width428">
											<div>
												<label class="" for="setnewpwd">Password<span
													class="required fm_fntRed">*</span> <span class="tip"
													data-original-title="<spring:theme code="text.Password.Validation.Error" text="Password validation constraints"/>"
													data-toggle="tooltip" data-placement="right" title="">
														<span class="fa fa-question-circle"></span>
												</span>
												</label>
											</div>
											<form:input id="setnewpwd" type="password"
												class="form-control width175" name="setnewpwd" value=""
												path="password" maxlength="26" Required="true"
												onkeypress="javascript:getpassword_reg()"
												onblur="validatePassword()" />
											<div class="fm_fntRed width195" id="errorPwd"></div>
										<div class="errorMsg fm_fntRed width220"
											style="display: none;">Please enter Password</div>
									</div>
									<div class="form-group  regFormFieldGroup  width237">
										<div>
											<label class="topMargn_10" for="confPassword">Confirm
												Password<span class="required fm_fntRed">*</span>
											</label>
										</div>
										<form:input id="confPassword" type="password"
											class="form-control width237" name="confPassword" value=""
												path="confpwd" maxlength="26" Required="true" onblur="validateConfPassword()"/>
											<div class="fm_fntRed" id="errorCpwd"></div>
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter Confirm Password</div>
									</div>
									<div class="form-group  regFormFieldGroup ">
										<label class="" for="firstName">First Name<span
											class="required fm_fntRed">*</span></label>
										<form:input type="text" id="firstName" name="firstName"
											class="form-control width375" path="firstName"
											Required="true" />
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter First Name</div>
									</div>
									<div class="form-group  regFormFieldGroup ">
										<label class="" for="lastName">Last Name<span
											class="required fm_fntRed">*</span></label>
										<form:input type="text" id="lastName" name="lastName"
											class="form-control width375" path="lastName" Required="true" />
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter Last Name</div>
									</div>
										<div class="form-group  regFormFieldGroup form-b2bRegbody"
											id="companyDiv">
											<div class="form-group  regFormFieldGroup ">
												<label class="" for="companyName">Company Name<span
													class="required fm_fntRed" id="companyNameSpan">*</span></label>
												<form:input type="text" id="companyName" name="company"
													class="form-control width375" path="companyName" value=" "
													Required="true" />
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													enter Company Name</div>
											</div>
											<div class="form-group  regFormFieldGroup ">
												<label class="" for="companyAddress">Company Address<span
													class="required fm_fntRed" id="companyAddressSpan">*</span></label>
												<form:input id="companyAddress" type="text"
													name="Address line" class="form-control width375"
													path="companyaddressline1" Required="true" />
												
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													enter Company Address</div>
											</div>
												<div class="form-group  regFormFieldGroup ">
											<form:input id="companyAddress2" type="text"
													name="Address line 2"
													class="form-control width375 topMargn_10"
													path="companyaddressline2" />
													</div>
											<div class="form-group  regFormFieldGroup ">
												<label class="" for="companyCity">Company City<span
													class="required fm_fntRed" id="companyCitySpan">*</span></label>
												<form:input id="companyCity" type="text" name="city"
													class="form-control width375" path="companycity"
													Required="true" />
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													enter Company City</div>
											</div>


											<div class="form-group  regFormFieldGroup">
												<label class="" for="Ccountry">Company Country<span
													class="required fm_fntRed" id="CcountrySpan">*</span></label>
												<form:select id="Ccountry" name="Ccountry"
													placeholder="country" class="form-control width165"
													onchange="getCompanyStates()" path="companycountry"
													Required="true">
													<c:choose>
														<c:when test="${fmdata.companycountry eq null}">
															<!-- <option value="Default">Select</option> -->
															<option value="" selected="selected">Select</option>
														</c:when>
														<c:otherwise>
															<option value="${fmdata.companycountry}">${companyCountrydata.name}</option>
														</c:otherwise>
													</c:choose>
													<c:forEach items="${countryData}" var="iso">
														<%-- 	<c:if test="${fn:contains(iso.isocode,'US')}"> --%>
														<option value="${iso.isocode}">${iso.name}</option>
														<%-- 		</c:if>
 --%>
													</c:forEach>
												</form:select>
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													select a Company Country</div>
											</div>




											<div class="form-group  regFormFieldGroup" id="unitState">
												<label class="" for="companyState">Company State /
													Province<span class="required fm_fntRed"
													id="companyStateSpan">*</span>
												</label>
												<form:select id="companyState" type="text" name="state"
													class="form-control width220" path="companystate"
													Required="true" onBlur="getCompanyCountry()">
													<c:choose>
														<c:when test="${fmdata.companystate eq null}">
															<!-- 	<option value="Default">Select</option> -->
															<option value="" selected="selected">Select</option>
														</c:when>
														<c:otherwise>
															<option value="${fmdata.companystate}">${CompanyRegiondata.name}</option>
														</c:otherwise>
													</c:choose>
													<c:forEach items="${regionsdatas}" var="reg">
														<c:forEach items="${reg}" var="val">
															<c:if test="${fn:contains(val.isocode,'US-')}">
																<option value="${val.isocode}">${val.name}</option>
															</c:if>
															<c:if test="${fn:contains(val.isocode,'CA-')}">
																<option value="${val.isocode}">${val.name}</option>
															</c:if>
														</c:forEach>
													</c:forEach>
												</form:select>
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													select a Company State</div>
											</div>
											<div class="form-group  regFormFieldGroup ">
												<label class="" for="companyZip">Company ZIP /
													Postal Code<span class="required fm_fntRed"
													id="companyZipSpan">*</span>
												</label>
												<form:input type="text" id="companyZip" name="companyZip"
													class="form-control width375" path="companyzipCode"
													Required="true" />
												<div class="errorMsg fm_fntRed" style="display: none;">Please
													enter Company ZIP / Postal Code</div>
											</div>
											<%-- 	<div class="form-group  regFormFieldGroup ">
												<label class="" for="Ccountry">Company Country<span
													class="fm_fntRed">*</span></label>
												<form:select id="Ccountry" name="Ccountry"
													 class="form-control width165"
													path="companycountry" >
													<option value="Default">Select</option>
													<c:forEach items="${countryData}" var="iso">
													<c:if test="${fn:contains(iso.isocode,'US')}">
														<option value="${iso.isocode}">${iso.name}</option>
														</c:if>
													</c:forEach>
												</form:select>

											</div>   --%>
											<div class="form-group  regFormFieldGroup clearfix">
												<div class="pull-left">
													<label class="" for="companyPhone">Company Phone
														Number<span class="required fm_fntRed"
														id="companyPhoneSpan">*</span>
													</label>
													<form:input type="tel" id="companyPhone" name="companyFax"
														maxlength="15" placeholder="(555)555-5555"
														class="form-control width165" path="companyPhoneNumber"
														Required="true" />
													<div class="errorMsg fm_fntRed" style="display: none;">Please
														enter Company Phone Number</div>
												</div>
												<div class="pull-left lftMrgn_20">
													<label class="" for="companyFax">Company Fax</label>
													<form:input type="tel" id="companyFax" name="companyFax"
														maxlength="15" placeholder="(555)555-5555"
														class="form-control width165" path="companyFax" />
												</div>
											</div>
										</div>

										<div class="form-group form-b2bSub">
											<ul class="list-group checkboxGroup">
												<li class="list-group-item"><label for="alert">
														<form:checkbox id="alert" path="techAcademySubscription" />
														&nbsp;Subscribe to receive new product alerts, promotions and training opportunities.
												</label></li>
												
											</ul>
										</div>


										<!--Subscription for B2C Starts-->
										<div class="form-group form-B2Cbody">
											<ul class="list-group checkboxGroup">
												<li class="list-group-item"><label for="alert">
														<form:checkbox id="alert" path="newProductAlerts" />
														&nbsp;Send me new product alerts
												</label></li>
												<li class="list-group-item"><label for="promo">
														<form:checkbox id="promo" path="promotionInfoSubscription" />
														&nbsp;Send me information about new promotions
												</label></li>
											</ul>
										</div>
										<!--Subscription for B2C Ends-->
									</div>

								<div class="panel-heading garagegurus form-b2bSub">
									<h3 class="panel-title text-uppercase">Garage Gurus&#8482;</h3>
									<span class="pull-right "></span>
								</div>
								<div class="form-accCode form-group regFormFieldGroup form-b2bSub garagegurus-body">
									<div class="regFormFieldGroup form-reg-b2t" >
										<br><label for="addressSame">Are you enrolled in the Garage Gurus&#8482; Training Program?</label>
										<br/>
										<form:radiobutton path="loyaltySignup" id="fmRole" name="loyaltySignup" label="Yes" value="Yes" class="lftMrgn_5"  onclick="$('.forLoyaltyRewards').show();"/> &nbsp;&nbsp;&nbsp;&nbsp;
										<form:radiobutton path="loyaltySignup" id="fmRole" name="loyaltySignup" label="No" value="No"  class="lftMrgn_5"  onclick="$('.forLoyaltyRewards').hide();"/>
									</div>
								</div>
								<div class="form-group regFormFieldGroup forLoyaltyRewards garagegurus-body">
									<label class="" for="lmsSigniId">What is your User ID?</label> </br>
									<form:input class="form-control width375" id="lmsSigniId" path="lmsSigniId" type="text"/>
								</div>
								<div class="panel-heading garagegurus form-b2bSub">
									<h3 class="panel-title text-uppercase">Garage Rewards</h3>
									<span class="pull-right "></span>
								</div>
									<br/>
									
									<div class="form-group  regFormFieldGroup form-b2bSub">
										<ul class="list-group checkboxGroup form-sameaddress">
											<li class="list-group-item">
													<label class="" for="GarageRewardMember"><form:checkbox id="GarageRewardMember"
														 path="isGarageRewardMember" onClick="GarageRewardMemberQuestion(this)" />
													&nbsp;Yes, join the Garage Rewards loyalty program and earn free gear!</br>By registering for a Garage Rewards account, you agree to the <a data-toggle="modal" href="#terms">Garage Rewards Loyalty Program Terms and Conditions</a></label>
											</li>
										</ul>
										
									</div>
										<div class="form-group  regFormFieldGroup" id="form-ifGarageGuruChecked">
										<div class="form-group  regFormFieldGroup form-b2bSub">
									<label class="" for="promoCode">Enter Promo Code here (if applicable):   </label>      <form:input type="text" 
														 path="promoCode"  />
									</div>
									<div class="form-group  regFormFieldGroup form-b2bSub ">
										<ul class="list-group checkboxGroup form-sameaddress">
											<li class="list-group-item"><label for="addressSame">
													Where should we ship your rewards?
											</label></li>
										</ul>
									</div>
								
										
										</div>		
											<div class="form-group  regFormFieldGroup " id="form-addresssamecheckbox">
										<ul class="list-group checkboxGroup form-sameaddress">
											
											<li class="list-group-item">
													<form:checkbox id="addressSame"
														onClick="return addresssame(this)" path="isSameAddress" />
													&nbsp;Use my company address as my home address
											</li>
										</ul>
										</div>
									<div class="form-group  regFormFieldGroup form-address1">
										
										<label class="" for="contactAddress">Home Address<span
											class="required fm_fntRed">*</span></label>
										<form:input id="contactAddress" type="text"
											name="Address line 1" placeholder="Address"
											class="form-control width375" path="addressline1"
											Required="true" />
											
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter Contact Address</div>
									</div>
										<div class="form-group  regFormFieldGroup  form-address2">
										<form:input id="contactAddress2" type="text"
											name="Address line 2" placeholder="Address"
											class="form-control width375 topMargn_10" path="addressline2" />
											</div>
									<div class="form-group  regFormFieldGroup form-city">
										<label class="" for="city">City<span
											class="required fm_fntRed">*</span></label>
										<form:input id="city" type="text" name="city"
											placeholder="City" class="form-control width375" path="city"
											Required="true" />
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter City</div>
									</div>

									<div class="form-group  regFormFieldGroup  form-country">
										<label class="" for="pcountry">Country<span
											class="required fm_fntRed">*</span></label>

										<form:select path="country" mandatory="true" id="pcountry"
											name="pcountry" placeholder="" class="form-control width165"
											Required="true" onBlur="getRegions()">

											<c:choose>
												<c:when test="${fmdata.country eq null}">
													<!-- 	<option value="Default">Select</option> -->
													<option value="" selected="selected">Select</option>
												</c:when>
												<c:otherwise>
													<option value="${fmdata.country}">${countrydata.name}</option>
													<%-- <c:if test="${fmdata.country eq 'US'}">
														<option value="${fmdata.country}">United States</option>
													</c:if>
													<c:if test="${fmdata.country eq 'CA'}">
														<option value="${fmdata.country}">Canada</option>
													</c:if> --%>
												</c:otherwise>
											</c:choose>


											<c:forEach items="${countryData}" var="iso">
												<%--  <c:if test="${fn:contains(iso.isocode,'US')}"> --%>
												<option value="${iso.isocode}">${iso.name}</option>
												<%-- 	</c:if>  --%>
											</c:forEach>

										</form:select>
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											select a country</div>
									</div>


									<div class="form-group  regFormFieldGroup form-state"
										id="userState">
										<label class="" for="state">State / Province<span
											class="required fm_fntRed">*</span></label>
										<form:select id="state" type="text" name="state"
											class="form-control width165" path="state" Required="true"
											onBlur="getCountry()">

											<c:choose>
												<c:when test="${fmdata.state eq null}">
													<!-- 	<option value="Default">Select</option> -->
													<option value="" selected="selected">Select</option>
												</c:when>
												<c:otherwise>
													<%-- 		<option value="${fmdata.state}">${fmdata.state}</option> --%>
													<option value="${fmdata.state}">${regiondata.name}</option>
												</c:otherwise>
											</c:choose>


											<c:forEach items="${regionsdatas}" var="reg">
												<c:forEach items="${reg}" var="val">
													<c:if test="${fn:contains(val.isocode,'US-')}">
														<option value="${val.isocode}">${val.name}</option>
													</c:if>
													<c:if test="${fn:contains(val.isocode,'CA-')}">
														<%-- 	<c:if test="${fn:contains(pcountry.value,'CA-')}"  > --%>
														<option value="${val.isocode}">${val.name}</option>
													</c:if>

												</c:forEach>
											</c:forEach>

										</form:select>
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											select a State</div>
									</div>
									<div class="form-group  regFormFieldGroup form-zipcode">
										<label class="" for="zip">ZIP / Postal Code<span
											class="required fm_fntRed">*</span></label>
										<form:input type="text" id="zip" name="zip"
											placeholder="ZIP Code" class="form-control width175"
											path="zipCode" Required="true" />
										<div class="errorMsg fm_fntRed" style="display: none;">Please
											enter ZIP / Postal Code</div>
									</div>


									<div class="form-group  regFormFieldGroup clearfix form-phonenumber">
										<div class="pull-left">
											<label class="" for="phone">Phone Number<span
												class="required fm_fntRed">*</span></label>
											<form:input type="tel" id="phone" name="companyFax"
												maxlength="15" placeholder="(555)555-5555"
												class="form-control width165" path="phoneno" Required="true" />
											<div class="errorMsg fm_fntRed" style="display: none;">Please
												enter Phone Number</div>
										</div>
									</div>
									<div class="regFormFieldGroup form-B2Bbody form-roles">
										<div class="regFormFieldGroup form-reg ">
											<label class="" for="fmRole">Access Required<span
												class="fm_fntRed">*</span></label><br />
											<span id="myspan1">
											<form:radiobutton path="fmRole" id="fmRole" name="fmRole"
												label="Full Access" value="FMFullAccessGroup"
												class="lftMrgn_5" checked="checked" />
												</span>
											<br />
											<span id="myspan2">
											<form:radiobutton path="fmRole" id="fmRole" name="fmRole"
												label="Limited Access - Can Place Orders / Cannot View Invoices"
												value="FMNoInvoiceGroup" />
												</span>
											<br />
											<span id="myspan3">
											<form:radiobutton path="fmRole" id="fmRole" name="fmRole"
												label="View Only - Cannot Place Orders / View Invoices"
												value="FMViewOnlyGroup" />
											<br />
											</span>
											<%-- <form:radiobutton path="fmRole" id="fmRole" name="fmRole"
												label="Business User view only role - Do everything a CSR does except place an order" value="FMBUVOGroup"/><br/>--%>
										</div>

									</div>

									<!--Subscription for B2b Starts-->
									<div class="form-group regFormFieldGroup  " id="tech-rewards-questions" >
										<ul class="checkboxGroup">
											<li class="list-group-item">
												<label class="" for="Preference">Please answer a few more questions so that we may customize your rewards based on your preference.</label>
											</li>
											<li class="list-group-item">
												<label class="" for="aboutShop">What best describes your shop?</label>
											</li>
										</ul>
											<form:select path="aboutShop" id="aboutShop"
												name="cars" class="form-control width165" onchange="onAboutShop(this)" >
												<option value="Select" >Select</option>
												<c:forEach items="${aboutShop}" var="reg">
												<c:choose>
													<c:when test="${reg.value eq fmdata.aboutShop}">
														<option selected="selected" value="${reg.value}">${reg.key}</option>
													</c:when>
													<c:otherwise>
														<option value="${reg.value}">${reg.key}</option>
													</c:otherwise>
												</c:choose>
													</c:forEach>
											</form:select>
											<div class="shopBanner" style="display: none;">
												<br/>
												<label class="" for="shopBanner">Banner Program Name</label>
												<form:input class="form-control width175" id="shopBanner" path="shopBanner" type="text"/>
											</div>
											<br/><label class="" for="shopType">Type of Shop</label><br />
										<form:select path="shopType" id="shopType" name="cars"
											class="form-control width165 ">
											<option value="Select">Select</option>
											<c:forEach items="${shopType}" var="shop">
												<c:choose>
													<c:when test="${shop.value eq fmdata.shopType}">
														<option selected="selected" value="${shop.value}">${shop.key}</option>
													</c:when>
													<c:otherwise>
														<option value="${shop.value}">${shop.key}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</form:select>


										<br/><label class="" for="shopBays">Number of Bays</label><br />
											<form:select path="shopBays" id="shopBays"
												name="cars" class="form-control width165 ">
												<option value="Select" >Select</option>
												<c:forEach items="${shopBays}" var="bay">
												<c:choose>
													<c:when test="${bay.value eq fmdata.shopBays}">
														<option selected="selected" value="${bay.value}">${bay.key}</option>
													</c:when>
													<c:otherwise>
														<option value="${bay.value}">${bay.key}</option>
													</c:otherwise>
												</c:choose>
													</c:forEach>
											</form:select>
											
											<br/><label class="" for="shopBays">What are you most interested in? (Check all that apply)</label><br />
											
											<ul class="list-group checkboxGroup">
												<c:forEach items="${mostIntersted}" var="mostInterst" >
													<li class="list-group-item">
															<form:checkbox id="${mostInterst.key}" value="${ mostInterst.value}" path="mostIntersted" />
															&nbsp;${mostInterst.key}
													</li>
												</c:forEach>
											</ul>
											<br/><label class="" for="brands">What brands are you currently installing? (Check all that apply)</label><br />
											<ul class="list-group checkboxGroup">
												<c:forEach items="${brands}" var="brand">
													<li class="list-group-item">
															<form:checkbox id="${brand.key}" value="${brand.value}" path="brands" />
															&nbsp;${brand.key}�
													</li>
												</c:forEach>
											</ul>
									</div>
									<div class="form-group  regFormFieldGroup form-b2cSub ">
										<ul class="list-group checkboxGroup">
											<li class="list-group-item"><label for="subscribe">
													<form:checkbox id="subscribe" path="newsLetterSubscription" />
													&nbsp;Subscribe to Federal-Mogul Motorparts Newsletters
											</label></li>
										</ul>
									</div>
								</div>
								<!--Subscription for B2b Ends-->

								<!-- <div class="row"> -->
									<div>
										<button id="regsubmit"
											class="btn btn-sm btn-fmDefault text-uppercase registrationBtns"
											type="submit">Submit</button>
									</div>
							<!-- 	</div> -->
							</div>

						</form:form>
					</div>

				</div>
			</div>
		</section>
	</section>


	<!-- Brandstrip -->

	<div class="well well-sm well-brandstrip clearfix">
		<ul class="nav ">
			<!-- <c:set var="fmComponentName" value="brandStripB2B" scope="session" />-->
			<c:set var="fmComponentName" value="brandStrip" scope="session" />

			<cms:pageSlot position="FMBrandstrip" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</ul>
	</div>
	
	


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="terms" class=" modal fade termsConditions shipToModel in" style="display: none;"><div class="modal-dialog">
    <button data-dismiss="modal" class="close" type="button" ><span class="fa fa-close" aria-hidden="true"></span><span class="sr-only">Close</span></button>
  <div class="modal-content">
<div class="modal-content">
    <div class="modal-body">
 <div class="termsConditionContent">
 <c:set var="fmComponentName" value="registrationloginblock" scope="session" />
	<cms:pageSlot position="TermsAndConditions" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
</div>
</div>
</div>
</div>
  </div>
</div>
<%-- <federalmogul:termsAndConditionsRegistration/> --%>	

				
	<c:set var="fmComponentName" value="footerImageLinks" scope="session" />
</template:page>


