<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="federalmogul"
	tagdir="/WEB-INF/tags/desktop/federalmogul"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">

	<section class="rewardsHomePage contentPage">
		<section class="lookupSignupBg">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 rewardsBanner">
						<c:set var="fmComponentName" value="loyaltyBanner" scope="session" />
						<cms:pageSlot position="HomeRewardsBannerSection" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</div>

					<!-- Lookup Rewards Section -->



					<!--- SIGN IN AND REGISTER PANEL -->
					 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="rewadsMyclassPanel redeemRewardsInfo"> 
            <!-- SIGN IN -->
            <div class="panel panel-default rewardsPanel" id="myrewards">
              <div class="panel-body">
               <c:set var="fmComponentName" value="Reward" scope="session" />
              		<cms:pageSlot position="HomeMyRewardsSection" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
                <div class="rewardsPoints"><i class="fa fa-wrench"></i><strong><fmt:formatNumber type="number" value="${TotalPoints}" /><sub>pts</sub></strong></div>
                <form role="form" class="form-horizontal rewardsPanelForm" id="rewardsform">
                  <div class="controls clearfix"> <a class="btn  btn-sm btn-fmDefault pull-rightn text-uppercase" href="${contextPath}/${currentsiteUID}/${currentLanguage.isocode}/${currentCurrency.isocode}/lsearch?q=:name-asc:&text=#" id="btn-login">redeem</a></div>
                <c:url value="loyalty/history" var="loyalHistoryUrl" />
                <div class="viewall text-center btmMrgn_30"><a class="text-capitalize" href="${contextPath}/federalmogul/${currentLanguage.isocode}/${currentCurrency.isocode}/loyalty/history?clear=true&site=federalmogul">view points history <span class="fa fa-angle-right fm_fntRed"></span></a></div>
              </div>
            </div>
          </div>
        </div>

				</div>
			</div>
		</section>
		<section class="accountDetailPage pageContet">
			<div class="container champion">
				<div class="row topMargn_20 ">
					<div class="col-lg-9 contentRHS">
						<div class="pull-left rghtMrgn_20">
							<h3 class="text-uppercase">Latest Gear:</h3>
						</div>
						<div class="pull-right">
							<form class="form-horizontal" role="form">
								<div class="controls clearfix topMargn_25">
									<a href="${contextPath}/${currentsiteUID}/${currentLanguage.isocode}/${currentCurrency.isocode}/lsearch?q=:name-asc:&text=#" class="btn  btn-sm btn-fmDefault text-uppercase">view all
										items</a>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 internalLanding">
						<div class="internalPage rightHandPanel clearfix">
							<div class="clearfix">
								
							<c:set var="fmComponentName" value="HomeCarousel" scope="session" />
							<cms:pageSlot position="HomeRewardsLatestProductsSection" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
									
									
								<!--.container-->
							</div>
							</div>
							<section class="SmallB2b technicianQuicklinksBg">
								<div class="row">
									<!--- Technician Forum Post PANEL -->
									<div class="col-lg-12">
										<div
											class="panel panel-default technicianForumPanel rewardsForumPanel">
											<div class="panel-body">
												<div class="row">
													<c:set var="fmComponentName" value="FAQ" scope="session" />
													<cms:pageSlot position="HomeRewardsFAQsSection"
														var="feature">
														<cms:component component="${feature}" />
													</cms:pageSlot>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<!-- Starts: Manage Account Left hand side panel -->
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 contentLHS">
							<!--- Order in Progress PANEL -->
							<div class="panel panel-default manageAccountLinks clearfix">
								<c:set var="fmComponentName" value="learn" scope="session" />
								<cms:pageSlot position="HomeRewardseLearnSection" var="feature">

									<cms:component component="${feature}" />
								</cms:pageSlot>
							</div>
						</div>
					</div>
				</div>
					</section>
	</section>
				<div class="well well-sm well-brandstrip clearfix ">
					<ul class="nav">
						<c:set var="fmComponentName" value="brandStrip" scope="session" />

						<cms:pageSlot position="FMBrandstrip" var="feature">
							<cms:component component="${feature}" />
						</cms:pageSlot>
					</ul>
				</div>
	





</template:page>
		
