<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/desktop/nav" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/desktop/product" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="breadcrumb" tagdir="/WEB-INF/tags/desktop/nav/breadcrumb" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>




<template:page pageTitle="${pageTitle}">


 <!-- InstanceBeginEditable name="Breadcrumb" -->
<section class="breadcrumbPanel visible-lg visible-md visible-sm"
		itemscope itemtype="http://schema.org/Product">
		<div class="container">
			<div class="row">
				<ul class="breadcrumb">
					<breadcrumb:breadcrumb breadcrumbs="${breadcrumbs}" />
				</ul>
			</div>
		</div>
	</section>
<!-- InstanceEndEditable -->


<!-- MAIN CONTAINER-->
<!-- InstanceBeginEditable name="Page Content Section" -->
<section class="rewardsAboutPage contentPage"> 
  
  <!-- Lookup Rewards Section -->
  
  <section class="wayToPoints accountDetailPage pageContet" >
    <div class="container champion">
      <div class=" clearfix bgwhite">
        <div class="col-lg-9 lftPad_0">
    	<c:set var="fmComponentName" value="waytolearn" scope="session" />
         
  			<cms:pageSlot position="FMSearchGridBannerSection" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
        </div>
        <div class="col-lg-3 rewardsPanel">
        
            <h1 class="panel-title"><span class="fa fa-user"></span><span class=" text-uppercase">&nbsp;Garage rewards</span></h1>
            <div class="rewardsPoints"><i class="fa fa-wrench"></i><strong><fmt:formatNumber type="number" value="${TotalPoints}" /><sub>pts</sub></strong></div>
            <div class="text-center">
                      <a class="text-capitalize addNewAddLink" href="${contextPath}/federalmogul/${currentLanguage.isocode}/${currentCurrency.isocode}/loyalty/history?clear=true&site=federalmogul">View Points History <span class="linkarow fa fa-angle-right "></span></a></div> 
          </div>
      </div>
      
      <div class="row">
        <div class="col-lg-12 col-xs-12 internalLanding">
          <div class="internalPage rightHandPanel clearfix">
  

							<%-- <c:set var="fmComponentName" value="learnpara" scope="session" />
         
  			<cms:pageSlot position="FMproductListGridSection" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot> --%>
							<!--.container-->
							<div class="clearfix">
								<h2 class="text-capitalize DINWebBold">
									<span class="fm_fntRed fa fa-star">&nbsp; </span><spring:theme code="text.EarnPoints.whatcanbedone"/>
								</h2>
								<div class="table-responsive userTable rewardsEarn">
									<table border="0" class="table ordeDetailsTable">
										<thead>
											<tr>
												<th class="col-lg-4 col-md-4 col-sm-4 col-xs-4 "><spring:theme code="text.EarnPoints.Activity"/></th>
												<th class="col-lg-2 col-md-2 col-sm-2 col-xs-2 "><spring:theme code="text.EarnPoints.Points"/></th>
												<th class="col-lg-3 col-md-3 col-sm-3 col-xs-3 "><spring:theme code="text.EarnPoints.Max.PointAllowance"/></th>
												<th class="col-lg-3 col-md-3 col-sm-3 col-xs-3 ">&nbsp;</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><spring:theme code="text.EarnPoints.GurusOnsite"/></td>
												<td><spring:theme code="text.EarnPoints.1,500(Perfulldayoftraining)"/> <br /></td>
												<td><spring:theme code="text.EarnPoints.7,500(5daysoftraining)"/> <br /></td>
												<td><spring:theme code="text.EarnPoints.FindaCourse"/></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.Gurus-On-The-Go"/></td>
												<td><spring:theme code="text.EarnPoints.500"/><br /></td>
												<td><spring:theme code="text.EarnPoints.5,000(10trainingsessions)"/><br /></td>
												<td><a
													onClick="$('.promoCode').show();$('.hideSizeLink').show();$('.promoCodeLink').hide();"
													class="text-capitalize addNewAddLink promoCodeLink"><spring:theme code="text.EarnPoints.PromoCodeRequired"/> <span class="linkarow fa fa-angle-right "></span></a> <a
													onClick="$('.promoCode').hide();$('.hideSizeLink').hide();$('.promoCodeLink').show();"
													class="text-capitalize addNewAddLink hideSizeLink"
													style="display: none"><spring:theme code="text.EarnPoints.PromoCodeRequired"/>
													<span class="linkarow fa fa-angle-right "></span></a></td>
											</tr>
											<tr class="promoCode" style="display: none;" id="promoCodeBlock">
												<td colspan="4">
													<h2 class="text-uppercase DINWebBold"><spring:theme code="text.EarnPoints.EnterPromoCode"/>
													</h2>
													<p class="topMargn_15"><spring:theme code="text.EarnPoints.PromoCodetext"/></p>
													

														<div class="form-group col-lg-3 lftPad_0">

															<input type="text" value="Promo Code"
																placeholder="Promo Code" class="form-control" id="promCode">
														</div>
														<button type="button"  id="promoCodeBtn" class="btn  btn-sm btn-fmDefault" onclick="javascript:submitPromocode()"><spring:theme code="text.EarnPoints.PromoCodeSubmit"/></button>
														<%-- <div class="form-group col-lg-3">
															<a class="btn  btn-sm btn-fmDefault" href=""><spring:theme code="text.EarnPoints.PromoCodeSubmit"/></a>
														</div> --%>
																									</td>
											</tr>
											<tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.ProductReviews"/></td>
												<td><spring:theme code="text.EarnPoints.250"/> <br /></td>
												<td><spring:theme code="text.EarnPoints.2,500(10reviews)"/><br /></td>
												<td><a href="/fmstorefront/federalmogul/en/USD/search?site=federalmogul&amp;q=:name-asc:" class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.FindaProduct"/> <span
							class="linkarow fa fa-angle-right "></span>
					</a></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.CompleteSurveys"/><br /></td>
												<td><spring:theme code="text.EarnPoints.250"/></td>
												<td><spring:theme code="text.EarnPoints.2,500(10Surveys)"/><br /></td>
												<td><a href="/fmstorefront/loyalty/en/USD/Survey"
													class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.FindaSurvey"/><span
														class="linkarow fa fa-angle-right "></span></a></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.GurusOnline"/><br /></td>
												<td><spring:theme code="text.EarnPoints.100"/></td>
												<td><spring:theme code="text.EarnPoints.1,500(15onlinetrainingsessions)"/><br /></td>
												<td><a
													href="https://federalmogul.plateau.com/learning/user/portal.do?siteID=FM%2dALL&amp;landingPage=login"
													class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.RegisterforOnlineClasses"/> <span
														class="linkarow fa fa-angle-right "></span>
												</a></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.ReferaTechnician"/></td>
												<td><spring:theme code="text.EarnPoints.50"/></td>
												<td><spring:theme code="text.EarnPoints.1,500(30referrals)"/><br /></td>
												<td><a
													href="/fmstorefront/loyalty/en/USD/loyalty/refer"
													class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.GotoTechReferralForm"/><span
														class="linkarow fa fa-angle-right "></span>
												</a></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.SharingProducts/PromotionsonSocialMedia"/></td>
												<td><spring:theme code="text.EarnPoints.25"/></td>
												<td><spring:theme code="text.EarnPoints.750(30shares)"/> <br /></td>
												<td><a
													href="/fmstorefront/federalmogul/en/USD/search?site=federalmogul&amp;q=:name-asc:"
													class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.FindaProduct"/><span
														class="linkarow fa fa-angle-right "></span>
												</a></td>
											</tr>
											<tr>
												<td><spring:theme code="text.EarnPoints.UpdatingContactInformation"/></td>
												<td><spring:theme code="text.EarnPoints.25"/></td>
												<td><spring:theme code="text.EarnPoints.50(2updates)"/> <br /></td>
												<td><a
													href="/fmstorefront/federalmogul/en/USD/my-fmaccount/update-profile"
													class=" fm_fnt_Blue"><spring:theme code="text.EarnPoints.UpdateYourInfo"/><span
														class="linkarow fa fa-angle-right "></span>
												</a></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="topMargn_20">
									<spring:theme code="text.EarnPoints.Maximumallowance"/>
									<spring:theme code="text.EarnPoints.AllowancePolicy"/>
								</div>
							</div>
						</div>

					</div>
				</div>
      
      
      
				<div class="row topMargn_20 btmMrgn_20">
      	<div class="col-lg-12 col-md-12 col-sm-2 col-xs-12 contentRHS">
        <div class="pull-left rghtMrgn_20 rewardsCarousel">
          <h3 class="text-uppercase"><spring:theme code="text.EarnPoints.LatestGear"/></h3>
        </div>
        <div class="pull-right">
          <form  class="form-horizontal" role="form">
            <div class="controls clearfix topMargn_5"> 
		<a href="${contextPath}/${currentsiteUID}/${currentLanguage.isocode}/${currentCurrency.isocode}/lsearch?q=:name-asc:&text=#"  
 class="btn  btn-sm btn-fmDefault text-uppercase"><spring:theme code="text.EarnPoints.viewallitems"/></a></div>
          </form>
        </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 internalLanding">
         <cms:pageSlot position="FMProductGridBannerSection" var="feature">
								<cms:component component="${feature}" />
							</cms:pageSlot>
          
        </div>
      </div>
    </div>
  </section>
  
  
  <!--<section class="B2B insight">
    <div class="container">
      <div class="row">
        <div class="trainingPanel col-lg-12">
          <div class="panel-heading clearfix trainingPanelTitle">
            <h3 class="panel-title pull-left"><span class="text-uppercase">available rewards</span></h3>
            <span class="pull-right viewall text-capitalize"><a href="#"> View all items <span class="fa fa-angle-right viewmorearrow"></span></a></span> </div>
          <div class="">
          
          <div class="row">
                <div class=" col-sm-3 col-md-3 col-xs-12">
                	<div class="lms_intro_hover " >
						<p class="col-xs-lms">
							<img src="images/reward_prod_3.jpg" alt="Insights"/>
						</p>
						<div class="caption col-xs-lms">
							<div class="blur"></div>
							<div class="caption-text">
								<h5 >champion<sup>&reg;</sup> classic logo Tee Item #8350-LG |</h5>
								<p class="visible-lg visible-md visible-sm lms_desc"></p>
                                
                                <div class="visible-lg visible-md visible-sm lms_btm_link">
                                 
                                 <span class="pull-left  text-capitalize"><a href="#"> Read more <span class="fa fa-angle-right "></span></a></span>
                                 <span class="pull-right  text-capitalize"><a href="#"><span class="fa fa-share-alt"> </span></a></span></div>
							</div>
						</div>
					</div>
                 
                </div>
                <div class=" col-sm-3 col-md-3 col-xs-12">
                	<div class="lms_intro_hover " >
						<p class="col-xs-lms">
							 <img src="images/reward_prod_4.jpg" alt="Training"/>
						</p>
						<div  class="caption col-xs-lms">
							<div class="blur"></div>
							<div class="caption-text">
								<h5 >champion<sup>&reg;</sup> classic logo Tee Item #8350-LG |</h5>
								<p class="visible-lg visible-md visible-sm lms_desc"></p>
                                
                                <div class="visible-lg visible-md visible-sm lms_btm_link">
                                 
                                 <span class="pull-left  text-capitalize"><a href="#"> Read more <span class="fa fa-angle-right "></span></a></span>
                                 <span class="pull-right  text-capitalize"><a href="#"><span class="fa fa-share-alt"> </span></a></span></div>
							</div>
						</div>
					</div>
                 
                </div>
                
               <div class=" col-sm-3 col-md-3 col-xs-12">
                	<div class="lms_intro_hover " >
						<p class="col-xs-lms">
							 <img src="images/reward_prod_6.jpg" alt="Training"/>
						</p>
						<div class="caption col-xs-lms">
							<div class="blur"></div>
							<div class="caption-text">
								<h5 >champion<sup>&reg;</sup> classic logo Tee Item #8350-LG |</h5>
								<p class="visible-lg visible-md visible-sm lms_desc"></p>
                                
                                <div class="visible-lg visible-md visible-sm lms_btm_link">
                                 
                                 <span class="pull-left  text-capitalize"><a href="#"> Play <span class="fa fa-angle-right "></span></a></span>
                                 <span class="pull-right  text-capitalize"><a href="#"><span class="fa fa-share-alt"> </span></a></span></div>
							</div>
						</div>
					</div>
                 
                </div>
              <div class=" col-sm-3 col-md-3 col-xs-12">
                	<div class="lms_intro_hover " >
						<p class="col-xs-lms">
							 <img src="images/reward_prod_5.jpg" alt="Training"/>
						</p>
						<div class="caption col-xs-lms">
							<div class="blur"></div>
							<div class="caption-text">
								<h5 >champion<sup>&reg;</sup> classic logo Tee Item #8350-LG |</h5>
								<p class="visible-lg visible-md visible-sm lms_desc"></p>
                                
                                <div class="visible-lg visible-md visible-sm lms_btm_link">
                                 
                                 <span class="pull-left  text-capitalize"><a href="#"> Read More <span class="fa fa-angle-right "></span></a></span>
                                 <span class="pull-right  text-capitalize"><a href="#"><span class="fa fa-share-alt"> </span></a></span></div>
							</div>
						</div>
					</div>
                 
                </div>
                
              </div>
           
          </div>
        </div>
      </div>
    </div>
  </section>-->

<!-- InstanceEndEditable -->
	
	<div class="well well-sm well-brandstrip clearfix">
		<ul class="nav ">
			<!-- <c:set var="fmComponentName" value="brandStripB2B" scope="session" />-->
			<c:set var="fmComponentName" value="brandStrip" scope="session" />

			<cms:pageSlot position="FMBrandstrip" var="feature">
				<cms:component component="${feature}" />
			</cms:pageSlot>
		</ul>
	</div>
	<c:set var="fmComponentName" value="footerImageLinks" scope="session" />
	</section>
	</template:page>









