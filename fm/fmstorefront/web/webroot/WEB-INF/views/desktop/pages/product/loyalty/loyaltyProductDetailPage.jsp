<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="breadcrumb"
	tagdir="/WEB-INF/tags/desktop/nav/breadcrumb"%>
<%@ taglib prefix="productloyalty"
	tagdir="/WEB-INF/tags/desktop/product/loyalty"%>

<%@ taglib prefix="product"
	tagdir="/WEB-INF/tags/desktop/product/loyalty"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<template:page pageTitle="${pageTitle}">
	<!-- InstanceBeginEditable name="Breadcrumb" -->
	<section class="breadcrumbPanel visible-lg visible-md visible-sm"
		itemscope itemtype="http://schema.org/Product">
		<div class="container">
			<div class="row">
					<breadcrumb:loyaltyProductBreadcrumb breadcrumbs="${breadcrumbs}"/>
			</div>
		</div>
	</section>

	<!-- InstanceEndEditable -->


	<!-- MAIN CONTAINER-->
	<!-- InstanceBeginEditable name="Page Content Section" -->
	<section class="myRewardDetail productDetailPage">
		<section class="productDetailContent">
			<div class="container">
				<div class="clearfix bgwhite">
					<div class="col-lg-6 topMargn_20">
						<div class="prodMainImg">
							<div class="">
								<a href="${contextPath}/${currentsiteUID}/${currentLanguage.isocode}/${currentCurrency.isocode}/lsearch?q=:name-asc:&text=#"
									class="addNewAddLink"><span
									class="linkarow fa fa-angle-left"></span> Back to Redeem List</a>							
							</div>
							<div class="col-lg-12">
								<productloyalty:loyaltyProductImage product="${product}"
									galleryImages="${galleryImages}" />
							</div>

						</div>
					</div>
					<div class="col-lg-6 topMargn_20">
						<div class="desNDetails">
							<div class="fitmentCheck rewardsPanel clearfix btmMrgn_30">
								<h3 class="panel-title col-lg-6 topMargn_10">
									<span class="fa fa-user"></span> <span
										class=" text-uppercase">Garage rewards</span>
								</h3>
								<div class="rewardsPoints col-lg-6">
									<span class="pull-right"><i class="fa fa-wrench"></i><strong> <fmt:formatNumber type="number" value="${TotalPoints}" /><sub>pts</sub></strong></span>
								</div>

							</div>
							<h2 >${fn:escapeXml(product.name)}</h2>
							<c:set value="${product.code}" var="productCode"/>
							<c:if test="${isVariantSelected eq false }">
								<c:set var="productCode" value="${fn:split(product.code , '-')}" />
							</c:if>
							<div class="partNoReviewLink clearfix">
								<div class="partNoNWarnty pull-left">
									<span class="partNoLabel">Item Number:</span> 
								   <c:choose>
									<c:when test="${isVariantSelected eq false  }">
										<c:if test="${not empty productCode[1]}">
											<span class="partNo" itemprop="productID">${productCode[0]}-${productCode[1]}</span>
										</c:if>
										<c:if test="${empty productCode[1]}">
											<span class="partNo" itemprop="productID">${productCode[0]}</span>
										</c:if>									
									</c:when>
									<c:otherwise>
										<span class="partNo" itemprop="productID">${product.code}</span>
									</c:otherwise>
								    </c:choose> 
								</div>
								<div class="partNoNWarnty pull-right">
									<span class="partNoLabel mainPrice"><fmt:formatNumber type="number" value="${product.loyaltyPoints}" /> </span>
									<span class="partNo">pts</span>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 topMargn_20">
									<p>${product.description}</p>
								</div>
								<div class="clearfix">
									<product:loyaltyProductVariantSelector product="${product}" userLoyaltyPoints="${TotalPoints}" />
								</div>
								<div style="display: none;" class="sizeChart clearfix">
									<div class="col-sm-12">
										<h4 class="text-uppercase DINWebBold">Sizing Chart for:</h4>
										
									</div>
									<div class="col-sm-12">
										<div class="table-responsive userTable btmMrgn_25">
											<table id="myTable" class="table">
												<thead>
													<tr class="text-capitalize">
														<th>Men's Champion Sizing</th>
														<th class="text-center">S</th>
														<th class="text-center">M</th>
														<th class="text-center">L</th>
														<th class="text-center">XL</th>
														<th class="text-center">2XL</th>
														<th class="text-center">3XL</th>
													</tr>
												</thead>
												<tbody>
													<tr class="">
														<td>Chest</td>
														<td>34-36</td>
														<td>38-40</td>
														<td>42-44</td>
														<td>46-48</td>
														<td>50-52</td>
														<td>54-56</td>
													</tr>
													<tr>
														<td>Waist</td>
														<td>28-30</td>
														<td>32-34</td>
														<td>36-38</td>
														<td>40-42</td>
														<td>44-46</td>
														<td>48-50</td>
													</tr>
													<tr>
														<td>Inseam</td>
														<td>30</td>
														<td>31</td>
														<td>32</td>
														<td>32.5</td>
														<td>33</td>
														<td>33.5</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</section>
		 <section class="suggestedProduct pageContet">
			<!-- <div class="container">
				<div class="row">
					<div class="learningPanel col-lg-12">
						<div class="panel-heading clearfix learningPanelTitle">
							<h3 class="panel-title">
								<span class="text-uppercase">You might also be interested
									in:</span>
							</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div
						class="col-lg-12 col-md-12 col-sm-12 col-xs-12 internalLanding">
						<div class="internalPage rightHandPanel clearfix">
							<div class="clearfix">
								<div class="slider4 carousel">
									<div class="slide">
										<a href="#" class="thumbnail"><img
											src="/fmstorefront/_ui/desktop/common/images/rewards-home-prod-img1.jpg"
											alt="Image"></a>
										<div class="caption">
											<p class="title text-capitalize">Champion&reg; Classic
												logo Tee</p>
											<p>
												iItem#: <span class="itemNum">8250-LG</span>
											</p>
											
										</div>
									</div>
									<div class="slide">
										<a href="#" class="thumbnail"><img
											src="/fmstorefront/_ui/desktop/common/images/rewards-home-prod-img2.jpg"
											alt="Image"></a>
										<div class="caption">
											<p class="title text-capitalize">Champion&reg; trucker
												hat</p>
											<p>
												iItem#: <span class="itemNum">8460</span>
											</p>
											
										</div>
									</div>
									<div class="slide">
										<a href="#" class="thumbnail"><img
											src="/fmstorefront/_ui/desktop/common/images/rewards-home-prod-img3.jpg"
											alt="Image"></a>
										<div class="caption">
											<p class="title text-capitalize">Champion&reg; $100 gift
												certificate</p>
											<p>
												iItem#: <span class="itemNum">G3</span>
											</p>
											
										</div>
									</div>
								</div>
							</div>
							

						</div>
					</div>
					

				</div>
			</div> -->
		</section> 


		<!-- Brandstrip -->

		<div class="well well-sm well-brandstrip clearfix">
			<ul class="nav ">
				<!-- <c:set var="fmComponentName" value="brandStripB2B" scope="session" />-->
				<c:set var="fmComponentName" value="brandStrip" scope="session" />

				<cms:pageSlot position="FMBrandstrip" var="feature">
					<cms:component component="${feature}" />
				</cms:pageSlot>
			</ul>
		</div>
	</section>
	<c:set var="fmComponentName" value="footerImageLinks" scope="session" />
</template:page>







