/*price range*/

$("#ex2").slider({});
/*responsive tabs*/
 $(document).ready(function () {
        $('#horizontalTab, .horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });

        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });	
	
/*Responsive Footer*/

$(function(){
	var winIsSmall;
	$(window).on('load resize', footerAccordion );

	function footerAccordion() {
		winIsSmall = window.innerWidth < 601;
		$('.col-xs-12 .mobslider').toggle(!winIsSmall);
	}

	$('.col-xs-12').find('h5').click(function () {
		if(winIsSmall){
			$(this).parent().find('.mobslider').stop().slideToggle();
			$(this).toggleClass('expandFooterLink');
		}
	});
});
/* display tooltip */
$('.tip').tooltip();

/* add slim scroll to div */



 $(function(){

      $('#productDescPanel').slimScroll({
          height: '115px',
          width: '96%'
      });
	  $('#myClassPointsPanel').slimScroll({
          height: '100px',
          width: '96%'
      });
});


 /*for custom input type file*/
 $(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
        
        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
        
        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }
        
    });
    
   
    
    

	$('select#whatdescribes').on('change', function() {
		$('div.panel-frm-filled').show();
		var optVal= $(this).val();
		
	var formEmail=$('select#whatdescribes :selected').text();
  $("input[type=hidden][name=technicianType]").val(formEmail);



$('#companyAddress').prop('disabled', true);
$('#companyAddress2').prop('disabled', true);
$('#companyCity').prop('disabled', true);
$('#companyState').prop('disabled', true);
$('#companyZip').prop('disabled', true);
$('#Ccountry').prop('disabled', true);
$('#companyPhone').prop('disabled', true);
$('#companyFax').prop('disabled', true);
$('#companyName').prop('disabled', true);



document.getElementById("companyAddress").value="";
document.getElementById("companyName").value="";
document.getElementById("companyAddress2").value="";
document.getElementById("companyState").value="Default";
document.getElementById("companyZip").value="";
document.getElementById("Ccountry").value="";
document.getElementById("companyPhone").value="";
document.getElementById("companyFax").value="";
document.getElementById("companyCity").value="";

		
	
		
		if(optVal == 'CONSUMERDIY'||optVal == 'CONSUMERDIFM'){
			$('.form-accCode').hide();
			$('.form-taxid').hide();
			$('.form-B2Cbody').hide();
			$('.form-B2Bbody').hide();
			$('.form-secretque').hide();
			$('.form-secretans').hide();
			$('.form-sameaddress').hide();
			$('.form-regdetailsheader').show();
			$('.form-b2bSub').hide();
			$('.panel-form-regdetails').hide();
			$('.form-city').show();
			$('.form-state').show();
			$('.form-zipcode').show();
			$('.form-b2cSub').show();
			$('.forLoyaltyRewards').hide();
			$('#tech-rewards-questions').hide();
			$('.forReferBy').hide();
			$('#form-ifGarageGuruChecked').hide();
			$('#form-addresssamecheckbox').show();
			document.registrationB2b.email.focus();
			 document.getElementById("taxid").required = false;
			document.getElementById("accNo").required = false;
			document.getElementById("companyName").required = false;
			document.getElementById("companyAddress").required = false;
			document.getElementById("companyCity").required = false;
			document.getElementById("Ccountry").required = false;
			document.getElementById("companyState").required = false;
			document.getElementById("companyZip").required = false;
			document.getElementById("companyPhone").required = false;

		}
		
		if(optVal == 'JOBBERPARTSTORE'){
			$('.form-accCode').hide();
			$('.form-B2Cbody').hide();
			$('.form-reg').hide();
			$('.form-IsUser').hide();
			$('.form-regdetailsheader').show();
			$('.form-taxid').show();
			$('.form-taxdoc').show();
			$('.form-B2Bbody').show();
			$('.form-city').show();
			$('.form-state').show();
			$('.form-zipcode').show();
			$('.form-b2bSub').show();
			$('.form-b2cSub').hide();
			showAstricForDisabledBoxes();
			$('#companyAddress').prop('disabled', false);
			$('#companyAddress2').prop('disabled', false);
			$('#companyCity').prop('disabled', false);
			$('#companyState').prop('disabled', false);
			$('#companyZip').prop('disabled', false);
			$('#Ccountry').prop('disabled', false);
			$('#companyPhone').prop('disabled', false);
			$('#companyFax').prop('disabled', false);
			$('#companyName').prop('disabled', false);	
			document.registrationB2b.taxid.focus();
 			document.getElementById("accNo").required = false;
 			$('.forLoyaltyRewards').hide();
 			$('#tech-rewards-questions').hide();
			$('.forReferBy').hide();
 			
			$('#form-ifGarageGuruChecked').hide();
			$('#form-addresssamecheckbox').show();
		}
		
		if(optVal == 'SHOPOWNERTECHNICIAN'){
			$('.form-accCode').hide();
			$('.form-B2Cbody').hide();
			$('.form-reg').hide();
			$('.form-IsUser').hide();
			$('.form-regdetailsheader').show();
			$('.form-taxid').hide();
			$('.form-taxdoc').show();
			$('.form-B2Bbody').show();
			$('.form-city').show();
			$('.form-state').show();
			$('.form-zipcode').show();
			$('.form-b2bSub').show();
			$('.form-b2cSub').hide();
			showAstricForDisabledBoxes();
			$('#companyAddress').prop('disabled', false);
			$('#companyAddress2').prop('disabled', false);
			$('#companyCity').prop('disabled', false);
			$('#companyState').prop('disabled', false);
			$('#companyZip').prop('disabled', false);
			$('#Ccountry').prop('disabled', false);
			$('#companyPhone').prop('disabled', false);
			$('#companyFax').prop('disabled', false);
			$('#companyName').prop('disabled', false);	
			$('.forLoyaltyRewards').hide();
			$('#tech-rewards-questions').hide();
			$('.forReferBy').hide();
			//document.registrationB2b.taxid.focus();
 			document.getElementById("accNo").required = false;
 			disableHomeaddress();			
			$('#form-ifGarageGuruChecked').hide();
			$('#form-addresssamecheckbox').hide();
		}
		//chnages for tooltip
		if(optVal == 'WAREHOUSEDISTRIBUTORLIGHTVEHICLE'|| optVal == 'WAREHOUSEDISTRIBUTORCOMMERCIALVEHICLE'||optVal == 'RETAILER' || optVal == 'SALESREP'){

			if(optVal == 'SALESREP'){
			//alert("in sales reppp");
		 $('#accCodeToolTip').attr('data-original-title', "Account code must start with SUS/SCA");
				//document.getElementById('accCodeToolTip').data-original-title="New tooltip";
$('.form-reg').hide();
			}
else
{
//document.getElementById("accCodeToolTip").className = "";
 $('#accCodeToolTip').attr('data-original-title', "Do not have an Account Code? Check with Customer Care");
$('.form-reg').show();
}

			$('.form-accCode').show();
			$('.form-taxid').hide();
			$('.form-taxdoc').hide();
			
			$('.form-IsUser').show();
			$('.form-B2Bbody').show();
			$('.form-B2Cbody').hide();
			$('.form-b2bSub').hide();
			$('.form-b2cSub').hide();
			$('.form-secretque').show();
			$('.form-secretans').show();
			$('.form-sameaddress').show();
			$('.form-city').show();
			$('.form-state').show();
			$('.form-zipcode').show();
			$('.form-regdetailsheader').show();
			$('.forLoyaltyRewards').hide();
			$('#tech-rewards-questions').hide();
			$('.forReferBy').hide();
			hideAstricForDisabledBoxes();
			document.registrationB2b.accNo.focus();
			 document.getElementById("taxid").required = false;
			$('#form-ifGarageGuruChecked').hide();
			$('#form-addresssamecheckbox').show();
			}	


	});
/*social share plugin*/

  $("#share").click(function(){
    $("#twitbtn,#fbbtn,#gglbtn,#stmblbtn,#lnkdbtn").slideToggle("slow");
  });



/* store locator google map slide*/


  $(".searchStLocator, .searchStLoc").click(function(){
    $("#map").slideDown(2000);
	$('#defaultmap').hide();
    $(".facloseFont").show();
  });
  
  $("#closestloc").click(function(){
    $("#map").slideUp(2000);
	$('#defaultmap').show();
    $(".facloseFont").hide();
  });
});

/*stop auto scroll for carousel*/

$('.carousel').carousel({
pause: false,
interval: 10000
});

/*Show modal popup vertically center*/

function centerModal() {
    $(this).css('display', 'block');
    var $dialog = $(this).find(".modal-dialog");
    var offset = ($(window).height() - $dialog.height()) / 2;
    // Center modal vertically in window
    $dialog.css("margin-top", offset);
}

$('.modal').on('show.bs.modal', centerModal);
$(window).on("resize", function () {
    $('.modal:visible').each(centerModal);
});


/*Expanding and Collapsing Panel or Div (E.g. In Registration page) */
$(document).on('click', '.panel div.clickable', function (e) {

    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        	
	$this.parents('.panel').find('.panel-body').slideDown();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('fa-plus').addClass('fa-minus');
		$this.parent('.panel').addClass('panel-frm-filled');
		
    } else {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('fa-minus').addClass('fa-plus');
		$this.parent('.panel').removeClass('panel-frm-filled');
		
    }

});
$(document).ready(function () {

    $('.panel-heading span.clickable').click();
	$('.panel div.clickable').click();
	
	var nabscode = $('#nabscode').val();
	$('.fm_btn_olt-overview').attr('href','http://fdm.epiinc.com/IFA/FDM/cgi-bin/login100?clkey=fdm&qqid01=262&qqid02='+nabscode);
	
});

function hideAstricForDisabledBoxes(){
	
	$('#companyPhoneSpan').hide();
	$('#companyZipSpan').hide();
	$('#companyStateSpan').hide();
	$('#CcountrySpan').hide();
	$('#companyCitySpan').hide();
	$('#companyAddressSpan').hide();
	$('#companyNameSpan').hide();	
	
}

function showAstricForDisabledBoxes(){
	
	$('#companyPhoneSpan').show();
	$('#companyZipSpan').show();
	$('#companyStateSpan').show();
	$('#CcountrySpan').show();
	$('#companyCitySpan').show();
	$('#companyAddressSpan').show();
	$('#companyNameSpan').show();	
}

$('#registrationB2b').submit(function(){
try{
  $("#regsubmit").prop("disabled", true).addClass("disabled");


    $(this).find('input:text').each(function(){
          $(this).val($.trim($(this).val()));
    });
    $(this).find('input:tel').each(function(){
        $(this).val($.trim($(this).val()));
  });
}catch(e){
}
});


function GarageRewardMemberQuestion(obj){
	if (obj.checked) {
		$('#form-ifGarageGuruChecked').show();
		$('#form-addresssamecheckbox').show();
		$('#tech-rewards-questions').show();
		enableHomeaddress();
	}else{
		$('#form-ifGarageGuruChecked').hide();
		$('#form-addresssamecheckbox').hide();
		$('#tech-rewards-questions').hide();
		disableHomeaddress();
	}
}

function onAboutShop(obj){
	if($(obj).val() == 'Reg003'){
		$('.shopBanner').show();
	}else{
		$('.shopBanner').hide();
	}
}
function disableHomeaddress(){
	
	//disable the fileds
	$('.form-address1').hide();
	$('.form-address2').hide();
	$('.form-country').hide();
	$('.form-state').hide();
	$('.form-zipcode').hide();
	$('.form-phonenumber').hide();
	$('.form-city').hide();
	// remove the required filed options.
	$('#contactAddress').prop('required', false);
	$('#city').prop('required', false);
	$('#pcountry').prop('required', false);
	$('#state').prop('required', false);
	$('#zip').prop('required', false);
	$('#phone').prop('required', false);
}
function enableHomeaddress(){
	
	//enable the fileds
	$('.form-address1').show();
	$('.form-address2').show();
	$('.form-country').show();
	$('.form-state').show();
	$('.form-zipcode').show();
	$('.form-phonenumber').show();
	$('.form-city').show();
	
	// add the required filed options.
	$('#contactAddress').prop('required', true);
	$('#city').prop('required', true);
	$('#pcountry').prop('required', true);
	$('#state').prop('required', true);
	$('#zip').prop('required', true);
	$('#phone').prop('required', true);
}