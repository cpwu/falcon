var actionUrl= "";
$(document).ready(function(){

	//alert("inside checkckkckkckc");
	$.support.placeholder = false;
	test = document.createElement('input');
	if('placeholder' in test) $.support.placeholder = true;

	$('.slider4').bxSlider({
		slideWidth: 500,
		minSlides: 1,
		maxSlides: 2,
		moveSlides: 1,
		slideMargin: 30,
		nextText: '<span class="fa fa-chevron-right"></span>',
		prevText: '<span class="fa fa-chevron-left"></span>'
	});

	/* For Loyalty Product Carousel */
	$('.loyaltyslider4').bxSlider({
		slideWidth: 202,
		minSlides: 2,
		maxSlides: 3,
		moveSlides: 1,
		slideMargin: 30,
		nextText: '<span class="fa fa-chevron-right"></span>',
		prevText: '<span class="fa fa-chevron-left"></span>'
	});

	$('.single-slider').jRange({
		from: 0,
		to: 10,
		step: 1,
		scale: [0,5 ,10],
		format: '%s',
		width: 300,
		showLabels: true
	});

	/* if($('#j_username').val() == '' && $('#j_password').val() == ''){
		$("#loginSubmit").attr("disabled", "disabled");

	} */
	//anonymous
	$('#loginbox input').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().parent().find('.errorMsg').show();

		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().parent().find('.errorMsg').hide();

		}/* if($(this).val() == ''){

			$("#loginSubmit").attr("disabled", "disabled");
		}else{
			$("#loginSubmit").removeAttr("disabled");
		} */
		/* });
	$('#loginbox input').load(function(){
		$("#loginSubmit").attr("disabled", "disabled"); */
	});

	//anonymous user -sign in
	$('#loginForm input').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().parent().find('.errorMsg').show();

		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().parent().find('.errorMsg').hide();

		}
	});
	//ProductDetailsPage
	$("#prodMainImg").elevateZoom({ gallery: 'prdDetail_thumbnail', cursor: 'pointer', galleryActiveClass: "active",
		zoomWindowWidth: 300,
		zoomWindowHeight: 300,
		zoomWindowWidth: 526,
		zoomWindowHeight: 500, 
		zoomWindowPosition: 1,
		easing: true,
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 500,
		lensFadeIn: 500,
		lensFadeOut: 500,
		zoomWindowOffety: -2,
		zoomWindowOffetx: 40
	});
	$("#prodMainImg").bind("click", function(e) {
		var ez = $('#prodMainImg').data('elevateZoom');
		ez.closeAll();
		return false;
	});
	$('#prodMainImg').on('click', function(event){
		$('#myModal').modal('toggle');
	});


	var reviewFlag = '${reviewFlag}';
//	alert("called" + reviewFlag );

	if(reviewFlag == 'FromReview')
	{
		$(window).scrollTop($("#tabsection").offset().top);
		//alert("called");
		$("ul.resp-tabs-list > li").removeClass("resp-tab-active");
		$(".resp-tabs-container > div").removeClass("resp-tab-content-active");
		$('#tab2').addClass("resp-tab-active");
		$("#reviewTab").addClass("resp-tab-content-active");
		$("#prodSpec").hide();
	}

//	For Online tools file downloads
	$(".fileDownloadTable #myTable").tablesorter();
//	commented by 279688 for about us company board of directors collapse
//	$('[id^=detail-]').hide();
	$('.toggle').click(function() {
		$(this).toggleClass('collapsed');
		$input = $( this );
		$target = $('#'+$input.attr('data-toggle'));
		$target.slideToggle();
	});
//	B2BHomePage

//	LeadGenerationCallBack
	$('.leadGen input,select,textarea').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' || $(this).val()==" " && $hasReqField.hasClass('required')){ 
			//if(!$(this).val()  && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});


	/* max char count for textarea */
	var text_max = $('textarea#callBackDescription').attr(
	'maxlength');
	$('.char-count').html(
			'<label>' + text_max
			+ '</label> characters remaining');

	$('#callBackDescription')
	.keyup(
			function() {
				var text_length = $(
				'#callBackDescription')
				.val().length;
				var text_remaining = text_max
				- text_length;

				$('.char-count')
				.html(
						'<label>'
						+ text_remaining
						+ '</label> characters remaining');
			});

//	fmOrderHistoryPage
	//$(".orderStatusTable #myTable").tablesorter();
	$(".invoiceTabTable #myTable").tablesorter(); 
	$(".backOrderTabTable #myTable").tablesorter();

//	fmOrderHistoryPage-uploadOrderStatus-fmCartPage
	/*$('.date-picker').datepicker().on('changeDate', function(ev){                 
	$('.date-picker').datepicker('hide');
})
	 */
//	fmOrderHistoryPage-uploadOrderStatus-fmCartPage
//	ApplicationUsageReport
	$('.date-picker').datepicker().on('changeDate', function(ev){                 
		$('.date-picker').datepicker('hide');
	});	
	$('.date-picker').keydown(function (event) {
		event.preventDefault();
	});

//	Update Password/fmUpdatePasswordPage
	$('.regFrm input').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});

//	for create user page
	$('.createuserpage input,select').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' || $("#whatdescribes").val()==" "  && $hasReqField.hasClass('required')){ 
			//if(!$(this).val()  && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});

	/*var options = {
		onLoad: function () {
			$('#messages').text('Start typing password');
		},
		onKeyUp: function (evt) {
			$(evt.target).pwstrength("outputErrorList");
		}
};*/
	$('#newPassword').pwstrength(options);
	$('.password-verdict, .progress').hide();

//	for EditUserPage
	$('.edituserpage input,select').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){ 
			//if(!$(this).val()  && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});


//	fmUpdateProfilePage
	$('.regFrm input').blur(function(){
		var $hasReqField = $(this).parent().find('span'); 
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});

//	for forgot password view page
	$('.resetPanel input').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');

			$(this).parent().parent().find('.errorMsg').show();

		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().parent().find('.errorMsg').hide();
		}
	});


	//for loyalty checkout shipping page
	$('.chekoutBillingShippingAddress input,select').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	}); 

//	quickOrderPage
	$("#brandprefix").hide();
//	for forgot password reset page
	$('#checkYourMail input').blur(function() {
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if ($(this).val() == '' && $hasReqField.hasClass('required')) {
			$(this).addClass('inputError');
			$(this).parent().parent().find('.errorMsg').show();
		} else if ($(this).hasClass('inputError')) {
			$(this).removeClass('inputError');
			$(this).parent().parent().find('.errorMsg').hide();
		}
	});
	$('.progress').hide();
	var options = {
			onLoad: function () {
				$('#messages').text('Start typing password');
			},
			onKeyUp: function (evt) {
				$(evt.target).pwstrength("outputErrorList");
			}
	};
	$('#setnewpwd').pwstrength(options);
	$('.password-verdict, .progress').hide();
	
	$('.regFrm input,select').blur(function(){
		var $hasReqField = $(this).parent().find('span'); //var to if input parent is having span and store it
		if($(this).val() == '' && $hasReqField.hasClass('required')){
			$(this).addClass('inputError');
			$(this).parent().find('.errorMsg').show();
		} else if($(this).hasClass('inputError')){
			$(this).removeClass('inputError');
			$(this).parent().find('.errorMsg').hide();
		}
	});

//	B2B HomePage
	$('#upload_order').on('click', function(e) {
		$(".alertContainer").addClass("show");
	});
	$('.close').on('click', function(e) {
		$(".alertContainer").removeClass("show");
	}); 
//	$("#brandprefix").hide();
	$("#shipToSelect").change(function(){
		$("#shipToSelect option:selected").each(function(){
			if($(this).attr("value")=="new"){
				$(".shipToForm .tab-pane").hide();
				$("body").addClass("modal-open");
				$(".modal").addClass("in");
				$(".modal").attr("aria-hidden", "false");
				$(".modal").show();
				$('body').append('<div class="modal-backdrop fade in"></div>');
				$(".brandprefix").removeClass("in");
				$("#upload-brandprefix").removeClass("in");
			}
			else if($(this).attr("value")=="chooseExist"){
				$(".shipToForm .tab-pane").hide();
				$("#shipTo").show();
			}
			else if($(this).attr("value")=="default"){
				$(".shipToForm .tab-pane").hide();
				$("#shipTodefaultAcc").show();
				$("#newShipTo").hide();
				$("#oldShipTo").show();
			}

		}); 
	});
	$("#soldToSelect").change(function(){
		$( "#soldToSelect option:selected").each(function(){
			if($(this).attr("value")=="default"){
				$(".soldToForm .tab-pane").hide();
				$("#soldTodefaultAcc").show();
			}
			if($(this).attr("value")=="chooseExist"){
				$(".soldToForm .tab-pane").hide();
				$("#soldTo").show();
			}
		});
	});

	/* 	$(".close, .cancel, #submitNewAddress" ).bind( "click", function() {
			$("body").removeClass("modal-open");
			$(".modal").removeClass("in");
			$(".modal").attr("aria-hidden", "true");
			$(".modal").hide();
			$(".modal-backdrop").remove();
	}); */
	$(".close, .cancel" ).bind( "click", function() {
		$("body").removeClass("modal-open");
		$(".modal").removeClass("in");
		$(".modal").attr("aria-hidden", "true");
		$(".modal").hide();
		$(".modal-backdrop").remove();
	});
	
//	SignupPage
	getUnits();
	/*var options = {
			onLoad : function() {
				$('#messages').text('Start typing password');
			},
			onKeyUp : function(evt) {
				$(evt.target).pwstrength("outputErrorList");
			}
	};*/
	//$('#newPassword').pwstrength(options);
	//$('#setnewpwd').pwstrength(options);
	//$('.password-verdict, .progress').hide();
	
	
	/*document.getElementById('j_remember').onclick = function() {
		// access properties using this keyword
		if ( this.checked ) {
			// if checked ...
			// alert( this.value );
		} else {
			// if not checked ...
			//alert("not clicked");        
			createCookie('fmstorefrontRememberMeCustom',"",-1);
			// alert("done");
		}
	};*/

});



function createCookie(name,value,days) {
	//alert("inside createcookie method");
	document.cookie = name +
	'=; expires=Thu, 01-Jan-70 00:00:01 GMT;';

}

function getpassword() {
	$('.password-verdict, .progress').show();
}
function getpassword_reg() {

	$('.password-verdict, .progress').show();
}
//for forgot password reset page
function getpassword_pwdreset() {
	$('.password-verdict, .progress').show();
}

function addNewAddressToSession(){
	//alert("inside addNewAddressToSession");
	if((myFMAddressForm.firstLastName.value != "") /* && (myFMAddressForm.lastName.value != "") */ && (myFMAddressForm.Addressline1.value != "") && (myFMAddressForm.city.value != "") && (myFMAddressForm.zip.value != "") /* && (myFMAddressForm.contactNumber.value != "") */)

	{	//alert("inside addNewAddressToSession IF loop ");
		var count=0;
		if(document.getElementById("coutntry").value == 'United States' )
		{
			var zipcode = "^\\d{5}(-\\d{4})?$";
			if(document.getElementById("zip").value.search(zipcode) == -1)
			{
				//alert("match failed");	
				document.getElementById("errorzip").innerHTML="Invalid ZIP/Postal Code ";
				//document.myFMAddressForm.zip.focus();

			}
			else
			{
				document.getElementById("errorzip").innerHTML=" ";		
				count++;
			}
		}
		else if(document.getElementById("coutntry").value == 'Canada' )
		{
			var zipcode = "(^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1} \\d{1}[A-Za-z]{1}\\d{1}$)";
			if(document.getElementById("zip").value.search(zipcode) == -1)
			{
				document.getElementById("errorzip").innerHTML="Invalid ZIP/Postal Code ";
				//document.myFMAddressForm.zip.focus();

			}
			else
			{	
				document.getElementById("errorzip").innerHTML=" ";		
				count++;
			}

		}

		/* var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			   if(document.getElementById("contactNumber").value.search(phoneno) == -1) 
				  {
				 	 document.getElementById("errorcnum").innerHTML="Please verify that you've entered a 10-digit phone number ";
				  }
			  		else 
				  {
			  			//alert("values entered");
			  			count++;
				 		 document.getElementById("errorcnum").innerHTML=" ";
				  } */

		//alert("count" + count);	 

		if(count >= 1)
		{

			ajaxUrl="USD/my-fmaccount/";
			currentPath= window.location.href;
			var pathName= "";
			var userNewAddress = "";

			try{
				if(currentPath.indexOf("/USD") != -1){
					pathName = currentPath.substring(0, currentPath.lastIndexOf("/USD") + 1);
				}else if(currentPath.indexOf("?site") != -1){
					pathName = currentPath.substring(0, currentPath.lastIndexOf("/?site") + 1)+currentPath.substring(currentPath.lastIndexOf("site=")+5,currentPath.length)+"/en/";
				}else{
					pathName = window.location.href;
				}
			}catch(e){
				// alert(e);
			}

			actionUrl  = pathName + ajaxUrl+"/add-address-tosession";
			//alert("actionUrl :: "+actionUrl);

			$.ajax({		
				url:actionUrl,
				type:'POST',
				mimeType:"multipart/form-data",
				data: $("#myFMAddressForm").serialize(),
				success: function(xmlDoc) {

					$("#shipTodefaultAcc").hide();
					var respDoc = $(xmlDoc).find("addNewShipToaddressSession").html();
					//alert("respDoc1" + respDoc);
					userNewAddress = respDoc.substr(respDoc.indexOf("NewAddress:")+11);
					//alert("userNewAddress1 " + userNewAddress );
					respDoc = respDoc.substr(0,respDoc.indexOf("NewAddress:"));
					//alert("respDoc2" + respDoc);	
					$("#shipTonewAddress1").html(respDoc);
				},
				error: function(xmlDoc) 
				{
					//alert('Error'+xmlDoc);
				}
			});

			$("body").removeClass("modal-open");
			$(".modal").removeClass("in");
			$(".modal").attr("aria-hidden", "true");
			$(".modal").hide();
			$(".modal-backdrop").remove();
			//var shipToSoldToFlag = "shipTo";
			//updateUserNewAddressToSession(userNewAddress,shipToSoldToFlag);
		}
	}
	else
	{
		if(myFMAddressForm.firstLastName.value == "")
		{
			document.getElementById("errorcname").innerHTML="Please enter Company Name";
		}
		else
		{ 
			document.getElementById("errorcname").innerHTML=" ";	
			//$(firstLastName).parent().find('.errorMsg').hide();
		}
		if(myFMAddressForm.Addressline1.value == "")
		{
			document.getElementById("erroraddress").innerHTML="Please enter Contact Address";
		}
		else
		{ 
			document.getElementById("erroraddress").innerHTML=" ";	
		}
		if(myFMAddressForm.city.value == "")
		{
			document.getElementById("errorcity").innerHTML="Please enter city";
		}
		else
		{ 
			document.getElementById("errorcity").innerHTML=" ";	
		}
		if(myFMAddressForm.zip.value == "")
		{
			document.getElementById("errorzip").innerHTML="Please enter Postal Code";
		}
		else
		{ 
			document.getElementById("errorzip").innerHTML=" ";	
		}
		if(myFMAddressForm.prov.value == ' ')
		{
			document.getElementById("errorstate").innerHTML="Please select state";
		}
		else
		{ 
			document.getElementById("errorstate").innerHTML=" ";	
		} 
		// $(firstLastName).removeClass('inputError');
		//$(Addressline1).removeClass('inputError');
	} 

} 

function validate() {

	if(myFMAddressForm.zip.value != "")
	{
		if(document.getElementById("coutntry").value == 'United States' )
		{
			var zipcode = "^\\d{5}(-\\d{4})?$";
			if(document.getElementById("zip").value.search(zipcode) == -1)
			{
				//alert("match failed");	
				document.getElementById("errorzip").innerHTML="Invalid ZIP/Postal Code ";
				//document.myFMAddressForm.zip.focus();
			}
			else
			{
				document.getElementById("errorzip").innerHTML=" ";				 
			}
		}
		else if(document.getElementById("coutntry").value == 'Canada' )
		{
			var zipcode = "(^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1} \\d{1}[A-Za-z]{1}\\d{1}$)";
			if(document.getElementById("zip").value.search(zipcode) == -1)
			{
				document.getElementById("errorzip").innerHTML="Invalid ZIP/Postal Code ";
				//document.myFMAddressForm.zip.focus();
			}
			else
			{	
				document.getElementById("errorzip").innerHTML=" ";				 
			}
		}
	}

	/*   if(myFMAddressForm.contactNumber.value != "")
	  { 
		   var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		   if(document.getElementById("contactNumber").value.search(phoneno) == -1) 
			  {
			 	 document.getElementById("errorcnum").innerHTML="Please verify that you've entered a 10-digit phone number ";
			  }
		  		else 
			  {
		  			//alert("values entered");
			 		 document.getElementById("errorcnum").innerHTML=" ";
			  }

	  }	  */
}

function hideAndShow(){
	var e = document.getElementById("shipToSelect");
	var strUser = e.options[e.selectedIndex].value;

	if(strUser =="chooseExist"){
		$('#shipTodefaultAcc').hide(); 
		$('#floatingBarsG4').hide(); 
		$('#shipTo').show(); 
		$('#changeShip').show(); 
		$('#changeShipbtn').show();
	} 

} 
function clearNewAddress(){
	$(".shipToForm .tab-pane").hide();
	$("#shipTodefaultAcc").show();
	$("#newShipTo").hide();
	$('#oldShipTo').show();
}

function checkHide(){

	if(myFMAddressForm.firstLastName.value == "")
	{
		//document.getElementById("errorcname").innerHTML="Please enter Company Name";
	}
	else
	{ 
		document.getElementById("errorcname").innerHTML=" ";	
	}
	if(myFMAddressForm.Addressline1.value == "")
	{
		//document.getElementById("erroraddress").innerHTML="Please enter Contact Address";
	}
	else
	{ 
		document.getElementById("erroraddress").innerHTML=" ";	
	}
	if(myFMAddressForm.city.value == "")
	{
		//document.getElementById("errorcity").innerHTML="Please enter city";
	}
	else
	{ 
		document.getElementById("errorcity").innerHTML=" ";	
	}
	if(myFMAddressForm.zip.value == "")
	{
		//document.getElementById("errorzip").innerHTML="Please enter Postal Code";
	}
	else
	{ 
		document.getElementById("errorzip").innerHTML=" ";	
	}
	if(myFMAddressForm.prov.value == ' ')
	{
		//document.getElementById("errorstate").innerHTML="Please select state";
	}
	else
	{ 
		document.getElementById("errorstate").innerHTML=" ";	
	} 
} 

function goToReview(){	

	$("ul.resp-tabs-list > li").removeClass("resp-tab-active");
	$(".resp-tabs-container > div").removeClass("resp-tab-content-active");
	$('#tab1').removeClass("resp-tab-content-active");
	$('#tab3').removeClass("resp-tab-content-active");
	$('#tab4').removeClass("resp-tab-content-active");
	$('#tab5').removeClass("resp-tab-content-active");

	$('#tab2').addClass("resp-tab-active");
	$("#reviewTab").addClass("resp-tab-content-active");
	$("#prodSpec").hide();
	$("#FAQTab").hide();
	$("#AlsoFitsTab").hide();
	$("#TechTipsTag").hide(); 

//	$('.reviewsDetails').show();	
}

function isNumber(evt) {
	evt = (evt) ? evt : window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		return false;
	}
	return true;
}
function validateuser() 
{
	var userexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$";
	if (document.getElementById("userid").value.search(userexp) == -1)
	{	
		document.getElementById("userid1").innerHTML = "Invalid user";

	} else 
	{	
		document.getElementById("userid1").innerHTML = "";
	}			
} 

function validateNewShipForm() {
//alert("inside on submit");
var count=0;
		if (newAddress.shipZipPostalCode.value != "")

		{
			//alert("inside");
			//alert("val"+window.document.getElementById("shipcountry").value);
			if (window.document.getElementById("shipcountry").value == 'US') {
				var zipcode = "^\\d{5}(-\\d{4})?$";
				if (window.document.getElementById("shipZipPostalCode").value
						.search(zipcode) == -1) {
					//alert("match failed");	
					window.document.getElementById("errorzip").innerHTML = "Invalid ZIP/Postal Code ";
					count++;
					//document.getElementById('continueToReview').disabled = true;
					//document.myFMAddressForm.zip.focus();

				} else {
					window.document.getElementById("errorzip").innerHTML = " ";
					//alert("not failed");
				//	document.getElementById('continueToReview').disabled = false;

				}
			} else if (window.document.getElementById("shipcountry").value == 'CA') {
				var zipcode = "(^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]{1}\\d{1}[A-Za-z]{1} \\d{1}[A-Za-z]{1}\\d{1}$)";
				if (window.document.getElementById("shipZipPostalCode").value
						.search(zipcode) == -1) {
					//alert("match failed");	
					window.document.getElementById("errorzip").innerHTML = "Invalid ZIP/Postal Code ";
					//document.getElementById('continueToReview').disabled = true;
					count++;
					//document.myFMAddressForm.zip.focus();

				} else {
					window.document.getElementById("errorzip").innerHTML = " ";
					//document.getElementById('continueToReview').disabled = false;
					//alert("not failed");
				}

			}
		}

		if (newAddress.phone.value != "") {
			var phonenum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			if (document.getElementById("phone").value.search(phonenum) == -1) {
				document.getElementById("errorcnum").innerHTML = "Please verify that you've entered a 10-digit phone number ";
				//document.getElementById('continueToReview').disabled = true;
				count++;
			} else {
				//alert("values entered");

				document.getElementById("errorcnum").innerHTML = " ";
				//document.getElementById('continueToReview').disabled = false;
			
			}
		}
		if(count>0)
			{
			document.getElementById('continueToReview').disabled = true;
			}
		else
			{
			document.getElementById('continueToReview').disabled = false;
			}

	}





function changeText(a){
	switch(a)
	{
	case 1: document.getElementById("displayDesc").innerHTML="<label style='font: bold;' >I hate it</label>";
		break;
	case 2: document.getElementById("displayDesc").innerHTML="<label style='font: bold;' >I don't like it</label>";
		break;
	case 3: document.getElementById("displayDesc").innerHTML="<label style='font: bold;' >It's okay</label>";
		break;
	case 4: document.getElementById("displayDesc").innerHTML="<label style='font: bold;' >I like it</label>";
		break;
	case 5: document.getElementById("displayDesc").innerHTML="<label style='font: bold;' >I love it</label>";
		break;	
	}
}

function changeEmpty()
{
	document.getElementById("displayDesc").innerHTML="<label style='font: bold;' ></label> ";
}



function checkPlaceHolderLabel()
{
//alert("insdie newwwwwwwwww");
	if(!$.support.placeholder) {
	 	//alert("not supports");
		document.getElementById("email_ie9").innerHTML = '<label class="" for="j_username">Email Address</label>'; 
		document.getElementById("pwd_ie9").innerHTML = '<label class="" for="j_password">Password</label>'; 
				
		}

	}
