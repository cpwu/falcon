/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Mar 14, 2016 11:11:59 AM                    ---
 * ----------------------------------------------------------------
 */
package com.falcon.constants;

/**
 * @deprecated use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedYcommercewebservicesConstants
{
	public static final String EXTENSIONNAME = "fmwebservice";
	public static class TC
	{
		public static final String OLDCARTREMOVALCRONJOB = "OldCartRemovalCronJob".intern();
		public static final String ORDERSTATUSUPDATECLEANERCRONJOB = "OrderStatusUpdateCleanerCronJob".intern();
		public static final String PRODUCTEXPRESSUPDATECLEANERCRONJOB = "ProductExpressUpdateCleanerCronJob".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedYcommercewebservicesConstants()
	{
		// private constructor
	}
	
	
}
